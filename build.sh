#!/bin/bash

usage()
{
    echo -e "\e[34musage:\e[39m"
    echo -e "\e[34mThis script need sudo privileges:\nsudo bash build.sh\e[39m"
    echo -e "\e[34m--simulation: setup for usage of simulation instead of real robot\e[39m"
}

# set shell option to allow exiting whole script if one command fails
set -e

# default values
SIMULATION=0
DEVELOP=0

#parse parameters
while [ "$1" != "" ]; do
    case $1 in
        -s | --simulation )     SIMULATION=1
                                ;;
        --develop )             DEVELOP=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

_user="$(id -u -n)"
_uid="$(id -u)"
echo "User name : $_user"
echo "User name ID (UID) : $_uid"

echo -e "\e[34mParameters:"
echo "SIMULATION: $SIMULATION"
echo "DEVELOP: $DEVELOP"
echo -e "CURRENT DIR: $(pwd)"
sleep 1

echo -e "Building the repository with all dependencies..."

cd ../../
WORKSPACE_DIR=$(pwd)
echo -e "\e[34mWORKSPACE DIR: $WORKSPACE_DIR\e[39m"

echo -e "\e[34mCloning ROS repository dependencies...\e[39m"
cd $WORKSPACE_DIR/src

# detector interface package
echo -e "\e[34mCloning detector interface package ximea_ros...\e[39m"
git clone git@gitlab.lrz.de:IP/RoAXDT/ximea_ros.git

# go to workspace for rosdep dependency install and catkin_make call
cd $WORKSPACE_DIR
echo -e "\e[34mInstall ROS dependencies of all packages\e[39m"
rosdep install --from-paths src --ignore-src -r -y --rosdistro noetic -y --skip-keys libfranka

echo -e "\e[34mBuilding ROS workspace with catkin_make\e[39m"

if [ $SAFETY -eq 1 ]
then
    if [ -z "$OPENCL_PATH" ]
    then
        echo "environment variable OPENCL_PATH is empty. Could cause error with catkin_make. for AMD ROCM set: /opt/rocm/opencl/lib/libOpenCL.so"
    fi
    if [ -z "$OPENCL_INCLUDE_PATH" ]
    then
        echo "environment variable OPENCL_INCLUDE_PATH is empty. Could cause error with catkin_make. for AMD ROCM set: /opt/rocm/opencl/include"
    fi
    echo -e "\e[34mBuilding workspace with safety features enabled\e[39m"
    catkin_make -j$(nproc) -DEigen3_DIR:PATH=/usr/lib/cmake/eigen3 -Djsoncpp_DIR:PATH=/usr/lib/x86_64-linux-gnu/cmake/jsoncpp -Dgazebo_DIR:PATH=/usr/lib/x86_64-linux-gnu/cmake/gazebo -DOpenCV_DIR:PATH=/usr/lib/x86_64-linux-gnu/cmake/opencv4 -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_CXX_STANDARD=14 -DSAFETY=$SAFETY -DVISP_DIR:PATH=/opt/ros/melodic/lib/cmake/visp -DOpenCL_LIBRARY=$OPENCL_PATH -DOpenCL_INCLUDE_DIR=$OPENCL_INCLUDE_PATH -DOpenMesh_PREFIX:PATH=/usr/local -DQt5Widgets_DIR=/usr/lib/x86_64-linux-gnu/cmake/Qt5Widgets
else
    echo -e "\e[34mBuilding workspace with safety features disabled\e[39m"
    catkin_make -DELSA_GPU=0 -j$(nproc) -DEigen3_DIR:PATH=/usr/lib/cmake/eigen3 -Djsoncpp_DIR:PATH=/usr/lib/x86_64-linux-gnu/cmake/jsoncpp -Dgazebo_DIR:PATH=/usr/lib/x86_64-linux-gnu/cmake/gazebo -DOpenCV_DIR:PATH=/usr/lib/x86_64-linux-gnu/cmake/opencv4 -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_CXX_STANDARD=14 -DSAFETY=$SAFETY -DOpenMesh_PREFIX:PATH=/usr/local -DQt5Widgets_DIR=/usr/lib/x86_64-linux-gnu/cmake/Qt5Widgets
fi

echo -e "\e[34mSource the new ROS workspace with:\nsource $WORKSPACE_DIR/devel/setup.bash\e[39m"

if [ $DEVELOP -eq 1 ]
then
    cd $WORKSPACE_DIR
    git clone git@gitlab.lrz.de:IP/RoAXDT/robotics_assisted_tomography_vscode.git
    mv robotics_assisted_tomography_vscode .vscode

    cd $HOME
    git clone --branch simulation-lab-settings git@gitlab.lrz.de:IP/RoAXDT/roaxdt_config_files.git
    mv roaxdt_config_files .roaxdt
    if [ $SIMULATION -eq 1 ]
    then
        cd .roaxdt
        git checkout simulation
    fi
fi
