[![pipeline status](https://gitlab.lrz.de/IP/RoAXDT/robotics_assisted_tomography/badges/master/pipeline.svg)](https://gitlab.lrz.de/IP/RoAXDT/robotics_assisted_tomography/commits/master)

# Installation & setup instructions

You can skip the preliminaries and got to the build instructions [here](#building-the-repository)

(Assuming a clean install of Ubuntu 18.04.1 amd64)

This repository needs GPU acceleration for 
* the built-in GAZEBO simulation environment
* the RViz robotics visualization tool
* and the collision detection mechanism for the robot.

Therefore it is essential to install the graphics drivers properly. This guide includes the installation procedure for AMD and NVIDIA cards.

## GPU driver installation

Follow the [wiki guide](https://gitlab.lrz.de/IP/RoAXDT/robotics_assisted_tomography/wikis/GPU-Driver-installation) for GPU driver installation.

## Software Packages

Follow the [wiki guide](https://gitlab.lrz.de/IP/RoAXDT/robotics_assisted_tomography/wikis/Software-Package-installation) for software package installation (ROS & Intel Realsense).

### Environment variables

Add the robots IP address and a configuration constant for RViz(in order to read floating-point numbers in the robot model correctly) to the end of the `.bashrc` file:
```
export FCI_IP="172.16.0.2"
export LC_NUMERIC="en_US.UTF-8"
```

### _libfranka_ & franka_ros

* _libfranka_: Low_level library communicating with the panda robot.
* _franka_ros_: ROS-wrapper for high-level robot communication with RViz integration.

We need to compile _libfranka_ on our own because unfortunately the linux-realtime kernel that is needed to use _libfranka_ in the first place is not compatible with binary GPU drivers from NVIDIA or AMD. In the [official documentation](https://frankaemika.github.io/docs/installation.html) there is a full guide on how to build _libfranka_ but we will outline the steps that worked for us in the following.

The `build.sh` and `realtime.sh` scripts will take care of setting up libfranka in our repository take care of building libfranka and setting the realtime priorities within the system.

If you have the robot plugged in and unlocked you can run `./examples/communication_test $FCI_IP` and check if the library was setup successfully.

## Building and using robotics_assisted_tomography

This repository contains two interfaces to the core problem of controlling the Panda robot in a safe manner:

* A _GAZEBO_ based simulation including two depth cameras mainly for verifying the safety of the collision detection and robot movement calibration implementations
* The _real_ interface that is also controlled by the _Qt_ GUI but that connects to the realsense cameras via USB and Panda robot via _MoveIt!_ and _libfranka_ at startup, instead of the _GAZEBO_ simulation interfaces

Both interfaces can be interacted with by the minimal _Qt_ GUI or the RViz control panel in the same way. Before we explain the repository I will outline the configuration first.

### Robot environment configuration

Follow the [wiki guide](https://gitlab.lrz.de/IP/RoAXDT/robotics_assisted_tomography/wikis/Robot-environment-configuration) for robot environment configuration.

### Building the Repository

Create a separate workspace in your home directory

```
mkdir -p ws_ros/src && cd ws_ros
git -C src clone git@gitlab.lrz.de:IP/RoAXDT/robotics_assisted_tomography.git
rosdep install --from-paths src --ignore-src -r -y --rosdistro melodic -y --skip-keys libfranka

cd src/robotics_assisted_tomography
sudo bash realtime.sh   # only for laboratory setup; restart required after this script
bash build.sh --safety
```
Optional parameters for `build.sh` are:

* `--simulation` -- determines if the user wants to setup the simulation or the panda robot environment
* `--nvidia` -- determines if an NVIDIA GPU is used
* `--safety` -- determines if the safety features are enabled (mandatory for laboratory setup!)

Use `clean.sh` to clean all the dependencies that were setup by the `build.sh` script (requires rebuild afterwards).

Sample build command for (re-)installing the simulation on a NVIDIA-based workstation:

```
bash clean.sh && bash build.sh --nvidia --simulation
```

The `build.sh` script will request your admin password once at the beginning.

After a successful build you can run the following command to start the robot operation, assuming the panda controller is started and the robot joints are unlocked:

(Do not forget to source your newly created workspace)

```
roslaunch robotics_assisted_tomography robot.launch simulation:=false robot_ip:=$FCI_IP robot_x:=0.0 robot_y:=0.0 robot_z:=0.0 pointcloud_viewer:=true --screen
```

For the simulation run the following command:

```
roslaunch robotics_assisted_tomography robot.launch simulation:=true pointcloud_viewer:=true --screen
```

The parameters for starting the package are specified in `launch/robot.launch`:

* `simulation:=true|false` -- determines if the user wants to start the simulation or the panda robot environment
* `robot_ip:=192.168.0.1|$FCI_IP` -- determines the target robot (localhost-gazebo or the panda IP)
* `save_pointcloud:=true|false` -- this flag will save the point cloud as `.pcl` file in `~/.ros` or `~/ros_ws` depending where roslaunch is called from. Setting true will deteriorate the performance as for each camera frame a point cloud file (after the robot was filtered) will be saved to the disk
* `load_gripper:=true|false` -- determines if the robots hand is mounted or not. Currently this is only used if `simulation:=true`
* `pointcloud_viewer:=true|false` -- opens a second _RViz_ window for displaying the point cloud from the depth cameras in real-time
* `robot_x` -- (lab only) determines the physical robot's x-offset from the world coordinate system origin
* `robot_y` -- (lab only) determines the physical robot's y-offset from the world coordinate system origin
* `robot_z` -- (lab only) determines the physical robot's z-offset from the world coordinate system origin

### Network setup for directly connecting _Panda_ to the workstation

After connecting the black controller box to the workstation's second LAN-port follow [this](https://github.com/ut-ims-robotics/tutorials/wiki/Franka-Emika-Panda-beginner-guide#Internet_access_vs_the_robot) guide to set the networking up.
The IP-address differs from the guide: It was set up as `172.16.0.1`.
After the setup open [the interface](https://172.16.0.2/) in your web browser.

For the credentials of the interface contact [Klaus Achterhold](mailto:klaus.achterhold@tum.de) and ask him for the credentials that he received 5th of December, 2018.
