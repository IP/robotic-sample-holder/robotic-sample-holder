#include <roaxdt_safety/TrajectoryRecorder.h>

using actionlib::SimpleClientGoalState;

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;

TrajectoryRecorder::TrajectoryRecorder(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void TrajectoryRecorder::init() {
  if (!private_node_handle.getParam(test::TEST_NAME, test_name)) {
    ROS_ERROR("no test_name param, 'default' chosen");
    test_name = "default";
  }
  if (!private_node_handle.getParam(test::TEST_SUITE, test_suite)) {
    ROS_ERROR("no test_suite param, 'default' chosen");
    test_suite = "default";
  }
  // load config values
  try {

    roaxdt::configuration::readConfigParameter<std::string>(node_handle, TRAJECTORY_SUB_GOAL_TOPIC, trajectory_sub_goal_topic);
    path_robotics_assisted_tomography_safety = ros::package::getPath(roaxdt::constants::ROAXDT_SAFETY_PKG);
    if (path_robotics_assisted_tomography_safety.length() == 0) {
      throw std::runtime_error(std::string("Could not find package paths"));
    }
  } catch (const std::runtime_error &exception) { ROS_ERROR_STREAM(exception.what()); }

  try {
  } catch (const std::runtime_error &exception) { ROS_ERROR_STREAM(exception.what()); }

  n_joints = (7);
  // sleep for tf_listener to prepare in the background. Otherwise the first tf request return error
  ros::Duration(3.0).sleep();

  trajectory_sub = node_handle.subscribe(trajectory_sub_goal_topic, 20, &TrajectoryRecorder::trajectoryCallback, this);

  if (!private_node_handle.getParam(test::TRAJ_START_NUMBER, traj_start_number)) {
    ROS_ERROR("no traj_start_number param, start is 1");
    traj_start_number = 1;
  }
  ROS_INFO_STREAM("Start Recording");
  traj_store_number = traj_start_number;
}

void TrajectoryRecorder::trajectoryCallback(const control_msgs::FollowJointTrajectoryActionGoal &joint_trajectory_goal) {
  this->write_points_to_file(joint_trajectory_goal);
}

void TrajectoryRecorder::write_points_to_file(const control_msgs::FollowJointTrajectoryActionGoal &joint_trajectory_goal) {
  std::ofstream trajectory_file;
  std::string sub_directory_test_path = filesystem::createTestDirectory(this->test_folder_, test_suite, test_name).string();
  if(sub_directory_test_path == ""){
    return;
  }
  trajectory_file.open(sub_directory_test_path + "/trajectory_" + std::string(5 - std::to_string(traj_store_number).length(), '0') + std::to_string(traj_store_number) + ".txt",
                       std::ifstream::out | std::ios::trunc);

  for (int i = 0; i < joint_trajectory_goal.goal.trajectory.points.size(); i++) {
    // trajectory
    for (int j = 0; j < joint_trajectory_goal.goal.trajectory.points[i].positions.size(); j++) {
      trajectory_file << joint_trajectory_goal.goal.trajectory.points[i].positions[j] << " ";
    }
    // speed and acceleration
    if (joint_trajectory_goal.goal.trajectory.points[i].velocities.size() ==
            joint_trajectory_goal.goal.trajectory.points[i].positions.size() &&
        joint_trajectory_goal.goal.trajectory.points[i].accelerations.size() ==
            joint_trajectory_goal.goal.trajectory.points[i].positions.size()) {
      for (int j = 0; j < joint_trajectory_goal.goal.trajectory.points[i].positions.size(); j++) {
        trajectory_file << joint_trajectory_goal.goal.trajectory.points[i].velocities[j] << " ";
      }
      for (int j = 0; j < joint_trajectory_goal.goal.trajectory.points[i].positions.size(); j++) {
        trajectory_file << joint_trajectory_goal.goal.trajectory.points[i].accelerations[j] << " ";
      }
    }
    trajectory_file << joint_trajectory_goal.goal.trajectory.points[i].time_from_start.toSec();
    if (i < joint_trajectory_goal.goal.trajectory.points.size() - 1) {
      trajectory_file << "\n";
    }
  }
  ROS_INFO_STREAM("File created: test_trajectory_" + std::to_string(traj_store_number) + ".txt");
  traj_store_number++;
  trajectory_file.close();
}


} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::COLLISION_DETECTOR_TEST);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");
  roaxdt::nodes::TrajectoryRecorder node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}
