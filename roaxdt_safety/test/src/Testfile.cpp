#include <roaxdt_safety/Testfile.h>

std::shared_ptr<ros::NodeHandle> nh;
//Fixed positions (name_of_object, position, quaternion)
// OFFSITE POSITIONS
const gazebo_msgs::SetModelState TELEPHONE_POLE_OFFSITE = createSetModelStateMsg("telephone_pole",  position(5, 5, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState TABLE_FOR_TEST_OFFSITE = createSetModelStateMsg("table_for_test",  position(5, 2, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_1_OFFSITE = createSetModelStateMsg("block_1",  position(5,  1.0, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_2_OFFSITE = createSetModelStateMsg("block_2",  position(5,  0.6, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_3_OFFSITE = createSetModelStateMsg("block_3",  position(5,  0.2, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_4_OFFSITE = createSetModelStateMsg("block_4",  position(5, -0.2, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_5_OFFSITE = createSetModelStateMsg("block_5",  position(5, -0.6, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_6_OFFSITE = createSetModelStateMsg("block_6",  position(5, -1.0, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_7_OFFSITE = createSetModelStateMsg("block_7",  position(5, -1.4, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_8_OFFSITE = createSetModelStateMsg("block_8",  position(5, -1.8, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState PERSON_STANDING_OFFSITE = createSetModelStateMsg("person_standing",  position(5, -3, 0), quaternion(0,0,0,0));

 // TESTSITE POSITION
 // pole
const gazebo_msgs::SetModelState TELEPHONE_POLE_TESTSITE_RIGHT = createSetModelStateMsg("telephone_pole", position(0, 0.55, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState TELEPHONE_POLE_TESTSITE_LEFT = createSetModelStateMsg("telephone_pole", position(0, -0.55, 0), quaternion(0,0,0,0));
// table with blocks and hole
const gazebo_msgs::SetModelState TABLE_FOR_TEST_TESTSITE = createSetModelStateMsg("table_for_test",  position(0, -0.85, 0), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_1_TESTSITE = createSetModelStateMsg("block_1",  position(0.4,  -0.75, 1.015), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_2_TESTSITE = createSetModelStateMsg("block_2",  position(-0.4,  -0.75, 1.015), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_3_TESTSITE = createSetModelStateMsg("block_3",  position(0.4,  -0.75, 1.16), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_4_TESTSITE = createSetModelStateMsg("block_4",  position(-0.4, -0.75, 1.16), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_5_TESTSITE = createSetModelStateMsg("block_5",  position(0.4, -0.75, 1.305), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_6_TESTSITE = createSetModelStateMsg("block_6",  position(-0.4, -0.75, 1.305), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_7_TESTSITE = createSetModelStateMsg("block_7",  position(0.4, -0.75, 1.46), quaternion(0,0,0,0));
const gazebo_msgs::SetModelState BLOCK_8_TESTSITE = createSetModelStateMsg("block_8",  position(-0.4, -0.75, 1.46), quaternion(0,0,0,0));
// person
const gazebo_msgs::SetModelState PERSON_STANDING_TESTSITE = createSetModelStateMsg("person_standing", position(0, 0.63, 0), quaternion(0,0,0,0));


// Case: Empty World

TEST(EmptyWorld, longMovementsNoCollision){
  
  bool exists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(exists);

  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  ASSERT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
}

TEST(EmptyWorld, noObstacleNoCollision){
  
  bool exists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(exists);

  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  ASSERT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
}

// Case: Single Telephone pole

TEST(SinglePole, longMovementsCloseButNoCollision){
  
  bool exists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(exists);
  bool movingWorked = moveObjectModel(TELEPHONE_POLE_TESTSITE_RIGHT);
  ASSERT_TRUE(movingWorked);

  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  ASSERT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
  bool resetWorked = moveObjectModel(TELEPHONE_POLE_OFFSITE);
  ASSERT_TRUE(resetWorked);
}

TEST(SinglePole, direktCollision){
  
  bool testClientExists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(testClientExists);
  bool movingWorked = moveObjectModel(TELEPHONE_POLE_TESTSITE_LEFT);
  ASSERT_TRUE(movingWorked);

  // test trajectory
  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  EXPECT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
  bool resetWorked = moveObjectModel(TELEPHONE_POLE_OFFSITE);
  ASSERT_TRUE(resetWorked);
}

TEST(SinglePole, noCollision){
  bool testClientExists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(testClientExists);
  bool movingWorked = moveObjectModel(TELEPHONE_POLE_TESTSITE_RIGHT);
  ASSERT_TRUE(movingWorked);

  // test trajectory
  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  EXPECT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
  bool resetWorked = moveObjectModel(TELEPHONE_POLE_OFFSITE);
  ASSERT_TRUE(resetWorked);
}

// Case: Person Standing

TEST(PersonStanding, noCollision){
  bool testClientExists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(testClientExists);
  bool movingWorked = moveObjectModel(PERSON_STANDING_TESTSITE);
  ASSERT_TRUE(movingWorked);

  // test trajectory
  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  EXPECT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
  // clean up world
  bool resetWorked = moveObjectModel(PERSON_STANDING_OFFSITE);
  ASSERT_TRUE(resetWorked);
}

TEST(PersonStanding, direktCollision){
  bool testClientExists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(testClientExists);
  bool movingWorked = moveObjectModel(PERSON_STANDING_TESTSITE);
  ASSERT_TRUE(movingWorked);

  // test trajectory
  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  EXPECT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
  // clean up world
  bool resetWorked = moveObjectModel(PERSON_STANDING_OFFSITE);
  ASSERT_TRUE(resetWorked);
}

// Case: Table with Blocks

TEST(TableWithBlocks, noCollision){
  bool testClientExists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(testClientExists);
  bool movingWorked;
  movingWorked = moveObjectModel(TABLE_FOR_TEST_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_1_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_2_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_3_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_4_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_5_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_6_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_7_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_8_TESTSITE);
  ASSERT_TRUE(movingWorked);
  // movingWorked = moveObjectModel("lumber_1", 0.0, -0.77, 1.52, 0,0,0,0);
  // ASSERT_TRUE(movingWorked);
  // movingWorked = moveObjectModel("lumber_2", 0.0, -0.73, 1.52, 0,0,0,0);
  // ASSERT_TRUE(movingWorked);

  // test trajectory
  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  EXPECT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
  bool resetWorked;
  resetWorked = moveObjectModel(TABLE_FOR_TEST_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_1_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_2_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_3_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_4_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_5_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_6_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_7_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_8_OFFSITE);
  ASSERT_TRUE(resetWorked);
}

TEST(TableWithBlocks, aceInTheHole){
  bool testClientExists(testServiceClient.waitForExistence(ros::Duration(100)));
  ASSERT_TRUE(testClientExists);
  bool movingWorked;
  movingWorked = moveObjectModel(TABLE_FOR_TEST_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_1_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_2_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_3_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_4_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_5_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_6_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_7_TESTSITE);
  ASSERT_TRUE(movingWorked);
  movingWorked = moveObjectModel(BLOCK_8_TESTSITE);
  ASSERT_TRUE(movingWorked);
  // movingWorked = moveObjectModel("lumber_1", 0.0, -0.77, 1.52, 0,0,0,0);
  // ASSERT_TRUE(movingWorked);
  // movingWorked = moveObjectModel("lumber_2", 0.0, -0.73, 1.52, 0,0,0,0);
  // ASSERT_TRUE(movingWorked);

  // test trajectory
  roaxdt_msgs::TestCollisionDetectionFileMsg srv;

  srv.request.test_name = test_info_->name(); 
  srv.request.test_suite = test_info_->test_case_name(); 
  testServiceClient.call(srv);
  EXPECT_TRUE(srv.response.test_files_found);
  for(int i = 0; i < srv.response.file_count; i++){
    EXPECT_EQ(srv.response.actual_results[i], srv.response.expected_results[i]) << ("Testfile path: " + srv.response.file_paths[i]);
  }
  bool resetWorked;
  resetWorked = moveObjectModel(TABLE_FOR_TEST_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_1_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_2_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_3_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_4_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_5_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_6_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_7_OFFSITE);
  ASSERT_TRUE(resetWorked);
  resetWorked = moveObjectModel(BLOCK_8_OFFSITE);
  ASSERT_TRUE(resetWorked);
}


geometry_msgs::Point position(float x, float y, float z){
  geometry_msgs::Point position;
  position.x = x;
  position.y = y;
  position.z = z;
  return position;
}

geometry_msgs::Quaternion quaternion(float x, float y, float z, float w){
  geometry_msgs::Quaternion quaternion;
  quaternion.x = x;
  quaternion.y = y;
  quaternion.z = z;
  quaternion.w = w;
  return quaternion;

}
gazebo_msgs::SetModelState createSetModelStateMsg(std::string name, geometry_msgs::Point position, geometry_msgs::Quaternion quaternion){
  gazebo_msgs::SetModelState srv_set_model_state;
  srv_set_model_state.request.model_state.model_name = name;
  srv_set_model_state.request.model_state.pose.position = position;
  srv_set_model_state.request.model_state.pose.orientation = quaternion;

  return srv_set_model_state;
}

bool moveObjectModel(gazebo_msgs::SetModelState srv_set_model_state){
  bool worldSetupClientExists(clientWorldSetup.waitForExistence(ros::Duration(100)));
  if(!worldSetupClientExists){
    return false;
  }
  
  clientWorldSetup.call(srv_set_model_state);
  if(!srv_set_model_state.response.success){
    return false;
  }
  return true;
}


int main(int argc, char** argv){
  ros::init(argc, argv, "test_gtest");
  nh.reset(new ros::NodeHandle);
  testing::InitGoogleTest(&argc, argv);
  testServiceClient  = nh->serviceClient<roaxdt_msgs::TestCollisionDetectionFileMsg>(
      "collision_detection_testing_service");
  clientWorldSetup = nh->serviceClient<gazebo_msgs::SetModelState>(
      "/gazebo/set_model_state");
  clientWorldReset = nh->serviceClient<std_srvs::Empty>(
      "/gazebo/reset_world");
  return RUN_ALL_TESTS();
}