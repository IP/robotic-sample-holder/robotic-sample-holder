#include <roaxdt_safety/CollisionDetectorTest.h>
#include <ros/ros.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;

CollisionDetectorTest::CollisionDetectorTest(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void CollisionDetectorTest::init() {
  service = node_handle.advertiseService("collision_detection_testing_service", &CollisionDetectorTest::beginnTesting, this);
  try {
    roaxdt::configuration::readConfigParameter<std::string>(node_handle, TRAJECTORY_SUB_RESULT_TOPIC,
                                                            trajectory_sub_result_topic);

    path_robotics_assisted_tomography = ros::package::getPath(roaxdt::constants::ROAXDT_PKG);
    if (path_robotics_assisted_tomography.length() == 0) {
      std::printf("Test print before throw");
      throw std::runtime_error(std::string("Could not find package paths"));
    }
  } catch (const std::runtime_error &exception) { ROS_ERROR_STREAM(exception.what()); }

  // Action client
  const std::string action_server_name = "/panda/panda_arm_controller/follow_joint_trajectory";
  action_client.reset(new ActionClient(action_server_name));

  robot_control_stop = node_handle.serviceClient<roaxdt_msgs::RobotStopMsg>(service_endpoints::ROBOT_STOP);

  n_joints = (7);
  // sleep for tf_listener to prepare in the background. Otherwise the first tf request return error
  ros::Duration(3.0).sleep();

  trajectory_result_sub =
      node_handle.subscribe(trajectory_sub_result_topic, 20, &CollisionDetectorTest::trajectoryResultCallback, this);

  ROS_INFO_STREAM("Collision Detector Test ready");
}

void CollisionDetectorTest::trajectoryResultCallback(
    const control_msgs::FollowJointTrajectoryActionResult &joint_trajectory_result) {
  ROS_WARN_STREAM("joint_trajectory_result: " << std::endl << joint_trajectory_result);

  switch (joint_trajectory_result.status.status) {
  case control_msgs::FollowJointTrajectoryActionResult::_status_type::ABORTED:
    ROS_WARN_STREAM("trajectory execution ABORTED");
    trajectory_status = "failed";
    break;
  case control_msgs::FollowJointTrajectoryActionResult::_status_type::SUCCEEDED:
    ROS_INFO_STREAM("Trajectory execution SUCCEEDED");
    trajectory_status = "finished";
    break;
  case control_msgs::FollowJointTrajectoryActionResult::_status_type::ACTIVE:
    ROS_INFO_STREAM("Trajectory is ACTIVE");
    trajectory_status = "pending";
    break;
  case control_msgs::FollowJointTrajectoryActionResult::_status_type::PENDING:
    ROS_INFO_STREAM("Trajectory is PENDING");
    trajectory_status = "pending";
    break;
  case control_msgs::FollowJointTrajectoryActionResult::_status_type::PREEMPTED:
    ROS_INFO_STREAM("Trajectory execution PREEMPTED");
    trajectory_status = "preempted";
    break;
  default:
    ROS_WARN_STREAM("Unexpected trajectory result type. Clearing trajectory transforms.");
    trajectory_status = "failed";
    break;
  }
}

bool CollisionDetectorTest::beginnTesting(roaxdt_msgs::TestCollisionDetectionFileMsgRequest &req,
                                          roaxdt_msgs::TestCollisionDetectionFileMsgResponse &res) {
  // load config values
  traj_start_number = 1;
  const std::string &test_suite = req.test_suite;
  const std::string &test_name = req.test_name;
  res.test_files_found = true;

  std::vector<std::string> expected_results;
  std::vector<std::string> actual_results;
  std::vector<std::string> file_paths;

  auto directory_iterator = filesystem::readTestFiles(this->node_name_, test_suite, test_name); // Sort iterator
  // trajectory_length_test = filesystem::countFilesInDirectory(directory_iterator);
  trajectory_status = "finished";
  res.file_count = 0;
  size_t traj_number = 0;
  std::vector<boost::filesystem::path> paths(directory_iterator, {});
  std::sort(paths.begin(), paths.end());

  for (auto &entry : paths) {
    ++traj_number;
    if (trajectory_status.compare("finished") == 0) {
      trajectory_status = "pending";
      if (createTrajectory(entry, traj_number - 1, res)) {
        ROS_INFO_STREAM(test_name + " : Trajectory " << entry.filename() << "-Start");
        callTrajectory();
      } else {
        res.test_files_found = false;
        ROS_WARN_STREAM(test_name + " : Trajectory " << entry.filename() << " Failed, could not create trajectory");
        break;
      }
    } else {
      ROS_WARN_STREAM("Test : " + test_name + " - trajectory " + trajectory_status + " | Trajectory " +
                      entry.filename().string());
      break;
    }
    while (trajectory_status.compare("pending") == 0) {
      ros::spinOnce();
      ros::Duration(0.1).sleep();
    }
    std::vector<std::string> current_trajectory_result;
    expected_results.push_back(this->hasEnding(entry.filename().string(), "collision.txt") ? "preempted" : "finished");
    actual_results.push_back(trajectory_status);
    file_paths.push_back(entry.string());
    res.file_count = res.file_count + 1;
  }
  res.expected_results = expected_results;
  res.actual_results = actual_results;
  res.file_paths = file_paths;
  ROS_INFO_STREAM("TEST : " + test_name + " - finished");
  return true;
}

bool CollisionDetectorTest::createTrajectory(fs::path directory_entry_path, size_t traj_number,
                                             roaxdt_msgs::TestCollisionDetectionFileMsgResponse &res) {
  if (!(read_points_from_file_full(directory_entry_path, res))) {
    return false;
  };

  n_joints = (7);
  joint_names.resize(n_joints);

  trajectory_msgs::JointTrajectoryPoint point;
  point.positions.resize(n_joints, 0.0);
  point.velocities.resize(n_joints, 0.0);
  point.accelerations.resize(n_joints, 0.0);

  points.resize(trajectory.size(), point);

  for (int i = 0; i < n_joints; i++) {
    joint_names[i] = "panda_joint" + std::to_string(i + 1);
  }
  for (int j = 0; j < trajectory.size(); j++) {
    points[j].time_from_start = ros::Duration((traj_number == 0 ? 1.0 + j : trajectory[j][trajectory[j].size() == 4 ? 3 : 1][0]));
    for (int i = 0; i < n_joints; i++) {
      points[j].positions[i] = trajectory[j][0][i];
      if (trajectory[j].size() == 4 && trajectory[j][1].size() == n_joints && trajectory[j][2].size() == n_joints) {
        points[j].velocities[i] = trajectory[j][1][i];
        points[j].accelerations[i] = trajectory[j][2][i];
      }
    }
  }

  traj.joint_names = joint_names;
  traj.points = points;

  // Action goals
  traj_goal.trajectory = traj;
  return true;
}

bool CollisionDetectorTest::read_points_from_file_full(fs::path directory_entry_path,
                                                       roaxdt_msgs::TestCollisionDetectionFileMsgResponse &res) {
  std::string line;
  std::ifstream point_file(directory_entry_path.string());

  trajectory.clear();
  if (point_file.is_open()) {
    while (getline(point_file, line)) {
      if (line.compare("") == 0) {
        continue;
      }
      std::istringstream iss(line);
      std::vector<std::string> results(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());
      positions.clear();
      velocities.clear();
      accelerations.clear();
      time_from_start.clear();
      trajectory_part.clear();
      if (results.size() == n_joints * 3 + 1) {
        for (int i = 0; i < n_joints; i++) {
          positions.push_back(std::stod(results[i]));
          velocities.push_back(std::stod(results[i + n_joints]));
          accelerations.push_back(std::stod(results[i + (n_joints * 2)]));
        }
        trajectory_part.push_back(positions);
        trajectory_part.push_back(velocities);
        trajectory_part.push_back(accelerations);
        time_from_start.push_back(std::stod(results[n_joints * 3]));
        trajectory_part.push_back(time_from_start);
      } else if (results.size() == n_joints + 1) {
        for (int i = 0; i < n_joints; i++) {
          positions.push_back(std::stod(results[i]));
        }
        trajectory_part.push_back(positions);
        time_from_start.push_back(std::stod(results[n_joints]));
        trajectory_part.push_back(time_from_start);
      } else {
        ROS_WARN_STREAM("Bad trajectory file input");
      }
      trajectory.push_back(trajectory_part);
      positions.clear();
    }
    point_file.close();
    return true;
  } else {
    return false;
  }
}

void CollisionDetectorTest::callTrajectory() {
  traj_goal.trajectory.header.stamp = ros::Time(0); // Start immediately
  action_client->sendGoal(traj_goal);
}

bool CollisionDetectorTest::hasEnding(std::string const &fullString, std::string const &ending) {
  if (fullString.length() >= ending.length()) {
    return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
  } else {
    return false;
  }
}

} // namespace nodes
} // namespace roaxdt
int main(int argc, char **argv) {

  ROS_INFO_STREAM("Collision Detector Test started");
  ros::init(argc, argv, "collision_detector_test");
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");
  roaxdt::nodes::CollisionDetectorTest node(node_handle, private_node_handle);

  ros::AsyncSpinner spinner(1);
  spinner.start();

  // Advertise service

  ros::waitForShutdown();
  spinner.stop();
  return 0;
}
