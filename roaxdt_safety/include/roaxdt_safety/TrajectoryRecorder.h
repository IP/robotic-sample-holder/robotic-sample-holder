#ifndef TRAJECTORY_RECORDER_H
#define TRAJECTORY_RECORDER_H
#pragma once

#include <control_msgs/FollowJointTrajectoryAction.h>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <roaxdt_msgs/RobotStopMsg.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <string>

#include <actionlib/client/simple_action_client.h>

#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <mutex>

#include <gtest/gtest.h>

#include <actionlib/client/simple_action_client.h>
#include <ros/ros.h>

#include <control_msgs/FollowJointTrajectoryAction.h>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <control_msgs/QueryTrajectoryState.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>

#include <controller_manager_msgs/LoadController.h>
#include <controller_manager_msgs/SwitchController.h>
#include <controller_manager_msgs/UnloadController.h>

using actionlib::SimpleClientGoalState;

namespace roaxdt {

namespace nodes {

class TrajectoryRecorder {

public:
  TrajectoryRecorder() = delete;
  ~TrajectoryRecorder() = default;

  TrajectoryRecorder(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

  int num_threads = 1;

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string test_folder_{"test_gtest"};
  ;

  std::string trajectory_sub_goal_topic, path_robotics_assisted_tomography_safety, test_name, test_suite;

  // ROS communication related - variables
  ros::Subscriber trajectory_sub;

  unsigned int n_joints;
  int traj_start_number, traj_store_number;
  std::vector<std::string> joint_names;

  void init();
  void write_points_to_file(const control_msgs::FollowJointTrajectoryActionGoal &joint_trajectory_goal);

  int trajectory_length_test = 0;

  // ROS communication related - methods
  void trajectoryCallback(const control_msgs::FollowJointTrajectoryActionGoal &joint_trajectory_goal);
};

} // namespace nodes
} // namespace roaxdt

#endif // COLLISION_DETECTOR_TEST_H