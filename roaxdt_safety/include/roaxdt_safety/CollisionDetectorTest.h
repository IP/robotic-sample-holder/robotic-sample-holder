#ifndef COLLISION_DETECTOR_TEST_H
#define COLLISION_DETECTOR_TEST_H
#pragma once

#include <control_msgs/FollowJointTrajectoryAction.h>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <roaxdt_msgs/RobotStopMsg.h>
#include <roaxdt_msgs/TestCollisionDetectionFileMsg.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/node_handle.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <string>

#include <actionlib/client/simple_action_client.h>

#include <ros/service_client.h>

#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <mutex>

#include <gtest/gtest.h>

#include <actionlib/client/simple_action_client.h>
#include <ros/ros.h>

#include <control_msgs/FollowJointTrajectoryAction.h>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <control_msgs/QueryTrajectoryState.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>

#include <controller_manager_msgs/LoadController.h>
#include <controller_manager_msgs/SwitchController.h>
#include <controller_manager_msgs/UnloadController.h>

using actionlib::SimpleClientGoalState;

namespace roaxdt {

namespace nodes {

class CollisionDetectorTest {

public:
  CollisionDetectorTest() = delete;
  ~CollisionDetectorTest() = default;

  CollisionDetectorTest(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

  int num_threads = 1;

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{"test_gtest"};

  std::string trajectory_sub_goal_topic, trajectory_sub_result_topic, path_robotics_assisted_tomography;
  std::string trajectory_status = "finished";
  unsigned int n_joints;
  int traj_start_number;

  ros::ServiceServer service;

  ros::ServiceClient robot_control_stop;
  ros::Subscriber trajectory_result_sub;

  typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> ActionClient;
  typedef std::shared_ptr<ActionClient> ActionClientPtr;
  typedef control_msgs::FollowJointTrajectoryGoal ActionGoal;
  typedef control_msgs::JointTrajectoryControllerStateConstPtr StateConstPtr;

  std::vector<float> positions;
  std::vector<float> accelerations;
  std::vector<float> velocities;
  std::vector<float> time_from_start;
  std::vector<std::vector<float>> trajectory_part;
  std::vector<std::vector<std::vector<float>>> trajectory;
  std::vector<std::string> joint_names;
  std::vector<trajectory_msgs::JointTrajectoryPoint> points;
  trajectory_msgs::JointTrajectory traj;

  ActionClientPtr action_client;
  ActionGoal traj_goal;

  void init();
  void trajectoryResultCallback(const control_msgs::FollowJointTrajectoryActionResult &joint_trajectory_result);
  bool beginnTesting(roaxdt_msgs::TestCollisionDetectionFileMsgRequest &req,
                     roaxdt_msgs::TestCollisionDetectionFileMsgResponse &res);
  bool createTrajectory(fs::path directory_entry_path, size_t traj_number,
                        roaxdt_msgs::TestCollisionDetectionFileMsgResponse &res);
  bool read_points_from_file_full(fs::path directory_entry_path, roaxdt_msgs::TestCollisionDetectionFileMsgResponse &res);
  void callTrajectory();
  bool hasEnding(std::string const &fullString, std::string const &ending);
};

} // namespace nodes
} // namespace roaxdt

#endif // COLLISION_DETECTOR_TEST_H