#pragma once

#include <string>
#include <vector>

struct CollisionVolume {
  std::string frame;
  std::string mesh_path;
  std::vector<int> dimensions;
  geometry_msgs::Point origin;
  Eigen::VectorXf points;
  std::vector<Eigen::Vector3f> points_in_link_fram;
  std::vector<double> spacing;
  Eigen::Isometry3d link_to_world_transform = Eigen::Isometry3d::Identity();
  Eigen::Isometry3d link_to_parent_transform = Eigen::Isometry3d::Identity();
  Eigen::Isometry3d link_vtk_transform = Eigen::Isometry3d::Identity();
  float scale;
};
