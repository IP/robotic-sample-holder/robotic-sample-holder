#ifndef TESTFILE_H
#define TESTFILE_H
#pragma once

#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>

#include <gazebo_msgs/SetModelState.h>
#include <std_srvs/Empty.h>
#include <gazebo/gazebo.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/physics/ModelState.hh>
#include <ros/service_client.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <roaxdt_msgs/RobotMoveToPointMsg.h>
#include <roaxdt_msgs/RobotStopMsg.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <string>

#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <mutex>

#include <gtest/gtest.h>

#include <actionlib/client/simple_action_client.h>
#include <ros/ros.h>

#include <control_msgs/FollowJointTrajectoryAction.h>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <control_msgs/QueryTrajectoryState.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>

#include <controller_manager_msgs/LoadController.h>
#include <controller_manager_msgs/SwitchController.h>
#include <controller_manager_msgs/UnloadController.h>
#include <gtest/gtest.h>
#include <roaxdt_safety/CollisionDetectorTest.h>
#include <roaxdt_msgs/TestCollisionDetectionFileMsg.h>

ros::ServiceClient testServiceClient;
ros::ServiceClient clientWorldSetup;
ros::ServiceClient clientWorldReset;
bool moveObjectModel(gazebo_msgs::SetModelState srv_set_model_state);
geometry_msgs::Point position(float x, float y, float z);
geometry_msgs::Quaternion quaternion(float x, float y, float z, float w);
gazebo_msgs::SetModelState createSetModelStateMsg(std::string name, geometry_msgs::Point position, geometry_msgs::Quaternion quaternion);


#endif // COLLISION_DETECTOR_TEST_H