
#include <roaxdt_utils/trajectory_utils.h>

using json = nlohmann::json;
namespace roaxdt {
namespace trajectory_utils {

void getHealpixMapVectors(int num_spherical_sides, std::vector<SphericalPose> &spherical_poses, int num_pixels_wanted) {
  int nside = num_spherical_sides;
  std::cout << "healpix map number of sides: " << nside << std::endl;

  Healpix_Map<float> map(nside, Healpix_Ordering_Scheme::RING, SET_NSIDE);
  auto npix = map.Npix();
  std::cout << "healpix map number of pixels: " << npix << std::endl;

  spherical_poses.clear();
  spherical_poses.reserve(npix);

  std::vector<int> in(npix), out;
  std::iota(std::begin(in), std::end(in), 0);
  out.reserve(npix);

  int pixels_to_sample;
  if (num_pixels_wanted == 1) {
    pixels_to_sample = npix;
  } else {
    pixels_to_sample = num_pixels_wanted;
  }
  // sample on sphere without replacement
  std::sample(in.begin(), in.end(), std::back_inserter(out), pixels_to_sample, std::mt19937{std::random_device{}()});

  // convert pixel indices to vectors
  for (const auto &index : out) {
    auto rot = map.pix2ang(index);

    SphericalPose pose;
    pose.index = index;
    pose.num_pixels = npix;
    pose.num_sides = nside;
    pose.theta = rot.theta;
    pose.phi = rot.phi;

    spherical_poses.push_back(pose);
  }
}

void getHealpixMapVectorSingle(int num_spherical_sides, int index, SphericalPose &spherical_pose) {
  int nside = num_spherical_sides;
  std::cout << "healpix map number of sides: " << nside << std::endl;

  Healpix_Map<float> map(nside, Healpix_Ordering_Scheme::RING, SET_NSIDE);
  auto npix = map.Npix();
  std::cout << "healpix map number of pixels: " << npix << std::endl;

  // convert pixel index to vector
  auto rot = map.pix2ang(index);

  spherical_pose.index = index;
  spherical_pose.num_pixels = npix;
  spherical_pose.num_sides = nside;
  spherical_pose.theta = rot.theta;
  spherical_pose.phi = rot.phi;
}

void getHealpixMapVectorsFromIndices(int num_spherical_sides, std::vector<SphericalPose> &spherical_poses,
                                     std::vector<int> &spherical_pose_indices) {
  int nside = num_spherical_sides;
  std::cout << "healpix map number of sides: " << nside << std::endl;

  Healpix_Map<float> map(nside, Healpix_Ordering_Scheme::RING, SET_NSIDE);
  auto npix = map.Npix();
  std::cout << "healpix map number of pixels: " << npix << std::endl;
  auto num_poses_requested = spherical_pose_indices.size();

  spherical_poses.clear();
  spherical_poses.reserve(num_poses_requested);

  // convert pixel indices to vectors
  for (const auto &index : spherical_pose_indices) {
    auto rot = map.pix2ang(index);

    SphericalPose pose;
    pose.index = index;
    pose.num_pixels = npix;
    pose.num_sides = nside;
    pose.theta = rot.theta;
    pose.phi = rot.phi;

    spherical_poses.push_back(pose);
  }
}

void writeTrajectoryToDisk(const TrajectoryData &data, const fs::path &trajectory_path, const std::string &environment_hash) {
  json trajectory;

  trajectory["robot_link_states"] = data.robot_link_states;

  std::vector<json> experiment_poses_json_vec;

  for (const auto &pose : data.experiment_poses) {
    json position, orientation, pose_json;

    position["x"] = pose.pose.position.x;
    position["y"] = pose.pose.position.y;
    position["z"] = pose.pose.position.z;

    orientation["x"] = pose.pose.orientation.x;
    orientation["y"] = pose.pose.orientation.y;
    orientation["z"] = pose.pose.orientation.z;
    orientation["w"] = pose.pose.orientation.w;

    pose_json["position"] = position;
    pose_json["orientation"] = orientation;
    pose_json["frame_id"] = pose.header.frame_id;

    experiment_poses_json_vec.push_back(pose_json);
  }

  trajectory["experiment_poses"] = json(experiment_poses_json_vec);

  std::vector<json> robot_trajectories_json_vec;

  for (const auto &trajectory_msg : data.robot_trajectories) {
    json trajectory_msg_json, joint_trajectory_json, header_json, joint_trajectory_points_json;

    header_json["seq"] = trajectory_msg.joint_trajectory.header.seq;
    header_json["stamp"] = trajectory_msg.joint_trajectory.header.stamp.sec;
    header_json["frame_id"] = trajectory_msg.joint_trajectory.header.frame_id;

    joint_trajectory_json["header"] = header_json;

    joint_trajectory_json["joint_names"] = trajectory_msg.joint_trajectory.joint_names;

    std::vector<json> joint_trajectory_points_json_vec;
    for (const auto &joint_trajectory_point : trajectory_msg.joint_trajectory.points) {
      json joint_trajectory_point_json;

      joint_trajectory_point_json["positions"] = joint_trajectory_point.positions;
      joint_trajectory_point_json["velocities"] = joint_trajectory_point.velocities;
      joint_trajectory_point_json["accelerations"] = joint_trajectory_point.accelerations;
      joint_trajectory_point_json["effort"] = joint_trajectory_point.effort;
      joint_trajectory_point_json["time_from_start"]["secs"] = joint_trajectory_point.time_from_start.sec;
      joint_trajectory_point_json["time_from_start"]["nsecs"] = joint_trajectory_point.time_from_start.nsec;

      joint_trajectory_points_json_vec.push_back(joint_trajectory_point_json);
    }

    joint_trajectory_json["points"] = json(joint_trajectory_points_json_vec);

    trajectory_msg_json["joint_trajectory"] = joint_trajectory_json;

    robot_trajectories_json_vec.push_back(trajectory_msg_json);
  }

  trajectory["robot_trajectories"] = json(robot_trajectories_json_vec);

  std::vector<json> robot_states_json_vec;

  for (const auto &state_msg : data.robot_states) {
    json state_msg_json, joint_state_json, header_json, attached_collision_object_json, collision_object_json;

    header_json["seq"] = state_msg.joint_state.header.seq;
    header_json["stamp"] = state_msg.joint_state.header.stamp.sec;
    header_json["frame_id"] = state_msg.joint_state.header.frame_id;

    joint_state_json["header"] = header_json;

    joint_state_json["name"] = state_msg.joint_state.name;
    joint_state_json["position"] = state_msg.joint_state.position;
    joint_state_json["velocity"] = state_msg.joint_state.velocity;
    joint_state_json["effort"] = state_msg.joint_state.effort;

    state_msg_json["joint_state"] = joint_state_json;

    // We only support a single attached collision object
    if (state_msg.attached_collision_objects.size() > 0) {
      collision_object_json["frame_id"] = state_msg.attached_collision_objects.at(0).object.header.frame_id;
      collision_object_json["id"] = state_msg.attached_collision_objects.at(0).object.id;

      collision_object_json["dimensions"] = state_msg.attached_collision_objects.at(0).object.primitives.at(0).dimensions;
      collision_object_json["position"]["x"] = state_msg.attached_collision_objects.at(0).object.primitive_poses.at(0).position.x;
      collision_object_json["position"]["y"] = state_msg.attached_collision_objects.at(0).object.primitive_poses.at(0).position.y;
      collision_object_json["position"]["z"] = state_msg.attached_collision_objects.at(0).object.primitive_poses.at(0).position.z;
      collision_object_json["orientation"]["x"] =
          state_msg.attached_collision_objects.at(0).object.primitive_poses.at(0).orientation.x;
      collision_object_json["orientation"]["y"] =
          state_msg.attached_collision_objects.at(0).object.primitive_poses.at(0).orientation.y;
      collision_object_json["orientation"]["z"] =
          state_msg.attached_collision_objects.at(0).object.primitive_poses.at(0).orientation.z;
      collision_object_json["orientation"]["w"] =
          state_msg.attached_collision_objects.at(0).object.primitive_poses.at(0).orientation.w;
      collision_object_json["operation"] = state_msg.attached_collision_objects.at(0).object.operation;

      attached_collision_object_json["link_name"] = state_msg.attached_collision_objects.at(0).link_name;
      attached_collision_object_json["touch_links"] = state_msg.attached_collision_objects.at(0).touch_links;
      attached_collision_object_json["object"] = collision_object_json;

      state_msg_json["attached_collision_object"] = attached_collision_object_json;
    }

    state_msg_json["is_diff"] = state_msg.is_diff;

    robot_states_json_vec.push_back(state_msg_json);
  }

  trajectory["robot_states"] = json(robot_states_json_vec);

  if (data.spherical_pose_metadata.size() > 0) {
    std::vector<json> spherical_pose_metadata_json_vec;

    for (const auto spherical_pose_metadata : data.spherical_pose_metadata) {
      json spherical_pose_metadata_json = {{"pose_indices", spherical_pose_metadata.pose_indices},
                                           {"num_pixels", spherical_pose_metadata.num_pixels},
                                           {"num_sides", spherical_pose_metadata.num_sides}};

      spherical_pose_metadata_json_vec.push_back(spherical_pose_metadata_json);
    }

    trajectory["spherical_pose_metadata_vec"] = spherical_pose_metadata_json_vec;
  }

  // set environment hash for security reasons !
  trajectory["environment_hash"] = environment_hash;

  std::ofstream o(trajectory_path.c_str()); // open in overwrite mode
  o << std::setw(6) << trajectory << std::endl;
}

bool readTrajectoryFromDisk(TrajectoryData &data, const fs::path &trajectory_path, std::string &environment_hash) {
  std::ifstream i(trajectory_path.c_str());
  json trajectory;
  i >> trajectory;

  // check environment hash for security reasons !
  std::string environment_hash_file = trajectory.value<std::string>("environment_hash", "");
  if (environment_hash_file.compare(environment_hash) != 0) {
    return false;
  }

  auto robot_link_states_json_vec = trajectory.find("robot_link_states");
  if (robot_link_states_json_vec != trajectory.end()) {
    data.robot_link_states = robot_link_states_json_vec.value().get<std::vector<float>>();
  }

  auto experiment_poses_json_vec = trajectory.find("experiment_poses");
  if (experiment_poses_json_vec != trajectory.end()) {
    for (const auto &pose_json : experiment_poses_json_vec.value()) {
      json position, orientation;
      geometry_msgs::Pose pose;
      geometry_msgs::PoseStamped pose_stamped;

      position = pose_json["position"];
      orientation = pose_json["orientation"];

      pose.position.x = position["x"];
      pose.position.y = position["y"];
      pose.position.z = position["z"];

      pose.orientation.x = orientation["x"];
      pose.orientation.y = orientation["y"];
      pose.orientation.z = orientation["z"];
      pose.orientation.w = orientation["w"];

      pose_stamped.pose = pose;
      pose_stamped.header.frame_id = pose_json["frame_id"];

      data.experiment_poses.push_back(pose_stamped);
    }
  }

  auto robot_trajectories_json_vec = trajectory.find("robot_trajectories");
  if (robot_trajectories_json_vec != trajectory.end()) {
    for (const auto &trajectory_json : robot_trajectories_json_vec.value()) {
      json header, points_vec;
      moveit_msgs::RobotTrajectory robot_trajectory;

      auto joint_trajectory_json = trajectory_json["joint_trajectory"];

      header = joint_trajectory_json["header"];
      robot_trajectory.joint_trajectory.header.seq = header["seq"];
      robot_trajectory.joint_trajectory.header.stamp = ros::Time(header.value<int>("stamp", 0)); // TODO check constructor
      robot_trajectory.joint_trajectory.header.frame_id = header["frame_id"];

      robot_trajectory.joint_trajectory.joint_names = joint_trajectory_json["joint_names"].get<std::vector<std::string>>();

      points_vec = joint_trajectory_json["points"];
      for (const auto &point_json : points_vec) {
        trajectory_msgs::JointTrajectoryPoint trajectory_point;

        trajectory_point.positions = point_json["positions"].get<std::vector<double>>();
        trajectory_point.velocities = point_json["velocities"].get<std::vector<double>>();
        trajectory_point.accelerations = point_json["accelerations"].get<std::vector<double>>();
        trajectory_point.effort = point_json["effort"].get<std::vector<double>>();
        trajectory_point.time_from_start.sec = point_json["time_from_start"]["secs"];
        trajectory_point.time_from_start.nsec = point_json["time_from_start"]["nsecs"];

        robot_trajectory.joint_trajectory.points.push_back(trajectory_point);
      }

      data.robot_trajectories.push_back(robot_trajectory);
    }
  }

  auto robot_states_json_vec = trajectory.find("robot_states");
  if (robot_states_json_vec != trajectory.end()) {
    for (const auto &robot_state_json : robot_states_json_vec.value()) {
      json header, joint_state_json, attached_collision_object_json, collision_object_json;
      moveit_msgs::RobotState robot_state;
      sensor_msgs::JointState joint_state;
      moveit_msgs::AttachedCollisionObject collision_object;

      joint_state_json = robot_state_json["joint_state"];
      header = joint_state_json["header"];
      robot_state.joint_state.header.seq = header["seq"];
      robot_state.joint_state.header.stamp = ros::Time(header.value<int>("stamp", 0));
      robot_state.joint_state.header.frame_id = header["frame_id"];

      robot_state.joint_state.name = joint_state_json["name"].get<std::vector<std::string>>();
      robot_state.joint_state.position = joint_state_json["position"].get<std::vector<double>>();
      robot_state.joint_state.velocity = joint_state_json["velocity"].get<std::vector<double>>();
      robot_state.joint_state.effort = joint_state_json["effort"].get<std::vector<double>>();

      attached_collision_object_json = robot_state_json["attached_collision_object"];
      collision_object_json = attached_collision_object_json["object"];
      collision_object.object.header.frame_id = collision_object_json["frame_id"];
      collision_object.object.id = collision_object_json["id"];
      shape_msgs::SolidPrimitive primitive;
      primitive.dimensions = collision_object_json["dimensions"].get<std::vector<double>>();
      collision_object.object.primitives.push_back(primitive);

      json position, orientation;
      geometry_msgs::Pose pose;

      position = collision_object_json["position"];
      orientation = collision_object_json["orientation"];

      pose.position.x = position["x"];
      pose.position.y = position["y"];
      pose.position.z = position["z"];

      pose.orientation.x = orientation["x"];
      pose.orientation.y = orientation["y"];
      pose.orientation.z = orientation["z"];
      pose.orientation.w = orientation["w"];

      collision_object.object.primitive_poses.push_back(pose);
      collision_object.object.operation = collision_object_json["operation"];

      collision_object.link_name = attached_collision_object_json["link_name"];
      collision_object.touch_links = attached_collision_object_json["touch_links"].get<std::vector<std::string>>();

      robot_state.attached_collision_objects.push_back(collision_object);

      robot_state.is_diff = robot_state_json["is_diff"];

      data.robot_states.push_back(robot_state);
    }
  }

  auto spherical_pose_metadata_json_vec = trajectory.find("spherical_pose_metadata_vec");
  if (spherical_pose_metadata_json_vec != trajectory.end()) {

    auto &spherical_pose_metadata_vec = data.spherical_pose_metadata;
    spherical_pose_metadata_vec.clear();
    for (const auto &spherical_pose_metadata_json : spherical_pose_metadata_json_vec.value()) {
      SphericalPoseMetadata spherical_pose_metadata;
      spherical_pose_metadata.pose_indices = spherical_pose_metadata_json["pose_indices"].get<std::vector<int>>();
      spherical_pose_metadata.num_pixels = spherical_pose_metadata_json["num_pixels"];
      spherical_pose_metadata.num_sides = spherical_pose_metadata_json["num_sides"];

      spherical_pose_metadata_vec.push_back(spherical_pose_metadata);
    }
  }

  return true;
}

} // namespace trajectory_utils
} // namespace roaxdt