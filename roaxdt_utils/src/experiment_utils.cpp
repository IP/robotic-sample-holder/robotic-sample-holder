#include <roaxdt_utils/experiment_utils.h>

using json = nlohmann::json;
using namespace CryptoPP;

std::string roaxdt::experiment::serializeObjects(std::vector<moveit_msgs::CollisionObject> &collision_objects) {
  std::ostringstream oss;

  for (const auto &collision_object : collision_objects) {
    oss << collision_object;
  }

  return oss.str();
}

std::string roaxdt::experiment::serializeObjects(std::vector<moveit_msgs::AttachedCollisionObject> &attached_collision_objects) {
  std::ostringstream oss;

  for (auto attached_collision_object : attached_collision_objects) {
    geometry_msgs::Pose pose_empty;
    // attached_collision_object.object.pose = pose_empty;
    attached_collision_object.object.primitive_poses.clear();
    oss << attached_collision_object;
  }

  return oss.str();
}

std::string roaxdt::experiment::serializeObject(const geometry_msgs::Point &point_stamped) {
  std::ostringstream oss;

  oss << point_stamped;

  return oss.str();
}

std::string roaxdt::experiment::serializeObject(const geometry_msgs::Quaternion &quaternion_stamped) {
  std::ostringstream oss;

  oss << quaternion_stamped;

  return oss.str();
}
std::string roaxdt::experiment::serializeObject(const moveit_msgs::AllowedCollisionMatrix &acm) {
  std::ostringstream oss;

  oss << acm;

  return oss.str();
}

std::string
roaxdt::experiment::getHashOfExperimentEnvironment(std::vector<moveit_msgs::CollisionObject> &collision_objects,
                                                   std::vector<moveit_msgs::AttachedCollisionObject> &attached_collision_objects,
                                                   moveit_msgs::AllowedCollisionMatrix &acm) {
  auto collision_objects_serialized = serializeObjects(collision_objects);
  auto attached_collision_objects_serialized = serializeObjects(attached_collision_objects);
  auto allowed_collision_matrix_serialized = serializeObject(acm);

  SHA hash;
  byte digest[SHA::DIGESTSIZE];

  hash.Update((const byte *)collision_objects_serialized.c_str(), collision_objects_serialized.length());
  hash.Update((const byte *)attached_collision_objects_serialized.c_str(), attached_collision_objects_serialized.length());
  hash.Update((const byte *)allowed_collision_matrix_serialized.c_str(), allowed_collision_matrix_serialized.length());
  hash.Final(digest);

  std::string hash_output;

  HexEncoder encoder;
  encoder.Attach(new StringSink(hash_output));
  encoder.Put(digest, sizeof(digest));
  encoder.MessageEnd();

  return hash_output;
}

std::string roaxdt::experiment::getHashOfSphericalTrajectory(const int &optimization_iteration, const int &num_iterations,
                                                             const geometry_msgs::PointStamped &sample_center,
                                                             const geometry_msgs::QuaternionStamped &goal_orientation,
                                                             const std::string &sample_holder_name,
                                                             const std::vector<std::string> &sample_names,
                                                             const std::string &trajectory_type, bool axdt) {
  auto sample_center_serialized = serializeObject(sample_center.point);
  auto quaternion_serialized = serializeObject(goal_orientation.quaternion);

  SHA hash;
  byte digest[SHA::DIGESTSIZE];

  hash.Update(reinterpret_cast<const byte *>(&optimization_iteration), sizeof(int));
  hash.Update(reinterpret_cast<const byte *>(&num_iterations), sizeof(int));
  hash.Update(reinterpret_cast<const byte *>(&axdt), sizeof(bool));
  hash.Update((const byte *)sample_center_serialized.c_str(), sample_center_serialized.length());
  hash.Update((const byte *)quaternion_serialized.c_str(), quaternion_serialized.length());
  hash.Update((const byte *)sample_holder_name.c_str(), sample_holder_name.length());
  for (const auto &sample_name : sample_names) {
    hash.Update((const byte *)sample_name.c_str(), sample_name.length());
  }
  hash.Update((const byte *)trajectory_type.c_str(), trajectory_type.length());
  hash.Final(digest);

  std::string hash_output;

  HexEncoder encoder;
  encoder.Attach(new StringSink(hash_output));
  encoder.Put(digest, sizeof(digest));
  encoder.MessageEnd();

  return hash_output;
}

std::string roaxdt::experiment::getHashOfCircularTrajectory(
    const double &min_angle, const double &max_angle, const int &num_iterations, const geometry_msgs::PointStamped &sample_center,
    const geometry_msgs::QuaternionStamped &goal_orientation, const std::string &sample_holder_name,
    const std::vector<std::string> &sample_names, const std::string &trajectory_type) {
  auto sample_center_serialized = serializeObject(sample_center.point);
  auto quaternion_serialized = serializeObject(goal_orientation.quaternion);

  SHA hash;
  byte digest[SHA::DIGESTSIZE];

  hash.Update(reinterpret_cast<const byte *>(&min_angle), sizeof(double));
  hash.Update(reinterpret_cast<const byte *>(&max_angle), sizeof(double));
  hash.Update(reinterpret_cast<const byte *>(&num_iterations), sizeof(int));
  hash.Update((const byte *)sample_center_serialized.c_str(), sample_center_serialized.length());
  hash.Update((const byte *)quaternion_serialized.c_str(), quaternion_serialized.length());
  hash.Update((const byte *)sample_holder_name.c_str(), sample_holder_name.length());
  for (const auto &sample_name : sample_names) {
    hash.Update((const byte *)sample_name.c_str(), sample_name.length());
  }
  hash.Update((const byte *)trajectory_type.c_str(), trajectory_type.length());
  hash.Final(digest);

  std::string hash_output;

  HexEncoder encoder;
  encoder.Attach(new StringSink(hash_output));
  encoder.Put(digest, sizeof(digest));
  encoder.MessageEnd();

  return hash_output;
}

void roaxdt::experiment::writeExperimentData(ExperimentData &experiment_data) {
  json metadata;

  auto goal = experiment_data.getGoalData();

  metadata["experiment_id"] = goal.experiment_id;
  metadata["sample_holder_name"] = goal.sample_holder_name;
  metadata["min_angle"] = goal.min_angle;
  metadata["max_angle"] = goal.max_angle;
  metadata["num_iterations"] = goal.num_iterations;
  metadata["trajectory_type"] = goal.trajectory_type;

  json goal_orientation_json;
  goal_orientation_json["frame_id"] = goal.goal_orientation.header.frame_id;
  goal_orientation_json["x"] = goal.goal_orientation.quaternion.x;
  goal_orientation_json["y"] = goal.goal_orientation.quaternion.y;
  goal_orientation_json["z"] = goal.goal_orientation.quaternion.z;
  goal_orientation_json["w"] = goal.goal_orientation.quaternion.w;
  metadata["goal_orientation"] = goal_orientation_json;

  metadata["trajectory_hash"] = experiment_data.getTrajectoryHash();

  std::vector<json> sample_holder_poses_json_vec;

  for (const auto &pose : experiment_data.getSampleHolderPoses()) {
    json position, orientation, pose_json;

    position["x"] = pose.position.x;
    position["y"] = pose.position.y;
    position["z"] = pose.position.z;

    orientation["x"] = pose.orientation.x;
    orientation["y"] = pose.orientation.y;
    orientation["z"] = pose.orientation.z;
    orientation["w"] = pose.orientation.w;

    pose_json["position"] = position;
    pose_json["orientation"] = orientation;

    sample_holder_poses_json_vec.push_back(pose_json);
  }

  metadata["sample_holder_poses"] = json(sample_holder_poses_json_vec);

  metadata["camera_matrix"] = experiment_data.getCameraMatrix();

  metadata["success_execute"] = experiment_data.getSuccessExecute();

  if (experiment_data.getSpherePixelsDetected().size() > 0) {
    std::vector<json> sphere_pixels_detected_json_vec;
    for (const auto &sphere_pixel : experiment_data.getSpherePixelsDetected()) {
      json pixel_detected;

      pixel_detected["x"] = sphere_pixel.x;
      pixel_detected["y"] = sphere_pixel.y;

      sphere_pixels_detected_json_vec.push_back(pixel_detected);
    }
    metadata["sphere_pixels_detected"] = sphere_pixels_detected_json_vec;
  }

  metadata["num_sphere_pixels_per_image"] = experiment_data.getNumSpherePixelsPerImage();

  if (experiment_data.getSpherePixelCorrespondences().size() > 0) {
    std::vector<json> sphere_correspondences_json_vec;
    for (const auto &sphere_correspondence : experiment_data.getSpherePixelCorrespondences()) {
      json correspondence;

      correspondence["x"] = sphere_correspondence.x;
      correspondence["y"] = sphere_correspondence.y;

      sphere_correspondences_json_vec.push_back(correspondence);
    }
    metadata["sphere_correspondences"] = sphere_correspondences_json_vec;
  }

  auto rotation_matrices = experiment_data.getRotationMatrices();
  if (rotation_matrices.size() > 0) {
    std::vector<json> rotation_matrices_json_vec;

    // create linear matrix vectors from flat vector
    auto vecSizeTmp = rotation_matrices.size() / 9;
    for (size_t i = 0; i < vecSizeTmp; i++) {
      auto index = i * 9;
      std::vector<float> vecMatrix = {
          rotation_matrices.at(index),     rotation_matrices.at(index + 1), rotation_matrices.at(index + 2),
          rotation_matrices.at(index + 3), rotation_matrices.at(index + 4), rotation_matrices.at(index + 5),
          rotation_matrices.at(index + 6), rotation_matrices.at(index + 7), rotation_matrices.at(index + 8)};

      rotation_matrices_json_vec.push_back(vecMatrix);
    }

    metadata["rotation_matrices"] = rotation_matrices_json_vec;
  }

  auto translation_vectors = experiment_data.getTranslationVectors();
  if (translation_vectors.size() > 0) {
    std::vector<json> translation_vectors_json_vec;

    // create linear matrix vectors from flat vector
    auto vecSizeTmp = translation_vectors.size() / 3;
    for (size_t i = 0; i < vecSizeTmp; i++) {
      auto index = i * 3;
      std::vector<float> vecMatrix = {translation_vectors.at(index), translation_vectors.at(index + 1),
                                      translation_vectors.at(index + 2)};

      translation_vectors_json_vec.push_back(vecMatrix);
    }

    metadata["translation_vectors"] = translation_vectors_json_vec;
  }

  auto camera_center_vectors = experiment_data.getCameraCenterVectors();
  if (camera_center_vectors.size() > 0) {
    std::vector<json> camera_center_vectors_json_vec;

    // create linear matrix vectors from flat vector
    auto vecSizeTmp = camera_center_vectors.size() / 3;
    for (size_t i = 0; i < vecSizeTmp; i++) {
      auto index = i * 3;
      std::vector<float> vecMatrix = {camera_center_vectors.at(index), camera_center_vectors.at(index + 1),
                                      camera_center_vectors.at(index + 2)};

      camera_center_vectors_json_vec.push_back(vecMatrix);
    }

    metadata["camera_center_vectors"] = camera_center_vectors_json_vec;
  }

  metadata["calibration_costs"] = experiment_data.getCalibrationCosts();

  metadata["blacklisted_images"] = experiment_data.getBlacklistedImages();

  fs::path metadata_path(experiment_data.getExperimentPath());
  metadata_path /= std::string("metadata.json");
  std::cout << "metadata json path: " << metadata_path << std::endl;
  std::ofstream o(metadata_path.c_str()); // open in overwrite mode
  o << std::setw(6) << metadata << std::endl;
}

void roaxdt::experiment::readExperimentData(ExperimentData &experiment_data, fs::path experiment_path) {
  fs::path metadata_path(experiment_path);
  metadata_path /= std::string("metadata.json");
  std::ifstream i(metadata_path.c_str());
  json metadata;
  i >> metadata;

  roaxdt_msgs::ExperimentGoal goal = experiment_data.getGoalData();

  json sample_holder_poses_json_vec = metadata["sample_holder_poses"];
  for (const auto &pose_json : sample_holder_poses_json_vec) {
    json position, orientation;
    geometry_msgs::Pose pose;

    position = pose_json["position"];
    orientation = pose_json["orientation"];

    pose.position.x = position["x"];
    pose.position.y = position["y"];
    pose.position.z = position["z"];

    pose.orientation.x = orientation["x"];
    pose.orientation.y = orientation["y"];
    pose.orientation.z = orientation["z"];
    pose.orientation.w = orientation["w"];

    experiment_data.getSampleHolderPoses().push_back(pose);
  }

  goal.experiment_id = metadata["experiment_id"];
  goal.sample_holder_name = metadata["sample_holder_name"];
  goal.min_angle = metadata["min_angle"];
  goal.max_angle = metadata["max_angle"];
  goal.num_iterations = metadata["num_iterations"];
  goal.trajectory_type = metadata["trajectory_type"];
  goal.trajectory_hash = metadata["trajectory_hash"];

  json goal_orientation_json = metadata["goal_orientation"];
  goal.goal_orientation.header.frame_id = goal_orientation_json["frame_id"];
  goal.goal_orientation.quaternion.x = goal_orientation_json["x"];
  goal.goal_orientation.quaternion.y = goal_orientation_json["y"];
  goal.goal_orientation.quaternion.z = goal_orientation_json["z"];
  goal.goal_orientation.quaternion.w = goal_orientation_json["w"];

  json sphere_pixel_detected_json_vec = metadata["sphere_pixels_detected"];
  for (const auto &sphere_pixel_detected_json : sphere_pixel_detected_json_vec) {
    geometry_msgs::Pose2D pixel_detected;

    pixel_detected.x = sphere_pixel_detected_json["x"];
    pixel_detected.y = sphere_pixel_detected_json["y"];

    experiment_data.getSpherePixelsDetected().push_back(pixel_detected);
  }

  auto num_sphere_pixels_per_image_json_vec = metadata.find("num_sphere_pixels_per_image");
  if (num_sphere_pixels_per_image_json_vec != metadata.end()) {
    experiment_data.getNumSpherePixelsPerImage() = num_sphere_pixels_per_image_json_vec.value().get<std::vector<int>>();
  }

  auto sphere_correspondences_json_vec = metadata.find("sphere_correspondences");
  if (sphere_correspondences_json_vec != metadata.end()) {
    auto &sphere_correspondences = experiment_data.getSpherePixelCorrespondences();
    sphere_correspondences.clear();

    for (const auto &sphere_correspondence_json : sphere_correspondences_json_vec.value()) {
      geometry_msgs::Pose2D sphere_correspondence;

      sphere_correspondence.x = sphere_correspondence_json["x"];
      sphere_correspondence.y = sphere_correspondence_json["y"];

      sphere_correspondences.push_back(sphere_correspondence);
    }
  }

  auto rotation_matrices_json_vec = metadata.find("rotation_matrices");
  if (rotation_matrices_json_vec != metadata.end()) {
    auto &rotation_matrices = experiment_data.getRotationMatrices();
    rotation_matrices.clear();
    auto sizeVec = goal.num_iterations * 9;
    rotation_matrices.reserve(sizeVec);
    for (const auto &error_matrix_json : rotation_matrices_json_vec.value()) {
      std::vector<float> error_matrix_vec = error_matrix_json.get<std::vector<float>>();

      for (const auto &elem : error_matrix_vec) {
        rotation_matrices.push_back(elem);
      }
    }
  }

  auto translation_vectors_json_vec = metadata.find("translation_vectors");
  if (translation_vectors_json_vec != metadata.end()) {
    auto &translation_vectors = experiment_data.getTranslationVectors();
    translation_vectors.clear();
    auto sizeVec = goal.num_iterations * 3;
    translation_vectors.reserve(sizeVec);
    for (const auto &error_matrix_json : translation_vectors_json_vec.value()) {
      std::vector<float> error_matrix_vec = error_matrix_json.get<std::vector<float>>();

      for (const auto &elem : error_matrix_vec) {
        translation_vectors.push_back(elem);
      }
    }
  }

  auto camera_center_vectors_json_vec = metadata.find("camera_center_vectors");
  if (camera_center_vectors_json_vec != metadata.end()) {
    auto &camera_center_vectors = experiment_data.getCameraCenterVectors();
    camera_center_vectors.clear();
    auto sizeVec = goal.num_iterations * 3;
    camera_center_vectors.reserve(sizeVec);
    for (const auto &error_matrix_json : camera_center_vectors_json_vec.value()) {
      std::vector<float> error_matrix_vec = error_matrix_json.get<std::vector<float>>();

      for (const auto &elem : error_matrix_vec) {
        camera_center_vectors.push_back(elem);
      }
    }
  }

  auto calibration_costs_json_vec = metadata.find("calibration_costs");
  if (calibration_costs_json_vec != metadata.end()) {
    experiment_data.getCalibrationCosts() = calibration_costs_json_vec.value().get<std::map<std::string, std::vector<float>>>();
  }

  auto camera_matrix_json_vec = metadata.find("camera_matrix");
  if (camera_matrix_json_vec != metadata.end()) {
    experiment_data.setCameraMatrix(camera_matrix_json_vec.value().get<std::vector<float>>());
  }

  auto success_execute_json_vec = metadata.find("success_execute");
  if (success_execute_json_vec != metadata.end()) {
    experiment_data.setSuccessExecute(success_execute_json_vec.value().get<std::vector<int>>());
  }

  auto blacklisted_images_json_vec = metadata.find("blacklisted_images");
  if (blacklisted_images_json_vec != metadata.end()) {
    experiment_data.getBlacklistedImages() = blacklisted_images_json_vec.value().get<std::vector<uint32_t>>();
  }

  experiment_data.setGoalData(goal);
}

void roaxdt::experiment::writeOnlineExperimentData(OnlineExperimentData &experiment_data) {
  json metadata;

  auto goal = experiment_data.getGoalData();

  metadata["experiment_id"] = goal.experiment_id;
  metadata["sample_holder_name"] = goal.sample_holder_name;
  metadata["trajectory_type"] = goal.trajectory_type;
  metadata["sphere_n_sides_low"] = goal.sphere_n_sides_low;
  metadata["sphere_n_sides_high"] = goal.sphere_n_sides_high;
  metadata["totalImageCounter"] = experiment_data.getTotalImageCounter();

  // write spherical data
  json sphericalDataLowJson, sphericalDataHighJson;
  writeSphericalData(sphericalDataLowJson, experiment_data.getSphericalDataLow());
  writeSphericalData(sphericalDataHighJson, experiment_data.getSphericalDataHigh());
  metadata["spherical_data_low"] = sphericalDataLowJson;
  metadata["spherical_data_high"] = sphericalDataHighJson;

  json goal_orientation_json;
  goal_orientation_json["frame_id"] = goal.goal_orientation.header.frame_id;
  goal_orientation_json["x"] = goal.goal_orientation.quaternion.x;
  goal_orientation_json["y"] = goal.goal_orientation.quaternion.y;
  goal_orientation_json["z"] = goal.goal_orientation.quaternion.z;
  goal_orientation_json["w"] = goal.goal_orientation.quaternion.w;
  metadata["goal_orientation"] = goal_orientation_json;

  std::vector<json> sample_holder_poses_json_vec;

  for (const auto &pose : experiment_data.getSampleHolderPoses()) {
    json position, orientation, pose_json;

    position["x"] = pose.position.x;
    position["y"] = pose.position.y;
    position["z"] = pose.position.z;

    orientation["x"] = pose.orientation.x;
    orientation["y"] = pose.orientation.y;
    orientation["z"] = pose.orientation.z;
    orientation["w"] = pose.orientation.w;

    pose_json["position"] = position;
    pose_json["orientation"] = orientation;

    sample_holder_poses_json_vec.push_back(pose_json);
  }

  metadata["sample_holder_poses"] = json(sample_holder_poses_json_vec);

  metadata["camera_matrix"] = experiment_data.getCameraMatrix();

  metadata["success_execute"] = experiment_data.getSuccessExecute();

  if (experiment_data.getSpherePixelsDetected().size() > 0) {
    std::vector<json> sphere_pixels_detected_json_vec;
    for (const auto &sphere_pixel : experiment_data.getSpherePixelsDetected()) {
      json pixel_detected;

      pixel_detected["x"] = sphere_pixel.x;
      pixel_detected["y"] = sphere_pixel.y;

      sphere_pixels_detected_json_vec.push_back(pixel_detected);
    }
    metadata["sphere_pixels_detected"] = sphere_pixels_detected_json_vec;
  }

  metadata["num_sphere_pixels_per_image"] = experiment_data.getNumSpherePixelsPerImage();

  if (experiment_data.getSpherePixelCorrespondences().size() > 0) {
    std::vector<json> sphere_correspondences_json_vec;
    for (const auto &sphere_correspondence : experiment_data.getSpherePixelCorrespondences()) {
      json correspondence;

      correspondence["x"] = sphere_correspondence.x;
      correspondence["y"] = sphere_correspondence.y;

      sphere_correspondences_json_vec.push_back(correspondence);
    }
    metadata["sphere_correspondences"] = sphere_correspondences_json_vec;
  }

  auto rotation_matrices = experiment_data.getRotationMatrices();
  if (rotation_matrices.size() > 0) {
    std::vector<json> rotation_matrices_json_vec;

    // create linear matrix vectors from flat vector
    auto vecSizeTmp = rotation_matrices.size() / 9;
    for (size_t i = 0; i < vecSizeTmp; i++) {
      auto index = i * 9;
      std::vector<float> vecMatrix = {
          rotation_matrices.at(index),     rotation_matrices.at(index + 1), rotation_matrices.at(index + 2),
          rotation_matrices.at(index + 3), rotation_matrices.at(index + 4), rotation_matrices.at(index + 5),
          rotation_matrices.at(index + 6), rotation_matrices.at(index + 7), rotation_matrices.at(index + 8)};

      rotation_matrices_json_vec.push_back(vecMatrix);
    }

    metadata["rotation_matrices"] = rotation_matrices_json_vec;
  }

  auto translation_vectors = experiment_data.getTranslationVectors();
  if (translation_vectors.size() > 0) {
    std::vector<json> translation_vectors_json_vec;

    // create linear matrix vectors from flat vector
    auto vecSizeTmp = translation_vectors.size() / 3;
    for (size_t i = 0; i < vecSizeTmp; i++) {
      auto index = i * 3;
      std::vector<float> vecMatrix = {translation_vectors.at(index), translation_vectors.at(index + 1),
                                      translation_vectors.at(index + 2)};

      translation_vectors_json_vec.push_back(vecMatrix);
    }

    metadata["translation_vectors"] = translation_vectors_json_vec;
  }

  auto camera_center_vectors = experiment_data.getCameraCenterVectors();
  if (camera_center_vectors.size() > 0) {
    std::vector<json> camera_center_vectors_json_vec;

    // create linear matrix vectors from flat vector
    auto vecSizeTmp = camera_center_vectors.size() / 3;
    for (size_t i = 0; i < vecSizeTmp; i++) {
      auto index = i * 3;
      std::vector<float> vecMatrix = {camera_center_vectors.at(index), camera_center_vectors.at(index + 1),
                                      camera_center_vectors.at(index + 2)};

      camera_center_vectors_json_vec.push_back(vecMatrix);
    }

    metadata["camera_center_vectors"] = camera_center_vectors_json_vec;
  }

  metadata["calibration_costs"] = experiment_data.getCalibrationCosts();

  metadata["blacklisted_images"] = experiment_data.getBlacklistedImages();

  metadata["pose_probabilities"] = experiment_data.getPoseProbabilities();

  fs::path metadata_path(experiment_data.getExperimentPath());
  metadata_path /= std::string("metadata.json");
  std::cout << "metadata json path: " << metadata_path << std::endl;
  std::ofstream o(metadata_path.c_str()); // open in overwrite mode
  o << std::setw(6) << metadata << std::endl;
}

void roaxdt::experiment::readOnlineExperimentData(OnlineExperimentData &experiment_data, fs::path experiment_path) {
  fs::path metadata_path(experiment_path);
  metadata_path /= std::string("metadata.json");
  std::ifstream i(metadata_path.c_str());
  json metadata;
  i >> metadata;

  roaxdt_msgs::OnlineExperimentGoal goal = experiment_data.getGoalData();

  // read spherical data
  SphericalData &spherical_data_low = experiment_data.getSphericalDataLow();
  SphericalData &spherical_data_high = experiment_data.getSphericalDataHigh();
  readSphericalData(metadata["spherical_data_low"], spherical_data_low);
  readSphericalData(metadata["spherical_data_high"], spherical_data_high);

  int totalImageCounter = metadata["totalImageCounter"];
  experiment_data.setTotalImageCounter(totalImageCounter);

  json sample_holder_poses_json_vec = metadata["sample_holder_poses"];
  for (const auto &pose_json : sample_holder_poses_json_vec) {
    json position, orientation;
    geometry_msgs::Pose pose;

    position = pose_json["position"];
    orientation = pose_json["orientation"];

    pose.position.x = position["x"];
    pose.position.y = position["y"];
    pose.position.z = position["z"];

    pose.orientation.x = orientation["x"];
    pose.orientation.y = orientation["y"];
    pose.orientation.z = orientation["z"];
    pose.orientation.w = orientation["w"];

    experiment_data.getSampleHolderPoses().push_back(pose);
  }

  goal.experiment_id = metadata["experiment_id"];
  goal.sample_holder_name = metadata["sample_holder_name"];
  goal.trajectory_type = metadata["trajectory_type"];

  json goal_orientation_json = metadata["goal_orientation"];
  goal.goal_orientation.header.frame_id = goal_orientation_json["frame_id"];
  goal.goal_orientation.quaternion.x = goal_orientation_json["x"];
  goal.goal_orientation.quaternion.y = goal_orientation_json["y"];
  goal.goal_orientation.quaternion.z = goal_orientation_json["z"];
  goal.goal_orientation.quaternion.w = goal_orientation_json["w"];

  json sphere_pixel_detected_json_vec = metadata["sphere_pixels_detected"];
  for (const auto &sphere_pixel_detected_json : sphere_pixel_detected_json_vec) {
    geometry_msgs::Pose2D pixel_detected;

    pixel_detected.x = sphere_pixel_detected_json["x"];
    pixel_detected.y = sphere_pixel_detected_json["y"];

    experiment_data.getSpherePixelsDetected().push_back(pixel_detected);
  }

  auto num_sphere_pixels_per_image_json_vec = metadata.find("num_sphere_pixels_per_image");
  if (num_sphere_pixels_per_image_json_vec != metadata.end()) {
    experiment_data.getNumSpherePixelsPerImage() = num_sphere_pixels_per_image_json_vec.value().get<std::vector<int>>();
  }

  auto sphere_correspondences_json_vec = metadata.find("sphere_correspondences");
  if (sphere_correspondences_json_vec != metadata.end()) {
    auto &sphere_correspondences = experiment_data.getSpherePixelCorrespondences();
    sphere_correspondences.clear();

    for (const auto &sphere_correspondence_json : sphere_correspondences_json_vec.value()) {
      geometry_msgs::Pose2D sphere_correspondence;

      sphere_correspondence.x = sphere_correspondence_json["x"];
      sphere_correspondence.y = sphere_correspondence_json["y"];

      sphere_correspondences.push_back(sphere_correspondence);
    }
  }

  auto rotation_matrices_json_vec = metadata.find("rotation_matrices");
  if (rotation_matrices_json_vec != metadata.end()) {
    auto &rotation_matrices = experiment_data.getRotationMatrices();
    rotation_matrices.clear();
    auto sizeVec = rotation_matrices_json_vec.value().size() * 9;
    rotation_matrices.reserve(sizeVec);
    for (const auto &error_matrix_json : rotation_matrices_json_vec.value()) {
      std::vector<float> error_matrix_vec = error_matrix_json.get<std::vector<float>>();

      for (const auto &elem : error_matrix_vec) {
        rotation_matrices.push_back(elem);
      }
    }
  }

  auto translation_vectors_json_vec = metadata.find("translation_vectors");
  if (translation_vectors_json_vec != metadata.end()) {
    auto &translation_vectors = experiment_data.getTranslationVectors();
    translation_vectors.clear();
    auto sizeVec = translation_vectors_json_vec.value().size() * 3;
    translation_vectors.reserve(sizeVec);
    for (const auto &error_matrix_json : translation_vectors_json_vec.value()) {
      std::vector<float> error_matrix_vec = error_matrix_json.get<std::vector<float>>();

      for (const auto &elem : error_matrix_vec) {
        translation_vectors.push_back(elem);
      }
    }
  }

  auto camera_center_vectors_json_vec = metadata.find("camera_center_vectors");
  if (camera_center_vectors_json_vec != metadata.end()) {
    auto &camera_center_vectors = experiment_data.getCameraCenterVectors();
    camera_center_vectors.clear();
    auto sizeVec = camera_center_vectors_json_vec.value().size() * 3;
    camera_center_vectors.reserve(sizeVec);
    for (const auto &error_matrix_json : camera_center_vectors_json_vec.value()) {
      std::vector<float> error_matrix_vec = error_matrix_json.get<std::vector<float>>();

      for (const auto &elem : error_matrix_vec) {
        camera_center_vectors.push_back(elem);
      }
    }
  }

  auto calibration_costs_json_vec = metadata.find("calibration_costs");
  if (calibration_costs_json_vec != metadata.end()) {
    experiment_data.getCalibrationCosts() = calibration_costs_json_vec.value().get<std::map<std::string, std::vector<float>>>();
  }

  auto camera_matrix_json_vec = metadata.find("camera_matrix");
  if (camera_matrix_json_vec != metadata.end()) {
    experiment_data.setCameraMatrix(camera_matrix_json_vec.value().get<std::vector<float>>());
  }

  auto success_execute_json_vec = metadata.find("success_execute");
  if (success_execute_json_vec != metadata.end()) {
    experiment_data.setSuccessExecute(success_execute_json_vec.value().get<std::vector<int>>());
  }

  auto blacklisted_images_json_vec = metadata.find("blacklisted_images");
  if (blacklisted_images_json_vec != metadata.end()) {
    experiment_data.getBlacklistedImages() = blacklisted_images_json_vec.value().get<std::vector<uint32_t>>();
  }

  experiment_data.setGoalData(goal);
}

void roaxdt::experiment::writeSphericalData(json &data, SphericalData &sphericalData) {
  data["num_pixels"] = sphericalData.num_pixels;
  data["num_sides"] = sphericalData.num_sides;
  data["pixel_scores"] = sphericalData.pixel_scores;

  std::vector<json> spherical_poses_json_vec;
  for (auto &sphericalPose : sphericalData.spherical_poses) {
    json sphericalPoseJson;
    writeSphericalPose(sphericalPoseJson, sphericalPose);
    spherical_poses_json_vec.push_back(sphericalPoseJson);
  }
  data["spherical_poses"] = spherical_poses_json_vec;
}

void roaxdt::experiment::writeSphericalPose(nlohmann::json &data, SphericalPose &sphericalPose) {
  data["num_pixels"] = sphericalPose.num_pixels;
  data["num_sides"] = sphericalPose.num_sides;
  data["index"] = sphericalPose.index;
  data["theta"] = sphericalPose.theta;
  data["phi"] = sphericalPose.phi;
  data["score"] = sphericalPose.score;
  data["success_execute"] = sphericalPose.success_execute;
  data["execution_attempted"] = sphericalPose.execution_attempted;
  data["image_id"] = sphericalPose.image_id;
}

void roaxdt::experiment::readSphericalData(json &data, SphericalData &sphericalData) {
  sphericalData.num_pixels = data["num_pixels"];
  sphericalData.num_sides = data["num_sides"];
  auto pixel_scores_json_vec = data.find("pixel_scores");
  if (pixel_scores_json_vec != data.end()) {
    sphericalData.pixel_scores = pixel_scores_json_vec.value().get<std::vector<int>>();
  }

  auto spherical_poses_json_vec = data.find("spherical_poses");
  if (spherical_poses_json_vec != data.end()) {
    auto &spherical_poses_vec = sphericalData.spherical_poses;
    spherical_poses_vec.clear();
    auto sizeVec = spherical_poses_json_vec.value().size();
    spherical_poses_vec.reserve(sizeVec);
    for (const auto &spherical_pose_json : spherical_poses_json_vec.value()) {
      SphericalPose sphericalPoseTmp;
      readSphericalPose(spherical_pose_json, sphericalPoseTmp);
      spherical_poses_vec.push_back(sphericalPoseTmp);
    }
  }
}

void roaxdt::experiment::readSphericalPose(const nlohmann::json &data, SphericalPose &sphericalPose) {
  sphericalPose.num_pixels = data["num_pixels"];
  sphericalPose.num_sides = data["num_sides"];
  sphericalPose.index = data["index"];
  sphericalPose.theta = data["theta"];
  sphericalPose.phi = data["phi"];
  sphericalPose.score = data["score"];
  sphericalPose.success_execute = data["success_execute"];
  sphericalPose.execution_attempted = data["execution_attempted"];
  sphericalPose.image_id = data["image_id"];
}

void roaxdt::experiment::convertSphericalPoseToMsg(SphericalPose &sphericalPose, roaxdt_msgs::SphericalPose &SphericalPoseMsg) {
  SphericalPoseMsg.execution_attempted = sphericalPose.execution_attempted;
  SphericalPoseMsg.index = sphericalPose.index;
  SphericalPoseMsg.num_pixels = sphericalPose.num_pixels;
  SphericalPoseMsg.num_sides = sphericalPose.num_sides;
  SphericalPoseMsg.phi = sphericalPose.phi;
  SphericalPoseMsg.theta = sphericalPose.theta;
  SphericalPoseMsg.score = sphericalPose.score;
  SphericalPoseMsg.success_execute = sphericalPose.success_execute;
  SphericalPoseMsg.image_id = sphericalPose.image_id;
}
