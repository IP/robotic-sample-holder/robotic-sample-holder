#include <roaxdt_utils/moveit_utils.h>

namespace roaxdt {
namespace moveit_utils {

std::vector<double> getJointPositions(moveit::planning_interface::MoveGroupInterface *move_group,
                                      const std::string &planning_group) {
  moveit::core::RobotStatePtr current_state = move_group->getCurrentState();
  const robot_state::JointModelGroup *joint_model_group = move_group->getCurrentState()->getJointModelGroup(planning_group);

  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);

  return joint_group_positions;
}

moveit::planning_interface::MoveGroupInterface::Plan generatePlan(moveit::planning_interface::MoveGroupInterface *move_group) {
  move_group->setStartStateToCurrentState();
  moveit::planning_interface::MoveGroupInterface::Plan plan;
  bool success = (move_group->plan(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
  ROS_INFO_STREAM("Planning to valid joint states " << success ? "SUCCESSFUL" : "FAILED");

  return plan;
}

bool executePlan(moveit::planning_interface::MoveGroupInterface *move_group,
                 moveit::planning_interface::MoveGroupInterface::Plan &plan, bool testRun) {
  // auto success_execute = move_group->execute(plan);
  auto success_execute = testRun ? false : move_group->execute(plan);

  if (success_execute == moveit::planning_interface::MoveItErrorCode::SUCCESS) {
    ROS_INFO_STREAM("Successfully moved to goal pose");
    return true;
  } else {
    ROS_INFO_STREAM("Failed moving to goal pose");
    return false;
  }
}

std::vector<double> getMaxJointPositions(moveit::planning_interface::MoveGroupInterface *move_group,
                                         const std::string &planning_group) {
  auto joint_models = move_group->getCurrentState()->getJointModelGroup(planning_group)->getActiveJointModels();

  std::vector<double> joint_positions_desired;
  for (auto joint_model : joint_models) {
    auto joint_bounds = joint_model->getVariableBounds();
    for (auto &bound : joint_bounds) {
      joint_positions_desired.push_back(bound.min_position_);
    }
  }

  return joint_positions_desired;
}

void printMoveGroupJointLimits(moveit::planning_interface::MoveGroupInterface *move_group, const std::string &planning_group) {
  auto joint_models = move_group->getCurrentState()->getJointModelGroup(planning_group)->getActiveJointModels();

  for (auto joint_model : joint_models) {
    auto joint_bounds = joint_model->getVariableBounds();
    ROS_WARN_STREAM("bounds");
    for (auto &bound : joint_bounds) {
      ROS_WARN_STREAM(bound);
    }
  }
}

void printMoveGroupJointStates(moveit::planning_interface::MoveGroupInterface *move_group, const std::string &planning_group) {
  auto joint_positions_start = getJointPositions(move_group, planning_group);
  for (auto &joint_position : joint_positions_start) {
    ROS_WARN_STREAM(joint_position);
  }
}

bool moveJointsToValidState(moveit::planning_interface::MoveGroupInterface *move_group, const std::string &planning_group) {
  printMoveGroupJointStates(move_group, planning_group);
  if (!move_group->getCurrentState()->satisfiesBounds()) {
    ROS_WARN_STREAM("Planning group " << planning_group << " joint bounds are not satisfied!");

    auto joint_positions_desired = getMaxJointPositions(move_group, planning_group);
    move_group->setJointValueTarget(joint_positions_desired);

    auto my_plan = generatePlan(move_group);
    roaxdt::moveit_utils::executePlan(move_group, my_plan, false);
  } else {
    ROS_INFO_STREAM("Planning group " << planning_group << " joints are within bounds.");
  }
}

std::vector<double> getGoalAnglesFromPlan(moveit::planning_interface::MoveGroupInterface::Plan plan) {
  auto traj_size = plan.trajectory_.joint_trajectory.points.size();
  auto joint_values_size = plan.trajectory_.joint_trajectory.points.at(traj_size - 1).positions.size();
  auto joint_angles = plan.trajectory_.joint_trajectory.points.at(traj_size - 1).positions;

  return std::move(joint_angles);
}

Eigen::Isometry3d forwardKinematics(std::unique_ptr<moveit::planning_interface::MoveGroupInterface> &move_group,
                                    robot_model::RobotModelPtr kinematic_model, const std::vector<double> &joint_angles,
                                    std::string link_name) {
  const robot_state::JointModelGroup *joint_model_group = kinematic_model->getJointModelGroup(move_group->getName());
  robot_state::RobotStatePtr kinematic_state = move_group->getCurrentState();
  kinematic_state->setJointGroupPositions(joint_model_group, joint_angles);
  const Eigen::Isometry3d &link_state = kinematic_state->getGlobalLinkTransform(link_name);

  return std::move(link_state);
}

} // namespace moveit_utils
} // namespace roaxdt