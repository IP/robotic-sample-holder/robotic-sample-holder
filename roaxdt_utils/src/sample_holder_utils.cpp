#include <roaxdt_utils/sample_holder_utils.h>

bool roaxdt::sample_holder_utils::readSampleHolderMetadata(const fs::path &metadataPath, std::vector<double> &gripper,
                                                           std::vector<double> &center, std::vector<double> &sample_center,
                                                           float &helix_height, std::vector<float> &volume_dimensions,
                                                           std::vector<float> &volume_dimensions_gripper,
                                                           std::vector<float> &volume_dimensions_head, float &sphere_diameter,
                                                           std::vector<geometry_msgs::Point> &spheres, double &helix_radius,
                                                           double &helix_omega, double &helix_phi) {
  auto volume_meta_data = roaxdt::filesystem::readJsonFile(metadataPath);

  try {
    auto center_field = volume_meta_data["center"];
    auto center_position_field = center_field["position"];
    auto center_orientation_field = center_field["orientation"];
    center = {center_position_field.value<float>("x", 0.0),    center_position_field.value<float>("y", 0.0),
              center_position_field.value<float>("z", 0.0),    center_orientation_field.value<float>("x", 0.0),
              center_orientation_field.value<float>("y", 0.0), center_orientation_field.value<float>("z", 0.0),
              center_orientation_field.value<float>("w", 0.0)};

    auto sample_center_field = volume_meta_data["sample-center"];
    sample_center = {sample_center_field.value<float>("x", 0.0), sample_center_field.value<float>("y", 0.0),
                     sample_center_field.value<float>("z", 0.0)};

    helix_height = volume_meta_data.value<float>("helix-height", 0.0);
    helix_radius = volume_meta_data.value<float>("helix-radius", 0.0);
    helix_omega = volume_meta_data.value<float>("helix-omega", 0.0);
    helix_phi = volume_meta_data.value<float>("helix-phi", 0.0);
    auto gripper_field = volume_meta_data["gripper"];
    auto gripper_position_field = gripper_field["position"];
    auto gripper_orientation_field = gripper_field["orientation"];
    gripper = {gripper_position_field.value<float>("x", 0.0),    gripper_position_field.value<float>("y", 0.0),
               gripper_position_field.value<float>("z", 0.0),    gripper_orientation_field.value<float>("x", 0.0),
               gripper_orientation_field.value<float>("y", 0.0), gripper_orientation_field.value<float>("z", 0.0),
               gripper_orientation_field.value<float>("w", 0.0)};

    if (volume_meta_data.find("dimensions") == volume_meta_data.end() || !volume_meta_data["dimensions"].is_array()) {
      throw std::runtime_error("volume dimensions not found or not expressed as array! Exiting");
    }
    volume_dimensions = {volume_meta_data["dimensions"].at(0), volume_meta_data["dimensions"].at(1),
                         volume_meta_data["dimensions"].at(2)};
    if (volume_meta_data.find("dimensions_gripper") != volume_meta_data.end() &&
        volume_meta_data["dimensions_gripper"].is_array()) {
      volume_dimensions_gripper = {volume_meta_data["dimensions_gripper"].at(0), volume_meta_data["dimensions_gripper"].at(1),
                                   volume_meta_data["dimensions_gripper"].at(2)};
    }
    if (volume_meta_data.find("dimensions_head") != volume_meta_data.end() && volume_meta_data["dimensions_head"].is_array()) {
      volume_dimensions_head = {volume_meta_data["dimensions_head"].at(0), volume_meta_data["dimensions_head"].at(1),
                                volume_meta_data["dimensions_head"].at(2)};
    }

    sphere_diameter = volume_meta_data.value<float>("sphere-diameter", 0.0);
    sphere_diameter /= 1000.0;

    if (volume_meta_data.contains("spheres")) {
      // write spheres to volume before we proceed to projection
      for (const auto &sphere : volume_meta_data["spheres"]) {
        if (!sphere.is_array()) {
          throw std::runtime_error("sphere coordiantes are not expressed as array! Exiting");
        }
        double x = sphere.at(0), y = sphere.at(1), z = sphere.at(2);
        x /= 1000.0;
        y /= 1000.0;
        z /= 1000.0;

        // ROS_INFO_STREAM("sphere pos: " << std::endl << "x " << x << " y " << y << " z " << z);

        geometry_msgs::Point point_tmp;
        point_tmp.x = x;
        point_tmp.y = y;
        point_tmp.z = z;
        spheres.push_back(point_tmp);
      }
    }
  } catch (const std::exception &e) { return false; }

  return true;
}

bool roaxdt::sample_holder_utils::readSampleMetadata(const fs::path &metadataPath, std::vector<float> &volume_dimensions,
                                                     std::vector<float> &volume_center, float &absorption) {
  auto volume_meta_data = roaxdt::filesystem::readJsonFile(metadataPath);

  try {
    auto center_field = volume_meta_data["sample-center"];
    volume_center = {center_field.value<float>("x", 0.0), center_field.value<float>("y", 0.0),
                     center_field.value<float>("z", 0.0)};

    if (volume_meta_data.find("dimensions") == volume_meta_data.end() || !volume_meta_data["dimensions"].is_array()) {
      throw std::runtime_error("volume dimensions not found or not expressed as array! Exiting");
    }
    volume_dimensions = {volume_meta_data["dimensions"].at(0), volume_meta_data["dimensions"].at(1),
                         volume_meta_data["dimensions"].at(2)};

    absorption = volume_meta_data.value<float>("absorption", 0.0);
  } catch (const std::exception &e) { return false; }

  return true;
}
