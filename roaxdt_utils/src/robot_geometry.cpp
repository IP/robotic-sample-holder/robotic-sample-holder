#include <roaxdt_utils/robot_geometry.h>

namespace roaxdt {
namespace robot_geometry {

geometry_msgs::Pose generateGraspPoseForSampleHolder(Eigen::Isometry3d transform_to_ee, geometry_msgs::Pose pose_sampled) {
  Eigen::Affine3d pose_sampled_eigen;
  tf::poseMsgToEigen(pose_sampled, pose_sampled_eigen);

  Eigen::Affine3d newState = pose_sampled_eigen * transform_to_ee;
  Eigen::Quaterniond quat_new_state(newState.rotation());

  geometry_msgs::Pose pose_result;
  tf::poseEigenToMsg(newState, pose_result);

  return pose_result;
}

} // namespace robot_geometry
} // namespace roaxdt