#include <roaxdt_utils/image_utils.h>

namespace roaxdt {
namespace image_utils {

cv::Point getTargetCenterFromImage(sensor_msgs::Image camera_image) {
  ROS_INFO_STREAM("detector image dimensions: width " << camera_image.width << " height " << camera_image.height);
  ROS_INFO_STREAM("Camera image encoding: " << camera_image.encoding);
  cv_bridge::CvImagePtr cv_ptr;
  try {
    cv_ptr = cv_bridge::toCvCopy(camera_image, sensor_msgs::image_encodings::BGR8);
  } catch (cv_bridge::Exception &e) { throw std::runtime_error(e.what()); }

  cv::Mat binarized, gray, opening, coins_background, coins_foreground, distance_transform, hsv, segment_blue;

  cv::cvtColor(cv_ptr->image, hsv, CV_BGR2HSV);
  cv::inRange(hsv, cv::Scalar(110, 50, 50), cv::Scalar(210, 255, 255), segment_blue);
  cv::Moments moments = cv::moments(segment_blue, true);
  cv::Point center(moments.m10 / moments.m00, moments.m01 / moments.m00);

  ROS_DEBUG_STREAM("current pixel " << std::endl << center);

  return center;
}

Eigen::Matrix3f getIntrinsicMatrix(float pixel_size_x, float pixel_size_y, float focal_length, float principal_point_x,
                                   float principal_point_y) {
  Eigen::Matrix3f intrinsic_K;

  intrinsic_K << focal_length / pixel_size_x, 0.0f, principal_point_x, 0.0f, focal_length / pixel_size_y, principal_point_y, 0.0f,
      0.0f, 1.0;

  return std::move(intrinsic_K);
}

} // namespace image_utils
} // namespace roaxdt