#include <roaxdt_utils/spherical_data_utils.h>

namespace roaxdt {
namespace spherical_data_utils {

size_t countSuccessfulAcquisitions(const SphericalData &spherical_data) {
  size_t num_finished_elements = 0;
  for (auto &sphericalPoseTmp : spherical_data.spherical_poses) {
    // Only increase if pose execution attempted
    if (sphericalPoseTmp.execution_attempted && sphericalPoseTmp.success_execute) {
      ++num_finished_elements;
    }
  }

  return num_finished_elements;
}

// Create separate weights vector with inverted probabilities
// We want higher absorption score to be less probable, invert probablities:
// https://stats.stackexchange.com/questions/495582/invert-probabilities-lowest-value-to-have-the-highest-probability-in-a-set
// std::vector<float> getScoreWeights(const std::vector<std::pair<size_t, float>> &scores_to_sample) {
std::vector<double> getScoreWeights(const SphericalData &spherical_data) {
  int s = 20; // penalty parameter s
  std::cout << "s: " << s << std::endl;
  std::vector<double> weights_normed, weights;
  weights_normed.reserve(spherical_data.num_pixels);
  weights.reserve(spherical_data.num_pixels);
  for (auto &sphericalPoseTmp : spherical_data.spherical_poses) {
    weights_normed.push_back(sphericalPoseTmp.score);
  }

  auto vec_min_it = std::min_element(weights_normed.begin(), weights_normed.end());
  auto vec_max_it = std::max_element(weights_normed.begin(), weights_normed.end());
  double min = *vec_min_it;
  double max = *vec_max_it;
  double ptp = max - min;
  for (auto &weight_normed : weights_normed) {
    weight_normed = (weight_normed - min) / ptp;
    weight_normed += 1.0;
  }

  for (size_t i = 0; i < weights_normed.size(); i++) {
    auto weight_normed = weights_normed.at(i);
    if (std::islessequal(weight_normed, 1.0)) {
      weight_normed = 0.0;
    }

    auto sphericalPoseTmp = spherical_data.spherical_poses.at(i);
    if (!sphericalPoseTmp.execution_attempted) {
      auto weight = std::pow(weight_normed, -s);
      if (!std::isfinite(weight)) {
        weight = 0.0;
      }
      weights.push_back(weight);
    } else {
      weights.push_back(0.0);
    }
  }

  return std::move(weights);
}

std::vector<int> getBlacklistedIndicesForIndex(const SphericalData &spherical_data, int indexOnLowerSampledSphere) {
  std::vector<int> indices_to_exclude;

  indices_to_exclude.reserve(spherical_data.num_pixels - 1);
  for (size_t j = 0; j < spherical_data.num_pixels; j++) {
    if (indexOnLowerSampledSphere != j) {
      indices_to_exclude.push_back(j);
    }
  }

  return std::move(indices_to_exclude);
}

} // namespace spherical_data_utils
} // namespace roaxdt