#include <roaxdt_utils/ros_utils.h>

using json = nlohmann::json;

tf::StampedTransform roaxdt::ros_utils::getTransformToTargetFrame(const tf::TransformListener &tf_listener,
                                                                  std::string target_frame, std::string source_frame,
                                                                  double wait_time) {
  tf::StampedTransform transform;
  try {
    ROS_INFO_STREAM("lookup for source " << source_frame << " - target " << target_frame);
    tf_listener.waitForTransform(target_frame, source_frame, ros::Time::now(), ros::Duration(wait_time));
    tf_listener.lookupTransform(target_frame, source_frame, ros::Time(), transform);
  } catch (tf::TransformException ex) { ROS_ERROR("ERROR %s", ex.what()); }

  return std::move(transform);
}

moveit_msgs::CollisionObject roaxdt::ros_utils::extractObstacleFromJson(json &root, const std::string &name) {
  moveit_msgs::CollisionObject collision_object;
  collision_object.header.frame_id = "world";
  collision_object.id = name;
  ROS_WARN_STREAM(collision_object.id);

  const json dimensions = root["dimensions"];
  ROS_INFO_STREAM("Extracted dimensions: " << dimensions);
  // Define a box to add to the world.
  shape_msgs::SolidPrimitive primitive;
  primitive.type = primitive.BOX;
  primitive.dimensions.resize(3);
  primitive.dimensions[0] = dimensions.value<double>("x", 0.0);
  primitive.dimensions[1] = dimensions.value<double>("y", 0.0);
  primitive.dimensions[2] = dimensions.value<double>("z", 0.0);

  const json position = root["position"];
  ROS_INFO_STREAM("Extracted position: " << position);

  const json orientation = root["orientation"];
  ROS_INFO_STREAM("Extracted orientation: " << orientation);
  // Define a pose for the box (specified relative to frame_id)
  geometry_msgs::Pose box_pose;
  box_pose.orientation.w = orientation.value<double>("w", 0.0);
  box_pose.orientation.x = orientation.value<double>("x", 0.0);
  box_pose.orientation.y = orientation.value<double>("y", 0.0);
  box_pose.orientation.z = orientation.value<double>("z", 0.0);
  box_pose.position.x = position.value<double>("x", 0.0) + primitive.dimensions[0] / 2.0;
  box_pose.position.y = position.value<double>("y", 0.0) + primitive.dimensions[1] / 2.0;
  box_pose.position.z = position.value<double>("z", 0.0) + primitive.dimensions[2] / 2.0;

  collision_object.primitives.push_back(primitive);
  collision_object.primitive_poses.push_back(box_pose);
  collision_object.operation = collision_object.ADD;

  return std::move(collision_object);
}

moveit_msgs::AttachedCollisionObject roaxdt::ros_utils::extractAttachedObjectFromJson(json &root, const std::string &name,
                                                                                      const std::string parent_link,
                                                                                      const float gripper_offset) {
  moveit_msgs::AttachedCollisionObject attached_object;
  attached_object.link_name = parent_link;
  attached_object.object.header.frame_id = parent_link;
  attached_object.object.id = name;

  const json dimensions = root["dimensions"];
  ROS_INFO_STREAM("Extracted dimensions: " << dimensions);

  /* Define a box to be attached */
  shape_msgs::SolidPrimitive primitive;
  primitive.type = primitive.BOX;
  primitive.dimensions.resize(3);
  primitive.dimensions[0] = dimensions.value<double>("x", 0.0);
  primitive.dimensions[1] = dimensions.value<double>("y", 0.0);
  primitive.dimensions[2] = dimensions.value<double>("z", 0.0);

  const json position = root["position"];
  ROS_INFO_STREAM("Extracted position: " << position);

  const json orientation = root["orientation"];
  ROS_INFO_STREAM("Extracted orientation: " << orientation);

  geometry_msgs::Pose pose_object;
  pose_object.orientation.w = orientation.value<double>("w", 0.0);
  pose_object.orientation.x = orientation.value<double>("x", 0.0);
  pose_object.orientation.y = orientation.value<double>("y", 0.0);
  pose_object.orientation.z = orientation.value<double>("z", 0.0);
  pose_object.position.x = position.value<double>("x", 0.0);
  pose_object.position.y = position.value<double>("y", 0.0);
  pose_object.position.z = position.value<double>("z", 0.0) + primitive.dimensions[2] / 2.0 + gripper_offset;

  attached_object.object.primitives.push_back(primitive);
  attached_object.object.primitive_poses.push_back(pose_object);

  attached_object.object.operation = attached_object.object.ADD;

  return std::move(attached_object);
}

tf::Transform roaxdt::ros_utils::extractCameraPoseFromJson(json &root) {
  tf::Transform transform;

  const json position = root["position"];
  ROS_INFO_STREAM("Extracted position: " << position);
  transform.setOrigin(
      tf::Vector3(position.value<double>("x", 0.0), position.value<double>("y", 0.0), position.value<double>("z", 0.0)));

  const json orientation = root["orientation"];
  ROS_INFO_STREAM("Extracted orientation: " << orientation);

  tf::Quaternion q(orientation.value<double>("x", 0.0), orientation.value<double>("y", 0.0), orientation.value<double>("z", 0.0),
                   orientation.value<double>("w", 0.0));
  transform.setRotation(q);

  return std::move(transform);
}

// https://github.com/ros-perception/perception_pcl/blob/melodic-devel/pcl_ros/src/transforms.cpp
void roaxdt::ros_utils::transformAsMatrix(const tf::Transform &bt, Eigen::Matrix4f &out_mat) {
  double mv[12];
  bt.getBasis().getOpenGLSubMatrix(mv);

  tf::Vector3 origin = bt.getOrigin();

  out_mat(0, 0) = mv[0];
  out_mat(0, 1) = mv[4];
  out_mat(0, 2) = mv[8];
  out_mat(1, 0) = mv[1];
  out_mat(1, 1) = mv[5];
  out_mat(1, 2) = mv[9];
  out_mat(2, 0) = mv[2];
  out_mat(2, 1) = mv[6];
  out_mat(2, 2) = mv[10];

  out_mat(3, 0) = out_mat(3, 1) = out_mat(3, 2) = 0;
  out_mat(3, 3) = 1;
  out_mat(0, 3) = origin.x();
  out_mat(1, 3) = origin.y();
  out_mat(2, 3) = origin.z();
}

bool roaxdt::ros_utils::switchControllers(ros::ServiceClient &switch_controller_client,
                                          std::vector<std::string> &stop_controllers,
                                          std::vector<std::string> &start_controllers) {
  controller_manager_msgs::SwitchController srv_switch_controller;
  srv_switch_controller.request.stop_controllers = stop_controllers;
  srv_switch_controller.request.start_controllers = start_controllers;
  srv_switch_controller.request.strictness = controller_manager_msgs::SwitchControllerRequest::BEST_EFFORT;

  std::ostringstream stream_start_controllers, stream_stop_controllers;
  for (const auto &elem : start_controllers) {
    stream_start_controllers << elem << ", ";
  }
  for (const auto &elem : stop_controllers) {
    stream_stop_controllers << elem << ", ";
  }

  if (switch_controller_client.call(srv_switch_controller)) {
    ROS_INFO_STREAM("Success switching controllers from " << stream_stop_controllers.str() << " to "
                                                          << stream_start_controllers.str());
    return true;
  } else {
    ROS_WARN_STREAM("Error switching controllers from " << stream_stop_controllers.str() << " to "
                                                        << stream_start_controllers.str());
    return false;
  }
}