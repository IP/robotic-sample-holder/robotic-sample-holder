#include <roaxdt_utils/robot_error_analysis.h>

/**
 * Reading the pose that the IK calc returns given the 3D goal state -> error in IK
 */
void roaxdt::robot::error::outputInverseKinematicsError(const Eigen::Isometry3d &pose_IK_result,
                                                        const Eigen::Affine3d &pose_desired) {
  auto total_error_IK_pos = pose_IK_result.translation() - pose_desired.translation();
  auto total_error_IK_rot = pose_IK_result.rotation() - pose_desired.rotation();
  double total_error_IK_pos_num = total_error_IK_pos.cwiseAbs().sum();
  double total_error_IK_rot_num = total_error_IK_rot.cwiseAbs().sum();
  ROS_WARN_STREAM("ROBOT transform IK:" << std::endl << pose_IK_result.translation());
  ROS_WARN_STREAM("ROBOT transform IK rotation:" << std::endl << pose_IK_result.rotation());
  ROS_INFO_STREAM("Sum of 3D translation error caused by IK: " << total_error_IK_pos_num);
  ROS_INFO_STREAM("Sum of 3D rotation error caused by IK: " << total_error_IK_rot_num);
}

/**
 * Reading the pose that the robot should have achieved given the IK calc result -> error in controller
 */
void roaxdt::robot::error::outputControllerError(const Eigen::Isometry3d &pose_IK_result, const Eigen::Affine3d &pose_desired,
                                                 const Eigen::Affine3d &pose_achieved) {
  auto total_error_IK_pos = pose_IK_result.translation() - pose_desired.translation();
  auto total_error_IK_rot = pose_IK_result.rotation() - pose_desired.rotation();
  double total_error_IK_pos_num = total_error_IK_pos.cwiseAbs().sum();
  double total_error_IK_rot_num = total_error_IK_rot.cwiseAbs().sum();

  auto total_error_controller_pos = pose_achieved.translation() - pose_IK_result.translation();
  auto total_error_controller_rot = pose_achieved.rotation() - pose_IK_result.rotation();
  ROS_WARN_STREAM("ROBOT transform Gazebo:" << std::endl << pose_achieved.translation());
  ROS_WARN_STREAM("ROBOT transform Gazebo:" << std::endl << pose_achieved.rotation());
  double total_error_controller_pos_num = total_error_controller_pos.cwiseAbs().sum();
  double total_error_controller_rot_num = total_error_controller_rot.cwiseAbs().sum();
  ROS_INFO_STREAM("Sum of 3D translation error caused controller: " << total_error_controller_pos_num);
  ROS_INFO_STREAM("Sum of 3D rotation error caused by controller: " << total_error_controller_rot_num);
  double total_error_pos = total_error_IK_pos_num + total_error_controller_pos_num;
  double total_error_rot = total_error_IK_rot_num + total_error_controller_rot_num;
  double error_share_pos_IK = total_error_IK_pos_num / total_error_pos;
  double error_share_rot_IK = total_error_IK_rot_num / total_error_rot;
  double error_share_pos_controller = total_error_controller_pos_num / total_error_pos;
  double error_share_rot_controller = total_error_controller_rot_num / total_error_rot;
  ROS_INFO_STREAM("Total IK pos error percentage: " << error_share_pos_IK * 100 << "%");
  ROS_INFO_STREAM("Total controller pos error percentage: " << error_share_pos_controller * 100 << "%");
  ROS_INFO_STREAM("Total IK rot error percentage: " << error_share_rot_IK * 100 << "%");
  ROS_INFO_STREAM("Total controller rot error percentage: " << error_share_rot_controller * 100 << "%");
}