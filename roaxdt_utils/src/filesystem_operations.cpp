#include <roaxdt_utils/filesystem_operations.h>

using json = nlohmann::json;

fs::path roaxdt::filesystem::getHomeDirectory() { return fs::path(getenv("HOME")); }

bool roaxdt::filesystem::homeDirectoryExists() {
  fs::path home(getenv("HOME"));

  return fs::is_directory(home);
}

bool roaxdt::filesystem::appDirectoryExists() {
  if (homeDirectoryExists()) {
    fs::path app_directory(getHomeDirectory());
    app_directory /= APP_DIRECTORY_NAME;

    if (!fs::exists(app_directory) && !fs::is_directory(app_directory)) {
      fs::create_directory(app_directory);
    }
    return fs::exists(app_directory) && fs::is_directory(app_directory);
  } else {
    return false;
  }
}

fs::path roaxdt::filesystem::getAppDirectory() {
  fs::path app_directory(getHomeDirectory());
  app_directory /= APP_DIRECTORY_NAME;

  if (appDirectoryExists()) {
    fs::create_directory(app_directory);
  }

  return app_directory;
}

fs::directory_iterator roaxdt::filesystem::readSubDirectory(const std::string &sub_directory) {
  if (appDirectoryExists()) {
    fs::path sub_directory_path(getAppDirectory());
    sub_directory_path /= sub_directory;

    if (!fs::exists(sub_directory_path) && !fs::is_directory(sub_directory_path)) {
      fs::create_directory(sub_directory_path);
    }

    return fs::directory_iterator(sub_directory_path);
  } else {
    return fs::directory_iterator();
  }
}

fs::path roaxdt::filesystem::readFileInSubDirectory(const std::string &sub_directory, const std::string &file) {
  if (appDirectoryExists()) {
    fs::path sub_directory_path(getAppDirectory());
    sub_directory_path /= sub_directory;

    fs::path file_path(sub_directory_path);
    file_path /= file;

    if (fs::exists(file_path) && fs::is_regular_file(file_path)) {
      return file_path;
    }
  }

  return fs::path();
}

fs::directory_iterator roaxdt::filesystem::readCollisionObjects(const std::string &collision_env) {
  fs::path collision_env_path(COLLISION_OBJECTS_DIRECTORY_NAME);
  collision_env_path /= collision_env;

  return readSubDirectory(collision_env_path.string());
}

fs::directory_iterator roaxdt::filesystem::readAttachedObjects() { return readSubDirectory(ATTACHED_OBJECT_DIRECTORY_NAME); }

fs::directory_iterator roaxdt::filesystem::readSampleHolders() { return readSubDirectory(SAMPLE_HOLDER_DIRECTORY_NAME); }

fs::directory_iterator roaxdt::filesystem::readSamples() { return readSubDirectory(SAMPLE_DIRECTORY_NAME); }

fs::directory_iterator roaxdt::filesystem::readTestFiles(const std::string &directory, const std::string &test_suite,
                                                         const std::string &test_name) {
  std::string test_path_relative(TEST_DIRECTORY);
  test_path_relative.append("/");
  test_path_relative.append(directory);
  test_path_relative.append("/");
  test_path_relative.append(test_suite);
  test_path_relative.append("/");
  test_path_relative.append(test_name);
  return readSubDirectory(test_path_relative);
}

fs::path roaxdt::filesystem::createTestDirectory(const std::string &directory, std::string &test_suite, std::string &test_name) {
  std::string test_path_relative(TEST_DIRECTORY);
  test_path_relative.append("/");
  test_path_relative.append(directory);
  test_path_relative.append("/");
  test_path_relative.append(test_suite);
  test_path_relative.append("/");
  test_path_relative.append(test_name);

  if (appDirectoryExists()) {
    fs::path sub_directory_path(getAppDirectory());
    sub_directory_path /= test_path_relative;

    if (!fs::exists(sub_directory_path) && !fs::is_directory(sub_directory_path)) {
      if (fs::create_directories(sub_directory_path)) {
        return sub_directory_path;
      }
    } else {
      return sub_directory_path;
    }
  }
}

size_t roaxdt::filesystem::countFilesInDirectory(const fs::directory_iterator &directory_iterator) {
  size_t count;
  for (auto &entry : boost::make_iterator_range(directory_iterator, {})) {
    ++count;
  }

  return count;
}

fs::path roaxdt::filesystem::readDepthCameraConfiguration(const std::string &camera_id) {
  return readFileInSubDirectory(DEPTH_CAMERAS_DIRECTORY_NAME, camera_id);
}

json roaxdt::filesystem::readJsonFile(const fs::path &path) {
  std::ifstream file_stream(path.string(), std::ifstream::binary);

  json root;
  if (file_stream) {
    file_stream >> root;
  }

  return root;
}

bool roaxdt::filesystem::writeJsonFile(const fs::path &path, nlohmann::json json) {
  try {
    std::ofstream o(path.c_str());
    o << std::setw(6) << json << std::endl;
  } catch (const std::exception &e) { return false; }

  return true;
}

void roaxdt::filesystem::makeDirectory(const fs::path &path) {
  if (!fs::exists(path)) {
    fs::create_directory(path);
  }
}