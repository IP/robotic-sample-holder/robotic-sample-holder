#pragma once

#include <geometry_msgs/PoseStamped.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <vector>

class TrajectoryData {
public:
  std::vector<geometry_msgs::PoseStamped> experiment_poses;
  std::vector<moveit_msgs::RobotTrajectory> robot_trajectories;
  std::vector<moveit_msgs::RobotState> robot_states;
  std::vector<float> robot_link_states;
  std::vector<SphericalPoseMetadata> spherical_pose_metadata;

public:
  TrajectoryData(std::vector<geometry_msgs::PoseStamped> &experiment_poses,
                 std::vector<moveit_msgs::RobotTrajectory> &robot_trajectories,
                 std::vector<moveit_msgs::RobotState> &robot_states, std::vector<float> &robot_link_states);
  TrajectoryData(std::vector<geometry_msgs::PoseStamped> &experiment_poses,
                 std::vector<moveit_msgs::RobotTrajectory> &robot_trajectories,
                 std::vector<moveit_msgs::RobotState> &robot_states, std::vector<float> &robot_link_states,
                 std::vector<SphericalPoseMetadata> &spherical_pose_metadata);
  ~TrajectoryData();
};

TrajectoryData::TrajectoryData(std::vector<geometry_msgs::PoseStamped> &experiment_poses,
                               std::vector<moveit_msgs::RobotTrajectory> &robot_trajectories,
                               std::vector<moveit_msgs::RobotState> &robot_states, std::vector<float> &robot_link_states) {
  this->experiment_poses = experiment_poses;
  this->robot_trajectories = robot_trajectories;
  this->robot_states = robot_states;
  this->robot_link_states = robot_link_states;
}

TrajectoryData::TrajectoryData(std::vector<geometry_msgs::PoseStamped> &experiment_poses,
                               std::vector<moveit_msgs::RobotTrajectory> &robot_trajectories,
                               std::vector<moveit_msgs::RobotState> &robot_states, std::vector<float> &robot_link_states,
                               std::vector<SphericalPoseMetadata> &spherical_pose_metadata) {
  this->experiment_poses = experiment_poses;
  this->robot_trajectories = robot_trajectories;
  this->robot_states = robot_states;
  this->robot_link_states = robot_link_states;
  this->spherical_pose_metadata = spherical_pose_metadata;
}

TrajectoryData::~TrajectoryData() {}
