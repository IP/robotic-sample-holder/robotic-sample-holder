#pragma once

#include "json.hpp"
#include <boost/filesystem.hpp>
#include <boost/range/irange.hpp>
#include <fstream>
#include <iomanip>

namespace fs = boost::filesystem;

namespace roaxdt {
namespace filesystem {

static const std::string APP_DIRECTORY_NAME = ".roaxdt";
static const std::string COLLISION_OBJECTS_DIRECTORY_NAME = "collision_objects";
static const std::string ATTACHED_OBJECT_DIRECTORY_NAME = "attached_object";
static const std::string DEPTH_CAMERAS_DIRECTORY_NAME = "depth_cameras";
static const std::string SAMPLE_HOLDER_DIRECTORY_NAME = "sample_holders";
static const std::string SAMPLE_DIRECTORY_NAME = "samples";
static const std::string RASTERIZED_SAMPLES_DIRECTORY_NAME = "rasterized_samples";
static const std::string TEST_DIRECTORY = "test";

fs::path getHomeDirectory();

bool homeDirectoryExists();

bool appDirectoryExists();

fs::path getAppDirectory();

fs::directory_iterator readSubDirectory(const std::string &sub_directory);

fs::path readFileInSubDirectory(const std::string &sub_directory, const std::string &file);

fs::directory_iterator readCollisionObjects(const std::string &collision_env);

fs::directory_iterator readAttachedObjects();

fs::path createTestDirectory(const std::string &directory, std::string &test_suite, std::string &test_name);

fs::directory_iterator readSampleHolders();

fs::directory_iterator readSamples();

fs::directory_iterator readTestFiles(const std::string &directory, const std::string &test_suite, const std::string &test_name);

size_t countFilesInDirectory(const fs::directory_iterator &directory_iterator);

fs::path readDepthCameraConfiguration(const std::string &camera_id);

nlohmann::json readJsonFile(const fs::path &path);

bool writeJsonFile(const fs::path &path, nlohmann::json json);

void makeDirectory(const fs::path &path);

} // namespace filesystem
} // namespace roaxdt