#include <eigen3/Eigen/Eigen>
#include <ros/ros.h>

namespace roaxdt {
namespace configuration {

template <typename T> void readConfigParameter(ros::NodeHandle &node_handle, std::string parameter_key, T &parameter_value) {
  if (node_handle.getParam(parameter_key, parameter_value)) {
    return;
  } else {
    throw std::runtime_error(ros::this_node::getName() + " not able to read parameter " + parameter_key);
  }
}

void setConfigParameterTransformMatrix(ros::NodeHandle &node_handle, std::string parameter_key, Eigen::Matrix4f &transform) {
  Eigen::Vector3f translation = transform.block<3, 1>(0, 3);
  Eigen::Matrix3f rotation = transform.block<3, 3>(0, 0);
  Eigen::Quaternionf quaternion(rotation);

  // see rosparam_shortcuts: https://github.com/PickNikRobotics/rosparam_shortcuts
  std::vector<double> shortcut_vec{translation.x(), translation.y(), translation.z(), quaternion.w(),
                                   quaternion.x(),  quaternion.y(),  quaternion.z()};
  node_handle.setParam(parameter_key, shortcut_vec);
}

} // namespace configuration
} // namespace roaxdt