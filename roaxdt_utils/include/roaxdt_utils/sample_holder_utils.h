#include <Eigen/Core>
#include <geometry_msgs/Point.h>
#include <roaxdt_utils/filesystem_operations.h>
#include <roaxdt_utils/json.hpp>

namespace fs = boost::filesystem;

namespace roaxdt {
namespace sample_holder_utils {

bool readSampleHolderMetadata(const fs::path &metadataPath, std::vector<double> &gripper, std::vector<double> &center,
                              std::vector<double> &sample_center, float &helix_height, std::vector<float> &volume_dimensions,
                              std::vector<float> &volume_dimensions_gripper, std::vector<float> &volume_dimensions_head,
                              float &sphere_diameter, std::vector<geometry_msgs::Point> &spheres, double &helix_radius,
                              double &helix_omega, double &helix_phi);

bool readSampleMetadata(const fs::path &metadataPath, std::vector<float> &volume_dimensions, std::vector<float> &volume_center,
                        float &absorption);

} // namespace sample_holder_utils
} // namespace roaxdt