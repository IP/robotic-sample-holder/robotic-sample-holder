#include <cv_bridge/cv_bridge.h>
#include <eigen3/Eigen/Eigen>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

namespace roaxdt {
namespace image_utils {

cv::Point getTargetCenterFromImage(sensor_msgs::Image camera_image);

Eigen::Matrix3f getIntrinsicMatrix(float pixel_size_x, float pixel_size_y, float focal_length, float principal_point_x,
                                   float principal_point_y);

} // namespace image_utils
} // namespace roaxdt