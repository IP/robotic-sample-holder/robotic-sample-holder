
#pragma once

#include <vector>

struct SphericalData {
  int num_pixels;
  int num_sides;
  std::vector<SphericalPose> spherical_poses;
  std::vector<int> pixel_scores;
};
