#pragma once

#include <string>
#include <vector>

namespace fs = boost::filesystem;

struct SampleHolder {
  std::string name;
  std::string grip_frame;
  std::string center_frame;
  fs::path mesh_path;
  fs::path rasterized_volume_path;
  std::vector<float> dimensions;
  std::vector<float> dimensions_gripper;
  std::vector<float> dimensions_head;
  geometry_msgs::Pose pose_center;
  geometry_msgs::Pose pose_gripper;
};
