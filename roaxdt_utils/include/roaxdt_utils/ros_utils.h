#include "json.hpp"
#include <controller_manager_msgs/SwitchController.h>
#include <eigen3/Eigen/Eigen>
#include <geometry_msgs/Pose.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
#include <tf/transform_listener.h>

namespace roaxdt {
namespace ros_utils {

tf::StampedTransform getTransformToTargetFrame(const tf::TransformListener &tf_listener, std::string target_frame,
                                               std::string source_frame, double wait_time = 1.0);

moveit_msgs::CollisionObject extractObstacleFromJson(nlohmann::json &root, const std::string &name);

moveit_msgs::AttachedCollisionObject extractAttachedObjectFromJson(nlohmann::json &root, const std::string &name,
                                                                   const std::string parent_link, const float gripper_offset);

tf::Transform extractCameraPoseFromJson(nlohmann::json &root);

void transformAsMatrix(const tf::Transform &bt, Eigen::Matrix4f &out_mat);

bool switchControllers(ros::ServiceClient &switch_controller_client, std::vector<std::string> &stop_controllers,
                       std::vector<std::string> &start_controllers);

} // namespace ros_utils
} // namespace roaxdt