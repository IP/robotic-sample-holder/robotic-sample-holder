#pragma once

#include <boost/filesystem.hpp>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/PoseStamped.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <numeric>
#include <roaxdt_msgs/BackProjectionAction.h>
#include <roaxdt_msgs/CalibrationAction.h>
#include <roaxdt_msgs/ComputePoseWeight.h>
#include <roaxdt_msgs/ComputedTomographyAction.h>
#include <roaxdt_msgs/GenerateWaypointTrajectory.h>
#include <roaxdt_msgs/GetTrajectoryGoal.h>
#include <roaxdt_msgs/LoadSampleHolderAction.h>
#include <roaxdt_msgs/OnlineExperimentAction.h>
#include <roaxdt_msgs/RegisterImagesAction.h>
#include <roaxdt_msgs/SegmentVolume.h>
#include <vector>

#include "SphericalData.h"
#include "SphericalPose.h"

class OnlineExperimentData {
private:
  roaxdt_msgs::OnlineExperimentGoal goal_mutable;
  boost::filesystem::path experiment_path;

  std::vector<float> cameraMatrixVec;
  std::vector<SphericalPoseMetadata> spherical_pose_metadata_vec;

  std::vector<geometry_msgs::Pose2D> sphere_pixels_detected;
  std::vector<int> num_sphere_pixels_per_image;
  std::vector<geometry_msgs::Pose2D> sphere_correspondences;
  std::vector<geometry_msgs::Pose> sample_holder_poses;
  std::vector<uint32_t> blacklisted_images;
  std::map<std::string, std::vector<float>> calibration_costs;
  std::vector<geometry_msgs::Point> spheres;
  std::vector<geometry_msgs::PoseStamped> experiment_poses;
  std::vector<int> success_execute;
  std::vector<float> rotation_matrices;
  std::vector<float> translation_vectors;
  std::vector<float> camera_center_vectors;
  int binning_factor = 12;
  SphericalData spherical_data_low, spherical_data_high;
  int totalImageCounter = 0;
  std::vector<std::vector<float>> poseProbabilities;

  // sample holder
  int numSpheres;
  float sampleHolderHeadCenter, helixHeight, sampleHolderSampleCenter;
  double helix_radius, helix_omega, helix_phi;

public:
  OnlineExperimentData();
  OnlineExperimentData(const roaxdt_msgs::OnlineExperimentGoalConstPtr &goal, const std::string &experiment_id,
                       std::string &experiments_base_path);
  ~OnlineExperimentData();

  boost::filesystem::path &getExperimentPath();
  roaxdt_msgs::OnlineExperimentGoal &getGoalData();
  std::vector<float> &getCameraMatrix();
  std::vector<SphericalPoseMetadata> &getSphericalPoseMetadata();
  std::vector<geometry_msgs::Pose> &getSampleHolderPoses();
  std::vector<geometry_msgs::PoseStamped> &getExperimentPoses();
  std::vector<int> &getSuccessExecute();
  std::map<std::string, std::vector<float>> &getCalibrationCosts();
  std::vector<uint32_t> &getBlacklistedImages();
  std::vector<geometry_msgs::Pose2D> &getSpherePixelsDetected();
  std::vector<int> &getNumSpherePixelsPerImage();
  std::vector<geometry_msgs::Point> &getSampleHolderSpheres();
  std::vector<float> &getRotationMatrices();
  std::vector<float> &getTranslationVectors();
  std::vector<float> &getCameraCenterVectors();
  std::vector<std::vector<float>> &getPoseProbabilities();
  std::vector<geometry_msgs::Pose2D> &getSpherePixelCorrespondences();
  SphericalData &getSphericalDataLow();
  SphericalData &getSphericalDataHigh();
  int &getTotalImageCounter();

  void setGoalData(roaxdt_msgs::OnlineExperimentGoal &goal);
  void setSphericalPoseMetadata(std::vector<SphericalPoseMetadata> &spherical_pose_metadata_vec);
  void setCameraMatrix(const std::vector<float> &cameraMatrixVec);
  void addExperimentPose(geometry_msgs::PoseStamped &experiment_pose);
  void setTrajectoryHash(const std::string &trajectory_hash);
  void setSuccessExecute(const std::vector<int> &success_execute);
  void setSampleHolderSpheres(const std::vector<geometry_msgs::Point> &spheres);
  void addSpherePixelsDetected(const std::vector<geometry_msgs::Pose2D> &sphere_pixels_detected,
                               const int &num_sphere_pixels_per_image);
  void setSampleHolder(float sampleHolderHeadCenter, float helixHeight, float sampleHolderSampleCenter, double helix_radius,
                       double helix_omega, double helix_phi);
  void setSphericalDataLow(SphericalData &sphericalDataLow);
  void setSphericalDataHigh(SphericalData &sphericalDataHigh);
  void setTotalImageCounter(int imageCounter);
  void setPoseProbabilities(const std::vector<std::vector<float>> &poseProbabilites);

  // utility methods
  void populateBackProjection(roaxdt_msgs::BackProjectionGoal &goal);
  void populateComputedTomography(roaxdt_msgs::ComputedTomographyGoal &goal);
  void populateCalibration(roaxdt_msgs::CalibrationGoal &goal, size_t image_id, float costThresholdPerSphere);
  void populateLoadSampleHolder(roaxdt_msgs::LoadSampleHolderResultConstPtr &goal);
  void populateRegisterImages(roaxdt_msgs::RegisterImagesGoal &goal);
  void populateRegisterImages(roaxdt_msgs::RegisterImagesGoal &goal, std::vector<int> &number_sequence);
  void populateGetTrajectory(roaxdt_msgs::GetTrajectoryGoal &goal);
  void populateSegmentVolume(roaxdt_msgs::SegmentVolume &srv);
  void populateComputePoseWeight(roaxdt_msgs::ComputePoseWeight &srv);
  void populateGenerateWaypointTrajectory(roaxdt_msgs::GenerateWaypointTrajectory &srv, int &num_sides,
                                          int &spherical_pose_index);

  void resizeGeometryMatrices();
};

OnlineExperimentData::OnlineExperimentData() {}

OnlineExperimentData::OnlineExperimentData(const roaxdt_msgs::OnlineExperimentGoalConstPtr &goal,
                                           const std::string &experiment_id, std::string &experiments_base_path) {
  goal_mutable.experiment_id = experiment_id;
  goal_mutable.trajectory_type = goal->trajectory_type;
  goal_mutable.sample_holder_name = goal->sample_holder_name;
  goal_mutable.num_iterations_recon = goal->num_iterations_recon;
  goal_mutable.binning_factor = goal->binning_factor;
  goal_mutable.simulate = goal->simulate;
  goal_mutable.num_spheres_random_sampling = goal->num_spheres_random_sampling;
  goal_mutable.helix_sampling_rate = goal->helix_sampling_rate;
  goal_mutable.goal_orientation = goal->goal_orientation;
  goal_mutable.sample_center = goal->sample_center;
  goal_mutable.num_optimization_steps = goal->num_optimization_steps;
  goal_mutable.sphere_n_sides_low = goal->sphere_n_sides_low;
  goal_mutable.sphere_n_sides_high = goal->sphere_n_sides_high;
  goal_mutable.trajectory_hash = goal->trajectory_hash;
  goal_mutable.sample_names = goal->sample_names;

  this->experiment_path = boost::filesystem::path(experiments_base_path + experiment_id);
}

OnlineExperimentData::~OnlineExperimentData() {}

boost::filesystem::path &OnlineExperimentData::getExperimentPath() { return experiment_path; }
roaxdt_msgs::OnlineExperimentGoal &OnlineExperimentData::getGoalData() { return goal_mutable; }
std::vector<float> &OnlineExperimentData::getCameraMatrix() { return cameraMatrixVec; }
std::vector<SphericalPoseMetadata> &OnlineExperimentData::getSphericalPoseMetadata() { return spherical_pose_metadata_vec; }
std::vector<geometry_msgs::Pose> &OnlineExperimentData::getSampleHolderPoses() { return sample_holder_poses; }
std::vector<geometry_msgs::PoseStamped> &OnlineExperimentData::getExperimentPoses() { return experiment_poses; }
std::vector<int> &OnlineExperimentData::getSuccessExecute() { return success_execute; }
std::map<std::string, std::vector<float>> &OnlineExperimentData::getCalibrationCosts() { return calibration_costs; }
std::vector<uint32_t> &OnlineExperimentData::getBlacklistedImages() { return blacklisted_images; }
std::vector<geometry_msgs::Pose2D> &OnlineExperimentData::getSpherePixelsDetected() { return sphere_pixels_detected; }
std::vector<int> &OnlineExperimentData::getNumSpherePixelsPerImage() { return num_sphere_pixels_per_image; }
std::vector<geometry_msgs::Point> &OnlineExperimentData::getSampleHolderSpheres() { return spheres; }
std::vector<float> &OnlineExperimentData::getRotationMatrices() { return rotation_matrices; }
std::vector<float> &OnlineExperimentData::getTranslationVectors() { return translation_vectors; }
std::vector<float> &OnlineExperimentData::getCameraCenterVectors() { return camera_center_vectors; }
std::vector<std::vector<float>> &OnlineExperimentData::getPoseProbabilities() { return poseProbabilities; }
std::vector<geometry_msgs::Pose2D> &OnlineExperimentData::getSpherePixelCorrespondences() { return sphere_correspondences; }
SphericalData &OnlineExperimentData::getSphericalDataLow() { return spherical_data_low; }
SphericalData &OnlineExperimentData::getSphericalDataHigh() { return spherical_data_high; }
int &OnlineExperimentData::getTotalImageCounter() { return totalImageCounter; }

void OnlineExperimentData::setGoalData(roaxdt_msgs::OnlineExperimentGoal &goal) { this->goal_mutable = goal; }

void OnlineExperimentData::setSphericalPoseMetadata(std::vector<SphericalPoseMetadata> &spherical_pose_metadata_vec) {
  this->spherical_pose_metadata_vec = spherical_pose_metadata_vec;
}

void OnlineExperimentData::setCameraMatrix(const std::vector<float> &cameraMatrixVec) { this->cameraMatrixVec = cameraMatrixVec; }

void OnlineExperimentData::addExperimentPose(geometry_msgs::PoseStamped &experiment_pose) {
  this->experiment_poses.push_back(experiment_pose);
}

void OnlineExperimentData::setTrajectoryHash(const std::string &trajectory_hash) {
  goal_mutable.trajectory_hash = trajectory_hash;
}

void OnlineExperimentData::setSuccessExecute(const std::vector<int> &success_execute) { this->success_execute = success_execute; }

void OnlineExperimentData::setSampleHolderSpheres(const std::vector<geometry_msgs::Point> &spheres) {
  this->spheres = spheres;
  numSpheres = spheres.size();
}

void OnlineExperimentData::addSpherePixelsDetected(const std::vector<geometry_msgs::Pose2D> &sphere_pixels_detected,
                                                   const int &num_sphere_pixels_per_image) {
  this->sphere_pixels_detected.insert(this->sphere_pixels_detected.end(), sphere_pixels_detected.begin(),
                                      sphere_pixels_detected.end());
  this->num_sphere_pixels_per_image.push_back(num_sphere_pixels_per_image);
}

void OnlineExperimentData::setSampleHolder(float sampleHolderHeadCenter, float helixHeight, float sampleHolderSampleCenter,
                                           double helix_radius, double helix_omega, double helix_phi) {
  this->sampleHolderHeadCenter = sampleHolderHeadCenter;
  this->helixHeight = helixHeight;
  this->sampleHolderSampleCenter = sampleHolderSampleCenter;
  this->helix_radius = helix_radius;
  this->helix_omega = helix_omega;
  this->helix_phi = helix_phi;
}

void OnlineExperimentData::setSphericalDataLow(SphericalData &sphericalDataLow) { this->spherical_data_low = sphericalDataLow; }

void OnlineExperimentData::setSphericalDataHigh(SphericalData &sphericalDataHigh) {
  this->spherical_data_high = sphericalDataHigh;
}

void OnlineExperimentData::setTotalImageCounter(int imageCounter) { this->totalImageCounter = imageCounter; }

void OnlineExperimentData::setPoseProbabilities(const std::vector<std::vector<float>> &poseProbabilities) {
  this->poseProbabilities = poseProbabilities;
}

void OnlineExperimentData::populateBackProjection(roaxdt_msgs::BackProjectionGoal &goal) {
  goal.experiment_id = goal_mutable.experiment_id;
  goal.experiment_path = experiment_path.string();
  goal.num_iterations = sample_holder_poses.size(); // TODO check!
  goal.rotation_matrices = rotation_matrices;
  goal.translation_vectors = translation_vectors;
  goal.camera_center_vectors = camera_center_vectors;
  goal.intrinsic_matrix = cameraMatrixVec;
  goal.binning_factor = goal_mutable.binning_factor;
  goal.blacklisted_images = blacklisted_images;
}

void OnlineExperimentData::populateComputedTomography(roaxdt_msgs::ComputedTomographyGoal &goal) {
  goal.experiment_id = goal_mutable.experiment_id;
  goal.experiment_path = experiment_path.string();
  goal.num_iterations = sample_holder_poses.size(); // TODO check!
  goal.num_iterations_recon = goal_mutable.num_iterations_recon;
  goal.rotation_matrices = rotation_matrices;
  goal.translation_vectors = translation_vectors;
  goal.camera_center_vectors = camera_center_vectors;
  goal.intrinsic_matrix = cameraMatrixVec;
  goal.binning_factor = goal_mutable.binning_factor;
  goal.blacklisted_images = blacklisted_images;
}

void OnlineExperimentData::populateCalibration(roaxdt_msgs::CalibrationGoal &goal, size_t image_id,
                                               float costThresholdPerSphere) {
  goal.experiment_path = experiment_path.string();
  goal.num_iterations = sample_holder_poses.size(); // TODO check!
  goal.num_spheres = spheres.size();
  goal.num_spheres_random_sampling = goal_mutable.num_spheres_random_sampling;
  goal.helix_sampling_rate = goal_mutable.helix_sampling_rate;
  goal.sphere_correspondences = sphere_pixels_detected;
  goal.sphere_correspondences_counts = num_sphere_pixels_per_image;
  goal.spheres = spheres;
  goal.cost_threshold_per_sphere = costThresholdPerSphere;
  goal.helix_height = helixHeight;
  goal.helix_radius = helix_radius;
  goal.helix_omega = helix_omega;
  goal.helix_phi = helix_phi;
  goal.image_id = image_id;
  goal.intrinsic_matrix = cameraMatrixVec;
  goal.sample_holder_pose = sample_holder_poses.at(image_id);
  goal.sample_holder_head_center = sampleHolderHeadCenter;
}

void OnlineExperimentData::populateLoadSampleHolder(roaxdt_msgs::LoadSampleHolderResultConstPtr &goal) {
  setSampleHolderSpheres(goal->spheres);
  numSpheres = goal->spheres.size();
  sampleHolderHeadCenter = goal->head_center.point.z;
  sampleHolderSampleCenter = goal->sample_center.point.z;
  helixHeight = goal->helix_height;
  helix_radius = goal->helix_radius;
  helix_omega = goal->helix_omega;
  helix_phi = goal->helix_phi;
}

void OnlineExperimentData::populateRegisterImages(roaxdt_msgs::RegisterImagesGoal &goal) {
  goal.experiment_id = goal_mutable.experiment_id;
  goal.num_spheres = numSpheres;
  std::vector<int> number_sequence(sample_holder_poses.size());
  std::iota(std::begin(number_sequence), std::end(number_sequence), 0);
  goal.image_ids = number_sequence;
}

void OnlineExperimentData::populateRegisterImages(roaxdt_msgs::RegisterImagesGoal &goal, std::vector<int> &number_sequence) {
  goal.experiment_id = goal_mutable.experiment_id;
  goal.num_spheres = numSpheres;
  goal.image_ids = number_sequence;
}

void OnlineExperimentData::populateGetTrajectory(roaxdt_msgs::GetTrajectoryGoal &goal) {
  goal.trajectory_type = goal_mutable.trajectory_type;
  goal.sample_center = goal_mutable.sample_center;
  goal.goal_orientation = goal_mutable.goal_orientation;
  goal.n_sides = goal_mutable.sphere_n_sides_high; // TODO
  goal.trajectory_hash = goal_mutable.trajectory_hash;
  goal.sample_holder_name = goal_mutable.sample_holder_name;
  goal.sample_names = goal_mutable.sample_names;
}

void OnlineExperimentData::populateSegmentVolume(roaxdt_msgs::SegmentVolume &srv) {
  srv.request.experiment_id = goal_mutable.experiment_id;
  srv.request.binning_factor = goal_mutable.binning_factor;
}

void OnlineExperimentData::populateComputePoseWeight(roaxdt_msgs::ComputePoseWeight &srv) {
  srv.request.experiment_id = goal_mutable.experiment_id;
  srv.request.rotation_matrices = rotation_matrices;
  srv.request.translation_vectors = translation_vectors;
  srv.request.camera_center_vectors = camera_center_vectors;
  srv.request.intrinsic_matrix = cameraMatrixVec;
  srv.request.binning_factor = goal_mutable.binning_factor;
  srv.request.blacklisted_images = blacklisted_images;
  srv.request.num_iterations = sample_holder_poses.size(); // TODO check!
}

void OnlineExperimentData::populateGenerateWaypointTrajectory(roaxdt_msgs::GenerateWaypointTrajectory &srv, int &num_sides,
                                                              int &spherical_pose_index) {
  srv.request.goal_orientation = goal_mutable.goal_orientation;
  srv.request.sample_center = goal_mutable.sample_center;
  srv.request.num_sides = num_sides;
  srv.request.spherical_pose_index = spherical_pose_index;
}

void OnlineExperimentData::resizeGeometryMatrices() {
  auto matrix_size = sample_holder_poses.size() * 9; // TODO check!
  auto vector_size = sample_holder_poses.size() * 3; // TODO check!
  // rotation_matrices.clear();
  rotation_matrices.resize(matrix_size);
  // translation_vectors.clear();
  translation_vectors.resize(vector_size);
  // camera_center_vectors.clear();
  camera_center_vectors.resize(vector_size);
}