#include <eigen3/Eigen/Eigen>
#include <iomanip>
#include <ros/ros.h>

namespace roaxdt {
namespace robot {
namespace error {

/**
 * Reading the pose that the IK calc returns given the 3D goal state -> error in IK
 */
void outputInverseKinematicsError(const Eigen::Isometry3d &pose_IK_result, const Eigen::Affine3d &pose_desired);

/**
 * Reading the pose that the robot should have achieved given the IK calc result -> error in controller
 */
void outputControllerError(const Eigen::Isometry3d &pose_IK_result, const Eigen::Affine3d &pose_desired,
                           const Eigen::Affine3d &pose_achieved);

} // namespace error
} // namespace robot
} // namespace roaxdt