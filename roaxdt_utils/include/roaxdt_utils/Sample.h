#pragma once

#include <boost/filesystem.hpp>
#include <string>
#include <vector>

namespace fs = boost::filesystem;

struct Sample {
  std::string name;
  fs::path metadata_path;
  fs::path mesh_path;
  fs::path volume_path;
  std::vector<float> dimensions;
  std::vector<double> spacing;
  std::vector<float> center;
  float absorption;
};
