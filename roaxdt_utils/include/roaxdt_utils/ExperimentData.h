#pragma once

#include <boost/filesystem.hpp>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/PoseStamped.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <numeric>
#include <roaxdt_msgs/CalibrationAction.h>
#include <roaxdt_msgs/ComputedTomographyAction.h>
#include <roaxdt_msgs/ExperimentAction.h>
#include <roaxdt_msgs/GetTrajectoryGoal.h>
#include <roaxdt_msgs/LoadSampleHolderAction.h>
#include <roaxdt_msgs/RegisterImagesAction.h>
#include <vector>

#include "SphericalPose.h"

class ExperimentData {
private:
  roaxdt_msgs::ExperimentGoal goal_mutable;
  boost::filesystem::path experiment_path;

  std::vector<float> cameraMatrixVec;
  std::vector<SphericalPoseMetadata> spherical_pose_metadata_vec;

  std::vector<geometry_msgs::Pose2D> sphere_pixels_detected;
  std::vector<int> num_sphere_pixels_per_image;
  std::vector<geometry_msgs::Pose2D> sphere_correspondences;
  std::vector<geometry_msgs::Pose> sample_holder_poses;
  std::vector<uint32_t> blacklisted_images;
  std::map<std::string, std::vector<float>> calibration_costs;
  std::vector<geometry_msgs::Point> spheres;
  std::vector<geometry_msgs::PoseStamped> experiment_poses;
  std::vector<int> success_execute;
  std::vector<float> rotation_matrices;
  std::vector<float> translation_vectors;
  std::vector<float> camera_center_vectors;

  // sample holder
  int numSpheres;
  float sampleHolderHeadCenter, helixHeight, sampleHolderSampleCenter;
  double helix_radius, helix_omega, helix_phi;

public:
  ExperimentData();
  ExperimentData(const roaxdt_msgs::ExperimentGoalConstPtr &goal, const std::string &experiment_id,
                 std::string &experiments_base_path);
  ~ExperimentData();

  boost::filesystem::path &getExperimentPath();
  roaxdt_msgs::ExperimentGoal &getGoalData();
  std::vector<float> &getCameraMatrix();
  std::vector<SphericalPoseMetadata> &getSphericalPoseMetadata();
  std::vector<geometry_msgs::Pose> &getSampleHolderPoses();
  std::vector<geometry_msgs::PoseStamped> &getExperimentPoses();
  std::vector<int> &getSuccessExecute();
  std::map<std::string, std::vector<float>> &getCalibrationCosts();
  std::vector<uint32_t> &getBlacklistedImages();
  std::vector<geometry_msgs::Pose2D> &getSpherePixelsDetected();
  std::vector<int> &getNumSpherePixelsPerImage();
  std::vector<geometry_msgs::Point> &getSampleHolderSpheres();
  std::vector<float> &getRotationMatrices();
  std::vector<float> &getTranslationVectors();
  std::vector<float> &getCameraCenterVectors();
  std::vector<geometry_msgs::Pose2D> &getSpherePixelCorrespondences();
  std::string &getTrajectoryHash();

  void setGoalData(roaxdt_msgs::ExperimentGoal &goal);
  void setSphericalPoseMetadata(std::vector<SphericalPoseMetadata> &spherical_pose_metadata_vec);
  void setCameraMatrix(const std::vector<float> &cameraMatrixVec);
  void setExperimentPoses(const std::vector<geometry_msgs::PoseStamped> &experiment_poses);
  void setTrajectoryHash(const std::string &trajectory_hash);
  void setSuccessExecute(const std::vector<int> &success_execute);
  void setSampleHolderSpheres(const std::vector<geometry_msgs::Point> &spheres);
  void setSpherePixelsDetected(const std::vector<geometry_msgs::Pose2D> &sphere_pixels_detected,
                               const std::vector<int> &num_sphere_pixels_per_image);
  void setSampleHolder(float sampleHolderHeadCenter, float helixHeight, float sampleHolderSampleCenter, double helix_radius,
                       double helix_omega, double helix_phi);

  // utility methods
  void populateComputedTomography(roaxdt_msgs::ComputedTomographyGoal &goal);
  void populateCalibration(roaxdt_msgs::CalibrationGoal &goal, size_t image_id, float costThresholdPerSphere);
  void populateLoadSampleHolder(roaxdt_msgs::LoadSampleHolderResultConstPtr &goal);
  void populateRegisterImages(roaxdt_msgs::RegisterImagesGoal &goal);
  void populateRegisterImages(roaxdt_msgs::RegisterImagesGoal &goal, std::vector<int> &number_sequence);
  void populateGetTrajectory(roaxdt_msgs::GetTrajectoryGoal &goal);

  void resizeGeometryMatrices();
};

ExperimentData::ExperimentData() {}

ExperimentData::ExperimentData(const roaxdt_msgs::ExperimentGoalConstPtr &goal, const std::string &experiment_id,
                               std::string &experiments_base_path) {
  goal_mutable.experiment_id = experiment_id;
  goal_mutable.trajectory_type = goal->trajectory_type;
  goal_mutable.sample_holder_name = goal->sample_holder_name;
  goal_mutable.num_iterations_recon = goal->num_iterations_recon;
  goal_mutable.binning_factor = goal->binning_factor;
  goal_mutable.run_sample_acquisition = goal->run_sample_acquisition;
  goal_mutable.run_optimize_trajectory = goal->run_optimize_trajectory;
  goal_mutable.run_register_images = goal->run_register_images;
  goal_mutable.run_calibration = goal->run_calibration;
  goal_mutable.run_computed_tomography = goal->run_computed_tomography;
  goal_mutable.simulate = goal->simulate;
  goal_mutable.num_spheres_random_sampling = goal->num_spheres_random_sampling;
  goal_mutable.helix_sampling_rate = goal->helix_sampling_rate;
  goal_mutable.min_angle = goal->min_angle;
  goal_mutable.max_angle = goal->max_angle;
  goal_mutable.num_iterations = goal->num_iterations;
  goal_mutable.goal_orientation = goal->goal_orientation;
  goal_mutable.sample_center = goal->sample_center;
  goal_mutable.num_optimization_steps = goal->num_optimization_steps;
  goal_mutable.sphere_n_sides = goal->sphere_n_sides;
  goal_mutable.trajectory_hash = goal->trajectory_hash;
  goal_mutable.sample_names = goal->sample_names;
  goal_mutable.axdt = goal->axdt;

  this->experiment_path = boost::filesystem::path(experiments_base_path + experiment_id);
}

ExperimentData::~ExperimentData() {}

boost::filesystem::path &ExperimentData::getExperimentPath() { return experiment_path; }
roaxdt_msgs::ExperimentGoal &ExperimentData::getGoalData() { return goal_mutable; }
std::vector<float> &ExperimentData::getCameraMatrix() { return cameraMatrixVec; }
std::vector<SphericalPoseMetadata> &ExperimentData::getSphericalPoseMetadata() { return spherical_pose_metadata_vec; }
std::vector<geometry_msgs::Pose> &ExperimentData::getSampleHolderPoses() { return sample_holder_poses; }
std::vector<geometry_msgs::PoseStamped> &ExperimentData::getExperimentPoses() { return experiment_poses; }
std::vector<int> &ExperimentData::getSuccessExecute() { return success_execute; }
std::map<std::string, std::vector<float>> &ExperimentData::getCalibrationCosts() { return calibration_costs; }
std::vector<uint32_t> &ExperimentData::getBlacklistedImages() { return blacklisted_images; }
std::vector<geometry_msgs::Pose2D> &ExperimentData::getSpherePixelsDetected() { return sphere_pixels_detected; }
std::vector<int> &ExperimentData::getNumSpherePixelsPerImage() { return num_sphere_pixels_per_image; }
std::vector<geometry_msgs::Point> &ExperimentData::getSampleHolderSpheres() { return spheres; }
std::vector<float> &ExperimentData::getRotationMatrices() { return rotation_matrices; }
std::vector<float> &ExperimentData::getTranslationVectors() { return translation_vectors; }
std::vector<float> &ExperimentData::getCameraCenterVectors() { return camera_center_vectors; }
std::vector<geometry_msgs::Pose2D> &ExperimentData::getSpherePixelCorrespondences() { return sphere_correspondences; }
std::string &ExperimentData::getTrajectoryHash() { return goal_mutable.trajectory_hash; }

void ExperimentData::setGoalData(roaxdt_msgs::ExperimentGoal &goal) { this->goal_mutable = goal; }

void ExperimentData::setSphericalPoseMetadata(std::vector<SphericalPoseMetadata> &spherical_pose_metadata_vec) {
  this->spherical_pose_metadata_vec = spherical_pose_metadata_vec;
}

void ExperimentData::setCameraMatrix(const std::vector<float> &cameraMatrixVec) { this->cameraMatrixVec = cameraMatrixVec; }

void ExperimentData::setExperimentPoses(const std::vector<geometry_msgs::PoseStamped> &experiment_poses) {
  this->experiment_poses = experiment_poses;
}

void ExperimentData::setTrajectoryHash(const std::string &trajectory_hash) { goal_mutable.trajectory_hash = trajectory_hash; }

void ExperimentData::setSuccessExecute(const std::vector<int> &success_execute) { this->success_execute = success_execute; }

void ExperimentData::setSampleHolderSpheres(const std::vector<geometry_msgs::Point> &spheres) {
  this->spheres = spheres;
  numSpheres = spheres.size();
}

void ExperimentData::setSpherePixelsDetected(const std::vector<geometry_msgs::Pose2D> &sphere_pixels_detected,
                                             const std::vector<int> &num_sphere_pixels_per_image) {
  this->sphere_pixels_detected = sphere_pixels_detected;
  this->num_sphere_pixels_per_image = num_sphere_pixels_per_image;
}

void ExperimentData::setSampleHolder(float sampleHolderHeadCenter, float helixHeight, float sampleHolderSampleCenter,
                                     double helix_radius, double helix_omega, double helix_phi) {
  this->sampleHolderHeadCenter = sampleHolderHeadCenter;
  this->helixHeight = helixHeight;
  this->sampleHolderSampleCenter = sampleHolderSampleCenter;
  this->helix_radius = helix_radius;
  this->helix_omega = helix_omega;
  this->helix_phi = helix_phi;
}

void ExperimentData::populateComputedTomography(roaxdt_msgs::ComputedTomographyGoal &goal) {
  goal.experiment_id = goal_mutable.experiment_id;
  goal.experiment_path = experiment_path.string();
  goal.num_iterations = sample_holder_poses.size(); // TODO check!
  goal.num_iterations_recon = goal_mutable.num_iterations_recon;
  goal.rotation_matrices = rotation_matrices;
  goal.translation_vectors = translation_vectors;
  goal.camera_center_vectors = camera_center_vectors;
  goal.intrinsic_matrix = cameraMatrixVec;
  goal.binning_factor = goal_mutable.binning_factor;
  goal.blacklisted_images = blacklisted_images;
}

void ExperimentData::populateCalibration(roaxdt_msgs::CalibrationGoal &goal, size_t image_id, float costThresholdPerSphere) {
  goal.experiment_path = experiment_path.string();
  goal.num_iterations = sample_holder_poses.size(); // TODO check!
  goal.num_spheres = spheres.size();
  goal.num_spheres_random_sampling = goal_mutable.num_spheres_random_sampling;
  goal.helix_sampling_rate = goal_mutable.helix_sampling_rate;
  goal.sphere_correspondences = sphere_pixels_detected;
  goal.sphere_correspondences_counts = num_sphere_pixels_per_image;
  goal.spheres = spheres;
  goal.cost_threshold_per_sphere = costThresholdPerSphere;
  goal.helix_height = helixHeight;
  goal.helix_radius = helix_radius;
  goal.helix_omega = helix_omega;
  goal.helix_phi = helix_phi;
  goal.image_id = image_id;
  goal.intrinsic_matrix = cameraMatrixVec;
  goal.sample_holder_pose = sample_holder_poses.at(image_id);
  goal.sample_holder_head_center = sampleHolderHeadCenter;
}

void ExperimentData::populateLoadSampleHolder(roaxdt_msgs::LoadSampleHolderResultConstPtr &goal) {
  setSampleHolderSpheres(goal->spheres);
  numSpheres = goal->spheres.size();
  sampleHolderHeadCenter = goal->head_center.point.z;
  sampleHolderSampleCenter = goal->sample_center.point.z;
  helixHeight = goal->helix_height;
  helix_radius = goal->helix_radius;
  helix_omega = goal->helix_omega;
  helix_phi = goal->helix_phi;
}

void ExperimentData::populateRegisterImages(roaxdt_msgs::RegisterImagesGoal &goal) {
  goal.experiment_id = goal_mutable.experiment_id;
  goal.num_spheres = numSpheres;
  std::vector<int> number_sequence(sample_holder_poses.size());
  std::iota(std::begin(number_sequence), std::end(number_sequence), 0);
  goal.image_ids = number_sequence;
}

void ExperimentData::populateRegisterImages(roaxdt_msgs::RegisterImagesGoal &goal, std::vector<int> &number_sequence) {
  goal.experiment_id = goal_mutable.experiment_id;
  goal.num_spheres = numSpheres;
  goal.image_ids = number_sequence;
}

void ExperimentData::populateGetTrajectory(roaxdt_msgs::GetTrajectoryGoal &goal) {
  goal.trajectory_type = goal_mutable.trajectory_type;
  goal.min_angle = goal_mutable.min_angle;
  goal.max_angle = goal_mutable.max_angle;
  goal.num_iterations = goal_mutable.num_iterations;
  goal.sample_center = goal_mutable.sample_center;
  goal.goal_orientation = goal_mutable.goal_orientation;
  goal.n_sides = goal_mutable.sphere_n_sides;
  goal.trajectory_hash = goal_mutable.trajectory_hash;
  goal.sample_holder_name = goal_mutable.sample_holder_name;
  goal.sample_names = goal_mutable.sample_names;
  goal.axdt = goal_mutable.axdt;
}

void ExperimentData::resizeGeometryMatrices() {
  auto matrix_size = sample_holder_poses.size() * 9; // TODO check!
  auto vector_size = sample_holder_poses.size() * 3; // TODO check!
  rotation_matrices.clear();
  rotation_matrices.resize(matrix_size);
  translation_vectors.clear();
  translation_vectors.resize(vector_size);
  camera_center_vectors.clear();
  camera_center_vectors.resize(vector_size);
}