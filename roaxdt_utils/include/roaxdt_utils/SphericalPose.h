#pragma once

#include <vector>

struct SphericalPose {
  int num_pixels;
  int num_sides;
  int index;
  double theta;
  double phi;
  int score;
  bool success_execute;
  bool execution_attempted;
  int image_id;
  double axdt_rotation;
};

struct SphericalPoseMetadata {
  std::vector<int> pose_indices;
  int num_pixels;
  int num_sides;
};