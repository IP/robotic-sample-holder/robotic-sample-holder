#ifndef ROAXDT_CONSTANTS_H
#define ROAXDT_CONSTANTS_H
#pragma once

#include <map>
#include <string>
#include <vector>

namespace roaxdt {
namespace constants {

static const std::string NAMESPACE = "/roaxdt";
static const std::string SIMULATION = NAMESPACE + "/simulation";
static const std::string EXPERIMENT = NAMESPACE + "/experiment";
static const std::string NUM_THREADS_PER_NODE = NAMESPACE + "/num_threads_per_node";

static const std::string IMAGE_PLANE_FRAME_SUFFIX = NAMESPACE + "/image_plane_frame_suffix";
static const std::string CAMERA_LINK_SUFFIX = NAMESPACE + "/camera_link_suffix";
static const std::string CAMERA_INFO_SUFFIX = NAMESPACE + "/camera_info_suffix";
static const std::string CAMERA_IMAGE_SUFFIX = NAMESPACE + "/camera_image_suffix";
static const std::string CAMERA_DEPTH_SUFFIX = NAMESPACE + "/camera_depth_suffix";
static const std::string CAMERA_CENTER_FRAME_SUFFIX = NAMESPACE + "/camera_center_frame_suffix";
static const std::string DETECTOR_CAMERA_NAME = NAMESPACE + "/detector_camera_name";
static const std::string CAMERA_GOAL_DISTANCE = NAMESPACE + "/camera_goal_distance";
static const std::string COLLISION_ENV = NAMESPACE + "/collision_env";

static const std::string TRAJECTORY_SUB_GOAL_TOPIC = NAMESPACE + "/trajectory_sub_goal_topic";
static const std::string TRAJECTORY_SUB_RESULT_TOPIC = NAMESPACE + "/trajectory_sub_result_topic";
static const std::string JOINT_POSITION_COMMAND_TOPIC = NAMESPACE + "/joint_position_command_topic";
static const std::string CONTROLLER_MANAGER_SWITCH_TOPIC = NAMESPACE + "/controller_manager_switch_topic";

namespace collision_detection {
static const std::string SUBSPACE("/collision_detection");
static const std::string OPENCL_KERNEL_FAST_DETECTION_PATH = NAMESPACE + SUBSPACE + "/opencl_kernel_fast_detection_path";
static const std::string OPENCL_KERNEL_VERIFICATION_PATH = NAMESPACE + SUBSPACE + "/opencl_kernel_verification_path";
static const std::string OPENCL_KERNEL_DETECTION_PATH = NAMESPACE + SUBSPACE + "/opencl_kernel_detection_path";
static const std::string OPENCL_KERNEL_MODE = NAMESPACE + SUBSPACE + "/opencl_kernel_mode";
static const std::string OPENCL_DEFAULT_DEVICE = NAMESPACE + SUBSPACE + "/opencl_default_device";
static const std::string OPENCL_DEVICE = NAMESPACE + SUBSPACE + "/opencl_device";
static const std::string OPENCL_PLATFORM = NAMESPACE + SUBSPACE + "/opencl_platform";
static const std::string MESH_VOXELATION_SPACING = NAMESPACE + SUBSPACE + "/mesh_voxelation_spacing";
static const std::string OPENCL_DEVICE_PRIVATE = "opencl_device";
} // namespace collision_detection

namespace kernel_modes {

static const std::string FAST("fast");
static const std::string ACCURATE("accurate");
} // namespace kernel_modes

namespace max_box_pcl {

static const std::string SUBSPACE("/max_box_pcl");
static const std::string MAX_BOX_PCL_MAX = NAMESPACE + SUBSPACE + "/max";
static const std::string MAX_BOX_PCL_MIN = NAMESPACE + SUBSPACE + "/min";

} // namespace max_box_pcl

namespace reconstruction {
static const std::string SUBSPACE("/reconstruction");
static const std::string DOMAIN_SPACING = NAMESPACE + SUBSPACE + "/domain_spacing";
static const std::string DETECTOR_SPACING = NAMESPACE + SUBSPACE + "/detector_spacing";
static const std::string DETECTOR_RESOLUTION = NAMESPACE + SUBSPACE + "/detector_resolution";

static const std::string DISTANCE_SOURCE_CENTER = NAMESPACE + SUBSPACE + "/distance_source_center";
static const std::string DISTANCE_CENTER_DETECTOR = NAMESPACE + SUBSPACE + "/distance_center_detector";
} // namespace reconstruction

namespace calibration {
static const std::string SUBSPACE("/calibration");
static const std::string COST_THRESHOLD_PER_SPHERE = NAMESPACE + SUBSPACE + "/cost_threshold_per_sphere";
static const std::string HELIX_SAMPLING_RATE = NAMESPACE + SUBSPACE + "/helix_sampling_rate";
} // namespace calibration

namespace depth_calibration {
static const std::string SUBSPACE("/depth_calibration");
static const std::string ERROR = NAMESPACE + SUBSPACE + "/error";
static const std::string CHESSBOARD_PATTERN_SQUARE_WIDTH = NAMESPACE + SUBSPACE + "/chessboard_pattern_square_width";
static const std::string CHESSBOARD_PATTERN_COLUMNS = NAMESPACE + SUBSPACE + "/chessboard_pattern_columns";
static const std::string CHESSBOARD_PATTERN_ROWS = NAMESPACE + SUBSPACE + "/chessboard_pattern_rows";
} // namespace depth_calibration

namespace experiments {
static const std::string SUBSPACE("/experiments");
static const std::string PATH = NAMESPACE + SUBSPACE + "/path";
static const std::string PROCESSED_PATH = NAMESPACE + SUBSPACE + "/processed_path";
static const std::string SEGMENTED_PATH = NAMESPACE + SUBSPACE + "/segmented_path";
static const std::string SINOGRAM_ANALYSED_PATH = NAMESPACE + SUBSPACE + "/sinogram_analysed_path";
static const std::string CALIBRATED_PATH = NAMESPACE + SUBSPACE + "/calibrated_path";
static const std::string CALIBRATED_GUESS_PATH = NAMESPACE + SUBSPACE + "/calibrated_guess_path";
static const std::string DETECTOR_PATH = NAMESPACE + SUBSPACE + "/detector_path";
static const std::string DETECTOR_RAW_PATH = NAMESPACE + SUBSPACE + "/detector_raw_path";
static const std::string DETECTOR_CORRECTED_PATH = NAMESPACE + SUBSPACE + "/detector_corrected_path";
static const std::string RECONSTRUCTED_PATH = NAMESPACE + SUBSPACE + "/reconstructed_path";
static const std::string REGISTER_IMAGES_TOPIC = NAMESPACE + SUBSPACE + "/register_images_topic";
static const std::string SAMPLE_NAME = NAMESPACE + SUBSPACE + "/sample_name";
static const std::string SAMPLE_HOLDER_NAME = NAMESPACE + SUBSPACE + "/sample_holder_name";
static const std::string SAMPLE_HOLDER_REFERENCE_LINK_ELSA = NAMESPACE + SUBSPACE + "/sample_holder_reference_link_elsa";
static const std::string SAMPLE_HOLDER_REFERENCE_LINK_VTK = NAMESPACE + SUBSPACE + "/sample_holder_reference_link_vtk";
static const std::string GRIP_SUFFIX = NAMESPACE + SUBSPACE + "/grip_suffix";
static const std::string SAMPLE_SUFFIX = NAMESPACE + SUBSPACE + "/sample_suffix";
static const std::string CENTER_SUFFIX = NAMESPACE + SUBSPACE + "/center_suffix";
static const std::string VTK_SUFFIX = NAMESPACE + SUBSPACE + "/vtk_suffix";
static const std::string CENTER_KEY = NAMESPACE + SUBSPACE + "/center_key";
static const std::string VTK_VOLUME_SPACING = NAMESPACE + SUBSPACE + "/vtk_volume_spacing";
static const std::string VTK_VOLUME_FILE_TYPE = NAMESPACE + SUBSPACE + "/vtk_volume_file_type";
static const std::string SAMPLE_HOLDER_FILE_TYPES = NAMESPACE + SUBSPACE + "/sample_holder_file_types";
static const std::string DETECTOR_DARK_FILE = NAMESPACE + SUBSPACE + "/detector_dark_file";
static const std::string DETECTOR_EMPTY_FILE = NAMESPACE + SUBSPACE + "/detector_empty_file";
static const std::string DETECTOR_HOST = NAMESPACE + SUBSPACE + "/detector_host";
static const std::string DETECTOR_PORT = NAMESPACE + SUBSPACE + "/detector_port";
static const std::string DETECTOR_REQUEST_STR = NAMESPACE + SUBSPACE + "/detector_request_str";
static const std::string SAMPLE_ACQUISITION_WAIT_TIME = NAMESPACE + SUBSPACE + "/sample_acquisition_wait_time";
static const std::string FLAT_FIELD_CORRECTION_SAMPLE_SIZE = NAMESPACE + SUBSPACE + "/flat_field_correction_sample_size";
static const std::string DEFAULT_IMAGE_FILE_TYPE = NAMESPACE + SUBSPACE + "/default_image_file_type";
static const std::string CORRECTED_IMAGE_FILE_TYPE = NAMESPACE + SUBSPACE + "/corrected_image_file_type";
static const std::string RECONSTRUCTION_FILE_TYPE = NAMESPACE + SUBSPACE + "/reconstruction_file_type";
static const std::string RECONSTRUCTION_FILE_NAME = NAMESPACE + SUBSPACE + "/reconstruction_file_name";
static const std::string BACK_PROJECTION_FILE_TYPE = NAMESPACE + SUBSPACE + "/back_projection_file_type";
static const std::string BACK_PROJECTION_FILE_NAME = NAMESPACE + SUBSPACE + "/back_projection_file_name";
static const std::string BACK_PROJECTION_SCALING = NAMESPACE + SUBSPACE + "/back_projection_scaling";
static const std::string SEGMENTATION_FILE_TYPE = NAMESPACE + SUBSPACE + "/segmentation_file_type";
static const std::string SEGMENTATION_FILE_NAME = NAMESPACE + SUBSPACE + "/segmentation_file_name";
} // namespace experiments

namespace trajectories {
static const std::string SUBSPACE("/trajectories");
static const std::string PATH = NAMESPACE + SUBSPACE + "/path";
static const std::string FILE_TYPE = NAMESPACE + SUBSPACE + "/file_type";
} // namespace trajectories

namespace robot_environment {
static const std::string SUBSPACE("/robot_environment");
static const std::string PATH = NAMESPACE + SUBSPACE + "/path";
static const std::string HASH = NAMESPACE + SUBSPACE + "/hash";
static const std::string XRAY_COLLISION_OBJECT_NAME = NAMESPACE + SUBSPACE + "/xray_collision_object_name";
static const std::string XRAY_ALLOWED_COLLISION_LINKS = NAMESPACE + SUBSPACE + "/xray_allowed_collision_links";
} // namespace robot_environment

namespace image_processing {
static const std::string SUBSPACE("/image_processing");

namespace image_cropping {
static const std::string SUBSPACE("/image_cropping");
static const std::string X = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/x";
static const std::string Y = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/y";
static const std::string WIDTH = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/width";
static const std::string HEIGHT = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/height";
} // namespace image_cropping

namespace image_leveling {
static const std::string SUBSPACE("/image_leveling");
static const std::string BLACK_POINT = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/black_point";
static const std::string WHITE_POINT = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/white_point";
static const std::string MAX_PIXEL = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/max_pixel";
static const std::string GAMMA = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/gamma";
} // namespace image_leveling

namespace image_filtering {
static const std::string SUBSPACE("/image_filtering");
static const std::string MEDIAN_KERNEL_SIZE = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/median_kernel_size";
} // namespace image_filtering

namespace circle_detection {
static const std::string SUBSPACE("/circle_detection");
static const std::string DP = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/dp";
static const std::string MIN_DIST = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/min_dist";
static const std::string PARAM_1 = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/param_1";
static const std::string PARAM_2 = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/param_2";
static const std::string MIN_RADIUS = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/min_radius";
static const std::string MAX_RADIUS = NAMESPACE + image_processing::SUBSPACE + SUBSPACE + "/max_radius";
} // namespace circle_detection

} // namespace image_processing

namespace robot_control {
static const std::string SUBSPACE("/robot_control");
static const std::string PANDA_ARM_TO_HAND_OFFSET = NAMESPACE + SUBSPACE + "/panda_arm_to_hand_offset";
static const std::string PANDA_ARM_VELOCITY_SCALING = NAMESPACE + SUBSPACE + "/panda_arm_velocity_scaling";
static const std::string PANDA_ARM_ACCELERATION_SCALING = NAMESPACE + SUBSPACE + "/panda_arm_acceleration_scaling";
static const std::string ATTACHED_OBJECT_LINK = NAMESPACE + SUBSPACE + "/attached_object_link";
static const std::string GOAL_ORIENTATION_TOLERANCE = NAMESPACE + SUBSPACE + "/goal_orientation_tolerance";
static const std::string GOAL_POSITION_TOLERANCE = NAMESPACE + SUBSPACE + "/goal_position_tolerance";
static const std::string GOAL_JOINT_TOLERANCE = NAMESPACE + SUBSPACE + "/goal_joint_tolerance";
static const std::string DETECTOR_EXPOSURE_TIME = NAMESPACE + SUBSPACE + "/detector_exposure_time";
static const std::string POSE_REFERENCE_FRAME = NAMESPACE + SUBSPACE + "/pose_reference_frame";
static const std::string GRIPPER_HEIGHT = NAMESPACE + SUBSPACE + "/gripper_height";
} // namespace robot_control

namespace gripper {
static const std::string SUBSPACE("/gripper");

static const std::string TOPIC = NAMESPACE + SUBSPACE + "/topic";
namespace grasp {
static const std::string SUBSPACE("/grasp");

static const std::string TOPIC = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/topic";
static const std::string EPSILON = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/epsilon";
static const std::string SPEED = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/speed";
static const std::string FORCE = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/force";
static const std::string TIMEOUT = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/timeout";

} // namespace grasp

namespace move {
static const std::string SUBSPACE("/move");

static const std::string TOPIC = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/topic";
static const std::string MAX_WIDTH = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/max_width";
static const std::string SPEED = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/speed";
static const std::string TIMEOUT = NAMESPACE + gripper::SUBSPACE + SUBSPACE + "/timeout";

} // namespace move

} // namespace gripper

static const float CAMERA_ROTATION_ERROR_THRESHOLD = 0.1;
static const float DISTANCE_NORM_TO_CAMERA = 1.0f;
static const float PANDA_JOINT8_OFFSET = 0.107f;

static const std::vector<double> PANDA_READY_STATE_WITH_FINGERS{0, -0.785, 0, -2.356, 0, 1.571, 0.785, 0.02, 0.02};
static const std::vector<double> PANDA_READY_STATE{0, -0.785, 0, -2.356, 0, 1.571, 0.785};

static const std::vector<int> COLLISION_SPACE_SPACING{512, 512, 512};

float PI = 2 * acos(0.0);
std::vector<std::vector<float>> DENAVIT_HARTENBERG_PARAMETERS({{0.0, 0.333, 0.0},
                                                               {0.0, 0.0, -PI / 2},
                                                               {0.0, 0.316, PI / 2},
                                                               {0.0825, 0.0, PI / 2},
                                                               {-0.0825, 0.384, -PI / 2},
                                                               {0.0, 0.0, PI / 2},
                                                               {0.088, 0.0, PI / 2},
                                                               {0.0, 0.107, 0}});

// Trajectory types
std::vector<std::string> TRAJECTORY_TYPES = {"circular", "spherical"};

static const std::string ROAXDT_PKG("roaxdt_core");
static const std::string ROAXDT_SAFETY_PKG("roaxdt_safety");
static const std::string ROAXDT_ASSETS("/assets");
static const std::string FRANKA_DESCRIPTION_PKG("franka_description");
static const std::string FRANKA_MESH_REL_PATH("/meshes/visual_obj_blender/");
static const std::string FRANKA_MESH_REL_PATH_STL("/meshes/collision/");

static const std::map<std::string, std::string> PANDA_FILE_TO_LINK_MAPPING_STL{
    {"link0.stl", "panda_link0"}, {"link1.stl", "panda_link1"}, {"link2.stl", "panda_link2"},
    {"link3.stl", "panda_link3"}, {"link4.stl", "panda_link4"}, {"link5.stl", "panda_link5"},
    {"link6.stl", "panda_link6"}, {"link7.stl", "panda_link7"}, {"hand.stl", "panda_hand"}};
static const std::map<std::string, std::string> PANDA_FILE_TO_LINK_MAPPING{
    {"link0.obj", "panda_link0"}, {"link1.obj", "panda_link1"}, {"link2.obj", "panda_link2"}, {"link3.obj", "panda_link3"},
    {"link4.obj", "panda_link4"}, {"link5.obj", "panda_link5"}, {"link6.obj", "panda_link6"}, {"link7.obj", "panda_link7"}};
std::vector<std::string> panda_links{"panda_link1", "panda_link2", "panda_link3", "panda_link4",
                                     "panda_link5", "panda_link6", "panda_link7"};
std::vector<std::string> panda_links_all{"panda_link1", "panda_link2", "panda_link3", "panda_link4",
                                         "panda_link5", "panda_link6", "panda_link7", "panda_link8"};
std::vector<std::string> panda_links_hand{"panda_link1", "panda_link2", "panda_link3", "panda_link4",
                                          "panda_link5", "panda_link6", "panda_link7", "panda_hand"};
std::vector<std::string> panda_joints{"panda_joint1", "panda_joint2", "panda_joint3", "panda_joint4",
                                      "panda_joint5", "panda_joint6", "panda_joint7"};
std::vector<std::string> panda_mesh_files_stl{"link0.stl", "link1.stl", "link2.stl", "link3.stl", "link4.stl",
                                              "link5.stl", "link6.stl", "link7.stl", "hand.stl"};
std::vector<std::string> panda_mesh_files{"link0.obj", "link1.obj", "link2.obj", "link3.obj", "link4.obj",
                                          "link5.obj", "link6.obj", "link7.obj", "hand.obj"};

static const std::string PLANNING_GROUP_ARM_HAND = "panda_arm_hand";
static const std::string PLANNING_GROUP_ARM = "panda_arm";
static const std::string PLANNING_GROUP_HAND = "hand";
static const std::string PANDA_EE_PARENT_LINK = "panda_link8";
static const std::string PANDA_EE_LINK = "panda_hand";
static const std::vector<std::string> PANDA_FINGER_LINKS{"panda_leftfinger", "panda_rightfinger"};
static const std::string WORLD_LINK = "world";
static const std::string VISUALIZE = "visualize";
static const std::string ROBOT_BASE_LINK = "panda_link0";
static const std::vector<std::string> PANDA_IGNORE_LINKS{"panda_hand", "panda_leftfinger", "panda_rightfinger"};

static const std::string CAMERAS = NAMESPACE + "/cameras";
// for any kind of camera (possibly multiple of them) e.g. depth camera
static const std::string CAMERA_ID = "camera";
static const std::string CAMERA_TYPE = "camera_type";
enum CAMERA_TYPES { DETECTOR_CAMERA = 1, DEPTH_CAMERA = 2, RGB_CAMERA = 3 };
static const std::string CAMERA_IMAGE_PLANE_FIELD = "image_plane";
static const std::string CAMERA_INFO_FIELD = "info";

namespace camera_info {

static const std::string PIXEL_SIZE_X("pixel_size_x");
static const std::string PIXEL_SIZE_Y("pixel_size_y");
static const std::string AREA_SIZE_X("area_size_x");
static const std::string AREA_SIZE_Y("area_size_y");
static const std::string RESOLUTION_X("resolution_x");
static const std::string RESOLUTION_Y("resolution_y");
static const std::string FOCAL_LENGTH("focal_length");
static const std::string PRINCIPAL_POINT_X("principal_point_x");
static const std::string PRINCIPAL_POINT_Y("principal_point_y");

} // namespace camera_info

namespace action_endpoints {

static const std::string SAMPLE_ROTATIONS("sample_rotations");
static const std::string RECONSTRUCT_TARGET("reconstruct_target");
static const std::string SAMPLE_ACQUISITION("sample_acquisition");
static const std::string COMPUTED_TOMOGRAPHY("computed_tomography");
static const std::string BACK_PROJECTION("back_projection");
static const std::string REGISTER_IMAGES("register_images");
static const std::string PREPARE_EXPERIMENT("prepare_experiment");
static const std::string EXPERIMENT("experiment");
static const std::string ONLINE_EXPERIMENT("online_experiment");
static const std::string CALIBRATION("calibration");
static const std::string LOAD_SAMPLE_HOLDER("load_sample_holder");
static const std::string LOAD_SAMPLE("load_sample");
static const std::string INITIALIZE_DETECTOR("initialize_detector");
static const std::string DEPTH_CAMERA_CALIBRATION("calibrate_depth_camera");
static const std::string GET_TRAJECTORY("get_trajectory");
static const std::string GET_TRAJECTORY_FROM_WHITELIST("get_trajectory_from_whitelist");
static const std::string START_SCOUT_SCAN("start_scout_scan");

// dynamic from parameter server
static const std::string GRIPPER_GRASP("gripper/grasp");
static const std::string GRIPPER_RELEASE("gripper/release");
static const std::string PICK_AND_PLACE("gripper/grasp");

// external endpoints
static const std::string ERROR_RECOVERY("/franka_control/error_recovery");
} // namespace action_endpoints

namespace service_endpoints {

static const std::string ROBOT_STOP("/robot_control_stop");
static const std::string ROBOT_MOVE_TRAJECTORY("/robot_control_move");
static const std::string ROBOT_MOVE_CARTESIAN("/robot_control_move_cartesian");
static const std::string ROBOT_MOVE_CONFIGURATION("/robot_control_move_configuration");
static const std::string ROBOT_VALIDATE_MOVE_POSE("robot_control_validate_move_pose");
static const std::string SPHERE_LIST_DISPLAY("/sphere_list_display_");
static const std::string FORWARD_PROJECTION("/forward_projection");
static const std::string SET_TARGET_VOLUME("/set_target_volume");
static const std::string READ_MESH("/read_mesh");
static const std::string SET_SAMPLE_HOLDER_GEOMETRY("/set_sample_holder_geometry");
static const std::string GET_SAMPLE_HOLDER_PARTS("/get_sample_holder_parts");
static const std::string GET_SAMPLE_PARTS("/get_sample_parts");
static const std::string DETECTOR_RETRIEVE_IMAGE("/detector_retrieve_image");
static const std::string CORRECT_FLAT_FIELD("/correct_flat_field");
static const std::string GENERATE_CIRCULAR_TRAJECTORY("/generate_circular_trajectory");
static const std::string GENERATE_SPHERICAL_TRAJECTORY("/generate_spherical_trajectory");
static const std::string GENERATE_SPHERICAL_AXDT_TRAJECTORY("/generate_spherical_axdt_trajectory");
static const std::string GENERATE_WAYPOINT_TRAJECTORY("/generate_waypoint_trajectory");
static const std::string ANALYZE_SINOGRAMS("/analyze_sinograms");
static const std::string ADD_SAMPLE_HOLDER_TO_ENVIRONMENT("/add_sample_holder_to_environment");
static const std::string ADD_SAMPLES_TO_ENVIRONMENT("/add_samples_to_environment");
static const std::string SAMPLE_SPHERE("/sample_sphere");
static const std::string SAMPLE_SPHERE_DISK("/sample_sphere_disk");
static const std::string PLACE_SPHERES("/place_spheres");
static const std::string CONCATENATE_SAMPLE_AND_HOLDER("/concatenate_sample_and_holder");
static const std::string SET_COLLISION_DETECTOR_PARTS("/set_collision_detector_parts");
static const std::string SEGMENT_VOLUME("/segment_volume");
static const std::string COMPUTE_POSE_WEIGHT("/compute_pose_weight");
static const std::string PLOT_SPHERICAL_MAP("/plot_spherical_map");
} // namespace service_endpoints

namespace nodes {

static const std::string CAMERA_STATE_PUBLISHER("camera_state_publisher_node");
static const std::string SAMPLE_HOLDER_STATE_PUBLISHER("sample_holder_state_publisher_node");
static const std::string CALIBRATION_BOX_PUBLISHER("calibration_box_publisher_node");
static const std::string ROBOT_CONTROL("robot_control_node");
static const std::string COLLISION_DETECTOR("collision_detector_node");
static const std::string COLLISION_DETECTOR_TEST("collision_detector_test_node");
static const std::string COLLISION_VISUALIZER("collision_visualizer_node");
static const std::string ROBOT_CALIBRATION("robot_calibration_node");
static const std::string SAMPLE_ACQUISITION("sample_acquisition_node");
static const std::string COMPUTED_TOMOGRAPHY("computed_tomography_node");
static const std::string GRIPPER("gripper_node");
static const std::string ROBOT_STATE_RESET("robot_state_reset_node");
static const std::string DEPTH_CAMERA_CALIBRATOR("depth_camera_calibrator");
static const std::string PICK_AND_PLACE("pick_and_place");
static const std::string MESH_READER("mesh_reader");
static const std::string IMAGE_REGISTRATION("image_registration");
static const std::string EXPERIMENT_PREPARATION("experiment_preparation");
static const std::string EXPERIMENT("experiment");
static const std::string ONLINE_EXPERIMENT("online_experiment");
static const std::string CALIBRATION("calibration");
static const std::string SAMPLE_HOLDER_MANAGER("sample_holder_manager");
static const std::string DETECTOR_MANAGER("detector_manager");
static const std::string DETECTOR_CLIENT("detector_client");
static const std::string TRAJECTORY_GENERATOR("trajectory_generator");
static const std::string TRAJECTORY_MANAGER("trajectory_manager");
static const std::string ROBOT_ENVIRONMENT_MANAGER("robot_environment_manager");
static const std::string ROBOT_STATE_MANAGER("robot_state_manager");

} // namespace nodes

namespace test {

static const std::string SUBSPACE("/test");

// private node params - not in test subspace of config.yaml
static const std::string ID = "test_id";
static const std::string STORE_TRAJECTORY = "store_trajectory";
static const std::string TRAJ_START_NUMBER = "traj_start_number";
static const std::string TEST_NAME = "test_name";
static const std::string TEST_SUITE = "test_suite";
} // namespace test

namespace visualization {

static const std::string SUBSPACE("/visualization");

static const std::string MARKER_DEPTH_COLLISION = NAMESPACE + SUBSPACE + "/marker_depth_collision";

} // namespace visualization

} // namespace constants
} // namespace roaxdt

#endif // ROAXDT_CONSTANTS_H