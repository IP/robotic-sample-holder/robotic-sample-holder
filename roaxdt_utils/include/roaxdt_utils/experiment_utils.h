#pragma once

#include "json.hpp"
#include <boost/filesystem.hpp>
#include <boost/range/irange.hpp>
#include <crypto++/hex.h>
#include <crypto++/sha.h>
#include <fstream>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/QuaternionStamped.h>
#include <iomanip>
#include <map>
#include <moveit_msgs/AllowedCollisionMatrix.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <roaxdt_msgs/ExperimentAction.h>
#include <roaxdt_msgs/OnlineExperimentAction.h>
#include <roaxdt_msgs/SphericalPose.h>
#include <sensor_msgs/CameraInfo.h>
#include <vector>

#include "ExperimentData.h"
#include "OnlineExperimentData.h"
#include "SphericalData.h"
#include "SphericalPose.h"
#include "constants.h"

namespace fs = boost::filesystem;

namespace roaxdt {
namespace experiment {

std::string serializeObjects(std::vector<moveit_msgs::CollisionObject> &collision_objects);
std::string serializeObjects(std::vector<moveit_msgs::AttachedCollisionObject> &attached_collision_objects);
std::string serializeObject(const geometry_msgs::Point &point_stamped);
std::string serializeObject(const geometry_msgs::Quaternion &quaternion_stamped);
std::string serializeObject(const moveit_msgs::AllowedCollisionMatrix &acm);

std::string getHashOfExperimentEnvironment(std::vector<moveit_msgs::CollisionObject> &collision_objects,
                                           std::vector<moveit_msgs::AttachedCollisionObject> &attached_collision_objects,
                                           moveit_msgs::AllowedCollisionMatrix &acm);

std::string getHashOfSphericalTrajectory(const int &optimization_iteration, const int &num_iterations,
                                         const geometry_msgs::PointStamped &sample_center,
                                         const geometry_msgs::QuaternionStamped &goal_orientation,
                                         const std::string &sample_holder_name, const std::vector<std::string> &sample_names,
                                         const std::string &trajectory_type, bool axdt = false);

std::string getHashOfCircularTrajectory(const double &min_angle, const double &max_angle, const int &num_iterations,
                                        const geometry_msgs::PointStamped &sample_center,
                                        const geometry_msgs::QuaternionStamped &goal_orientation,
                                        const std::string &sample_holder_name, const std::vector<std::string> &sample_names,
                                        const std::string &trajectory_type);

void writeExperimentData(ExperimentData &experiment_data);

void readExperimentData(ExperimentData &experiment_data, fs::path experiment_path);

void writeOnlineExperimentData(OnlineExperimentData &experiment_data);

void readOnlineExperimentData(OnlineExperimentData &experiment_data, fs::path experiment_path);

void writeSphericalData(nlohmann::json &data, SphericalData &sphericalData);

void writeSphericalPose(nlohmann::json &data, SphericalPose &sphericalPose);

void readSphericalData(nlohmann::json &data, SphericalData &sphericalData);

void readSphericalPose(const nlohmann::json &data, SphericalPose &sphericalPose);

void convertSphericalPoseToMsg(SphericalPose &sphericalPose, roaxdt_msgs::SphericalPose &SphericalPoseMsg);

} // namespace experiment
} // namespace roaxdt