#include <eigen3/Eigen/Eigen>
#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/Pose.h>
#include <tf/tf.h>
#include <tf_conversions/tf_eigen.h>

namespace roaxdt {
namespace robot_geometry {

geometry_msgs::Pose generateGraspPoseForSampleHolder(Eigen::Isometry3d transform_to_ee, geometry_msgs::Pose pose_sampled);

} // namespace robot_geometry
} // namespace roaxdt