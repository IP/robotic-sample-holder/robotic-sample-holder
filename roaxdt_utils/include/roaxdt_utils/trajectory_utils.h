#include <algorithm>
#include <boost/filesystem.hpp>
#include <iterator>
#include <random>
#include <ros/ros.h>

#include "SphericalPose.h"
#include "TrajectoryData.h"

#include "healpix_base.h"
#include "healpix_data_io.h"
#include "healpix_map.h"
#include "healpix_map_fitsio.h"

#include "json.hpp"

namespace fs = boost::filesystem;

namespace roaxdt {
namespace trajectory_utils {

void getHealpixMapVectors(int num_spherical_sides, std::vector<SphericalPose> &spherical_poses, int num_pixels_wanted);

void getHealpixMapVectorSingle(int num_spherical_sides, int index, SphericalPose &spherical_pose);

void getHealpixMapVectorsFromIndices(int num_spherical_sides, std::vector<SphericalPose> &spherical_poses,
                                     std::vector<int> &spherical_pose_indices);

void writeTrajectoryToDisk(const TrajectoryData &data, const fs::path &trajectory_path, const std::string &environment_hash);

bool readTrajectoryFromDisk(TrajectoryData &data, const fs::path &trajectory_path, std::string &environment_hash);

} // namespace trajectory_utils
} // namespace roaxdt