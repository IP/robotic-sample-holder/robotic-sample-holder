#include <eigen3/Eigen/Eigen>
#include <memory>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit_visual_tools/moveit_visual_tools.h>

namespace roaxdt {
namespace moveit_utils {

std::vector<double> getJointPositions(moveit::planning_interface::MoveGroupInterface *move_group,
                                      const std::string &planning_group);

moveit::planning_interface::MoveGroupInterface::Plan generatePlan(moveit::planning_interface::MoveGroupInterface *move_group);

bool executePlan(moveit::planning_interface::MoveGroupInterface *move_group,
                 moveit::planning_interface::MoveGroupInterface::Plan &plan, bool testRun);

std::vector<double> getMaxJointPositions(moveit::planning_interface::MoveGroupInterface *move_group,
                                         const std::string &planning_group);

void printMoveGroupJointLimits(moveit::planning_interface::MoveGroupInterface *move_group, const std::string &planning_group);

void printMoveGroupJointStates(moveit::planning_interface::MoveGroupInterface *move_group, const std::string &planning_group);

bool moveJointsToValidState(moveit::planning_interface::MoveGroupInterface *move_group, const std::string &planning_group);

std::vector<double> getGoalAnglesFromPlan(moveit::planning_interface::MoveGroupInterface::Plan);

Eigen::Isometry3d forwardKinematics(std::unique_ptr<moveit::planning_interface::MoveGroupInterface> &move_group,
                                    robot_model::RobotModelPtr kinematic_model, const std::vector<double> &joint_angles,
                                    std::string link_name);

} // namespace moveit_utils
} // namespace roaxdt