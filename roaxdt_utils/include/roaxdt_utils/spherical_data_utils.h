
#include "SphericalPose.h"
#include <cmath>
// #include <eigen3/Eigen/Eigen>
// #include <iomanip>
#include <algorithm>
#include <iostream>

#include "SphericalData.h"

namespace roaxdt {
namespace spherical_data_utils {

size_t countSuccessfulAcquisitions(const SphericalData &spherical_data);

std::vector<double> getScoreWeights(const SphericalData &spherical_data);

std::vector<int> getBlacklistedIndicesForIndex(const SphericalData &spherical_data, int indexOnLowerSampledSphere);

} // namespace spherical_data_utils
} // namespace roaxdt