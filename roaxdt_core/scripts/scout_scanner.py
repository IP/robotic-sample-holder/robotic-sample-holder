#! /usr/bin/env python3

import rospy

from roaxdt_core.scout_scanner import ScoutScanner


def main():
    rospy.init_node('scout_scanner')
    while rospy.get_time() == 0.0:
        pass

    ir = ScoutScanner()

    rospy.spin()


if __name__ == '__main__':
    main()
