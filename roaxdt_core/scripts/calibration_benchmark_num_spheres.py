#! /usr/bin/env python3

import rospy
import roaxdt_msgs.msg
import actionlib
import numpy as np


def main():
    rospy.init_node('calibration_benchmark')
    while rospy.get_time() == 0.0:
        pass

    client = actionlib.SimpleActionClient('/experiment', roaxdt_msgs.msg.ExperimentAction)

    rospy.loginfo("Waiting for /experiment action server to appear")
    client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = roaxdt_msgs.msg.ExperimentGoal()
    goal.experiment_id = "9526d085-18c6-4f62-a51e-5ba0fdde9c62"
    goal.run_calibration = True
    goal.sample_holder_base_name = "sample-holder-base-3"
    goal.sample_holder_head_name = "sample-holder-head-3"
    goal.helix_sampling_rate = 10000

    num_spheres = 50
    benchmark_stop_index = 10
    num_runs = int(((num_spheres - benchmark_stop_index) / 2) + 1)
    for i in np.linspace(num_spheres, benchmark_stop_index, num_runs, dtype=np.int):
        goal.num_spheres_random_sampling = i

        rospy.loginfo("Sending request to /experiment action server with num spheres %i" % i)
        client.send_goal_and_wait(goal)

        _ = client.get_result()
        rospy.loginfo("Received result from /experiment action server for sphere sampling number %i", i)

    return

if __name__ == '__main__':
    main()