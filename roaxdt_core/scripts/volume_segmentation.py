#! /usr/bin/env python3

import rospy

from roaxdt_core.volume_segmentation import VolumeSegmentation


def main():
    rospy.init_node('volume_segmentation')
    while rospy.get_time() == 0.0:
        pass

    vu = VolumeSegmentation()

    rospy.spin()


if __name__ == '__main__':
    main()
