#! /usr/bin/env python3

import rospy

from roaxdt_core.trajectory_plotter import TrajectoryPlotter


def main():
    rospy.init_node('trajectory_plotter')
    while rospy.get_time() == 0.0:
        pass

    ir = TrajectoryPlotter()

    rospy.spin()


if __name__ == '__main__':
    main()
