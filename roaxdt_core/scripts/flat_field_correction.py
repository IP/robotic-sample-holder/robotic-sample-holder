#! /usr/bin/env python3

import rospy

from roaxdt_core.flat_field_correction import FlatFieldCorrection


def main():
    rospy.init_node('flat_field_correction')
    while rospy.get_time() == 0.0:
        pass

    ir = FlatFieldCorrection()

    rospy.spin()


if __name__ == '__main__':
    main()
