#! /usr/bin/env python3

import rospy

from roaxdt_core.sinogram_analyzer import SinogramAnalyzer


def main():
    rospy.init_node('sinogram_analyzer')
    while rospy.get_time() == 0.0:
        pass

    ir = SinogramAnalyzer()

    rospy.spin()


if __name__ == '__main__':
    main()
