#! /usr/bin/env python3

import rospy

from roaxdt_core.image_retriever import ImageRetriever


def main():
    rospy.init_node('image_retriever')
    while rospy.get_time() == 0.0:
        pass

    ir = ImageRetriever()

    rospy.spin()


if __name__ == '__main__':
    main()
