#! /usr/bin/env python3

import rospy

from roaxdt_core.sphere_sampler import SphereSampler


def main():
    rospy.init_node('sphere_sampler')
    while rospy.get_time() == 0.0:
        pass

    ir = SphereSampler()

    rospy.spin()


if __name__ == '__main__':
    main()
