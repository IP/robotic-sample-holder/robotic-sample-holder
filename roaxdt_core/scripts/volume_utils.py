#! /usr/bin/env python3

import rospy

from roaxdt_core.volume_utils import VolumeUtils


def main():
    rospy.init_node('volume_utils')
    while rospy.get_time() == 0.0:
        pass

    vu = VolumeUtils()

    rospy.spin()


if __name__ == '__main__':
    main()
