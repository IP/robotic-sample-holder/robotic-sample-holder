#! /usr/bin/env python3

import rospy
import geometry_msgs.msg
import roaxdt_msgs.msg
import roaxdt_msgs.srv
import actionlib
import numpy as np
import math
import uuid
import json
import os


def validatePose(translation, rotation, center_translation, goal_frame_id):
    rospy.wait_for_service('/robot_control_validate_move_pose')
    try:
        validate_move_to_pose = rospy.ServiceProxy(
            '/robot_control_validate_move_pose', roaxdt_msgs.srv.RobotValidateMoveToPoseMsg)

        goal_pose = geometry_msgs.msg.PoseStamped()
        goal_pose.header.frame_id = goal_frame_id
        goal_pose.pose.position.x = translation[0]
        goal_pose.pose.position.y = translation[1]
        goal_pose.pose.position.z = translation[2]
        goal_pose.pose.orientation.x = rotation[0]
        goal_pose.pose.orientation.y = rotation[1]
        goal_pose.pose.orientation.z = rotation[2]
        goal_pose.pose.orientation.w = rotation[3]

        response = validate_move_to_pose(goal_pose, center_translation)
        translation = response.goal_result.pose.position
        orientation = response.goal_result.pose.orientation
        rospy.loginfo("validated pose: translation (%f, %f, %f)" %
                      (translation.x, translation.y, translation.z))
        rospy.loginfo("validated pose: orientation (%f, %f, %f, %f)" %
                      (orientation.x, orientation.y, orientation.z, orientation.w))

        return response
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)

        result = roaxdt_msgs.srv.RobotValidateMoveToPoseMsgResponse()
        result.success = False
        return result


def main():
    rospy.init_node('robot_benchmark')
    while rospy.get_time() == 0.0:
        pass

    num_iterations = 50
    min_angle = 0.0
    max_angle = 2.0 * math.pi
    experiment_id = str(uuid.uuid4())
    metadata_file_name = 'metadata.json'
    experiment_metadata = {}
    experiment_metadata['experiment_id'] = experiment_id
    experiment_metadata['num_iterations'] = num_iterations
    experiment_metadata['min_angle'] = min_angle
    experiment_metadata['max_angle'] = max_angle
    experiment_metadata['camera_matrix'] = [14133.33203125, 0.0, 1440.0,
                                            0.0, 14133.33203125, 1440.0,
                                            0.0, 0.0, 1.0]

    experiments_path = rospy.get_param("/roaxdt/experiments/path")
    detector_raw_directory_name = rospy.get_param("/roaxdt/experiments/detector_raw_path")
    detector_corrected_directory_name = rospy.get_param("/roaxdt/experiments/detector_corrected_path")
    detector_directory_name = rospy.get_param("/roaxdt/experiments/detector_path")

    experiment_path = os.path.join(experiments_path, experiment_id)
    detector_raw_path = os.path.join(experiment_path, detector_raw_directory_name)
    detector_corrected_path = os.path.join(experiment_path, detector_corrected_directory_name)
    detector_path = os.path.join(experiment_path, detector_directory_name)
    os.makedirs(experiment_path)
    os.makedirs(detector_path)
    os.makedirs(detector_raw_path)
    os.makedirs(detector_corrected_path)
    metadata_path = os.path.join(experiment_path, metadata_file_name)

    sample_holder_base = 'sample-holder-base-3'
    sample_holder_head = 'sample-holder-head-3'
    experiment_metadata['sample_holder_base_name'] = sample_holder_base
    experiment_metadata['sample_holder_head_name'] = sample_holder_head
    robot_goal_translation = np.array([0.35, 0.0, 0.598])
    robot_goal_rotation_quat = np.array([0.0, 1.0, 0.0, 0.0])
    goal_frame_id = 'panda_link0'

    load_sample_holder_client = actionlib.SimpleActionClient(
        '/load_sample_holder', roaxdt_msgs.msg.LoadSampleHolderAction)

    rospy.loginfo("Waiting for /load_sample_holder action server to appear")
    load_sample_holder_client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = roaxdt_msgs.msg.LoadSampleHolderGoal()
    goal.sample_holder_base_name = sample_holder_base
    goal.sample_holder_head_name = sample_holder_head

    rospy.loginfo("Sending request to /load_sample_holder action server with base %s and head %s" %
                  (sample_holder_base, sample_holder_head))
    load_sample_holder_client.send_goal_and_wait(goal)

    rospy.loginfo("Received result from /load sample holder action server")
    loadSampleHolderResult = load_sample_holder_client.get_result()

    # Generate base pose
    validate_pose_result = validatePose(robot_goal_translation, robot_goal_rotation_quat,
                                        loadSampleHolderResult.sample_center, goal_frame_id)
    pose_goal_base = validate_pose_result.goal_result

    # sample points on circle
    circle_radius = 0.18
    circle_center = robot_goal_translation
    angles = np.linspace(start=min_angle, stop=max_angle, num=num_iterations, endpoint=False)

    experiment_poses = []
    index_counter = 0
    for angle in angles:
        # move to circle pose first
        point_on_circle_x = circle_radius * math.cos(angle)
        point_on_circle_y = circle_radius * math.sin(angle)
        point_on_circle = np.array([point_on_circle_x, point_on_circle_y, 0.0])
        rospy.loginfo('point on circle: (%f, %f)' % (point_on_circle[0], point_on_circle[1]))
        robot_goal_translation_circle = circle_center + point_on_circle
        validate_pose_result = validatePose(robot_goal_translation_circle, robot_goal_rotation_quat,
                                            loadSampleHolderResult.sample_center, goal_frame_id)

        if validate_pose_result.success:
            experiment_poses.append(validate_pose_result.goal_result)

            # then move to pose on center ray and calibrate position/rotation of helix
            experiment_poses.append(pose_goal_base)
        else:
            rospy.logwarn("pose %i failed" % index_counter)

        index_counter += 1

    sample_acquisition_client = actionlib.SimpleActionClient(
        '/sample_acquisition', roaxdt_msgs.msg.SampleAcquisitionAction)

    rospy.loginfo("Waiting for /sample_acquisition action server to appear")
    sample_acquisition_client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = roaxdt_msgs.msg.SampleAcquisitionGoal()
    goal.experiment_path = experiment_path
    goal.experiment_id = experiment_id
    goal.experiment_poses = experiment_poses
    goal.spheres = loadSampleHolderResult.spheres

    rospy.loginfo("Sending request to /sample_acquisition action server")
    sample_acquisition_client.send_goal_and_wait(goal)

    rospy.loginfo("Received result from /sample_acquisition action server")
    sampleAcquisitionResult = sample_acquisition_client.get_result()

    experiment_metadata['poses_planned'] = []
    for pose in experiment_poses:
        obj = {
            'frame_id': pose.header.frame_id,
            'orientation': {
                'w': pose.pose.orientation.w,
                'x': pose.pose.orientation.x,
                'y': pose.pose.orientation.y,
                'z': pose.pose.orientation.z
            },
            'position': {
                'x': pose.pose.position.x,
                'y': pose.pose.position.y,
                'z': pose.pose.position.z
            }
        }
        experiment_metadata['poses_planned'].append(obj)

    experiment_metadata['sample_holder_poses'] = []
    for pose in sampleAcquisitionResult.sample_holder_poses:
        obj = {
            'orientation': {
                'w': pose.orientation.w,
                'x': pose.orientation.x,
                'y': pose.orientation.y,
                'z': pose.orientation.z
            },
            'position': {
                'x': pose.position.x,
                'y': pose.position.y,
                'z': pose.position.z
            }
        }
        experiment_metadata['sample_holder_poses'].append(obj)

    try:
        with open(metadata_path, "w") as metadata_file:
            json.dump(experiment_metadata, metadata_file, sort_keys=True, indent=4)
    except Exception as exception:
        print(exception)
        print("error writing json file ", metadata_path)

    return


if __name__ == '__main__':
    main()
