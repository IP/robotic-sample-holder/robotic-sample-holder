#! /usr/bin/env python3

import rospy

from roaxdt_core.calibration import Calibration


def main():
    rospy.init_node('calibration')
    while rospy.get_time() == 0.0:
        pass

    ir = Calibration()

    rospy.spin()


if __name__ == '__main__':
    main()
