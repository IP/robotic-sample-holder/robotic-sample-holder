#! /usr/bin/env python3

import rospy
import roslaunch
import multiprocessing


def main():
    rospy.init_node('calibration_launcher')
    while rospy.get_time() == 0.0:
        pass

    base_name = "calibration_"
    num_cores = int(multiprocessing.cpu_count())
    nodes = []
    for i in range(num_cores):
        node_name = base_name + str(i)
        nodes.append(roslaunch.core.Node("roaxdt_core", "calibration.py", name=node_name, args="_id:=" + str(i)))

    launch = roslaunch.scriptapi.ROSLaunch()
    launch.start()

    scripts = []
    for i in range(num_cores):
        scripts.append(launch.launch(nodes[i]))

    rospy.spin()


if __name__ == '__main__':
    main()
