#! /usr/bin/env python3

import rospy
import roaxdt_msgs.msg
import actionlib
import numpy as np


def main():
    rospy.init_node('calibration_benchmark')
    while rospy.get_time() == 0.0:
        pass

    client = actionlib.SimpleActionClient('/experiment', roaxdt_msgs.msg.ExperimentAction)

    rospy.loginfo("Waiting for /experiment action server to appear")
    client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = roaxdt_msgs.msg.ExperimentGoal()
    goal.experiment_id = "9526d085-18c6-4f62-a51e-5ba0fdde9c62"
    goal.run_calibration = True
    goal.num_spheres_random_sampling = 50

    helix_sampling_rates = [500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 15000]
    rospy.loginfo("Starting helix sampling rate benchmark with %i runs", len(helix_sampling_rates))
    rospy.loginfo("helix sampling rate between %i and %i ", helix_sampling_rates[0], helix_sampling_rates[-1])
    for i in helix_sampling_rates:
        goal.helix_sampling_rate = i

        rospy.loginfo("Sending request to /experiment action server with sampling rate %i" % i)
        client.send_goal_and_wait(goal)

        _ = client.get_result()
        rospy.loginfo("Received result from /experiment action server for sphere sampling number %i", i)

    return

if __name__ == '__main__':
    main()