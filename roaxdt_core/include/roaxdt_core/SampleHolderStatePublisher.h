#pragma once

#include <roaxdt_msgs/SetSampleHolderGeometry.h>
#include <roaxdt_utils/roaxdt.h>
#include <sensor_msgs/CameraInfo.h>
#include <sstream>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>

namespace roaxdt {

namespace nodes {

class SampleHolderStatePublisher {
public:
  SampleHolderStatePublisher() = delete;
  ~SampleHolderStatePublisher() = default;

  SampleHolderStatePublisher(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::SAMPLE_HOLDER_STATE_PUBLISHER};

  // configuration parameters
  std::string sample_holder_name, sample_name, sample_holder_grip_frame, sample_holder_center_frame,
      sample_holder_center_vtk_frame, sample_holder_sample_vtk_frame, center_suffix, vtk_suffix, grip_suffix, sample_suffix,
      sample_holder_parent_frame, sample_center_frame, sample_holder_reference_link_vtk, attached_object_link;
  double gripper_height;
  Eigen::Isometry3d world_to_vtk_eigen;

  /**
   * sample_holder_base/base: 0,0,0 in local coord. system of holder base part
   * sample_holder_base/center: outer-most point that touches the finger of the holder base in holder coord. system
   * sample_holder_head/base: 0,0,0 in local coord. system of holder head part
   * sample_holder_head/center: center of rotation for x-ray CT in local coord. system of holder head part
   * sample/base: 0,0,0 in local coord. system of the sample that is currently placed in the sample holder head
   * sample/center: center of sample that is currently placed in the sample holder head
   *                by definition sample_center should coincide with sample_head_center
   *                as the sample will then appear centered in the recontructed volume
   */

  tf::Transform transform_sample_holder_grip;
  tf::Transform transform_sample_holder_center;
  tf::Transform transform_sample_holder_sample;
  tf::Transform transform_sample_holder_center_vtk;
  tf::Transform transform_sample_holder_sample_vtk;
  Eigen::Vector3d leftFingerPosition, rightFingerPosition;

  tf::TransformBroadcaster tf_broadcaster;
  tf::TransformListener tf_listener;
  ros::ServiceServer set_sample_holder_geometry_server;

  void init();
  void publishState();
  bool setSampleHolderGeometryCallback(roaxdt_msgs::SetSampleHolderGeometry::Request &req,
                                       roaxdt_msgs::SetSampleHolderGeometry::Response &res);
};

} // namespace nodes
} // namespace roaxdt
