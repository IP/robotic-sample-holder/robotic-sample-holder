#ifndef GRIPPER_H
#define GRIPPER_H
#pragma once

#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <franka_gripper/GraspAction.h>
#include <franka_gripper/MoveAction.h>
#include <iostream>
#include <memory>
#include <roaxdt_msgs/GraspAction.h>
#include <roaxdt_msgs/ReleaseAction.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/ros.h>

namespace roaxdt {

namespace nodes {

class Gripper {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::GraspAction> GraspServer;
  typedef actionlib::SimpleActionServer<roaxdt_msgs::ReleaseAction> ReleaseServer;

public:
  Gripper() = delete;
  ~Gripper() = default;

  Gripper(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::GRIPPER};
  float grasp_timeout, move_timeout;

  double grasp_force, grasp_speed, move_max_width, move_speed;
  std::string gripper_topic, grasp_topic, grasp_topic_full, move_topic, move_topic_full;
  std::vector<double> grasp_epsilon;

  // ROS communication related - variables
  typedef actionlib::SimpleActionClient<franka_gripper::GraspAction> GraspClient;
  typedef actionlib::SimpleActionClient<franka_gripper::MoveAction> MoveClient;
  std::unique_ptr<GraspClient> grasp_client;
  std::unique_ptr<MoveClient> move_client;
  std::unique_ptr<GraspServer> grasp_server;
  std::unique_ptr<ReleaseServer> release_server;

  void init();

  // ROS communication related - methods
  void graspCallback(const roaxdt_msgs::GraspGoalConstPtr &goal);
  void releaseCallback(const roaxdt_msgs::ReleaseGoalConstPtr &goal);
};

} // namespace nodes
} // namespace roaxdt

#endif // GRIPPER_H