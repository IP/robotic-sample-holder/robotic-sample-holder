
#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <cv_bridge/cv_bridge.h>
#include <iostream>
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <roaxdt_msgs/DetectorRetrieveImage.h>
#include <roaxdt_msgs/InitializeDetectorAction.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <utility>

namespace fs = boost::filesystem;

namespace roaxdt {

namespace nodes {

class DetectorManager {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::InitializeDetectorAction> InitializeDetectorServer;

public:
  DetectorManager() = delete;
  ~DetectorManager() = default;

  DetectorManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::DETECTOR_MANAGER};

  // config values loaded during startup
  std::string experiments_base_path, detector_dark_file, detector_empty_file, default_image_file_type;
  std::vector<int> detector_resolution;
  int flat_field_correction_sample_size;

  // ROS communication related - variables
  ros::ServiceClient detectorRetrieveImageClient;
  std::unique_ptr<InitializeDetectorServer> initializeDetectorServer;

  void init();
  void initializeDetectorCallback(const roaxdt_msgs::InitializeDetectorGoalConstPtr &goal);
};

} // namespace nodes
} // namespace roaxdt