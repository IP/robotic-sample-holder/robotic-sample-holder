#ifndef PICK_AND_PLACE_H
#define PICK_AND_PLACE_H
#pragma once

#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <controller_manager_msgs/SwitchController.h>
#include <iostream>
#include <roaxdt_msgs/GraspAction.h>
#include <roaxdt_msgs/PickAndPlaceAction.h>
#include <roaxdt_msgs/ReleaseAction.h>
#include <roaxdt_msgs/RobotMoveToPointCartesianSpace.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/ros.h>

namespace roaxdt {

namespace nodes {

class PickAndPlace {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::PickAndPlaceAction> PickAndPlaceServer;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::GraspAction> GraspClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::ReleaseAction> ReleaseClient;

public:
  PickAndPlace() = delete;
  ~PickAndPlace() = default;

  PickAndPlace(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::PICK_AND_PLACE};

  std::string controller_manager_switch_topic;

  // ROS communication related - variables
  ros::ServiceClient switch_controller_client;
  ros::ServiceClient robot_move_client;
  std::unique_ptr<PickAndPlaceServer> pick_and_place_server;
  std::unique_ptr<GraspClient> grasp_client;
  std::unique_ptr<ReleaseClient> release_client;

  void init();

  // ROS communication related - methods
  void pickAndPlaceCallback(const roaxdt_msgs::PickAndPlaceGoalConstPtr &goal);
};

} // namespace nodes
} // namespace roaxdt

#endif // PICK_AND_PLACE_H