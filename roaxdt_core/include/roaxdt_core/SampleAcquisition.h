#ifndef SAMPLE_ACQUISITION_H
#define SAMPLE_ACQUISITION_H
#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <boost/lambda/bind.hpp>
#include <cv_bridge/cv_bridge.h>
#include <franka_msgs/FrankaState.h>
#include <geometry_msgs/Pose2D.h>
#include <iostream>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <roaxdt_msgs/CorrectFlatField.h>
#include <roaxdt_msgs/DetectorRetrieveImage.h>
#include <roaxdt_msgs/ForwardProjection.h>
#include <roaxdt_msgs/RobotMoveToPointConfigurationSpace.h>
#include <roaxdt_msgs/RobotMoveToPointTrajectory.h>
#include <roaxdt_msgs/SampleAcquisitionAction.h>
#include <roaxdt_msgs/SphereListDisplayMsg.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <thread>
#include <utility>

namespace roaxdt {

namespace nodes {

class SampleAcquisition {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::SampleAcquisitionAction> SampleAcquisitionServer;

public:
  SampleAcquisition() = delete;
  ~SampleAcquisition() = default;

  SampleAcquisition(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::SAMPLE_ACQUISITION};

  // config values loaded during startup
  bool use_validate_move_pose = false;
  std::string experiments_base_path, detector_raw_path, detector_path, package_path, sample_name, image_plane_frame_suffix,
      camera_link_suffix, camera_imageplane_frame, detector_camera_id, camera_frame, sample_holder_name, center_suffix,
      grip_suffix, vtk_suffix, camera_info_suffix, camera_info_topic, sample_holder_center_frame, sample_holder_center_vtk_frame,
      sample_holder_sample_vtk_frame, default_image_file_type, sample_center_frame, sample_suffix;
  bool simulation;
  std::vector<int> detector_resolution;
  fs::path detector_dark_path, detector_empty_path;
  Eigen::Matrix<uint16_t, Eigen::Dynamic, Eigen::Dynamic> img_dark, img_empty;
  double sample_acquisition_wait_time;

  Eigen::Matrix4f transform_image_plane_to_base;
  Eigen::Vector4f transform_goal;
  // TODO write Acquisition data class for storing related data

  // ROS communication related - variables
  tf::TransformBroadcaster tf_broadcaster;
  tf::TransformListener tf_listener;
  ros::ServiceClient moveRobotToPointTrajectoryClient;
  ros::ServiceClient moveRobotToPointConfigurationSpaceClient;
  ros::ServiceClient validateRobotMoveClient;
  ros::ServiceClient validateRobotMovePoseClient;
  ros::ServiceClient detectorRetrieveImageClient;
  ros::ServiceClient forwardProjectionClient;
  ros::ServiceClient sphereListDisplayClient;
  ros::ServiceClient correctFlatFieldClient;
  std::unique_ptr<SampleAcquisitionServer> sampleAcquisitionServer;

  void init();
  void sampleAcquisitionCallback(const roaxdt_msgs::SampleAcquisitionGoalConstPtr &goal);
  void visualizeSpheres(std::vector<geometry_msgs::Point> spheres);
  void robotStateCallback(const franka_msgs::FrankaStatePtr &robot_state);
};

} // namespace nodes
} // namespace roaxdt

#endif // SAMPLE_ACQUISITION_H