#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <cmath>
#include <deque>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <iostream>
#include <limits>
#include <map>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <omp.h>
#include <queue>
#include <random>
#include <roaxdt_msgs/CalibrationAction.h>
#include <roaxdt_msgs/ComputePoseWeight.h>
#include <roaxdt_msgs/ComputedTomographyAction.h>
#include <roaxdt_msgs/GenerateWaypointTrajectory.h>
#include <roaxdt_msgs/GetTrajectoryAction.h>
#include <roaxdt_msgs/LoadSampleHolderAction.h>
#include <roaxdt_msgs/OnlineExperimentAction.h>
#include <roaxdt_msgs/PlotSphericalMap.h>
#include <roaxdt_msgs/PrepareExperimentAction.h>
#include <roaxdt_msgs/RegisterImagesAction.h>
#include <roaxdt_msgs/SampleAcquisitionAction.h>
#include <roaxdt_msgs/SampleSphere.h>
#include <roaxdt_msgs/SampleSphereDisk.h>
#include <roaxdt_msgs/SegmentVolume.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <thread>
#include <utility>

namespace roaxdt {

namespace nodes {

class OnlineExperiment {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::OnlineExperimentAction> OnlineExperimentServer;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::PrepareExperimentAction> PrepareExperimentClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::SampleAcquisitionAction> SampleAcquisitionClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::RegisterImagesAction> RegisterImagesClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::CalibrationAction> CalibrationClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::ComputedTomographyAction> ComputedTomographyClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::LoadSampleHolderAction> LoadSampleHolderClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::GetTrajectoryAction> GetTrajectoryClient;

public:
  OnlineExperiment() = delete;
  ~OnlineExperiment() = default;

  OnlineExperiment(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::ONLINE_EXPERIMENT};

  // config values loaded during startup
  std::string experiments_base_path, segmented_path, detector_path, calibrated_path, reconstructed_path;
  float costThresholdPerSphere;
  OnlineExperimentData experiment_data;

  // ROS communication related - variables
  std::unique_ptr<OnlineExperimentServer> onlineExperimentServer;
  std::unique_ptr<PrepareExperimentClient> prepareExperimentClient;
  std::unique_ptr<SampleAcquisitionClient> sampleAcquisitionClient;
  std::unique_ptr<RegisterImagesClient> registerImagesClient;
  std::unique_ptr<CalibrationClient> calibrationClient;
  std::unique_ptr<ComputedTomographyClient> computedTomographyClient;
  std::unique_ptr<LoadSampleHolderClient> loadSampleHolderClient;
  std::unique_ptr<GetTrajectoryClient> getTrajectoryClient;
  int calibrationClientID, totalImageCounter;

  ros::ServiceClient segmentVolumeClient;
  ros::ServiceClient computePoseWeightClient;
  ros::ServiceClient generateWaypointTrajectoryClient;
  ros::ServiceClient sphereSamplerClient;
  ros::ServiceClient sphereSamplerDiskClient;
  ros::ServiceClient plotSphericalMapClient;

  std::mt19937 rg_engine;

  void init();
  void onlineExperimentCallback(const roaxdt_msgs::OnlineExperimentGoalConstPtr &goal);
  bool acquireAndCalibrateImage(SphericalPose &sphericalPose, std::map<int, SphericalPose> &image_id_to_spherical_pose,
                                roaxdt_msgs::OnlineExperimentFeedback &action_feedback);
  bool recalculateScores(const std::vector<float> &scores, std::map<int, std::vector<int>> &sphere_mapping_low_to_high,
                         std::map<int, SphericalPose> &image_id_to_spherical_pose, SphericalData &spherical_data_low,
                         SphericalData &spherical_data_high);

  roaxdt_msgs::LoadSampleHolderResultConstPtr loadSampleHolder(const std::string &sample_holder);
};

} // namespace nodes
} // namespace roaxdt