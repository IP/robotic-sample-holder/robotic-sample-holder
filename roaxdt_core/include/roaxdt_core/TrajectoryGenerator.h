#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <algorithm>
#include <boost/bind.hpp>
#include <geometry_msgs/Point.h>
#include <iostream>
#include <moveit_msgs/RobotTrajectory.h>
#include <roaxdt_msgs/GenerateCircularTrajectory.h>
#include <roaxdt_msgs/GenerateSphericalAXDTTrajectory.h>
#include <roaxdt_msgs/GenerateSphericalTrajectory.h>
#include <roaxdt_msgs/GenerateWaypointTrajectory.h>
#include <roaxdt_msgs/RobotValidateMoveToPoseMsg.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <utility>

namespace fs = boost::filesystem;

namespace roaxdt {

namespace nodes {

class TrajectoryGenerator {

public:
  TrajectoryGenerator() = delete;
  ~TrajectoryGenerator() = default;

  TrajectoryGenerator(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::TRAJECTORY_GENERATOR};

  // config values loaded during startup
  std::string camera_link_suffix, detector_camera_id, camera_frame, trajectories_path, trajectory_file_type, environment_hash;
  geometry_msgs::Point goalPoseTranslation;
  Eigen::Isometry3d xray_to_sample_base_eigen, xray_to_pose_base_eigen;

  // ROS communication related - variables
  tf::TransformListener tf_listener;
  ros::ServiceServer generateCircularTrajectoryService;
  ros::ServiceServer generateSphericalTrajectoryService;
  ros::ServiceServer generateSphericalAXDTTrajectoryService;
  ros::ServiceServer generateWaypointTrajectoryService;
  ros::ServiceClient validateRobotMovePoseClient;

  void init();
  geometry_msgs::Point calculateGoalPoseTranslation(const std::string &frame_id);
  bool preparePose(std::string &frame_id, geometry_msgs::Quaternion &orientation, geometry_msgs::Point &translation,
                   geometry_msgs::PointStamped &sample_center, geometry_msgs::PoseStamped &result_pose,
                   moveit_msgs::RobotState &result_state, moveit_msgs::RobotTrajectory &trajectory,
                   std::vector<float> &robot_link_states, moveit_msgs::RobotState &start_state);
  bool generateCircularTrajectory(roaxdt_msgs::GenerateCircularTrajectory::Request &req,
                                  roaxdt_msgs::GenerateCircularTrajectory::Response &res);
  bool generateSphericalTrajectory(roaxdt_msgs::GenerateSphericalTrajectory::Request &req,
                                   roaxdt_msgs::GenerateSphericalTrajectory::Response &res);
  bool generateSphericalAXDTTrajectory(roaxdt_msgs::GenerateSphericalAXDTTrajectory::Request &req,
                                       roaxdt_msgs::GenerateSphericalAXDTTrajectory::Response &res);
  bool generateWaypointTrajectory(roaxdt_msgs::GenerateWaypointTrajectory::Request &req,
                                  roaxdt_msgs::GenerateWaypointTrajectory::Response &res);
};

} // namespace nodes
} // namespace roaxdt