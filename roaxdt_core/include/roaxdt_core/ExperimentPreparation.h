#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <geometry_msgs/Point.h>
#include <iostream>
#include <roaxdt_msgs/PrepareExperimentAction.h>
#include <roaxdt_msgs/ReadMesh.h>
#include <roaxdt_msgs/RobotValidateMoveToPoseMsg.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <utility>

namespace fs = boost::filesystem;

namespace roaxdt {

namespace nodes {

class ExperimentPreparation {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::PrepareExperimentAction> PrepareExperimentServer;

public:
  ExperimentPreparation() = delete;
  ~ExperimentPreparation() = default;

  ExperimentPreparation(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::EXPERIMENT_PREPARATION};

  // config values loaded during startup
  bool simulation, experiment;
  std::string experiments_base_path, image_plane_frame_suffix, camera_link_suffix, camera_info_suffix, detector_camera_id,
      camera_frame, camera_imageplane_frame, camera_info_topic, detector_path, detector_raw_path, detector_corrected_path,
      reconstructed_path, base_suffix;
  geometry_msgs::Point goalPoseTranslation;
  Eigen::Isometry3d xray_to_sample_base_eigen, xray_to_pose_base_eigen;

  // ROS communication related - variables
  tf::TransformListener tf_listener;
  std::unique_ptr<PrepareExperimentServer> prepareExperimentServer;
  ros::ServiceClient validateRobotMovePoseClient;

  void init();
  void prepareExperimentCallback(const roaxdt_msgs::PrepareExperimentGoalConstPtr &goal);
  std::string prepareFilesystem(const std::string &experiment_id);
};

} // namespace nodes
} // namespace roaxdt