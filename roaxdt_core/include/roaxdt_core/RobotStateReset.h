#ifndef ROBOT_STATE_RESET_H
#define ROBOT_STATE_RESET_H
#pragma once

#include <controller_manager_msgs/SwitchController.h>
#include <iostream>
#include <roaxdt_utils/roaxdt.h>
#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>

namespace roaxdt {

namespace nodes {

class RobotStateReset {

public:
  RobotStateReset() = delete;
  ~RobotStateReset() = default;

  RobotStateReset(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::ROBOT_STATE_RESET};

  std::string joint_position_command_topic, controller_manager_switch_topic;

  // ROS communication related - variables
  ros::ServiceClient switch_controller_client;
  ros::Publisher joint_position_publisher;

  void init();
};

} // namespace nodes
} // namespace roaxdt

#endif // ROBOT_STATE_RESET_H