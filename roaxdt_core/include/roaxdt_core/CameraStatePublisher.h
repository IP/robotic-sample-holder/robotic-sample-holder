#ifndef CAMERA_STATE_PUBLISHER_H
#define CAMERA_STATE_PUBLISHER_H
#pragma once

#include <roaxdt_utils/roaxdt.h>
#include <sensor_msgs/CameraInfo.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>

namespace roaxdt {

namespace nodes {

class CameraStatePublisher {
public:
  CameraStatePublisher() = delete;
  ~CameraStatePublisher() = default;

  CameraStatePublisher(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::CAMERA_STATE_PUBLISHER};

  tf::TransformBroadcaster tf_broadcaster;
  ros::Publisher camera_info_pub;
  bool camera_corrected = false;

  std::string xray_camera_id, camera_id, child_frame, child_frame_image_plane, camera_info_topic;
  int camera_type;
  // configuration parameters
  std::string image_plane_frame_suffix, camera_info_suffix, camera_link_suffix, detector_camera_name;

  nlohmann::json camera_json;
  tf::Transform transform_camera, transform_image_plane;
  sensor_msgs::CameraInfo camera_info;

  void init();
  void publishState();
};

} // namespace nodes
} // namespace roaxdt

#endif // CAMERA_STATE_PUBLISHER_H