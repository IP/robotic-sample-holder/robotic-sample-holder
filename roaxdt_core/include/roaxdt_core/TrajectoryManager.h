#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <actionlib/server/simple_action_server.h>
#include <algorithm>
#include <boost/bind.hpp>
#include <geometry_msgs/Point.h>
#include <iostream>
#include <moveit_msgs/RobotTrajectory.h>
#include <roaxdt_msgs/GenerateCircularTrajectory.h>
#include <roaxdt_msgs/GenerateSphericalAXDTTrajectory.h>
#include <roaxdt_msgs/GenerateSphericalTrajectory.h>
#include <roaxdt_msgs/GetTrajectoryAction.h>
#include <roaxdt_msgs/GetTrajectoryFromWhitelistAction.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <utility>

namespace fs = boost::filesystem;

namespace roaxdt {

namespace nodes {

class TrajectoryManager {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::GetTrajectoryAction> GetTrajectoryServer;
  typedef actionlib::SimpleActionServer<roaxdt_msgs::GetTrajectoryFromWhitelistAction> GetTrajectoryFromWhitelistServer;

public:
  TrajectoryManager() = delete;
  ~TrajectoryManager() = default;

  TrajectoryManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::TRAJECTORY_MANAGER};

  // config values loaded during startup
  std::string trajectories_path, trajectory_file_type, environment_hash;

  // ROS communication related - variables
  std::unique_ptr<GetTrajectoryServer> getTrajectoryServer;
  std::unique_ptr<GetTrajectoryFromWhitelistServer> getTrajectoryFromWhitelistServer;
  ros::ServiceClient generateCircularTrajectoryClient;
  ros::ServiceClient generateSphericalTrajectoryClient;
  ros::ServiceClient generateSphericalAXDTTrajectoryClient;

  void init();
  void getTrajectoryCallback(const roaxdt_msgs::GetTrajectoryGoalConstPtr &goal);
  void getTrajectoryFromWhitelistCallback(const roaxdt_msgs::GetTrajectoryFromWhitelistGoalConstPtr &goal);
  void getEnvironmentHash();
};

} // namespace nodes
} // namespace roaxdt