#ifndef ROBOT_ENVIRONMENT_MANAGER_H
#define ROBOT_ENVIRONMENT_MANAGER_H
#pragma once

#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <eigen_conversions/eigen_msg.h>
#include <iostream>
#include <memory>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/robot_state/conversions.h>
#include <moveit_msgs/AllowedCollisionEntry.h>
#include <moveit_msgs/RobotState.h>
#include <roaxdt_msgs/AddSampleHolderToEnvironment.h>
#include <roaxdt_msgs/AddSamplesToEnvironment.h>
#include <roaxdt_msgs/RobotStopMsg.h>
#include <roaxdt_msgs/RobotValidateMoveToPoseMsg.h>
#include <roaxdt_msgs/Sample.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/ros.h>
#include <set>
#include <tf/transform_datatypes.h>

namespace roaxdt {

namespace nodes {

class RobotEnvironmentManager {

public:
  RobotEnvironmentManager() = delete;
  ~RobotEnvironmentManager() = default;

  RobotEnvironmentManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::ROBOT_ENVIRONMENT_MANAGER};

  std::vector<moveit_msgs::CollisionObject> collision_objects;
  std::vector<moveit_msgs::AttachedCollisionObject> attached_objects;

  double end_effector_z_offset, attached_object_height;
  std::string attached_object_link, collision_env, environment_hash, sample_holder_grip_frame, sample_holder_center_frame,
      sample_holder_sample_frame, center_suffix, grip_suffix, sample_suffix, vtk_suffix, xray_collision_object_name;
  roaxdt_msgs::SampleHolder current_sample_holder;
  std::vector<roaxdt_msgs::Sample> current_samples;
  std::vector<std::string> xray_allowed_collision_links;

  moveit_msgs::PlanningScene planning_scene_msg;
  std::unique_ptr<planning_scene::PlanningScene> planning_scene_ptr;
  robot_model_loader::RobotModelLoader robot_model_loader;
  robot_model::RobotModelPtr kinematic_model;
  geometry_msgs::Pose pose_grip_to_hand, pose_center_to_hand, pose_sample_to_hand;
  // Eigen::Isometry3d pose_sample_to_hand_eigen;

  ros::Publisher planning_scene_diff_publisher;

  // ROS communication related - variables
  tf::TransformListener tf_listener;
  ros::ServiceServer add_sample_holder_service;
  ros::ServiceServer add_samples_service;

  void init();

  // ROS communication related - methods
  bool addSampleHolder(roaxdt_msgs::AddSampleHolderToEnvironment::Request &request,
                       roaxdt_msgs::AddSampleHolderToEnvironment::Response &response);
  bool addSamples(roaxdt_msgs::AddSamplesToEnvironment::Request &request,
                  roaxdt_msgs::AddSamplesToEnvironment::Response &response);
  void addAllowedCollision(const std::string &entry1, const std::string &entry2, bool allowed);
  void removeAllowedCollision(const std::string &entry);
  void setUpXrayConeCollisionObject();
  void retrieveTransforms(std::string &sample_holder_name);
  void clearEnvironment();
  void buildEnvironment();
};

} // namespace nodes
} // namespace roaxdt

#endif // ROBOT_ENVIRONMENT_MANAGER_H