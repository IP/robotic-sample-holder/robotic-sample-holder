#ifndef ROBOT_STATE_MANAGER_H
#define ROBOT_STATE_MANAGER_H
#pragma once

#include <actionlib/client/simple_action_client.h>
#include <franka_msgs/ErrorRecoveryAction.h>
#include <franka_msgs/FrankaState.h>
#include <iostream>
#include <roaxdt_utils/roaxdt.h>
#include <ros/ros.h>

namespace roaxdt {

namespace nodes {

class RobotStateManager {

  typedef actionlib::SimpleActionClient<franka_msgs::ErrorRecoveryAction> ErrorRecoveryClient;

public:
  RobotStateManager() = delete;
  ~RobotStateManager() = default;

  RobotStateManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::ROBOT_STATE_MANAGER};

  std::string joint_position_command_topic, controller_manager_switch_topic;

  // ROS communication related - variables
  ros::ServiceClient switch_controller_client;
  ros::Publisher joint_position_publisher;
  ros::Subscriber robot_state_sub;
  std::unique_ptr<ErrorRecoveryClient> error_recovery_client;

  void robotStateCallback(const franka_msgs::FrankaStatePtr &robot_state);
  void executeErrorRecovery();

  void init();
};

} // namespace nodes
} // namespace roaxdt

#endif // ROBOT_STATE_MANAGER_H