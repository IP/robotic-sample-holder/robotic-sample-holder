#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <cv_bridge/cv_bridge.h>
#include <iostream>
#include <memory>
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <roaxdt_msgs/DetectorRetrieveImage.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <utility>

namespace fs = boost::filesystem;

namespace roaxdt {

namespace nodes {

typedef unsigned short int detector_image_encoding;

using namespace boost::asio;
using ip::tcp;

class DetectorClient {

public:
  DetectorClient() = delete;
  ~DetectorClient() = default;

  DetectorClient(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::DETECTOR_CLIENT};

  std::string detector_host, detector_request_str;
  int detector_port;
  size_t image_size;

  // config values loaded during startup
  std::string experiments_base_path, detector_dark_file, detector_empty_file;
  std::vector<int> detector_resolution;

  // ROS communication related - variables
  ros::ServiceServer detectorRetrieveImageService;

  void init();
  bool retrieveImageFromDetector(roaxdt_msgs::DetectorRetrieveImage::Request &req,
                                 roaxdt_msgs::DetectorRetrieveImage::Response &res);
};

} // namespace nodes
} // namespace roaxdt