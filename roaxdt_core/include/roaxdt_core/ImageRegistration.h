#pragma once

#include <ImageMagick-6/Magick++.h>
#include <actionlib/server/simple_action_server.h>
#include <algorithm>
#include <geometry_msgs/Pose2D.h>
#include <iterator>
#include <numeric>
#include <omp.h>
#include <opencv2/highgui/highgui.hpp>
#include <roaxdt_msgs/RegisterImagesAction.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/ros.h>
#include <thread>

namespace roaxdt {

namespace nodes {

struct ImageCropping {
  int x, y, width, height;
};

struct ImageLeveling {
  double black_point, white_point, gamma;
  int max_pixel;
};

struct ImageFiltering {
  int median_kernel_size;
};

struct CircleDetection {
  double dp, min_dist, param_1, param_2;
  int min_radius, max_radius;
};

struct less_than_key {
  inline bool operator()(const cv::Vec3f &circle1, const cv::Vec3f &circle2) { return (circle1[1] < circle2[1]); }
};

class ImageRegistration {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::RegisterImagesAction> RegisterImagesServer;

public:
  ImageRegistration() = delete;
  ~ImageRegistration() = default;

  ImageRegistration(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::IMAGE_REGISTRATION};

  std::string experiments_base_path, detector_path, processed_path, segmented_path, default_image_file_type;
  ImageCropping image_cropping;
  ImageLeveling image_leveling;
  ImageFiltering image_filtering;
  CircleDetection circle_detection;

  // ROS communication related - variables
  //   ros::ServiceClient switch_controller_client;
  //   ros::Publisher joint_position_publisher;
  std::unique_ptr<RegisterImagesServer> registerImagesServer;

  void init();
  void registerImagesCallback(const roaxdt_msgs::RegisterImagesGoalConstPtr &goal);
  void levelCropImage(Magick::Image &image, const std::string &image_path, cv::Mat &cv_image);
  std::vector<cv::Vec3f> houghCircleDetection(cv::Mat &image);
};

} // namespace nodes
} // namespace roaxdt