#ifndef ROBOT_CONTROL_H
#define ROBOT_CONTROL_H
#pragma once

#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <eigen_conversions/eigen_msg.h>
#include <franka_gripper/GraspAction.h>
#include <iostream>
#include <memory>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/robot_state/conversions.h>
#include <moveit_msgs/GetPlanningScene.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <roaxdt_controllers/CalibrateJointsMsg.h>
#include <roaxdt_msgs/RobotMoveToPointCartesianSpace.h>
#include <roaxdt_msgs/RobotMoveToPointConfigurationSpace.h>
#include <roaxdt_msgs/RobotMoveToPointTrajectory.h>
#include <roaxdt_msgs/RobotStopMsg.h>
#include <roaxdt_msgs/RobotValidateMoveToPoseMsg.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/ros.h>
#include <tf/transform_datatypes.h>

namespace roaxdt {

namespace nodes {

class RobotControl {

public:
  RobotControl() = delete;
  ~RobotControl() = default;

  RobotControl(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::ROBOT_CONTROL};

  std::vector<moveit_msgs::AttachedCollisionObject> attached_objects;
  Eigen::Matrix4f transform_robot_to_detector_image_plane;
  Eigen::Vector4d pos_object_center_expected;

  bool simulation;
  double end_effector_z_offset, panda_arm_velocity_scaling, panda_arm_acceleration_scaling, attached_object_height,
      goal_orientation_tolerance, goal_position_tolerance, goal_joint_tolerance;
  std::string pose_reference_frame;

  bool STOP_SIGNAL = false;

  robot_model::RobotModelPtr kinematic_model;

  robot_model_loader::RobotModelLoader robot_model_loader;
  std::unique_ptr<moveit::planning_interface::MoveGroupInterface> move_group_arm;
  std::unique_ptr<moveit::planning_interface::MoveGroupInterface> move_group_hand;

  // ROS communication related - variables
  tf::TransformListener tf_listener;
  ros::ServiceServer stop_service;
  ros::ServiceServer move_trajectory_service;
  ros::ServiceServer move_cartesian_space_service;
  ros::ServiceServer move_configuration_space_service;
  ros::ServiceServer validate_move_service;
  ros::ServiceServer validate_move_pose_service;
  ros::ServiceClient get_planning_scene_client;

  void init();

  // ROS communication related - methods
  bool stop(roaxdt_msgs::RobotStopMsg::Request &request, roaxdt_msgs::RobotStopMsg::Response &response);
  bool moveTrajectory(roaxdt_msgs::RobotMoveToPointTrajectory::Request &request,
                      roaxdt_msgs::RobotMoveToPointTrajectory::Response &response);
  bool moveCartesianSpace(roaxdt_msgs::RobotMoveToPointCartesianSpace::Request &request,
                          roaxdt_msgs::RobotMoveToPointCartesianSpace::Response &response);
  bool moveConfigurationSpace(roaxdt_msgs::RobotMoveToPointConfigurationSpace::Request &request,
                              roaxdt_msgs::RobotMoveToPointConfigurationSpace::Response &response);
  bool validateMovePose(roaxdt_msgs::RobotValidateMoveToPoseMsg::Request &request,
                        roaxdt_msgs::RobotValidateMoveToPoseMsg::Response &response);
  void trajectoryResultCallback(const control_msgs::FollowJointTrajectoryActionResult &joint_trajectory_result);
  void executeErrorRecovery();

public:
  void getPlanningSceneTimerCallback(const ros::TimerEvent &e);
};

} // namespace nodes
} // namespace roaxdt

#endif // ROBOT_CONTROL_H