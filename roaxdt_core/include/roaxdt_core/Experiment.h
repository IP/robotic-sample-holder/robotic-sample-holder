#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <deque>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <iostream>
#include <map>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <omp.h>
#include <roaxdt_msgs/CalibrationAction.h>
#include <roaxdt_msgs/ComputedTomographyAction.h>
#include <roaxdt_msgs/ExperimentAction.h>
#include <roaxdt_msgs/ForwardProjection.h>
#include <roaxdt_msgs/GetTrajectoryAction.h>
#include <roaxdt_msgs/LoadSampleHolderAction.h>
#include <roaxdt_msgs/PrepareExperimentAction.h>
#include <roaxdt_msgs/RegisterImagesAction.h>
#include <roaxdt_msgs/SampleAcquisitionAction.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <thread>
#include <utility>

namespace roaxdt {

namespace nodes {

class Experiment {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::ExperimentAction> ExperimentServer;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::PrepareExperimentAction> PrepareExperimentClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::SampleAcquisitionAction> SampleAcquisitionClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::RegisterImagesAction> RegisterImagesClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::CalibrationAction> CalibrationClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::ComputedTomographyAction> ComputedTomographyClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::LoadSampleHolderAction> LoadSampleHolderClient;
  typedef actionlib::SimpleActionClient<roaxdt_msgs::GetTrajectoryAction> GetTrajectoryClient;

public:
  Experiment() = delete;
  ~Experiment() = default;

  Experiment(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::EXPERIMENT};

  // config values loaded during startup
  std::string experiments_base_path, segmented_path, detector_path, calibrated_path, reconstructed_path;
  uint16_t numIterations;
  float costThresholdPerSphere;
  const uint processorCount;
  ExperimentData experiment_data;

  // ROS communication related - variables
  std::unique_ptr<ExperimentServer> experimentServer;
  std::unique_ptr<PrepareExperimentClient> prepareExperimentClient;
  std::unique_ptr<SampleAcquisitionClient> sampleAcquisitionClient;
  std::unique_ptr<RegisterImagesClient> registerImagesClient;
  std::vector<std::unique_ptr<CalibrationClient>> calibrationClients;
  std::unique_ptr<ComputedTomographyClient> computedTomographyClient;
  std::unique_ptr<LoadSampleHolderClient> loadSampleHolderClient;
  std::unique_ptr<GetTrajectoryClient> getTrajectoryClient;

  nlohmann::json metadata;

  void init();
  void experimentCallback(const roaxdt_msgs::ExperimentGoalConstPtr &goal);

  // boilerplate for providing action feedback to the UI
  void sampleAcquisitionFeedbackCallback(const roaxdt_msgs::SampleAcquisitionFeedbackConstPtr &feedback);
  void sampleAcquisitionDoneCallback(const actionlib::SimpleClientGoalState &state,
                                     const roaxdt_msgs::SampleAcquisitionResultConstPtr &result);
  void sampleAcquisitionActiveCallback();
  void registerImagesFeedbackCallback(const roaxdt_msgs::RegisterImagesFeedbackConstPtr &feedback);
  void registerImagesDoneCallback(const actionlib::SimpleClientGoalState &state,
                                  const roaxdt_msgs::RegisterImagesResultConstPtr &result);
  void registerImagesActiveCallback();
  void computedTomographyFeedbackCallback(const roaxdt_msgs::ComputedTomographyFeedbackConstPtr &feedback);
  void computedTomographyDoneCallback(const actionlib::SimpleClientGoalState &state,
                                      const roaxdt_msgs::ComputedTomographyResultConstPtr &result);
  void computedTomographyActiveCallback();
  roaxdt_msgs::LoadSampleHolderResultConstPtr loadSampleHolder(const std::string &sample_holder);
};

} // namespace nodes
} // namespace roaxdt