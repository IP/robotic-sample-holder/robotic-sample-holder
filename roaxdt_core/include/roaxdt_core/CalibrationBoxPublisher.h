#ifndef CALIBRATION_BOX_PUBLISHER_H
#define CALIBRATION_BOX_PUBLISHER_H
#pragma once

#include <gazebo/gazebo.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/physics/ModelState.hh>
#include <gazebo_msgs/SetModelState.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <roaxdt_utils/roaxdt.h>
#include <sensor_msgs/JointState.h>

namespace roaxdt {

namespace nodes {

class CalibrationBoxPublisher {
public:
  CalibrationBoxPublisher() = delete;
  ~CalibrationBoxPublisher() = default;

  CalibrationBoxPublisher(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::CALIBRATION_BOX_PUBLISHER};

  ros::Subscriber joint_states_sub;
  ros::Publisher gazebo_model_state_pub;
  robot_model::RobotModelPtr kinematic_model;
  robot_state::RobotStatePtr kinematic_state;

  double end_effector_z_offset;

  void init();
  void jointStatesCallback(const sensor_msgs::JointState &joint_states_current);
};

} // namespace nodes
} // namespace roaxdt

#endif // CALIBRATION_BOX_PUBLISHER_H