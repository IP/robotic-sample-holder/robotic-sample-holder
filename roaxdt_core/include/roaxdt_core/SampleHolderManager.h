#pragma once

#include "ros/ros.h"
#include <Eigen/Dense>
#include <actionlib/server/simple_action_server.h>
#include <boost/bind.hpp>
#include <geometry_msgs/Point.h>
#include <iostream>
#include <map>
#include <opencv2/highgui/highgui.hpp>
#include <roaxdt_msgs/AddSampleHolderToEnvironment.h>
#include <roaxdt_msgs/AddSamplesToEnvironment.h>
#include <roaxdt_msgs/ConcatenateSampleAndHolder.h>
#include <roaxdt_msgs/GetSampleHolderParts.h>
#include <roaxdt_msgs/GetSampleParts.h>
#include <roaxdt_msgs/LoadSampleAction.h>
#include <roaxdt_msgs/LoadSampleHolderAction.h>
#include <roaxdt_msgs/PlaceSpheres.h>
#include <roaxdt_msgs/ReadMesh.h>
#include <roaxdt_msgs/Sample.h>
#include <roaxdt_msgs/SampleHolder.h>
#include <roaxdt_msgs/SetCollisionDetectorParts.h>
#include <roaxdt_msgs/SetSampleHolderGeometry.h>
#include <roaxdt_msgs/SetTargetVolume.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/package.h>
#include <sensor_msgs/Image.h>
#include <string>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <utility>

namespace fs = boost::filesystem;

namespace roaxdt {

namespace nodes {

class SampleHolderManager {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::LoadSampleHolderAction> LoadSampleHolderServer;
  typedef actionlib::SimpleActionServer<roaxdt_msgs::LoadSampleAction> LoadSampleServer;

public:
  SampleHolderManager() = delete;
  ~SampleHolderManager() = default;

  SampleHolderManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::SAMPLE_HOLDER_MANAGER};

  // config values loaded during startup
  bool simulation, experiment, forwardProjection;
  std::string sample_holder_name;
  std::string sample_holder_reference_link_elsa, center_suffix, sample_suffix, sample_holder_center_frame,
      sample_holder_sample_frame, sample_holder_vtk_frame, vtk_suffix, sample_holder_grip_frame, sample_holder_reference_link_vtk,
      vtk_volume_file_type;
  fs::path holder_rasterized_volume_path;
  std::vector<std::string> sampleHolderFileTypes;
  std::vector<geometry_msgs::Point> spheres;
  std::vector<double> spacing;
  std::vector<float> volume_dimensions_sample_holder, volume_dimensions_gripper, volume_dimensions_head;
  Eigen::Isometry3d vtk_to_world_eigen;
  geometry_msgs::PointStamped sample_center, head_center;
  geometry_msgs::Point center_sample;
  geometry_msgs::Pose center_sample_holder, gripper_sample_holder;
  float sphere_diameter, helix_height;
  std::map<std::string, std::map<std::string, fs::path>> sampleHolderFiles, sampleFiles;
  double helix_radius, helix_omega, helix_phi;
  // SampleHolder current_sample_holder;
  std::vector<Sample> current_samples;
  std::vector<roaxdt_msgs::Sample> current_samples_msgs;
  SampleHolder current_sample_holder;
  roaxdt_msgs::SampleHolder current_sample_holder_msg;

  // ROS communication related - variables
  tf::TransformListener tf_listener;
  std::unique_ptr<LoadSampleHolderServer> loadSampleHolderServer;
  std::unique_ptr<LoadSampleServer> loadSampleServer;
  ros::ServiceServer sampleHolderPartsServer;
  ros::ServiceServer samplePartsServer;
  ros::ServiceClient readMeshClient;
  ros::ServiceClient setSampleHolderClient;
  ros::ServiceClient setTargetVolumeClient;
  ros::ServiceClient addSampleHolderToEnvironmentClient;
  ros::ServiceClient addSamplesToEnvironmentClient;
  ros::ServiceClient placeSpheresClient;
  ros::ServiceClient concatenateSampleAndHolderClient;
  ros::ServiceClient setCollisionDetectorPartsClient;

  void init();
  void readFiles(fs::directory_iterator directoryIterator, std::map<std::string, std::map<std::string, fs::path>> &files);
  void readSampleHolderFiles();
  void readSampleFiles();
  void loadSampleHolderCallback(const roaxdt_msgs::LoadSampleHolderGoalConstPtr &goal);
  void loadSampleCallback(const roaxdt_msgs::LoadSampleGoalConstPtr &goal);
  bool sampleHolderPartsCallback(roaxdt_msgs::GetSampleHolderParts::Request &req,
                                 roaxdt_msgs::GetSampleHolderParts::Response &res);
  bool samplePartsCallback(roaxdt_msgs::GetSampleParts::Request &req, roaxdt_msgs::GetSampleParts::Response &res);
  bool readMeshVolume(std::string sample_name, fs::path &volumePath, std::vector<double> &spacing, fs::path &rasterizedVolumePath,
                      float absorption);
  bool readMeshMetadata(std::string sample_name, fs::path &metadataPath, geometry_msgs::Pose &gripper_center,
                        geometry_msgs::Pose &volume_center, geometry_msgs::Point &sample_center, float &helix_height,
                        std::vector<float> &volume_dimensions, float &sphere_diameter, std::vector<geometry_msgs::Point> &spheres,
                        double &helix_radius, double &helix_omega, double &helix_phi);
  bool readSampleHolderPart(const std::string &sampleHolderPart, fs::path &metadataPath, fs::path &volumePath,
                            fs::path &rasterizedVolumePath, bool readVolume, std::vector<double> &spacing,
                            geometry_msgs::Pose &gripper_center, geometry_msgs::Pose &volume_center,
                            geometry_msgs::Point &sample_center, std::vector<float> &volume_dimensions, float &sphere_diameter,
                            float &helix_height, std::vector<geometry_msgs::Point> &spheres, double &helix_radius,
                            double &helix_omega, double &helix_phi);
  bool readSamplePart(Sample &sample_new, bool readVolume);
  bool setTargetVolume();
  bool setSampleHolderUp(bool readMeshRequired, const std::string &sample_holder_new);
  bool setSamplesUp(bool readMeshRequired, const std::vector<std::string> &samples_new);
  void getSampleHolderPartPaths(const std::string &sampleHolderBaseName,
                                std::map<std::string, std::map<std::string, fs::path>> &filesMap, fs::path &metadataPath,
                                fs::path &volumePath);
};

} // namespace nodes
} // namespace roaxdt