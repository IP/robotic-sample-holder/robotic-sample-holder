#include <roaxdt_core/SampleAcquisition.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

SampleAcquisition::SampleAcquisition(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void SampleAcquisition::init() {
  try {
    readConfigParameter<std::string>(node_handle, experiments::PATH, experiments_base_path);
    readConfigParameter<std::string>(node_handle, IMAGE_PLANE_FRAME_SUFFIX, image_plane_frame_suffix);
    readConfigParameter<std::string>(node_handle, CAMERA_LINK_SUFFIX, camera_link_suffix);
    readConfigParameter<std::string>(node_handle, experiments::SAMPLE_SUFFIX, sample_suffix);
    readConfigParameter<std::string>(node_handle, DETECTOR_CAMERA_NAME, detector_camera_id);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_RAW_PATH, detector_raw_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_PATH, detector_path);
    readConfigParameter<std::vector<int>>(node_handle, reconstruction::DETECTOR_RESOLUTION, detector_resolution);
    readConfigParameter<std::string>(node_handle, experiments::GRIP_SUFFIX, grip_suffix);
    readConfigParameter<std::string>(node_handle, experiments::CENTER_SUFFIX, center_suffix);
    readConfigParameter<std::string>(node_handle, experiments::VTK_SUFFIX, vtk_suffix);
    readConfigParameter<bool>(node_handle, SIMULATION, simulation);
    readConfigParameter<double>(node_handle, experiments::SAMPLE_ACQUISITION_WAIT_TIME, sample_acquisition_wait_time);
    readConfigParameter<std::string>(node_handle, experiments::DEFAULT_IMAGE_FILE_TYPE, default_image_file_type);
    package_path = ros::package::getPath(ROAXDT_PKG);
    if (package_path.length() == 0) {
      throw std::runtime_error(std::string("Could not find roaxdt package path"));
    }
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  camera_frame = std::string("/" + detector_camera_id + camera_link_suffix);
  camera_imageplane_frame = std::string("/" + detector_camera_id + image_plane_frame_suffix);

  ros::Duration(2.0).sleep(); // wait for -tf- service to appear
  moveRobotToPointTrajectoryClient =
      node_handle.serviceClient<roaxdt_msgs::RobotMoveToPointTrajectory>(service_endpoints::ROBOT_MOVE_TRAJECTORY);
  moveRobotToPointConfigurationSpaceClient =
      node_handle.serviceClient<roaxdt_msgs::RobotMoveToPointConfigurationSpace>(service_endpoints::ROBOT_MOVE_CONFIGURATION);
  detectorRetrieveImageClient =
      node_handle.serviceClient<roaxdt_msgs::DetectorRetrieveImage>(service_endpoints::DETECTOR_RETRIEVE_IMAGE);
  forwardProjectionClient = node_handle.serviceClient<roaxdt_msgs::ForwardProjection>(service_endpoints::FORWARD_PROJECTION);

  // manually start action server to avoid race conditions
  sampleAcquisitionServer.reset(new SampleAcquisitionServer(node_handle, action_endpoints::SAMPLE_ACQUISITION,
                                                            boost::bind(&SampleAcquisition::sampleAcquisitionCallback, this, _1),
                                                            false));
  sampleAcquisitionServer->start();

  sphereListDisplayClient =
      node_handle.serviceClient<roaxdt_msgs::SphereListDisplayMsg>(service_endpoints::SPHERE_LIST_DISPLAY + "camera1");

  if (!simulation) {
    correctFlatFieldClient = node_handle.serviceClient<roaxdt_msgs::CorrectFlatField>(service_endpoints::CORRECT_FLAT_FIELD);
  }
}

void SampleAcquisition::sampleAcquisitionCallback(const roaxdt_msgs::SampleAcquisitionGoalConstPtr &goal) {
  roaxdt_msgs::SampleAcquisitionFeedback action_feedback;
  roaxdt_msgs::SampleAcquisitionResult action_result;

  // Read sample holder metadata
  try {
    readConfigParameter<std::string>(node_handle, experiments::SAMPLE_HOLDER_NAME, sample_holder_name);

    std::stringstream stream_sample_center;
    stream_sample_center << sample_holder_name << sample_suffix;
    sample_center_frame = stream_sample_center.str();

    std::stringstream stream_center_frame;
    stream_center_frame << sample_holder_name << center_suffix;
    sample_holder_center_frame = stream_center_frame.str();

    std::stringstream stream_center_vtk_frame;
    stream_center_vtk_frame << sample_holder_center_frame << vtk_suffix;
    sample_holder_center_vtk_frame = stream_center_vtk_frame.str();

    std::stringstream stream_sample_vtk_frame;
    stream_sample_vtk_frame << sample_center_frame << vtk_suffix;
    sample_holder_sample_vtk_frame = stream_sample_vtk_frame.str();
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());

    action_result.success = false;
    sampleAcquisitionServer->setAborted(action_result);
    return;
  }

  auto detector_images_path = fs::path(goal->experiment_path) / fs::path(detector_raw_path);
  int images_count = std::count_if(fs::directory_iterator(detector_images_path), fs::directory_iterator(),
                                   static_cast<bool (*)(const fs::path &)>(fs::is_regular_file));

  if (goal->start_states.size() == 0) {
    ROS_WARN("Given trajectory has no states! Don't know where to start. Exiting");
    ROS_ERROR("Failed calling service: %s", service_endpoints::ROBOT_MOVE_CONFIGURATION.c_str());
    sampleAcquisitionServer->setAborted();
    return;
  }

  // move to start state of trajectory first
  auto start_state = goal->start_states.at(goal->start_from_trajectory);
  std::vector<double> first_state_angles(start_state.joint_state.position.begin(), start_state.joint_state.position.end());
  // auto first_state_angles = goal->trajectories.at(0).joint_trajectory.points.front().positions;
  roaxdt_msgs::RobotMoveToPointConfigurationSpace srv_move_first;
  srv_move_first.request.goal_joint_angles = first_state_angles;

  ROS_INFO("Calling robot move joint to experiment start state");
  if (moveRobotToPointConfigurationSpaceClient.call(srv_move_first)) {
    ROS_INFO_STREAM("Response robot move first to point config space: " << srv_move_first.response.success);
    ROS_INFO_STREAM("Error recovey executed ? : " << srv_move_first.response.error_recovery);
    if (srv_move_first.response.error_recovery) {
      sampleAcquisitionServer->setAborted();
      return;
    }
  } else {
    ROS_ERROR("Failed calling service: %s", service_endpoints::ROBOT_MOVE_CONFIGURATION.c_str());
    sampleAcquisitionServer->setAborted();
    return;
  }

  bool error_recovery_flag = false;
  for (size_t i = goal->start_from_trajectory; i < goal->experiment_poses.size(); i++) {
    // check if last trajectory was executed successfully. Otherwise replan by config space goal
    roaxdt_msgs::RobotMoveToPointTrajectory srv_move;
    srv_move.request.goal = goal->experiment_poses.at(i);
    srv_move.request.trajectory = goal->trajectories.at(i);
    srv_move.request.start_state = goal->start_states.at(i);

    // retrieve state until idle
    franka_msgs::FrankaStateConstPtr robot_state =
        ros::topic::waitForMessage<franka_msgs::FrankaState>(std::string("/franka_state_controller/franka_states"), node_handle);
    while (robot_state->robot_mode == franka_msgs::FrankaState::ROBOT_MODE_IDLE) {
      ROS_WARN("Robot is not idle. Waiting...");
      ros::Duration(1.0).sleep();

      robot_state = ros::topic::waitForMessage<franka_msgs::FrankaState>(std::string("/franka_state_controller/franka_states"),
                                                                         node_handle);
    }

    if (error_recovery_flag) {
      ROS_WARN("Trajectory waypoint %i was not reached. Planning from intermediate state to next waypoint %i", i - 1, i);
      roaxdt_msgs::RobotMoveToPointConfigurationSpace srv_move;
      srv_move.request.goal_joint_angles = goal->trajectories.at(i).joint_trajectory.points.back().positions;

      if (moveRobotToPointConfigurationSpaceClient.call(srv_move)) {
        ROS_INFO_STREAM("Response robot move to point config space: " << srv_move.response.success);
        ROS_INFO_STREAM("Error recovey executed ? : " << srv_move.response.error_recovery);
        error_recovery_flag = srv_move.response.error_recovery;
      } else {
        ROS_ERROR("Failed calling service: %s", service_endpoints::ROBOT_MOVE_CONFIGURATION.c_str());
        sampleAcquisitionServer->setAborted();
        return;
      }
    } else {
      if (moveRobotToPointTrajectoryClient.call(srv_move)) {
        ROS_INFO_STREAM("Response robot move to point: " << srv_move.response.success);
        ROS_INFO_STREAM("Error recovey executed ? : " << srv_move.response.error_recovery);
        error_recovery_flag = srv_move.response.error_recovery;
      } else {
        ROS_ERROR("Failed calling service: %s", service_endpoints::ROBOT_MOVE_TRAJECTORY.c_str());
        sampleAcquisitionServer->setAborted();
        return;
      }
    }

    int success_execute = error_recovery_flag ? 0 : 1;
    action_result.success_execute.push_back(success_execute);

    // !!! iteration i != image_index !!!
    int image_index = images_count;
    ++images_count;

    tf::StampedTransform sample_holder_head_vtk_to_xray_tf =
        roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, camera_frame, sample_holder_center_vtk_frame);
    geometry_msgs::Pose sample_holder_vtk_to_xray_msg;
    tf::poseTFToMsg(sample_holder_head_vtk_to_xray_tf, sample_holder_vtk_to_xray_msg);

    tf::StampedTransform sample_holder_head_vtk2_to_xray_tf =
        roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, camera_frame, sample_holder_sample_vtk_frame);
    geometry_msgs::Pose sample_holder_vtk2_to_xray_msg;
    tf::poseTFToMsg(sample_holder_head_vtk2_to_xray_tf, sample_holder_vtk2_to_xray_msg);
    action_result.sample_holder_poses.push_back(sample_holder_vtk2_to_xray_msg);

    std::ostringstream file_name;
    file_name << image_index << default_image_file_type;
    fs::path file_path{detector_images_path};
    file_path /= file_name.str();
    cv::Mat img(detector_resolution.at(0), detector_resolution.at(1), CV_16UC1);
    if (simulation) {
      if (goal->forward_projection) {
        // in the simulation the forward projection should be done with elsa
        roaxdt_msgs::ForwardProjection srv_forward_projection;
        srv_forward_projection.request.goal_pose = sample_holder_vtk_to_xray_msg;

        if (forwardProjectionClient.call(srv_forward_projection)) {
          ROS_INFO("forward projection call successful for iteration %u", i);

          sensor_msgs::Image sinogram = srv_forward_projection.response.sinogram;
          sensor_msgs::ImageConstPtr image_ros_ptr = boost::make_shared<sensor_msgs::Image>(sinogram);
          img = cv_bridge::toCvCopy(image_ros_ptr, sinogram.encoding)->image;
        } else {
          ROS_ERROR("forward projection call error for iteration %u", i);
          img = cv::Mat(detector_resolution.at(0), detector_resolution.at(1), CV_16UC1, cv::Scalar(0));
        }
      } else {
        img = cv::Mat(detector_resolution.at(0), detector_resolution.at(1), CV_16UC1, cv::Scalar(0));
        ros::Duration(1.0).sleep();
      }
    } else {
      // in the lab the detector should be triggered
      ROS_INFO_STREAM("Waiting for " << sample_acquisition_wait_time << " seconds before retrieving detector image");
      ros::Duration(sample_acquisition_wait_time).sleep();

      roaxdt_msgs::DetectorRetrieveImage srv_detector_image;
      while (!detectorRetrieveImageClient.call(srv_detector_image)) {
        ROS_WARN("Error retrieving detector image for iteration %u. Retrying in 3 sec...", i);
        ros::Duration(3.0).sleep();
      }
      ROS_INFO("Retrieved image from detector service for iteration %u", i);
      boost::shared_ptr<sensor_msgs::Image const> shared_image;
      shared_image = boost::make_shared<sensor_msgs::Image>(std::move(srv_detector_image.response.image));

      img = cv_bridge::toCvCopy(shared_image, shared_image->encoding)->image;
    }

    // write full image to file system
    cv::imwrite(file_path.c_str(), img);

    if (!simulation) {
      roaxdt_msgs::CorrectFlatField srv_correct_flat_field;
      srv_correct_flat_field.request.experiment_id = goal->experiment_id;
      srv_correct_flat_field.request.image_id = image_index;
      if (correctFlatFieldClient.call(srv_correct_flat_field)) {
        ROS_INFO("%s call successful for iteration %u", service_endpoints::CORRECT_FLAT_FIELD.c_str(), i);
      } else {
        ROS_ERROR("%s call error for iteration %u", service_endpoints::CORRECT_FLAT_FIELD.c_str(), i);
      }
    } else {
      // No flat-field correction needed in simulation, directly copy to "detector" directory
      auto detector_images_corrected_path = fs::path(goal->experiment_path) / fs::path(detector_path);
      fs::path file_path_corrected{detector_images_corrected_path};
      file_path_corrected /= file_name.str();

      cv::imwrite(file_path_corrected.c_str(), img);
    }

    // Calculate sphere coordinates in camera frame

    visualizeSpheres(goal->spheres);

    action_feedback.iterations_finished = i + 1;
    action_feedback.image_id = image_index;
    action_feedback.sample_holder_pose = action_result.sample_holder_poses.back();
    action_feedback.success_execute = action_result.success_execute.back();
    sampleAcquisitionServer->publishFeedback(action_feedback);
  }

  action_result.success = true;
  sampleAcquisitionServer->setSucceeded(action_result);
}

void SampleAcquisition::visualizeSpheres(std::vector<geometry_msgs::Point> spheres) {
  roaxdt_msgs::SphereListDisplayMsg sphereListDisplayMsg;
  sphereListDisplayMsg.request.sphere_list = spheres;
  sphereListDisplayMsg.request.frame_id = sample_holder_center_vtk_frame;
  if (sphereListDisplayClient.call(sphereListDisplayMsg)) {
    ROS_INFO_STREAM("client call successfull for " << service_endpoints::SPHERE_LIST_DISPLAY);
  } else {
    ROS_INFO_STREAM("client call not successfull " << service_endpoints::SPHERE_LIST_DISPLAY);
  }
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::SAMPLE_ACQUISITION);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::SampleAcquisition node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}