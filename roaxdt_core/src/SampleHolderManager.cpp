#include <roaxdt_core/SampleHolderManager.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

SampleHolderManager::SampleHolderManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void SampleHolderManager::init() {
  try {
    readConfigParameter<bool>(node_handle, SIMULATION, simulation);
    readConfigParameter<bool>(node_handle, EXPERIMENT, experiment);
    readConfigParameter<std::string>(node_handle, experiments::SAMPLE_HOLDER_REFERENCE_LINK_ELSA,
                                     sample_holder_reference_link_elsa);
    readConfigParameter<std::string>(node_handle, experiments::SAMPLE_HOLDER_REFERENCE_LINK_VTK,
                                     sample_holder_reference_link_vtk);
    readConfigParameter<std::string>(node_handle, experiments::CENTER_SUFFIX, center_suffix);
    readConfigParameter<std::string>(node_handle, experiments::SAMPLE_SUFFIX, sample_suffix);
    readConfigParameter<std::vector<double>>(node_handle, experiments::VTK_VOLUME_SPACING, spacing);
    readConfigParameter<std::string>(node_handle, experiments::VTK_VOLUME_FILE_TYPE, vtk_volume_file_type);
    readConfigParameter<std::vector<std::string>>(node_handle, experiments::SAMPLE_HOLDER_FILE_TYPES, sampleHolderFileTypes);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  forwardProjection = experiment && simulation;
  if (forwardProjection) {
    setTargetVolumeClient = node_handle.serviceClient<roaxdt_msgs::SetTargetVolume>(service_endpoints::SET_TARGET_VOLUME);
  }

  ros::Duration(2.0).sleep(); // wait for -tf- service to appear

  loadSampleHolderServer.reset(new LoadSampleHolderServer(node_handle, action_endpoints::LOAD_SAMPLE_HOLDER,
                                                          boost::bind(&SampleHolderManager::loadSampleHolderCallback, this, _1),
                                                          false));
  loadSampleHolderServer->start();

  loadSampleServer.reset(new LoadSampleServer(node_handle, action_endpoints::LOAD_SAMPLE,
                                              boost::bind(&SampleHolderManager::loadSampleCallback, this, _1), false));
  loadSampleServer->start();

  sampleHolderPartsServer = node_handle.advertiseService(service_endpoints::GET_SAMPLE_HOLDER_PARTS,
                                                         &SampleHolderManager::sampleHolderPartsCallback, this);

  samplePartsServer =
      node_handle.advertiseService(service_endpoints::GET_SAMPLE_PARTS, &SampleHolderManager::samplePartsCallback, this);

  readMeshClient = node_handle.serviceClient<roaxdt_msgs::ReadMesh>(service_endpoints::READ_MESH);

  if (experiment) {
    setSampleHolderClient =
        node_handle.serviceClient<roaxdt_msgs::SetSampleHolderGeometry>(service_endpoints::SET_SAMPLE_HOLDER_GEOMETRY);
  }

  addSampleHolderToEnvironmentClient =
      node_handle.serviceClient<roaxdt_msgs::AddSampleHolderToEnvironment>(service_endpoints::ADD_SAMPLE_HOLDER_TO_ENVIRONMENT);
  addSamplesToEnvironmentClient =
      node_handle.serviceClient<roaxdt_msgs::AddSamplesToEnvironment>(service_endpoints::ADD_SAMPLES_TO_ENVIRONMENT);

  placeSpheresClient = node_handle.serviceClient<roaxdt_msgs::PlaceSpheres>(service_endpoints::PLACE_SPHERES);

  concatenateSampleAndHolderClient =
      node_handle.serviceClient<roaxdt_msgs::ConcatenateSampleAndHolder>(service_endpoints::CONCATENATE_SAMPLE_AND_HOLDER);

  setCollisionDetectorPartsClient =
      node_handle.serviceClient<roaxdt_msgs::SetCollisionDetectorParts>(service_endpoints::SET_COLLISION_DETECTOR_PARTS);

  tf::StampedTransform vtk_to_world_tf =
      roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, WORLD_LINK, sample_holder_reference_link_vtk);
  tf::poseTFToEigen(vtk_to_world_tf, vtk_to_world_eigen);

  readSampleHolderFiles();
  readSampleFiles();
}

void SampleHolderManager::readSampleHolderFiles() {
  ROS_INFO("Reading sample holder files");
  readFiles(filesystem::readSampleHolders(), sampleHolderFiles);
}

void SampleHolderManager::readSampleFiles() {
  ROS_INFO("Reading sample files");
  readFiles(filesystem::readSamples(), sampleFiles);
}

void SampleHolderManager::readFiles(fs::directory_iterator directoryIterator,
                                    std::map<std::string, std::map<std::string, fs::path>> &files) {
  for (auto &entry : boost::make_iterator_range(directoryIterator, {})) {
    auto partName = entry.path().filename().string();
    ROS_INFO("Part name: %s", partName.c_str());

    if (fs::is_directory(entry)) {
      std::map<std::string, fs::path> filesTmp;
      for (auto &subDirEntry : boost::make_iterator_range(fs::directory_iterator(entry), {})) {
        auto fileEndingTmp = subDirEntry.path().extension().string();
        auto fileNameWithoutEnding = subDirEntry.path().stem().string();
        auto fileName = subDirEntry.path().filename().string();

        if (std::find(sampleHolderFileTypes.begin(), sampleHolderFileTypes.end(), fileEndingTmp) != sampleHolderFileTypes.end()) {
          // If file ending matches we should still check if the filename matches the sample holder part name
          if (partName.compare(fileNameWithoutEnding) == 0) {
            filesTmp.insert(std::make_pair(fileEndingTmp, subDirEntry.path()));
          } else {
            ROS_WARN("Filename %s not matching part name %s", fileName.c_str(), partName.c_str());
          }
        } else {
          ROS_WARN("File not needed based on file type: %s", fileName.c_str());
        }
      }

      if (filesTmp.size() != sampleHolderFileTypes.size()) {
        ROS_WARN("Not adding files from %s", entry.path().c_str());
        ROS_WARN("Number of files %i not matching expected number %i", filesTmp.size(), sampleHolderFileTypes.size());
      } else {
        files.insert(std::make_pair(partName, filesTmp));
        ROS_INFO("Adding part: %s", partName.c_str());
        for (auto elem : filesTmp) {
          ROS_INFO("Associated files: %s", elem.second.c_str());
        }
      }
    }
  }
}

// TODO hot-swap sample holder base /and or head
void SampleHolderManager::loadSampleHolderCallback(const roaxdt_msgs::LoadSampleHolderGoalConstPtr &goal) {
  roaxdt_msgs::LoadSampleHolderResult action_result;

  if (sample_holder_name.compare(goal->sample_holder_name) != 0) {
    bool success = setSampleHolderUp(forwardProjection, goal->sample_holder_name);

    roaxdt_msgs::SetSampleHolderGeometry srv_set_sample_holder_geometry;
    if (experiment) {
      srv_set_sample_holder_geometry.request.spacing = spacing;
      srv_set_sample_holder_geometry.request.center_sample_holder = center_sample_holder;
      srv_set_sample_holder_geometry.request.center_sample = center_sample;
      srv_set_sample_holder_geometry.request.gripper_sample_holder = gripper_sample_holder;
      srv_set_sample_holder_geometry.request.dimensions = volume_dimensions_sample_holder;
      if (setSampleHolderClient.call(srv_set_sample_holder_geometry)) {
        ROS_INFO("set sample holder geometry call successful");
      } else {
        ROS_ERROR("set sample holder geometry call failed");
        success = false;
      }
    }

    if (success) {
      // Add sample holder to robot collision environment
      roaxdt_msgs::AddSampleHolderToEnvironment srv_add_sample_holder;
      roaxdt_msgs::SampleHolder sample_holder;
      sample_holder.dimensions =
          std::vector<double>{volume_dimensions_sample_holder.at(0) / 1000.0, volume_dimensions_sample_holder.at(1) / 1000.0,
                              volume_dimensions_sample_holder.at(2) / 1000.0};
      sample_holder.dimensions_gripper =
          std::vector<double>{volume_dimensions_gripper.at(0) / 1000.0, volume_dimensions_gripper.at(1) / 1000.0,
                              volume_dimensions_gripper.at(2) / 1000.0};
      sample_holder.dimensions_head = std::vector<double>{
          volume_dimensions_head.at(0) / 1000.0, volume_dimensions_head.at(1) / 1000.0, volume_dimensions_head.at(2) / 1000.0};
      // std::vector<double>(volume_dimensions_sample_holder.begin(), volume_dimensions_sample_holder.end());
      sample_holder.position = std::vector<double>{0, 0, 0};
      sample_holder.orientation = std::vector<double>{0, 0, 0, 0};
      sample_holder.name = goal->sample_holder_name;
      sample_holder.grip_frame = srv_set_sample_holder_geometry.response.grip_frame;
      sample_holder.center_frame = srv_set_sample_holder_geometry.response.center_frame;
      sample_holder.mesh_path = current_sample_holder.mesh_path.string();
      current_sample_holder_msg = sample_holder;

      srv_add_sample_holder.request.sample_holder = sample_holder;
      ROS_INFO("calling %s ...", service_endpoints::ADD_SAMPLE_HOLDER_TO_ENVIRONMENT.c_str());
      if (addSampleHolderToEnvironmentClient.call(srv_add_sample_holder)) {
        ROS_INFO("%s call successful", service_endpoints::ADD_SAMPLE_HOLDER_TO_ENVIRONMENT.c_str());
      } else {
        ROS_ERROR("%s call not successful", service_endpoints::ADD_SAMPLE_HOLDER_TO_ENVIRONMENT.c_str());
      }

      // Add sample holder to collision detector
      roaxdt_msgs::SetCollisionDetectorParts set_collision_detector_parts;
      set_collision_detector_parts.request.sample_holder = current_sample_holder_msg;
      ROS_INFO("calling %s ...", service_endpoints::SET_COLLISION_DETECTOR_PARTS.c_str());
      if (setCollisionDetectorPartsClient.call(set_collision_detector_parts)) {
        ROS_INFO("%s call successful", service_endpoints::SET_COLLISION_DETECTOR_PARTS.c_str());
      } else {
        ROS_ERROR("%s call not successful", service_endpoints::SET_COLLISION_DETECTOR_PARTS.c_str());
      }
    }
  }
  action_result.spheres = spheres;
  action_result.sample_center = sample_center;
  action_result.head_center = head_center;
  action_result.helix_height = helix_height;
  action_result.helix_radius = helix_radius;
  action_result.helix_omega = helix_omega;
  action_result.helix_phi = helix_phi;

  loadSampleHolderServer->setSucceeded(action_result);
  return;
}

void SampleHolderManager::loadSampleCallback(const roaxdt_msgs::LoadSampleGoalConstPtr &goal) {
  roaxdt_msgs::LoadSampleResult action_result;

  std::vector<Sample> samples_backup = current_samples;
  std::vector<roaxdt_msgs::Sample> samples_msgs_backup = current_samples_msgs;
  current_samples.clear();
  bool success = setSamplesUp(forwardProjection, goal->sample_names);

  if (success) {
    // Add sample to robot collision environmentr
    roaxdt_msgs::AddSamplesToEnvironment srv_add_samples;
    srv_add_samples.request.samples = current_samples_msgs;
    ROS_INFO("calling %s ...", service_endpoints::ADD_SAMPLES_TO_ENVIRONMENT.c_str());
    if (addSamplesToEnvironmentClient.call(srv_add_samples)) {
      ROS_INFO("%s call successful", service_endpoints::ADD_SAMPLES_TO_ENVIRONMENT.c_str());
    } else {
      ROS_ERROR("%s call not successful", service_endpoints::ADD_SAMPLES_TO_ENVIRONMENT.c_str());
    }

    // Add sample holder to collision detector
    roaxdt_msgs::SetCollisionDetectorParts set_collision_detector_parts;
    set_collision_detector_parts.request.sample_holder = current_sample_holder_msg;
    if (current_samples_msgs.size() > 0) {
      set_collision_detector_parts.request.sample = current_samples_msgs.at(0);
    }
    ROS_INFO("calling %s ...", service_endpoints::SET_COLLISION_DETECTOR_PARTS.c_str());
    if (setCollisionDetectorPartsClient.call(set_collision_detector_parts)) {
      ROS_INFO("%s call successful", service_endpoints::SET_COLLISION_DETECTOR_PARTS.c_str());
    } else {
      ROS_ERROR("%s call not successful", service_endpoints::SET_COLLISION_DETECTOR_PARTS.c_str());
    }
  } else {
    current_samples = samples_backup;
    current_samples_msgs = samples_msgs_backup;
  }

  loadSampleServer->setSucceeded(action_result);
  return;
}

bool SampleHolderManager::sampleHolderPartsCallback(roaxdt_msgs::GetSampleHolderParts::Request &req,
                                                    roaxdt_msgs::GetSampleHolderParts::Response &res) {
  std::vector<std::string> sampleHolders;

  for (auto const &elem : sampleHolderFiles) {
    sampleHolders.push_back(elem.first);
  }

  res.sample_holders = sampleHolders;
  res.current_sample_holder = sample_holder_name;
  res.sample_center = sample_center;

  return true;
}

bool SampleHolderManager::samplePartsCallback(roaxdt_msgs::GetSampleParts::Request &req,
                                              roaxdt_msgs::GetSampleParts::Response &res) {
  std::vector<std::string> samples;

  for (auto const &elem : sampleFiles) {
    samples.push_back(elem.first);
  }

  res.samples = samples;
  for (const auto &sample_obj : current_samples) {
    res.current_samples.push_back(sample_obj.name);
  }

  return true;
}

bool SampleHolderManager::readSampleHolderPart(const std::string &sampleHolderPart, fs::path &metadataPath, fs::path &volumePath,
                                               fs::path &rasterizedVolumePath, bool readVolume, std::vector<double> &spacing,
                                               geometry_msgs::Pose &gripper_center, geometry_msgs::Pose &volume_center,
                                               geometry_msgs::Point &sample_center, std::vector<float> &volume_dimensions,
                                               float &sphere_diameter, float &helix_height,
                                               std::vector<geometry_msgs::Point> &spheres, double &helix_radius,
                                               double &helix_omega, double &helix_phi) {
  if (!readMeshMetadata(sampleHolderPart, metadataPath, gripper_center, volume_center, sample_center, helix_height,
                        volume_dimensions, sphere_diameter, spheres, helix_radius, helix_omega, helix_phi)) {
    ROS_ERROR("Couldn't read metadata of sample holder part %s !", sampleHolderPart.c_str());
    return false;
  } else if (readVolume) {
    int volume_size = volume_dimensions.at(0) * volume_dimensions.at(1) * volume_dimensions.at(2);
    if (!readMeshVolume(sampleHolderPart, volumePath, spacing, rasterizedVolumePath, 2.0)) {
      ROS_ERROR("Couldn't read mesh of sample holder part %s!", sampleHolderPart.c_str());
      ROS_ERROR("Path %s!", volumePath.c_str());
      return false;
    }
  }
}

bool SampleHolderManager::readSamplePart(Sample &sample_new, bool readVolume) {
  if (!sample_holder_utils::readSampleMetadata(sample_new.metadata_path, sample_new.dimensions, sample_new.center,
                                               sample_new.absorption)) {
    ROS_ERROR_STREAM("Couldn't read sample metadata in file: " << sample_new.metadata_path.string());
    return false;
  } else if (readVolume) {
    if (!readMeshVolume(sample_new.name, sample_new.mesh_path, sample_new.spacing, sample_new.volume_path,
                        sample_new.absorption)) {
      ROS_ERROR("Couldn't read mesh of sample part %s!", sample_new.name.c_str());
      ROS_ERROR("Path %s!", sample_new.mesh_path.c_str());
      return false;
    }
  }
}

void SampleHolderManager::getSampleHolderPartPaths(const std::string &sampleHolderPartName,
                                                   std::map<std::string, std::map<std::string, fs::path>> &filesMap,
                                                   fs::path &metadataPath, fs::path &volumePath) {
  auto sampleHolderPartPathsIter = filesMap.find(sampleHolderPartName);
  if (sampleHolderPartPathsIter != filesMap.end()) {
    auto sampleHolderPartMetadataPathIter = sampleHolderPartPathsIter->second.find(sampleHolderFileTypes.at(0));
    auto sampleHolderPartVolumePathIter = sampleHolderPartPathsIter->second.find(sampleHolderFileTypes.at(1));

    if (sampleHolderPartMetadataPathIter != sampleHolderPartPathsIter->second.end() &&
        sampleHolderPartVolumePathIter != sampleHolderPartPathsIter->second.end()) {
      metadataPath = sampleHolderPartMetadataPathIter->second;
      volumePath = sampleHolderPartVolumePathIter->second;
      ROS_INFO("volume path: %s", volumePath.c_str());
    }
  }
}

bool SampleHolderManager::setSampleHolderUp(bool readMeshRequired, const std::string &sample_holder_new) {
  roaxdt_msgs::LoadSampleHolderFeedback feedback;

  // determine target frame for goal pose - on the fly because sample holder can change at any time
  ROS_INFO("sample holder %s", sample_holder_name.c_str());
  ROS_INFO("sample holder new %s", sample_holder_new.c_str());

  ROS_WARN("Waiting for MeshReader services to appear for 10 seconds. Start them now!");
  readMeshClient.waitForExistence(ros::Duration(10.0));

  ROS_WARN("Waiting for %s service to appear for 10 seconds.", service_endpoints::ADD_SAMPLE_HOLDER_TO_ENVIRONMENT.c_str());
  addSampleHolderToEnvironmentClient.waitForExistence(ros::Duration(10.0));

  // read sample holder
  // if (sample_holder_name.compare(sample_holder_new) != 0) {  //TODO why?
  fs::path sampleHolderMetadataPath, sampleHolderVolumePath;
  getSampleHolderPartPaths(sample_holder_new, sampleHolderFiles, sampleHolderMetadataPath, sampleHolderVolumePath);
  if (sampleHolderMetadataPath.empty() || sampleHolderVolumePath.empty()) {
    ROS_ERROR("Couldn't determine paths for sample holder part %s. Exiting.", sample_holder_name);
    return false;
  }
  readSampleHolderPart(sample_holder_new, sampleHolderMetadataPath, sampleHolderVolumePath, holder_rasterized_volume_path,
                       readMeshRequired, spacing, gripper_sample_holder, center_sample_holder, center_sample,
                       volume_dimensions_sample_holder, sphere_diameter, helix_height, spheres, helix_radius, helix_omega,
                       helix_phi);

  ROS_INFO_STREAM("sample holder volume size: " << volume_dimensions_sample_holder.at(0) << " - "
                                                << volume_dimensions_sample_holder.at(1) << " - "
                                                << volume_dimensions_sample_holder.at(2));
  // }
  sample_holder_name = sample_holder_new;
  ros::param::set(experiments::SAMPLE_HOLDER_NAME, sample_holder_name);

  std::stringstream stream_head_center_frame, stream_holder_sample_frame;
  stream_head_center_frame << sample_holder_name << center_suffix;
  sample_holder_center_frame = stream_head_center_frame.str();
  stream_holder_sample_frame << sample_holder_name << sample_suffix;
  sample_holder_sample_frame = stream_holder_sample_frame.str();
  feedback.sample_holder_swapped = true;
  loadSampleHolderServer->publishFeedback(feedback);

  if (readMeshRequired) {
    ROS_WARN("Waiting for CT service -setTargetVolume- to appear for 10 seconds. Start it now!");
    setTargetVolumeClient.waitForExistence(ros::Duration(10.0));
    if (!setTargetVolume()) {
      ROS_ERROR("Couldn't set merged sample holder as target volume!", sample_holder_name);
      return false;
    }
  }
  feedback.set_target_volume_called = true;
  loadSampleHolderServer->publishFeedback(feedback);

  // set sample center as sample_holder head center for now - TODO change
  head_center.point = center_sample_holder.position; // TODO refactor for geometry_msgs::Pose
  head_center.header.frame_id = sample_holder_center_frame;
  sample_center.point = center_sample;
  sample_center.header.frame_id = sample_holder_sample_frame;

  feedback.sample_holder_geometry_set = true;
  loadSampleHolderServer->publishFeedback(feedback);

  SampleHolder sample_holder_obj;
  sample_holder_obj.name = sample_holder_new;
  sample_holder_obj.mesh_path = sampleHolderVolumePath;
  sample_holder_obj.rasterized_volume_path = holder_rasterized_volume_path;
  sample_holder_obj.dimensions = volume_dimensions_sample_holder;
  sample_holder_obj.dimensions_gripper = volume_dimensions_gripper;
  sample_holder_obj.dimensions_head = volume_dimensions_head;
  sample_holder_obj.pose_center = center_sample_holder;
  sample_holder_obj.pose_gripper = gripper_sample_holder;
  current_sample_holder = sample_holder_obj;

  return true;
}

bool SampleHolderManager::setSamplesUp(bool readMeshRequired, const std::vector<std::string> &samples_new) {
  roaxdt_msgs::LoadSampleFeedback feedback;

  ROS_WARN("Waiting for %s service to appear for 10 seconds.", service_endpoints::CONCATENATE_SAMPLE_AND_HOLDER.c_str());
  concatenateSampleAndHolderClient.waitForExistence(ros::Duration(10.0));

  // read sample holder
  for (auto &sample_new : samples_new) {
    fs::path sampleMetadataPath, sampleMeshPath;
    getSampleHolderPartPaths(sample_new, sampleFiles, sampleMetadataPath, sampleMeshPath);
    if (sampleMetadataPath.empty() || sampleMeshPath.empty()) {
      ROS_ERROR("Couldn't determine paths for sample part %s. Exiting.", sample_new);
      return false;
    }
    Sample sample_obj;
    sample_obj.name = sample_new;
    sample_obj.metadata_path = sampleMetadataPath;
    sample_obj.mesh_path = sampleMeshPath;
    sample_obj.spacing = spacing;
    readSamplePart(sample_obj, readMeshRequired);

    ROS_INFO_STREAM("sample volume size: " << sample_obj.dimensions.at(0) << " - " << sample_obj.dimensions.at(1) << " - "
                                           << sample_obj.dimensions.at(2));
    current_samples.push_back(sample_obj);
  }

  feedback.sample_swapped = true;
  loadSampleServer->publishFeedback(feedback);

  std::vector<roaxdt_msgs::Sample> samples_msgs;
  samples_msgs.reserve(current_samples.size());
  for (const auto &sample : current_samples) {
    roaxdt_msgs::Sample sample_msg;
    sample_msg.name = sample.name;
    sample_msg.metadata_path = sample.metadata_path.string();
    sample_msg.mesh_path = sample.mesh_path.string();
    sample_msg.volume_path = sample.volume_path.string();
    sample_msg.dimensions = sample.dimensions;
    sample_msg.spacing = sample.spacing;
    sample_msg.center = sample.center;

    samples_msgs.push_back(sample_msg);
  }

  if (readMeshRequired) {
    // Merge holder volume with sample volume before we set the volume
    if (samples_new.size() > 0) {
      roaxdt_msgs::ConcatenateSampleAndHolder concatenate_sample_and_holder_msg;
      concatenate_sample_and_holder_msg.request.samples = samples_msgs;
      concatenate_sample_and_holder_msg.request.holder_dimensions = volume_dimensions_sample_holder;
      concatenate_sample_and_holder_msg.request.holder_spacing = spacing;
      concatenate_sample_and_holder_msg.request.holder_path = holder_rasterized_volume_path.string();

      // create new filename from holder and sample name
      boost::filesystem::path resulting_volume_path =
          holder_rasterized_volume_path.parent_path() /
          boost::filesystem::path(holder_rasterized_volume_path.stem().string() + "_" +
                                  current_samples.at(0).volume_path.stem().string() +
                                  holder_rasterized_volume_path.extension().string());
      ROS_INFO("Saving concatenated volume to path: %s", resulting_volume_path.c_str());
      concatenate_sample_and_holder_msg.request.resulting_volume_path = resulting_volume_path.string();
      if (concatenateSampleAndHolderClient.call(concatenate_sample_and_holder_msg)) {
        ROS_INFO("/concatenate_sample_and_holder call successful");
        current_samples_msgs = samples_msgs;
      } else {
        ROS_ERROR("/concatenate_sample_and_holder call error");
      }

      roaxdt_msgs::SetTargetVolume srv_set_target_volume;
      srv_set_target_volume.request.spacing = concatenate_sample_and_holder_msg.response.resulting_volume_spacing;
      srv_set_target_volume.request.dimensions = concatenate_sample_and_holder_msg.response.resulting_volume_dimensions;
      srv_set_target_volume.request.volume_path = resulting_volume_path.string();
      if (setTargetVolumeClient.call(srv_set_target_volume)) {
        ROS_INFO("set target volume call successful");
        return true;
      } else {
        ROS_ERROR("set target volume call error");
        return false;
      }
    }
  } else {
    current_samples_msgs = samples_msgs;
  }
  feedback.set_target_volume_called = true;
  loadSampleServer->publishFeedback(feedback);

  return true;
}

bool SampleHolderManager::setTargetVolume() {
  // place spheres in volume before setting as target volume
  roaxdt_msgs::PlaceSpheres place_spheres_msg;
  place_spheres_msg.request.volume_path = holder_rasterized_volume_path.string();
  place_spheres_msg.request.spheres = spheres;
  place_spheres_msg.request.sphere_diameter = sphere_diameter;
  place_spheres_msg.request.volume_dimensions = volume_dimensions_sample_holder;
  place_spheres_msg.request.volume_spacing = spacing;
  if (placeSpheresClient.call(place_spheres_msg)) {
    ROS_INFO("/place_spheres call successful");
  } else {
    ROS_ERROR("/place_spheres call error");
  }

  roaxdt_msgs::SetTargetVolume srv_set_target_volume;
  srv_set_target_volume.request.spacing = spacing;
  srv_set_target_volume.request.dimensions = volume_dimensions_sample_holder;
  srv_set_target_volume.request.volume_path = holder_rasterized_volume_path.string();
  if (setTargetVolumeClient.call(srv_set_target_volume)) {
    ROS_INFO("set target volume call successful");
    return true;
  } else {
    ROS_ERROR("set target volume call error");
    return false;
  }
}

bool SampleHolderManager::readMeshMetadata(std::string sample_name, fs::path &metadataPath, geometry_msgs::Pose &gripper_center,
                                           geometry_msgs::Pose &volume_center, geometry_msgs::Point &sample_center,
                                           float &helix_height, std::vector<float> &volume_dimensions, float &sphere_diameter,
                                           std::vector<geometry_msgs::Point> &spheres, double &helix_radius, double &helix_omega,
                                           double &helix_phi) {
  std::vector<double> gripper, center, sample_center_vec;
  if (!sample_holder_utils::readSampleHolderMetadata(metadataPath, gripper, center, sample_center_vec, helix_height,
                                                     volume_dimensions, volume_dimensions_gripper, volume_dimensions_head,
                                                     sphere_diameter, spheres, helix_radius, helix_omega, helix_phi)) {
    ROS_ERROR_STREAM("Couldn't read sample holder metadata in file: " << metadataPath.string());
    return false;
  }

  for (auto &sphere : spheres) {
    Eigen::Vector3d sphere_in_vtk(sphere.x, sphere.y, sphere.z + (sphere_diameter / 2.0));
    auto sphere_in_link_frame = sphere_in_vtk.homogeneous();
    sphere.x = sphere_in_link_frame.x();
    sphere.y = sphere_in_link_frame.y();
    sphere.z = sphere_in_link_frame.z();
  }

  geometry_msgs::Pose pose_center;
  geometry_msgs::Point point_center;
  geometry_msgs::Quaternion quaternion_center;
  point_center.x = center.at(0);
  point_center.y = center.at(1);
  point_center.z = center.at(2);
  quaternion_center.x = center.at(3);
  quaternion_center.y = center.at(4);
  quaternion_center.z = center.at(5);
  quaternion_center.w = center.at(6);
  pose_center.position = point_center;
  pose_center.orientation = quaternion_center;
  volume_center = pose_center;

  geometry_msgs::Pose pose_gripper;
  geometry_msgs::Point point_gripper;
  geometry_msgs::Quaternion quaternion_gripper;
  point_gripper.x = gripper.at(0);
  point_gripper.y = gripper.at(1);
  point_gripper.z = gripper.at(2);
  quaternion_gripper.x = gripper.at(3);
  quaternion_gripper.y = gripper.at(4);
  quaternion_gripper.z = gripper.at(5);
  quaternion_gripper.w = gripper.at(6);
  pose_gripper.position = point_gripper;
  pose_gripper.orientation = quaternion_gripper;
  gripper_center = pose_gripper;

  geometry_msgs::Point point_sample;
  point_sample.x = sample_center_vec.at(0);
  point_sample.y = sample_center_vec.at(1);
  point_sample.z = sample_center_vec.at(2);
  sample_center = point_sample;

  return true;
}

bool SampleHolderManager::readMeshVolume(std::string sample_name, fs::path &volumePath, std::vector<double> &spacing,
                                         fs::path &rasterizedVolumePath, float absorption) {
  rasterizedVolumePath = roaxdt::filesystem::getAppDirectory();
  rasterizedVolumePath /= filesystem::RASTERIZED_SAMPLES_DIRECTORY_NAME;
  filesystem::makeDirectory(rasterizedVolumePath); // Directory might not yet exist
  rasterizedVolumePath /= sample_name + vtk_volume_file_type;

  // Check if volume was already rasterized and saved to filesystem
  if (fs::exists(rasterizedVolumePath)) {
    ROS_INFO_STREAM(rasterizedVolumePath.c_str() << " already exists. Not reading mesh again.");
  } else {
    roaxdt_msgs::ReadMesh srv_read_mesh;
    srv_read_mesh.request.volume_read_path = volumePath.string();

    srv_read_mesh.request.return_volume = false; // volume is too big (~ 6 GB). Write to filesystem
    srv_read_mesh.request.volume_write_path = rasterizedVolumePath.string();
    srv_read_mesh.request.spacing = spacing;
    srv_read_mesh.request.absorption = absorption;
    ROS_INFO("calling read mesh ...");
    if (readMeshClient.call(srv_read_mesh)) {
      ROS_INFO("Read mesh call successful");
    } else {
      ROS_ERROR("Read mesh call not successful");
      return false;
    }
  }

  return true;
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::SAMPLE_HOLDER_MANAGER);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::SampleHolderManager node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}