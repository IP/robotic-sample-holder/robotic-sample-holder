#include <roaxdt_core/OnlineExperiment.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;
using namespace roaxdt::spherical_data_utils;

OnlineExperiment::OnlineExperiment(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle), calibrationClientID(0) {
  this->init();
}

void OnlineExperiment::init() {
  try {
    readConfigParameter<std::string>(node_handle, experiments::PATH, experiments_base_path);
    readConfigParameter<std::string>(node_handle, experiments::SEGMENTED_PATH, segmented_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_PATH, detector_path);
    readConfigParameter<std::string>(node_handle, experiments::CALIBRATED_PATH, calibrated_path);
    readConfigParameter<std::string>(node_handle, experiments::RECONSTRUCTED_PATH, reconstructed_path);
    readConfigParameter<float>(node_handle, calibration::COST_THRESHOLD_PER_SPHERE, costThresholdPerSphere);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  loadSampleHolderClient.reset(new LoadSampleHolderClient(action_endpoints::LOAD_SAMPLE_HOLDER, true));
  ROS_INFO("Waiting for load_sample_holder client to show up...");
  loadSampleHolderClient->waitForServer();
  ROS_INFO("load_sample_holder showed up");

  registerImagesClient.reset(new RegisterImagesClient(action_endpoints::REGISTER_IMAGES, true));
  ROS_INFO("Waiting for register_images client to show up...");
  registerImagesClient->waitForServer();
  ROS_INFO("register_images showed up");

  calibrationClient.reset(new CalibrationClient(action_endpoints::CALIBRATION + "_" + std::to_string(calibrationClientID), true));
  ROS_INFO_STREAM("Waiting for calibration client #" << calibrationClientID << " to show up...");
  calibrationClient->waitForServer();
  ROS_INFO_STREAM("calibration client #" << calibrationClientID << " showed up");

  computedTomographyClient.reset(new ComputedTomographyClient(action_endpoints::COMPUTED_TOMOGRAPHY, true));
  ROS_INFO("Waiting for %s client to show up...", action_endpoints::COMPUTED_TOMOGRAPHY.c_str());
  computedTomographyClient->waitForServer();
  ROS_INFO("%s showed up", action_endpoints::COMPUTED_TOMOGRAPHY.c_str());

  getTrajectoryClient.reset(new GetTrajectoryClient(action_endpoints::GET_TRAJECTORY, true));
  ROS_INFO("Waiting for %s client to show up...", action_endpoints::GET_TRAJECTORY.c_str());
  getTrajectoryClient->waitForServer();
  ROS_INFO("%s showed up", action_endpoints::GET_TRAJECTORY.c_str());

  segmentVolumeClient = node_handle.serviceClient<roaxdt_msgs::SegmentVolume>(service_endpoints::SEGMENT_VOLUME);
  computePoseWeightClient = node_handle.serviceClient<roaxdt_msgs::ComputePoseWeight>(service_endpoints::COMPUTE_POSE_WEIGHT);
  generateWaypointTrajectoryClient =
      node_handle.serviceClient<roaxdt_msgs::GenerateWaypointTrajectory>(service_endpoints::GENERATE_WAYPOINT_TRAJECTORY);
  sphereSamplerClient = node_handle.serviceClient<roaxdt_msgs::SampleSphere>(service_endpoints::SAMPLE_SPHERE);
  sphereSamplerDiskClient = node_handle.serviceClient<roaxdt_msgs::SampleSphereDisk>(service_endpoints::SAMPLE_SPHERE_DISK);
  plotSphericalMapClient = node_handle.serviceClient<roaxdt_msgs::PlotSphericalMap>(service_endpoints::PLOT_SPHERICAL_MAP);

  // set up random number generator for choosing the next pose with the highest probability
  std::random_device device;
  rg_engine = std::mt19937(device());

  onlineExperimentServer.reset(new OnlineExperimentServer(node_handle, action_endpoints::ONLINE_EXPERIMENT,
                                                          boost::bind(&OnlineExperiment::onlineExperimentCallback, this, _1),
                                                          false));
  onlineExperimentServer->start();
}

void OnlineExperiment::onlineExperimentCallback(const roaxdt_msgs::OnlineExperimentGoalConstPtr &goal) {
  roaxdt_msgs::OnlineExperimentFeedback action_feedback;
  roaxdt_msgs::OnlineExperimentResult action_result;

  if (!prepareExperimentClient) {
    prepareExperimentClient.reset(new PrepareExperimentClient(action_endpoints::PREPARE_EXPERIMENT, true));
    ROS_INFO("Waiting for prepare_experiment client to show up...");
    prepareExperimentClient->waitForServer();
    ROS_INFO("prepare_experiment showed up");
  }

  if (!sampleAcquisitionClient) {
    sampleAcquisitionClient.reset(new SampleAcquisitionClient(action_endpoints::SAMPLE_ACQUISITION, true));
    ROS_INFO("Waiting for sample_acquisition client to show up...");
    sampleAcquisitionClient->waitForServer();
    ROS_INFO("sample_acquisition showed up");
  }

  // keep track of image_id to spherical_pose mapping for fast access during score update step
  std::map<int, SphericalPose> image_id_to_spherical_pose;
  if (goal->experiment_id.length() == 0) {
    boost::uuids::uuid uuid = boost::uuids::random_generator()();
    auto experiment_id = boost::lexical_cast<std::string>(uuid);
    experiment_data = OnlineExperimentData(goal, experiment_id, experiments_base_path);

    roaxdt_msgs::PrepareExperimentGoal prepareExperimentGoal;
    prepareExperimentGoal.experiment_id = experiment_data.getGoalData().experiment_id;

    prepareExperimentClient->sendGoal(prepareExperimentGoal);
    prepareExperimentClient->waitForResult();
    ROS_INFO("Sent request to prepare_experiment server. Waiting for result...");
    auto prepareExperimentResult = prepareExperimentClient->getResult();

    // execute experiment preparation and sample acquisition
    action_feedback.experiment_id = experiment_data.getGoalData().experiment_id;
    action_feedback.experiment_path = experiment_data.getExperimentPath().string();
    onlineExperimentServer->publishFeedback(action_feedback);

    auto loadSampleHolderResult = loadSampleHolder(experiment_data.getGoalData().sample_holder_name);
    experiment_data.populateLoadSampleHolder(loadSampleHolderResult);

    experiment_data.setCameraMatrix(prepareExperimentResult->intrinsic_matrix);
    roaxdt::experiment::writeOnlineExperimentData(experiment_data);

    SphericalData &spherical_data_low = experiment_data.getSphericalDataLow();
    SphericalData &spherical_data_high = experiment_data.getSphericalDataHigh();
    spherical_data_low.num_sides = goal->sphere_n_sides_low;
    trajectory_utils::getHealpixMapVectors(goal->sphere_n_sides_low, spherical_data_low.spherical_poses, 1);
    spherical_data_low.num_pixels = spherical_data_low.spherical_poses.at(0).num_pixels;

    spherical_data_high.num_sides = goal->sphere_n_sides_high;
    trajectory_utils::getHealpixMapVectors(goal->sphere_n_sides_high, spherical_data_high.spherical_poses, 1);
    spherical_data_high.num_pixels = spherical_data_high.spherical_poses.at(0).num_pixels;

    totalImageCounter = 0;
    for (size_t i = 0; i < spherical_data_low.num_pixels; i++) {
      auto &spherical_pose = spherical_data_low.spherical_poses.at(i);
      spherical_pose.success_execute = false;
      bool successAcquire = acquireAndCalibrateImage(spherical_pose, image_id_to_spherical_pose, action_feedback);
      if (!successAcquire) {
        action_result.success = false;
        onlineExperimentServer->setAborted(action_result);
        return;
      }
    }

    experiment_data.setTotalImageCounter(totalImageCounter);
    roaxdt::experiment::writeOnlineExperimentData(experiment_data);
  } else {
    // just replay experiment - read robot positions from json
    auto experiment_path = fs::path(experiments_base_path + goal->experiment_id);
    ROS_INFO("Reading file: %s", experiment_path.c_str());
    experiment_data = OnlineExperimentData(goal, goal->experiment_id, experiments_base_path);

    try {
      roaxdt::experiment::readOnlineExperimentData(experiment_data, experiment_path);
      totalImageCounter = experiment_data.getTotalImageCounter();

      auto loadSampleHolderResult = loadSampleHolder(experiment_data.getGoalData().sample_holder_name);
      experiment_data.populateLoadSampleHolder(loadSampleHolderResult);
    } catch (const std::exception &e) {
      ROS_ERROR(e.what());
      action_result.success = false;
      onlineExperimentServer->setAborted(action_result);
      return;
    }
  }

  SphericalData &spherical_data_low = experiment_data.getSphericalDataLow();
  SphericalData &spherical_data_high = experiment_data.getSphericalDataHigh();
  ROS_INFO("sample center z: %f", experiment_data.getGoalData().sample_center.point.z);

  if (goal->run_spherical_plot) {
    roaxdt_msgs::PlotSphericalMap plot_spherical_map_msg;
    plot_spherical_map_msg.request.experiment_id = experiment_data.getGoalData().experiment_id;
    ROS_INFO("Plotting spherical score map for experiment %s", plot_spherical_map_msg.request.experiment_id.c_str());
    if (plotSphericalMapClient.call(plot_spherical_map_msg)) {
      ROS_INFO("Received response from %s", service_endpoints::PLOT_SPHERICAL_MAP.c_str());
    } else {
      ROS_WARN("Failed calling service: %s", service_endpoints::PLOT_SPHERICAL_MAP.c_str());
    }
  }

  if (goal->run_optimize_trajectory) {
    // pre-compute sphere mappings
    std::map<int, std::vector<int>> sphere_mapping_low_to_high;
    spherical_data_high.pixel_scores.resize(spherical_data_high.num_pixels, 0);
    int estimatedRegionSize = spherical_data_high.num_pixels / spherical_data_low.num_pixels;
    for (size_t i = 0; i < spherical_data_low.num_pixels; i++) {
      auto &sphericalPoseCurrent = spherical_data_low.spherical_poses.at(i);
      std::vector<int> sphere_mapping_local;
      sphere_mapping_local.reserve(estimatedRegionSize);

      // 1. build blacklisted indices array
      std::vector<int> indices_to_exclude = getBlacklistedIndicesForIndex(spherical_data_low, i);

      // 2. get indices of the index from lower sampled sphere on higher sampled sphere
      roaxdt_msgs::SampleSphere sample_sphere_msg;
      sample_sphere_msg.request.blacklisted_image_indices = indices_to_exclude;
      sample_sphere_msg.request.num_pixels = spherical_data_low.num_pixels;
      sample_sphere_msg.request.num_sides = spherical_data_low.num_sides;
      sample_sphere_msg.request.num_sides_new = spherical_data_high.num_sides;
      if (sphereSamplerClient.call(sample_sphere_msg)) {
        ROS_INFO("Received response from %s for index %i - %i", service_endpoints::SAMPLE_SPHERE.c_str(),
                 sphericalPoseCurrent.num_sides, i);

        for (auto &sphericalPoseIndex : sample_sphere_msg.response.spherical_pose_indices_new) {
          sphere_mapping_local.push_back(sphericalPoseIndex);
        }
        sphere_mapping_low_to_high.insert({i, sphere_mapping_local});
      } else {
        ROS_ERROR("Failed calling service: %s", service_endpoints::SAMPLE_SPHERE.c_str());
        onlineExperimentServer->setAborted();
        return;
      }
    }

    // alternative 1: reconstruction
    roaxdt_msgs::ComputedTomographyGoal computedTomographyGoal;
    experiment_data.populateComputedTomography(computedTomographyGoal);

    computedTomographyClient->sendGoal(computedTomographyGoal);
    computedTomographyClient->waitForResult();
    ROS_INFO("Sent request to computed_tomography server. Waiting for result...");
    auto computedTomographyErrorResult = computedTomographyClient->getResult();
    ROS_INFO("Recevied result from computed tomography server");

    roaxdt_msgs::SegmentVolume segment_volume_srv;
    experiment_data.populateSegmentVolume(segment_volume_srv);
    if (segmentVolumeClient.call(segment_volume_srv)) {
      ROS_INFO("Received response from %s", service_endpoints::SEGMENT_VOLUME.c_str());
    } else {
      ROS_ERROR("Failed calling service: %s", service_endpoints::SEGMENT_VOLUME.c_str());
      onlineExperimentServer->setAborted();
      return;
    }

    // set scores on the map for regions were the acquisition was successful
    spherical_data_low.pixel_scores.resize(spherical_data_low.num_pixels, 0);
    roaxdt_msgs::ComputePoseWeight compute_pose_weight_srv;
    experiment_data.populateComputePoseWeight(compute_pose_weight_srv);
    std::vector<int> ids_tmp(totalImageCounter);
    std::iota(std::begin(ids_tmp), std::end(ids_tmp), 0);
    ROS_INFO("totalimage counter: %i", totalImageCounter);
    ROS_INFO("image ids size: %u", ids_tmp.size());
    compute_pose_weight_srv.request.image_ids = ids_tmp;
    if (computePoseWeightClient.call(compute_pose_weight_srv)) {
      ROS_INFO("Received response from %s", service_endpoints::COMPUTE_POSE_WEIGHT.c_str());
      bool success_recalculate = recalculateScores(compute_pose_weight_srv.response.scores, sphere_mapping_low_to_high,
                                                   image_id_to_spherical_pose, spherical_data_low, spherical_data_high);
      if (!success_recalculate) {
        ROS_ERROR("Failed recalculating scores!");
        onlineExperimentServer->setAborted();
        return;
      }
    } else {
      ROS_ERROR("Failed calling service: %s", service_endpoints::COMPUTE_POSE_WEIGHT.c_str());
      onlineExperimentServer->setAborted();
      return;
    }

    roaxdt::experiment::writeOnlineExperimentData(experiment_data);

    // Plot scores
    roaxdt_msgs::PlotSphericalMap plot_spherical_map_msg;
    plot_spherical_map_msg.request.experiment_id = experiment_data.getGoalData().experiment_id;
    ROS_INFO("Plotting spherical score map for experiment %s", plot_spherical_map_msg.request.experiment_id.c_str());
    if (plotSphericalMapClient.call(plot_spherical_map_msg)) {
      ROS_INFO("Received response from %s", service_endpoints::PLOT_SPHERICAL_MAP.c_str());
    } else {
      ROS_WARN("Failed calling service: %s", service_endpoints::PLOT_SPHERICAL_MAP.c_str());
    }

    // Reset all pose's execution_attempted flag
    for (auto &sphericalPoseTmp : spherical_data_high.spherical_poses) {
      sphericalPoseTmp.execution_attempted = false;
    }

    // Determine number of attempted images on lower sampled sphere
    int num_attempts_low_sphere = 0;
    for (auto &sphericalPoseTmp : spherical_data_low.spherical_poses) {
      if (sphericalPoseTmp.execution_attempted) {
        ++num_attempts_low_sphere;
      }
    }
    ROS_INFO("number of attempted spots on lower sampled sphere: %i", num_attempts_low_sphere);

    // Run open ended optimization loop where the spots with lower absorption scores are more likely to be chosen as next pose
    // If not all elements in array are set to -1 then there are poses that were not attempted yet
    size_t counter_run = 0;
    size_t num_finished_elements = 0;
    size_t num_reachable_elements = spherical_data_high.num_pixels;
    std::vector<std::vector<float>> poseProbabilites;
    // Consider user input for early stopping count
    // substract num of images that were already acquired in phase 1
    int early_stopping_count = std::min(goal->early_stopping_count - num_attempts_low_sphere, spherical_data_high.num_pixels);
    ROS_INFO("Starting trajectory optimization loop with early stopping count %i", early_stopping_count);
    ROS_INFO("Maximum number of spots on sphere %i", spherical_data_high.num_pixels);
    while (num_finished_elements < early_stopping_count) {
      ++counter_run;

      std::vector<double> weights = getScoreWeights(spherical_data_high);

      // Create discrete distribution with scores vector
      std::discrete_distribution<> distribution(weights.begin(), weights.end());

      ROS_INFO("probabilities of indices:");
      const auto &probabilities = distribution.probabilities();
      for (auto &sphericalPoseTmp : spherical_data_high.spherical_poses) {
        if (!sphericalPoseTmp.execution_attempted) {
          ROS_INFO("index probability score: %u %.3f%% %i", sphericalPoseTmp.index,
                   probabilities.at(sphericalPoseTmp.index) * 100.0, sphericalPoseTmp.score);
        }
      }

      // Sample from discrete probability distribution
      auto nextIndex = distribution(rg_engine);
      ROS_INFO("Probability distribution next index: %i", nextIndex);

      auto &sphericalPoseHighestScore = spherical_data_high.spherical_poses.at(nextIndex);

      // Skip if execution was already attempted - no need to acquire two images for same pose
      if (!sphericalPoseHighestScore.execution_attempted) {
        ROS_INFO("Choosing index %i - %i with score %i as next goal", sphericalPoseHighestScore.num_sides,
                 sphericalPoseHighestScore.index, sphericalPoseHighestScore.score);

        sphericalPoseHighestScore.success_execute = false;
        bool successAcquire = acquireAndCalibrateImage(sphericalPoseHighestScore, image_id_to_spherical_pose, action_feedback);
        if (!successAcquire) {
          action_result.success = false;
          onlineExperimentServer->setAborted(action_result);
          return;
        }
        ROS_INFO("exec att %u succ exec %u", sphericalPoseHighestScore.execution_attempted,
                 sphericalPoseHighestScore.success_execute);
        // If image couldn't be calibrated / was blacklisted, no need to continue
        if (!sphericalPoseHighestScore.success_execute) {
          ROS_WARN("Could not plan to pose %i or calibrate image %i. Continuing with next spherical index",
                   sphericalPoseHighestScore.index, sphericalPoseHighestScore.image_id);
          sphericalPoseHighestScore.execution_attempted = true;
          --num_reachable_elements;

          num_finished_elements = countSuccessfulAcquisitions(spherical_data_high);
          ROS_INFO("number of finished spots on sphere: %u", num_finished_elements);
          ROS_INFO("total number of reachable spots on sphere: %u", num_reachable_elements);
          continue;
        }

        // alternative 1: reconstruction
        roaxdt_msgs::ComputedTomographyGoal computedTomographyGoal;
        experiment_data.populateComputedTomography(computedTomographyGoal);

        computedTomographyClient->sendGoal(computedTomographyGoal);
        computedTomographyClient->waitForResult();
        ROS_INFO("Sent request to computed_tomography server. Waiting for result...");
        auto computedTomographyErrorResult = computedTomographyClient->getResult();
        ROS_INFO("Recevied result from computed tomography server");

        roaxdt_msgs::SegmentVolume segment_volume_srv;
        experiment_data.populateSegmentVolume(segment_volume_srv);
        if (segmentVolumeClient.call(segment_volume_srv)) {
          ROS_INFO("Received response from %s", service_endpoints::SEGMENT_VOLUME.c_str());
        } else {
          ROS_ERROR("Failed calling service: %s", service_endpoints::SEGMENT_VOLUME.c_str());
          onlineExperimentServer->setAborted();
          return;
        }

        roaxdt_msgs::ComputePoseWeight compute_pose_weight_srv;
        experiment_data.populateComputePoseWeight(compute_pose_weight_srv);
        // Recalculate all scores, since reconstruction changed after adding another image to pool
        std::vector<int> ids_tmp(totalImageCounter);
        std::iota(std::begin(ids_tmp), std::end(ids_tmp), 0);
        ROS_INFO("totalimage counter: %i", totalImageCounter);
        ROS_INFO("image ids size: %u", ids_tmp.size());
        compute_pose_weight_srv.request.image_ids = ids_tmp;
        if (computePoseWeightClient.call(compute_pose_weight_srv)) {
          ROS_INFO("Received response from %s", service_endpoints::COMPUTE_POSE_WEIGHT.c_str());
          bool success_recalculate = recalculateScores(compute_pose_weight_srv.response.scores, sphere_mapping_low_to_high,
                                                       image_id_to_spherical_pose, spherical_data_low, spherical_data_high);
          if (!success_recalculate) {
            ROS_ERROR("Failed recalculating scores!");
            onlineExperimentServer->setAborted();
            return;
          }
        } else {
          ROS_ERROR("Failed calling service: %s", service_endpoints::COMPUTE_POSE_WEIGHT.c_str());
          onlineExperimentServer->setAborted();
          return;
        }

        // Plot scores
        if ((totalImageCounter % 20) == 0) {
          roaxdt_msgs::PlotSphericalMap plot_spherical_map_msg;
          plot_spherical_map_msg.request.experiment_id = experiment_data.getGoalData().experiment_id;
          ROS_INFO("Plotting spherical score map for experiment %s", plot_spherical_map_msg.request.experiment_id.c_str());
          if (plotSphericalMapClient.call(plot_spherical_map_msg)) {
            ROS_INFO("Received response from %s", service_endpoints::PLOT_SPHERICAL_MAP.c_str());
          } else {
            ROS_WARN("Failed calling service: %s", service_endpoints::PLOT_SPHERICAL_MAP.c_str());
          }
        }

        // Write probabilities to file
        std::vector<float> probabilities_float(probabilities.begin(), probabilities.end());
        poseProbabilites.push_back(probabilities_float);
        ROS_INFO("Probabalities vector size: %i", probabilities_float.size());
        experiment_data.setPoseProbabilities(poseProbabilites);

        sphericalPoseHighestScore.execution_attempted = true;
        ROS_INFO("Writing experiment data to file");
        roaxdt::experiment::writeOnlineExperimentData(experiment_data);
      }
      ROS_INFO("Finished run no. %u", counter_run);

      num_finished_elements = countSuccessfulAcquisitions(spherical_data_high);
      ROS_INFO("number of finished spots on sphere: %u", num_finished_elements);
      ROS_INFO("total number of reachable spots on sphere: %u", num_reachable_elements);
    }
  }

  if (goal->run_computed_tomography) {
    roaxdt_msgs::ComputedTomographyGoal computedTomographyGoal;
    experiment_data.populateComputedTomography(computedTomographyGoal);

    computedTomographyClient->sendGoal(computedTomographyGoal);
    computedTomographyClient->waitForResult();
    ROS_INFO("Sent request to computed_tomography server. Waiting for result...");
    auto computedTomographyErrorResult = computedTomographyClient->getResult();
    ROS_INFO("Recevied result from computed tomography server");
  }

  action_result.success = true;
  action_result.experiment_id = action_feedback.experiment_id;
  onlineExperimentServer->setSucceeded(action_result);

  // cleanup
  experiment_data = OnlineExperimentData();
}

bool OnlineExperiment::acquireAndCalibrateImage(SphericalPose &sphericalPose,
                                                std::map<int, SphericalPose> &image_id_to_spherical_pose,
                                                roaxdt_msgs::OnlineExperimentFeedback &action_feedback) {
  roaxdt_msgs::GenerateWaypointTrajectory generate_waypoint_trajectory_srv;
  experiment_data.populateGenerateWaypointTrajectory(generate_waypoint_trajectory_srv, sphericalPose.num_sides,
                                                     sphericalPose.index);
  if (generateWaypointTrajectoryClient.call(generate_waypoint_trajectory_srv)) {
    ROS_INFO("Succeeded planning to spherical pose index %i", generate_waypoint_trajectory_srv.request.spherical_pose_index);
    ROS_INFO("Received response from %s", service_endpoints::GENERATE_WAYPOINT_TRAJECTORY.c_str());
    if (generate_waypoint_trajectory_srv.response.success) {
      experiment_data.addExperimentPose(generate_waypoint_trajectory_srv.response.experiment_pose);

      roaxdt_msgs::SampleAcquisitionGoal sampleAcquisitionGoal;
      sampleAcquisitionGoal.experiment_path = experiment_data.getExperimentPath().string();
      sampleAcquisitionGoal.experiment_id = experiment_data.getGoalData().experiment_id;
      sampleAcquisitionGoal.experiment_poses = {generate_waypoint_trajectory_srv.response.experiment_pose};
      sampleAcquisitionGoal.spheres = experiment_data.getSampleHolderSpheres();
      sampleAcquisitionGoal.forward_projection = experiment_data.getGoalData().simulate;
      sampleAcquisitionGoal.trajectories = {generate_waypoint_trajectory_srv.response.trajectory};
      sampleAcquisitionGoal.start_states = {generate_waypoint_trajectory_srv.response.start_state};

      sphericalPose.execution_attempted = true;
      sampleAcquisitionClient->sendGoal(sampleAcquisitionGoal);
      bool success_sample_acquisition = sampleAcquisitionClient->waitForResult();
      ROS_INFO("Sent request to sample_acquisition server. Waiting for result...");
      if (!success_sample_acquisition) {
        ROS_ERROR("Sample Acquisition failed. Returning error!");
        return false;
      }
      auto sampleAcquisitionResult = sampleAcquisitionClient->getResult();

      experiment_data.getSampleHolderPoses().push_back(sampleAcquisitionResult->sample_holder_poses.at(0));
      experiment_data.getSuccessExecute().push_back(sampleAcquisitionResult->success_execute.at(0));
      roaxdt::experiment::writeOnlineExperimentData(experiment_data); // checkpoint

      roaxdt_msgs::RegisterImagesGoal registerImagesGoal;
      std::vector<int> images_to_process = {static_cast<int>(totalImageCounter)};
      experiment_data.populateRegisterImages(registerImagesGoal, images_to_process);

      registerImagesClient->sendGoal(registerImagesGoal);
      registerImagesClient->waitForResult();
      ROS_INFO("Sent request to register_images server. Waiting for result...");
      auto registerImagesResult = registerImagesClient->getResult();
      experiment_data.addSpherePixelsDetected(registerImagesResult->sphere_pixels_detected,
                                              registerImagesResult->num_sphere_pixels_per_image.at(0));
      ROS_INFO("Recevied result from register_images server");
      ROS_INFO("sphere pixels detected size: %i", registerImagesResult->sphere_pixels_detected.size());

      ROS_INFO("Writing received detected sphere pixel coordinates to metadata file");
      roaxdt::experiment::writeOnlineExperimentData(experiment_data); // checkpoint

      std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
      experiment_data.resizeGeometryMatrices(); // TODO

      std::string calibrationCostsIndex = std::to_string(experiment_data.getGoalData().num_spheres_random_sampling) + "-" +
                                          std::to_string(experiment_data.getGoalData().helix_sampling_rate);
      ROS_INFO("Calibration costs index: %s", calibrationCostsIndex.c_str());
      auto costElement = experiment_data.getCalibrationCosts().find(calibrationCostsIndex);
      if (costElement == experiment_data.getCalibrationCosts().end()) {
        experiment_data.getCalibrationCosts().insert({calibrationCostsIndex, {}});
      }
      experiment_data.getCalibrationCosts().at(calibrationCostsIndex).resize(totalImageCounter + 1);
      std::string calibrationPath = (fs::path(calibrated_path) / fs::path(calibrationCostsIndex)).string();

      ROS_INFO_STREAM("blacklisted images size:" << experiment_data.getBlacklistedImages().size());

      if (sampleAcquisitionResult->success == 1) {
        roaxdt_msgs::CalibrationGoal calibrationGoal;
        experiment_data.populateCalibration(calibrationGoal, totalImageCounter, costThresholdPerSphere);

        calibrationClient->sendGoal(calibrationGoal);
        ROS_INFO_STREAM("Sent request to calibration server #" << calibrationClientID << ". Waiting for result...");
        calibrationClient->waitForResult();
        auto calibrationResult = calibrationClient->getResult();

        auto index_matrix = totalImageCounter * 9;
        auto index_vec = totalImageCounter * 3;
        for (size_t index_tmp = 0; index_tmp < 9; index_tmp++) {
          experiment_data.getRotationMatrices().at(index_matrix + index_tmp) = calibrationResult->rotation_matrix.at(index_tmp);
        }
        for (size_t index_tmp = 0; index_tmp < 3; index_tmp++) {
          experiment_data.getTranslationVectors().at(index_vec + index_tmp) = calibrationResult->translation_vector.at(index_tmp);
          experiment_data.getCameraCenterVectors().at(index_vec + index_tmp) =
              calibrationResult->camera_center_vector.at(index_tmp);
        }

        if (calibrationResult->blacklist_image) {
          experiment_data.getBlacklistedImages().push_back(totalImageCounter);
        } else { // all acquisition steps were successful
          sphericalPose.success_execute = true;
        }

        experiment_data.getCalibrationCosts().at(calibrationCostsIndex).at(totalImageCounter) =
            calibrationResult->calibration_cost;

        ROS_INFO_STREAM("Recevied result from calibration server #" << calibrationClientID);

        action_feedback.image_id = totalImageCounter;
        action_feedback.directory = calibrationPath;

        onlineExperimentServer->publishFeedback(action_feedback);
      } else {
        experiment_data.getBlacklistedImages().push_back(totalImageCounter);
      }

      ROS_INFO("Writing received calibration results to metadata file");
      roaxdt::experiment::writeOnlineExperimentData(experiment_data);

      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      ROS_INFO_STREAM("Calibration took " << std::chrono::duration_cast<std::chrono::hours>(end - begin).count() << ":"
                                          << std::chrono::duration_cast<std::chrono::minutes>(end - begin).count() << " [h:min]");

      sphericalPose.image_id = totalImageCounter;
      image_id_to_spherical_pose.insert({totalImageCounter, sphericalPose});
      totalImageCounter += 1; // only increase if planning and acquisition were successful
      return true;
    } else {
      ROS_WARN("Failed planning to spherical pose index %i", generate_waypoint_trajectory_srv.request.spherical_pose_index);
      return true;
    }

  } else {
    ROS_ERROR("Failed calling service: %s", service_endpoints::GENERATE_WAYPOINT_TRAJECTORY.c_str());
    onlineExperimentServer->setAborted();
    return false;
  }
}

bool OnlineExperiment::recalculateScores(const std::vector<float> &scores,
                                         std::map<int, std::vector<int>> &sphere_mapping_low_to_high,
                                         std::map<int, SphericalPose> &image_id_to_spherical_pose,
                                         SphericalData &spherical_data_low, SphericalData &spherical_data_high) {
  try {

    // 1
    size_t image_count_scout_scan = 0;
    for (size_t i = 0; i < spherical_data_low.num_pixels; i++) {
      auto &sphericalPose = spherical_data_low.spherical_poses.at(i);
      if (sphericalPose.success_execute) {
        auto score_tmp = scores.at(image_count_scout_scan);
        sphericalPose.score = score_tmp;
        spherical_data_low.pixel_scores.at(i) = static_cast<int>(score_tmp);
        ++image_count_scout_scan;
      }
    }

    // 2
    // initialize new more densely sampled sphere with given scores
    std::map<int, int> sphere_mapping_high_to_low;
    spherical_data_high.pixel_scores.resize(spherical_data_high.num_pixels, 0);
    for (const auto &sphericalMappingPair : sphere_mapping_low_to_high) {
      SphericalPose &sphericalPoseCurrent = spherical_data_low.spherical_poses.at(sphericalMappingPair.first);
      for (auto &sphericalPoseIndex : sphericalMappingPair.second) {
        SphericalPose &sphericalPoseHigherSampling = spherical_data_high.spherical_poses.at(sphericalPoseIndex);
        sphericalPoseHigherSampling.score = sphericalPoseCurrent.score;
        // sphericalPoseHigherSampling.execution_attempted = false;
        spherical_data_high.pixel_scores.at(sphericalPoseIndex) = sphericalPoseCurrent.score;

        ROS_INFO("Setting score %i on index %i - %i on new sphere with index %i - %i", sphericalPoseCurrent.score,
                 sphericalPoseCurrent.num_sides, sphericalPoseCurrent.index, sphericalPoseHigherSampling.num_sides,
                 sphericalPoseHigherSampling.index);
        sphere_mapping_high_to_low.insert({sphericalPoseIndex, sphericalMappingPair.first});
      }
    }

    // 3
    // for all scores after scout scan redo update step on new sphere
    ROS_INFO("image count scout scan %i", image_count_scout_scan);
    ROS_INFO("scores size %i", scores.size());
    for (size_t i = image_count_scout_scan; i < scores.size(); ++i) {
      // this object is not used as it is not a reference and might containt outdated information
      ROS_INFO("image id %i", i);
      SphericalPose &sphericalPoseTmp = image_id_to_spherical_pose.at(i);
      ROS_INFO("spherical pose index %i", sphericalPoseTmp.index);
      auto &current_pose_index = sphericalPoseTmp.index;
      ROS_INFO("current pose index %i", current_pose_index);
      SphericalPose &sphericalPoseCurrent = spherical_data_high.spherical_poses.at(current_pose_index);
      if (sphere_mapping_high_to_low.find(sphericalPoseCurrent.index) != sphere_mapping_high_to_low.end()) {
        auto indexOnLowerSampledSphere = sphere_mapping_high_to_low[sphericalPoseCurrent.index];
        ROS_INFO("index on lower sampled sphere %i", indexOnLowerSampledSphere);
        ROS_INFO("Index %i - %i maps to %i - %i on lower sampled sphere", sphericalPoseCurrent.num_sides,
                 sphericalPoseCurrent.index, spherical_data_low.num_sides, indexOnLowerSampledSphere);

        // update neighborhood of pose on higher sampled sphere with new score
        roaxdt_msgs::SampleSphereDisk sample_sphere_disk_msg;
        sample_sphere_disk_msg.request.num_sides = spherical_data_high.num_sides;
        sample_sphere_disk_msg.request.spherical_index = sphericalPoseCurrent.index;
        sample_sphere_disk_msg.request.disk_radius_radians = 5.0f;
        if (sphereSamplerDiskClient.call(sample_sphere_disk_msg)) {
          ROS_INFO("Received response from %s for index %i - %i", service_endpoints::SAMPLE_SPHERE_DISK.c_str(),
                   sphericalPoseCurrent.num_sides, indexOnLowerSampledSphere);

          for (auto &sphericalPoseIndex : sample_sphere_disk_msg.response.spherical_pose_indices_new) {
            SphericalPose &sphericalPoseHigherSampling = spherical_data_high.spherical_poses.at(sphericalPoseIndex);
            // Only set if pose was not already attempted
            if (!sphericalPoseHigherSampling.execution_attempted) {
              sphericalPoseHigherSampling.score = sphericalPoseCurrent.score;
              spherical_data_high.pixel_scores.at(sphericalPoseIndex) = sphericalPoseCurrent.score;

              ROS_INFO("Setting score %i on index %i - %i on new sphere with index %i - %i", sphericalPoseCurrent.score,
                       sphericalPoseCurrent.num_sides, indexOnLowerSampledSphere, sphericalPoseHigherSampling.num_sides,
                       sphericalPoseHigherSampling.index);
            }
            sphere_mapping_high_to_low.insert({sphericalPoseIndex, indexOnLowerSampledSphere});
          }
        } else {
          ROS_ERROR("Failed calling service: %s", service_endpoints::SAMPLE_SPHERE_DISK.c_str());
          onlineExperimentServer->setAborted();
          return false;
        }
      }
    }
  } catch (const std::exception &e) {
    ROS_ERROR(e.what());
    return false;
  }

  return true;
}

roaxdt_msgs::LoadSampleHolderResultConstPtr OnlineExperiment::loadSampleHolder(const std::string &sample_holder) {
  roaxdt_msgs::LoadSampleHolderGoal loadSampleHolderGoal;
  loadSampleHolderGoal.sample_holder_name = sample_holder;

  loadSampleHolderClient->sendGoal(loadSampleHolderGoal);
  loadSampleHolderClient->waitForResult();
  ROS_INFO("Sent request to load_sample_holder server. Waiting for result...");

  return loadSampleHolderClient->getResult();
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::ONLINE_EXPERIMENT);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::OnlineExperiment node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}