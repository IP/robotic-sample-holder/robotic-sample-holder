#include "roaxdt_core/CameraStatePublisher.h"

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using json = nlohmann::json;

CameraStatePublisher::CameraStatePublisher(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
  this->publishState();
}

void CameraStatePublisher::init() {
  // load config values
  try {
    roaxdt::configuration::readConfigParameter<std::string>(node_handle, IMAGE_PLANE_FRAME_SUFFIX, image_plane_frame_suffix);
    roaxdt::configuration::readConfigParameter<std::string>(node_handle, CAMERA_LINK_SUFFIX, camera_link_suffix);
    roaxdt::configuration::readConfigParameter<std::string>(node_handle, CAMERA_INFO_SUFFIX, camera_info_suffix);
    roaxdt::configuration::readConfigParameter<std::string>(node_handle, DETECTOR_CAMERA_NAME, detector_camera_name);
  } catch (const std::runtime_error &exception) { ROS_ERROR_STREAM(exception.what()); }

  if (!this->private_node_handle.getParam(CAMERA_ID, camera_id)) {
    throw std::runtime_error("Parameter \"" + CAMERA_ID +
                             "\" was not set when starting script! Specify which camera you are starting!");
  }

  if (!this->private_node_handle.getParam(CAMERA_TYPE, camera_type)) {
    throw std::runtime_error("Parameter \"" + CAMERA_TYPE +
                             "\" was not set when starting script! Specify what kind of camera you are starting!");
  }

  // load depthcamera config
  // The config for now consists of the transform of the depth camera relative to the /world coord. system
  fs::path camera_file = roaxdt::filesystem::readDepthCameraConfiguration(camera_id + ".json");

  ROS_INFO_STREAM("Reading file: " << camera_file.string());

  camera_json = roaxdt::filesystem::readJsonFile(camera_file);
  transform_camera = roaxdt::ros_utils::extractCameraPoseFromJson(camera_json);

  // also extract image plane transform
  if (camera_json.find(CAMERA_IMAGE_PLANE_FIELD) != camera_json.end()) {
    transform_image_plane = roaxdt::ros_utils::extractCameraPoseFromJson(camera_json["image_plane"]);
  }

  child_frame = camera_id + camera_link_suffix;
  child_frame_image_plane = camera_id + image_plane_frame_suffix;
  camera_info_topic = "/" + camera_id + camera_info_suffix;

  // camera info only needed in the laboratory
  switch (camera_type) {
  case CAMERA_TYPES::DETECTOR_CAMERA:
    try {
      const json info_json = camera_json[CAMERA_INFO_FIELD];

      const float pixel_size_x = info_json[camera_info::PIXEL_SIZE_X].get<float>();
      const float pixel_size_y = info_json[camera_info::PIXEL_SIZE_X].get<float>();
      const float area_size_x = info_json[camera_info::AREA_SIZE_X].get<float>();
      const float area_size_y = info_json[camera_info::AREA_SIZE_Y].get<float>();
      const int resolution_x = info_json[camera_info::RESOLUTION_X].get<int>();
      const int resolution_y = info_json[camera_info::RESOLUTION_Y].get<int>();
      const float focal_length = info_json[camera_info::FOCAL_LENGTH].get<float>();
      const float principal_point_x = info_json[camera_info::PRINCIPAL_POINT_X].get<float>();
      const float principal_point_y = info_json[camera_info::PRINCIPAL_POINT_Y].get<float>();

      Eigen::Matrix3f camera_intrinsic_K =
          roaxdt::image_utils::getIntrinsicMatrix(pixel_size_x, pixel_size_y, focal_length, principal_point_x, principal_point_y);

      camera_info.K = {camera_intrinsic_K(0, 0), camera_intrinsic_K(0, 1), camera_intrinsic_K(0, 2),
                       camera_intrinsic_K(1, 0), camera_intrinsic_K(1, 1), camera_intrinsic_K(1, 2),
                       camera_intrinsic_K(2, 0), camera_intrinsic_K(2, 1), camera_intrinsic_K(2, 2)};

      camera_info.P = {camera_intrinsic_K(0, 0), camera_intrinsic_K(0, 1), camera_intrinsic_K(0, 2), 0.0,
                       camera_intrinsic_K(1, 0), camera_intrinsic_K(1, 1), camera_intrinsic_K(1, 2), 0.0,
                       camera_intrinsic_K(2, 0), camera_intrinsic_K(2, 1), camera_intrinsic_K(2, 2), 0.0};

      camera_info.width = resolution_x;
      camera_info.height = resolution_y;

      camera_info.header.frame_id = child_frame;

      ROS_INFO_STREAM("camera intrinsic matrix: " << std::endl << camera_intrinsic_K);
    } catch (const std::exception &ex) {
      throw std::runtime_error(std::string("error when extracting camera info: ") + ex.what());
    }
    break;

  default:
    break;
  }

  camera_info_pub = node_handle.advertise<sensor_msgs::CameraInfo>(camera_info_topic, 1);
}

void CameraStatePublisher::publishState() {
  ros::Rate rate(10.0);
  while (node_handle.ok()) {
    tf_broadcaster.sendTransform(tf::StampedTransform(transform_camera, ros::Time::now(), "world", child_frame));

    if (camera_json.find(CAMERA_IMAGE_PLANE_FIELD) != camera_json.end()) {
      tf_broadcaster.sendTransform(
          tf::StampedTransform(transform_image_plane, ros::Time::now(), child_frame, child_frame_image_plane));
    }

    if (camera_json.find(CAMERA_INFO_FIELD) != camera_json.end()) {
      camera_info_pub.publish(camera_info);
    }

    rate.sleep();
  }
  rate.sleep();
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  namespace fs = boost::filesystem;

  ros::init(argc, argv, roaxdt::constants::nodes::CAMERA_STATE_PUBLISHER);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");
  roaxdt::nodes::CameraStatePublisher node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}