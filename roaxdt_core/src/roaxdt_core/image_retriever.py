import rospy
import roaxdt_msgs.srv

import cv2
import base64
import os
import time


class ImageRetriever:

    def __init__(self):
        self._retrieve_image_service = rospy.Service('/retrieve_image', roaxdt_msgs.srv.RetrieveImage, self.execute_cb)
        rospy.loginfo("Started /retrieve_image service")

    def execute_cb(self, request):
        rospy.loginfo("Executing request for /retrieve_image with parameters:")
        rospy.loginfo("image_id: %i", request.image_id)
        rospy.loginfo("experiment_path: %s", request.experiment_path)
        rospy.loginfo("directory: %s", request.directory)

        image_filename = str(request.image_id) + ".png"
        image_directory_path = os.path.join(request.experiment_path, request.directory)
        image_path = os.path.join(image_directory_path, image_filename)

        response = roaxdt_msgs.srv.RetrieveImageResponse()

        try:
            while not os.path.exists(image_path):
                time.sleep(0.5)

            image = cv2.imread(image_path)
            image_resized = cv2.resize(image, (700, 700))
            retval, buffer_img = cv2.imencode('.png', image_resized)
            image_base64 = base64.b64encode(buffer_img).decode("utf-8")
            # image_base64 = None
            # with open(image_path, "rb") as img_file:
            #     image_base64 = base64.b64encode(img_file.read()).decode('utf-8')

            response.image_base64 = image_base64
        except Exception as exception:
            rospy.logerr("Could not process image id %i in /retrieve_image", request.image_id)
            rospy.logerr(exception)

        rospy.loginfo("/retrieve_image service call successful.")

        return response
