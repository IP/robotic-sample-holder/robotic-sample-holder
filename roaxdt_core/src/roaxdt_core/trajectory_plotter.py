from posixpath import basename
import rospy
import roaxdt_msgs.srv
import roaxdt_msgs.msg

import os
import numpy as np
import json
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
import healpy
from scipy.spatial.transform import Rotation

METADATA_FILE = 'metadata.json'
PLOT_NAME_LOW = 'spherical_plot-low'
PLOT_NAME_HIGH = 'spherical_plot-high'


class TrajectoryPlotter:

    def __init__(self):
        self.trajectories_path_key = '/roaxdt/trajectories/path'
        self.experiments_path_key = '/roaxdt/experiments/path'
        self.default_image_file_type_key = '/roaxdt/experiments/default_image_file_type'

        # retrieve config params
        self.trajectories_path = rospy.get_param(self.trajectories_path_key)
        self.experiments_path = rospy.get_param(self.experiments_path_key)
        self.plots_path = self.trajectories_path  # TODO change this
        self.default_image_file_type = rospy.get_param(self.default_image_file_type_key)
        self.trajectory_file_type = ".json"

        # set up watchdog on trajectory files for generating plot when planning is finished
        self.event_handler = PatternMatchingEventHandler(
            patterns=["*" + self.trajectory_file_type], ignore_patterns=[], ignore_directories=True)
        self.event_handler.on_any_event = self.on_any_event
        self.observer = Observer()
        self.observer.schedule(self.event_handler, path=self.trajectories_path, recursive=False)
        self.observer.start()

        self._plot_spherical_map_service = rospy.Service(
            '/plot_spherical_map', roaxdt_msgs.srv.PlotSphericalMap, self.plot_spherical_map_cb)
        rospy.loginfo("Started /plot_spherical_map service")

    def on_any_event(self, event):
        base_name = os.path.basename(event.src_path)
        if (event.event_type == 'created' or event.event_type == 'modified'):
            rospy.loginfo("Detected file change for %s", base_name)

            filename_split = base_name.split('.')
            if len(filename_split) != 2:
                rospy.logerr("Couldn't retrieve trajectory ID from filename %s" % base_name)
                return

            trajectory_id = filename_split[0]
            rospy.loginfo("Generating plot for trajectory %s", trajectory_id)

            success_plot = self.plotTrajectory(event.src_path, trajectory_id)
            if not success_plot:
                rospy.logerr("Couldn't create plot for trajectory ID %s at path %s" % (trajectory_id, event.src_path))

    def plotTrajectory(self, file_path, trajectory_id):
        if os.path.isfile(file_path):
            trajectory_data = None
            try:
                with open(file_path, "r", encoding='utf-8') as trajectory_file:
                    trajectory_data = json.load(trajectory_file)
            except OSError:
                print("error reading json file ", file_path)
                return False

            plt.figure(figsize=(14, 8))
            m = Basemap(projection='moll', lon_0=0, resolution='c')
            # draw parallels and meridians.
            m.drawparallels(np.arange(-90., 120., 30.))
            m.drawmeridians(np.arange(0., 420., 60.))
            m.drawmapboundary(fill_color='white')

            if "spherical_pose_metadata_vec" in trajectory_data:
                print("%s is a spherical trajectory" % trajectory_id)

                spherical_pose_metadata_vec = trajectory_data["spherical_pose_metadata_vec"]
                if len(spherical_pose_metadata_vec) == 0:
                    print("no spherical pose metadata in trajectory %s. Exiting" % trajectory_id)
                    return False

                spherical_metadata = spherical_pose_metadata_vec[0]
                # num_pixels = spherical_metadata['num_pixels']
                num_sides = spherical_metadata['num_sides']
                pose_indices = spherical_metadata['pose_indices']

                x_success = []
                y_success = []
                colors = []
                for index in pose_indices:
                    theta, phi = healpy.pix2ang(num_sides, index, lonlat=True)
                    x_tmp, y_tmp = m(theta, phi)

                    colors.append(1)
                    x_success.append(x_tmp)
                    y_success.append(y_tmp)

                scatter = m.scatter(x_success, y_success, c="#F608FF", marker="o", s=12)
            else:
                print("%s is a circular trajectory" % trajectory_id)

                experiment_poses = trajectory_data["experiment_poses"]
                if len(experiment_poses) == 0:
                    print("no experiment poses metadata in trajectory %s. Exiting" % trajectory_id)
                    return False

                num_pixels = len(experiment_poses)
                x_success = []
                y_success = []
                counter_success = 0
                for pose in experiment_poses:
                    quat_tmp = pose["orientation"]
                    R = Rotation.from_quat([quat_tmp["x"], quat_tmp["y"], quat_tmp["z"], quat_tmp["w"]]).as_matrix()

                    rotvec = np.matmul(R, np.array([1.0, 0.0, 0.0]))
                    theta, phi = healpy.vec2ang(rotvec, lonlat=True)
                    x_tmp, y_tmp = m(theta, phi)

                    x_success.append(x_tmp)
                    y_success.append(y_tmp)
                    counter_success += 1

                scatter = m.scatter(x_success, y_success, color='b', marker="x",
                                    label='success: %i' % (counter_success), s=14)
                print("number of poses: ", len(experiment_poses))

            # plt.legend(fontsize=14, markerscale=1.8)
            plot_filename = trajectory_id + self.default_image_file_type
            plot_path = os.path.join(self.plots_path, plot_filename)
            rospy.loginfo("Saving plot for trajectory %s at path %s" % (trajectory_id, plot_path))
            plt.savefig(plot_path, dpi=300, bbox_inches='tight')
            plt.close()

            return True

        else:
            rospy.logerr("Couldn't read trajectory file.")
            return False

    def plot_spherical_map_cb(self, request):
        rospy.loginfo("Executing request for /plot_spherical_map for experiment: %s", request.experiment_id)

        experiment_path = os.path.join(self.experiments_path, request.experiment_id)
        metadata_path = os.path.join(experiment_path, METADATA_FILE)
        experiment_data = None
        if os.path.isfile(metadata_path):
            try:
                with open(metadata_path, "r", encoding='utf-8') as experiment_file:
                    experiment_data = json.load(experiment_file)
            except OSError:
                print("error reading json file ", metadata_path)
                return False

        # low sampled sphere plot
        plt.figure(figsize=(14, 8))
        m = Basemap(projection='moll', lon_0=0, resolution='c')
        # draw parallels and meridians.
        m.drawparallels(np.arange(-90., 120., 30.))
        m.drawmeridians(np.arange(0., 420., 60.))
        m.drawmapboundary(fill_color='white')

        num_pixels = experiment_data['spherical_data_low']['num_pixels']
        num_sides = healpy.npix2nside(num_pixels)

        x_success = []
        y_success = []
        colors = []
        for spherical_pose in experiment_data['spherical_data_low']['spherical_poses']:
            if spherical_pose['execution_attempted'] and spherical_pose['success_execute']:
                index = spherical_pose['index']
                theta, phi = healpy.pix2ang(num_sides, index, lonlat=True)
                x_tmp, y_tmp = m(theta, phi)

                colors.append(spherical_pose['score'])
                x_success.append(x_tmp)
                y_success.append(y_tmp)

        scatter = m.scatter(x_success, y_success, c=colors, marker="o", s=12, cmap="cool")

        cbar = plt.colorbar(scatter, fraction=0.022, pad=0.02)
        for t in cbar.ax.get_yticklabels():
            t.set_fontsize(20)
        plot_filename = PLOT_NAME_LOW + self.default_image_file_type
        plot_path = os.path.join(experiment_path, plot_filename)
        rospy.loginfo("Saving plot for experiment %s at path %s" % (request.experiment_id, plot_path))
        plt.savefig(plot_path, dpi=300, bbox_inches='tight')
        plt.close()

        # high sampled sphere plot
        plt.figure(figsize=(14, 8))
        m = Basemap(projection='moll', lon_0=0, resolution='c')
        # draw parallels and meridians.
        m.drawparallels(np.arange(-90., 120., 30.))
        m.drawmeridians(np.arange(0., 420., 60.))
        m.drawmapboundary(fill_color='white')

        num_pixels = experiment_data['spherical_data_high']['num_pixels']
        num_sides = healpy.npix2nside(num_pixels)

        x_success = []
        y_success = []
        colors = []
        for spherical_pose in experiment_data['spherical_data_high']['spherical_poses']:
            if spherical_pose['execution_attempted'] and spherical_pose['success_execute']:
                index = spherical_pose['index']
                theta, phi = healpy.pix2ang(num_sides, index, lonlat=True)
                x_tmp, y_tmp = m(theta, phi)

                colors.append(spherical_pose['score'])
                x_success.append(x_tmp)
                y_success.append(y_tmp)

        scatter = m.scatter(x_success, y_success, c=colors, marker="o", s=12, cmap="cool")

        cbar = plt.colorbar(scatter, fraction=0.022, pad=0.02)
        for t in cbar.ax.get_yticklabels():
            t.set_fontsize(20)
        plot_filename = PLOT_NAME_HIGH + self.default_image_file_type
        plot_path = os.path.join(experiment_path, plot_filename)
        rospy.loginfo("Saving plot for experiment %s at path %s" % (request.experiment_id, plot_path))
        plt.savefig(plot_path, dpi=300, bbox_inches='tight')
        plt.close()

        response = roaxdt_msgs.srv.PlotSphericalMapResponse()

        rospy.loginfo("/plot_spherical_map service call successful.")

        return response
