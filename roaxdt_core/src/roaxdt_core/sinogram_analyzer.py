import rospy
import roaxdt_msgs.srv

import cv2
import os
import glob
import numpy as np
import json
import math


class SinogramAnalyzer:

    def __init__(self):
        self.experiments_path_key = '/roaxdt/experiments/path'
        self.processed_path_key = '/roaxdt/experiments/processed_path'
        self.calibrated_path_key = '/roaxdt/experiments/calibrated_path'
        self.sinogram_analysed_path_key = '/roaxdt/experiments/sinogram_analysed_path'

        # retrieve config params
        self.experiments_path = rospy.get_param(self.experiments_path_key)
        self.processed_directory_name = rospy.get_param(self.processed_path_key)
        self.calibrated_directory_name = rospy.get_param(self.calibrated_path_key)
        self.output_directory_name = rospy.get_param(self.sinogram_analysed_path_key)

        self._analyze_sinograms_service = rospy.Service(
            '/analyze_sinograms', roaxdt_msgs.srv.AnalyzeSinograms, self.execute_cb)
        rospy.loginfo("Started /analyze_sinograms service")

    def execute_cb(self, request):
        experiment_path = os.path.join(self.experiments_path, request.experiment_id)
        calibrated_images_path = os.path.join(experiment_path, self.calibrated_directory_name)
        output_images_path = os.path.join(experiment_path, self.output_directory_name)
        if not os.path.exists(output_images_path):
            os.makedirs(output_images_path)

        metadata_file_name = 'metadata.json'
        metadata_path = os.path.join(self.experiments_path, request.experiment_id, metadata_file_name)
        experiment_metadata = None
        try:
            with open(metadata_path, "r") as metadata_file:
                experiment_metadata = json.load(metadata_file)
        except:
            print("error reading json file ", metadata_path)

        rospy.loginfo("Executing request for /analyze_sinograms with parameters:")
        rospy.loginfo("experiment_id: %s", request.experiment_id)

        intrinsic_matrix = experiment_metadata["camera_matrix"]
        rotation_matrices = experiment_metadata["rotation_matrices"]
        translation_vectors = experiment_metadata["translation_vectors"]
        blacklisted_images = experiment_metadata["blacklisted_images"]
        success_execute = experiment_metadata["success_execute"]
        # needed for reading drawing input images later
        calibration_subfolder = next(iter(experiment_metadata["calibration_costs"]))

        percentile_mark = 99.5

        image_directory_path = os.path.join(
            self.experiments_path, request.experiment_id, self.processed_directory_name)
        detector_images_matcher = os.path.join(image_directory_path, "*.png")
        detector_images_paths = glob.glob(detector_images_matcher)

        # sort in ascending order
        detector_images_paths.sort(key=lambda x: int(os.path.basename(x).split('.')[0]))

        blacklisted_images_new = []
        whitelisted_images_new = []
        for image_path in detector_images_paths:
            image_basename = os.path.basename(image_path)
            image_id = int(image_basename.split('.')[0])
            output_image_path = os.path.join(output_images_path, image_basename)

            if not image_id in blacklisted_images and success_execute[image_id] != 0:
                rospy.loginfo("image: %s", image_path)

                # For drawing we use the calibration output image
                drawing_input_image_path = os.path.join(
                    calibrated_images_path, calibration_subfolder, image_basename)

                K = np.array(intrinsic_matrix).reshape((3, 3))
                R = np.array(rotation_matrices[image_id]).reshape((3, 3))
                t = np.array(translation_vectors[image_id]).reshape((3, 1))
                T = np.hstack((R.reshape((3, 3)), t.reshape((3, 1))))
                P = np.matmul(K, T)

                # Place markers on image
                offset = 0.065
                offset += 0.02  # Go up the z-axis by 2cm
                x_range = [-0.025, 0.025]
                x_step = 0.01
                num_x = int((x_range[1] - x_range[0]) / x_step) + 1
                y_range = [0.0, 0.07]
                y_step = 0.01
                num_y = int((y_range[1] - y_range[0]) / y_step) + 1
                points = np.empty((0, 4))
                for y_elem in np.linspace(y_range[0], y_range[1], num=num_y):
                    for x_elem in np.linspace(x_range[0], x_range[1], num=num_x):
                        points = np.vstack((points, np.array([x_elem, 0.0, offset + y_elem, 1.0])))
                points *= 1000
                points[:, 3] = 1.0
                projections_2d = np.matmul(P, points.T)
                # normalize homogenous coordinates
                projections_2d = projections_2d.T
                projections_2d[:, 0] /= projections_2d[:, 2]
                projections_2d[:, 1] /= projections_2d[:, 2]

                img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
                rospy.loginfo("Reading drawing input image: %s", drawing_input_image_path)
                img_color = cv2.imread(drawing_input_image_path)

                # Determine percentile threshold
                ROI_center_index_x = math.floor((num_x) / 2)
                ROI_center_index = ROI_center_index_x + (num_x * math.floor(num_y / 2))
                rospy.loginfo("ROI_center_index: %i", ROI_center_index)
                ROI_center = np.array([projections_2d[ROI_center_index, 0],
                                       projections_2d[ROI_center_index, 1]], dtype=int)
                ROI_window = 500
                img_ROI = img[(ROI_center[1] - ROI_window):(ROI_center[1] +
                                                            ROI_window), (ROI_center[0] - ROI_window):(ROI_center[0] + ROI_window)]
                # Draw ROI on image
                cv2.rectangle(img_color, (ROI_center[0] - ROI_window, ROI_center[1] - ROI_window),
                              (ROI_center[0] + ROI_window, ROI_center[1] + ROI_window), (0, 0, 255), 3)
                ROI_min = np.min(img_ROI)
                ROI_max = np.max(img_ROI)
                percentile_threshold = np.percentile(img_ROI, 99)
                rospy.loginfo("ROI_min: %f", ROI_min)
                rospy.loginfo("ROI_max: %f", ROI_max)
                rospy.loginfo('percentile_threshold: %f', percentile_threshold)

                counter = 0
                window = 12

                num_points = points.shape[0]
                for i in range(num_points):
                    circle_center = np.array([projections_2d[i, 0], projections_2d[i, 1]], dtype=int)
                    img_region = img[(circle_center[1] - window):(circle_center[1] +
                                                                  window), (circle_center[0] - window):(circle_center[0] + window)]
                    img_statistics = np.percentile(img_region, percentile_mark)
                    rospy.loginfo("image statistics for region %i: %i", i, img_statistics)

                    cv2.rectangle(img_color, (circle_center[0] - window, circle_center[1] - window),
                                  (circle_center[0] + window, circle_center[1] + window), (0, 255, 0), 3)
                    if img_statistics < percentile_threshold:
                        counter += 1

                percentage_not_occluded = counter / num_points
                rospy.loginfo('percentage not occluded: %f', percentage_not_occluded)
                if percentage_not_occluded <= 0.6:
                    blacklisted_images_new.append(image_id)
                else:
                    whitelisted_images_new.append(image_id)

                img_color_resized = cv2.resize(img_color, (1000, 1000), interpolation=cv2.INTER_AREA)
                cv2.imwrite(output_image_path, img_color_resized)
                print(output_image_path)

        print("blacklisted images new: ", blacklisted_images_new)
        print("whitelisted images new: ", whitelisted_images_new)

        response = roaxdt_msgs.srv.AnalyzeSinogramsResponse()
        response.blacklisted_image_indices = blacklisted_images_new

        rospy.loginfo("/analyze_sinograms service call successful.")

        return response
