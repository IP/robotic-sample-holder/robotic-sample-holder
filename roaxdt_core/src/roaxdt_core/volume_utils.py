import rospy
import roaxdt_msgs.srv
import roaxdt_msgs.msg

import numpy as np
from skimage.draw import ellipsoid


class VolumeUtils:

    def __init__(self):
        self._place_spheres_service = rospy.Service(
            '/place_spheres', roaxdt_msgs.srv.PlaceSpheres, self.place_spheres_cb)
        rospy.loginfo("Started /place_spheres service")

        self._concatenate_sample_and_holder_service = rospy.Service(
            '/concatenate_sample_and_holder', roaxdt_msgs.srv.ConcatenateSampleAndHolder, self.concatenate_sample_and_holder_cb)
        rospy.loginfo("Started /concatenate_sample_and_holder service")

    def place_spheres_cb(self, request):
        rospy.loginfo("Executing request for /place_spheres with parameters:")
        rospy.loginfo("volume_path: %s", request.volume_path)

        response = roaxdt_msgs.srv.PlaceSpheresResponse()

        try:
            spheres = []
            for sphere_point in request.spheres:
                spheres.append([sphere_point.x * 1000.0, sphere_point.y * 1000.0, sphere_point.z * 1000.0])

            print(request.spheres)
            print(spheres)

            spheres_np = np.array(spheres)
            volume_spacing = request.volume_spacing[0]
            volume_dimensions = np.array([request.volume_dimensions[0] / volume_spacing, request.volume_dimensions[1] /
                                         volume_spacing, request.volume_dimensions[2] / volume_spacing], dtype=int)
            print(volume_dimensions)
            volume_center = np.array([volume_dimensions[0] / 2.0,
                                     volume_dimensions[1] / 2.0, 0.0], dtype=int)
            print("volume center: ", volume_center)
            spheres_np = spheres_np / volume_spacing
            spheres_np = spheres_np + volume_center
            volume_helix_offset = np.array([0.0, 0.0, 15.75 / volume_spacing])
            spheres_np = spheres_np + volume_helix_offset
            print(spheres_np)

            sample_holder_np = np.fromfile(request.volume_path, dtype=np.float32)
            print(sample_holder_np.dtype)
            print(sample_holder_np.shape)
            print(np.max(sample_holder_np))
            print(np.min(sample_holder_np))

            # insert spheres
            sphere_diameter = 2 / volume_spacing
            sphere_radius = int(sphere_diameter / 2.0)
            ellipsoid_mask = ellipsoid(sphere_radius, sphere_radius, sphere_radius)
            for s in range(len(request.spheres)):
                sphere_np = spheres_np[s].astype(int)

                # draw ellipsoid at sphere_np
                for i in range(-sphere_radius, sphere_radius + 1, 1):
                    for j in range(-sphere_radius, sphere_radius + 1, 1):
                        for k in range(-sphere_radius, sphere_radius + 1, 1):
                            if ellipsoid_mask[i + sphere_radius, j + sphere_radius, k + sphere_radius]:
                                index_tmp = sphere_np + np.array([i, j, k])
                                index_3d = np.ravel_multi_index(index_tmp, tuple(volume_dimensions), order='F')
                                sample_holder_np[index_3d] = 20.0

            sample_holder_np.tofile(request.volume_path)

        except Exception as exception:
            rospy.logerr("Could not process volume_file %s in /place_spheres", request.volume_path)
            rospy.logerr(exception)

        rospy.loginfo("/place_spheres service call successful.")

        return response

    def concatenate_sample_and_holder_cb(self, request):
        rospy.loginfo("Executing request for /concatenate_sample_and_holder with parameters:")
        for sample in request.samples:
            rospy.loginfo("sample_path: %s", sample.volume_path)
            rospy.loginfo("sample_dimensions: %f %f %f" %
                          (sample.dimensions[0], sample.dimensions[1], sample.dimensions[2]))
        rospy.loginfo("holder_path: %s", request.holder_path)
        rospy.loginfo("holder_dimensions: %f %f %f" %
                      (request.holder_dimensions[0], request.holder_dimensions[1], request.holder_dimensions[2]))

        response = roaxdt_msgs.srv.ConcatenateSampleAndHolderResponse()

        try:
            head_np = np.fromfile(request.holder_path, dtype=np.float32)
            head_dimensions = np.array([request.holder_dimensions[0] / request.holder_spacing[0], request.holder_dimensions[1] /
                                        request.holder_spacing[1], request.holder_dimensions[2] / request.holder_spacing[2]], dtype=int)

            samples = []
            samples_dimensions = []
            for sample in request.samples:
                sample_np = np.fromfile(sample.volume_path, dtype=np.float32)
                sample_dimensions = np.array([sample.dimensions[0] / sample.spacing[0], sample.dimensions[1] /
                                              sample.spacing[1], sample.dimensions[2] / sample.spacing[2]], dtype=int)
                samples.append(sample_np)
                samples_dimensions.append(sample_dimensions)
                rospy.loginfo("sample initial shape: %s" % ((sample_np.shape,)))
                rospy.loginfo("sample initial dimensions: %f %f %f" %
                              (sample_dimensions[0], sample_dimensions[1], sample_dimensions[2]))

            # Determine padding in z-direction first
            sample_height = 0.0
            for i in range(len(samples_dimensions)):
                height = samples_dimensions[i][2]
                if (height > sample_height):
                    sample_height = height
            rospy.loginfo("sample height: %f" % (sample_height))

            # Add padding to holder volume by sample height
            head_np_resized = head_np.reshape(head_dimensions, order='F')
            head_padded = np.pad(head_np_resized, ((0, 0), (0, 0), (sample_height, 0)),
                                 'constant', constant_values=((0, 0), (0, 0), (0, 0)))

            # Place samples in an empty volume with desired dimensions
            # The placement is determined by the sample center property
            sample_dimensions_desired = (head_dimensions[0], head_dimensions[1], sample_height)
            sample_final = np.zeros(sample_dimensions_desired, dtype=np.float32)
            rospy.loginfo("sample padded final dimensions: %s" % ((sample_final.shape,)))
            for i in range(len(request.samples)):
                sample = request.samples[i]
                center = np.array(sample.center, dtype=float)
                spacing = np.array(sample.spacing, dtype=float)
                coordinates = np.array(center / spacing, dtype=float)
                sample_np_resized = samples[i].reshape(samples_dimensions[i], order='F')
                sample_offset = np.array((samples_dimensions[i] / 2.0), dtype=np.float32)
                volume_offset = (head_dimensions / 2)
                volume_offset[2] = 0
                sample_position = np.floor(np.array(coordinates + volume_offset, dtype=int))
                sample_position_start = np.array(np.ceil(sample_position - sample_offset), dtype=int)
                sample_position_end = np.array(np.ceil(sample_position + sample_offset), dtype=int)

                rospy.logdebug("coordinates: %i %i %i" % (coordinates[0], coordinates[1], coordinates[2]))
                rospy.logdebug("sample offset: %i %i %i" % (sample_offset[0], sample_offset[1], sample_offset[2]))
                rospy.logdebug("vol offset: %i %i %i" % (volume_offset[0], volume_offset[1], volume_offset[2]))
                rospy.logdebug("pos start: %i %i %i" %
                               (sample_position_start[0], sample_position_start[1], sample_position_start[2]))
                rospy.logdebug("pos end: %i %i %i" %
                               (sample_position_end[0], sample_position_end[1], sample_position_end[2]))
                rospy.logdebug("sample shape: %i %i %i" %
                               (sample_np_resized.shape[0], sample_np_resized.shape[1], sample_np_resized.shape[2]))
                rospy.loginfo("sample absorption: %f" % (np.max(sample_np_resized)))
                sample_final[sample_position_start[0]:sample_position_end[0],
                             sample_position_start[1]:sample_position_end[1],
                             sample_position_start[2]:sample_position_end[2]] += sample_np_resized

            # Concatenate volumes
            concatenated_np = np.concatenate((head_padded, sample_final), axis=2)

            # Write new volume to filesystem
            concatenated_np.flatten(order='F').tofile(request.resulting_volume_path)

            response.resulting_volume_dimensions = np.array([concatenated_np.shape[0] * sample.spacing[0], concatenated_np.shape[1] *
                                                             sample.spacing[1], concatenated_np.shape[2] * sample.spacing[2]], dtype=int)
            response.resulting_volume_spacing = request.holder_spacing
            rospy.loginfo("resluting volume dimensions: %f %f %f" %
                          (response.resulting_volume_dimensions[0], response.resulting_volume_dimensions[1], response.resulting_volume_dimensions[2]))

        except Exception as exception:
            rospy.logerr("Could not process /concatenate_sample_and_holder")
            rospy.logerr(exception)

        rospy.loginfo("/concatenate_sample_and_holder service call successful.")

        return response
