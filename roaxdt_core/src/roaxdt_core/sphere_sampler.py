import rospy
import roaxdt_msgs.srv

import healpy as hp
import math
import numpy as np


class SphereSampler:

    def __init__(self):
        self._analyze_sinograms_service = rospy.Service(
            '/sample_sphere', roaxdt_msgs.srv.SampleSphere, self.execute_cb)
        rospy.loginfo("Started /sample_sphere service")

        self._analyze_sinograms_service = rospy.Service(
            '/sample_sphere_disk', roaxdt_msgs.srv.SampleSphereDisk, self.execute_cb_disk)
        rospy.loginfo("Started /sample_sphere_disk service")

    def execute_cb(self, request):
        # Choose pixels with new map of higher order based statistics of small map
        num_sides_old = hp.npix2nside(request.num_pixels)
        num_sides_new = request.num_sides_new
        num_pixels_new = hp.nside2npix(num_sides_new)
        spherical_indices_new = np.arange(hp.nside2npix(num_sides_new))

        rospy.loginfo("spherical map num sides new (healpy): %i", num_sides_new)
        rospy.loginfo("spherical map num pixels new: %i", num_pixels_new)
        print(request.blacklisted_image_indices)

        whitelisted_pixels_on_higher_order_sphere = set()
        # for spherical_index in spherical_indices_old:
        #     # TODO image_index not always equal to spherical index
        #     if not spherical_index in request.blacklisted_image_indices:

        for spherical_index_new in spherical_indices_new:
            # Check if pose falls into blacklisted zone on lower sampled sphere
            pixel_angles = hp.pix2ang(num_sides_new, spherical_index_new)
            pixel_index_old = hp.ang2pix(num_sides_old, pixel_angles[0], pixel_angles[1])
            if not pixel_index_old in request.blacklisted_image_indices:
                # add current pixel to new sphere
                whitelisted_pixels_on_higher_order_sphere.update([spherical_index_new])

        response = roaxdt_msgs.srv.SampleSphereResponse()
        response.num_sides_new = num_sides_new
        response.num_pixels_new = num_pixels_new
        response.spherical_pose_indices_new = list(whitelisted_pixels_on_higher_order_sphere)

        rospy.loginfo("/sample_sphere service call successful.")

        return response

    def execute_cb_disk(self, request):
        rospy.loginfo("spherical map num sides (healpy): %i", request.num_sides)
        num_pixels = hp.nside2npix(request.num_sides)
        rospy.loginfo("spherical map num pixels: %i", num_pixels)
        rospy.loginfo("requested pixel index: %i", request.spherical_index)
        rospy.loginfo("requested disk size in radians: %f", request.disk_radius_radians)

        response = roaxdt_msgs.srv.SampleSphereDiskResponse()
        if request.spherical_index < num_pixels:
            vec = hp.pix2vec(nside=request.num_sides, ipix=request.spherical_index)
            ipix_disc = hp.query_disc(nside=request.num_sides, vec=vec, radius=np.radians(request.disk_radius_radians))
            rospy.loginfo("printing neighboring pixel indices for: %i", request.spherical_index)
            for pixel_index in ipix_disc:
                rospy.loginfo("neighboring pixel index: %i", pixel_index)

            response.spherical_pose_indices_new = list(ipix_disc)
        else:
            rospy.logwarn("requested spherical index out of bounds: %i < %i ?",
                          request.spherical_index, num_pixels)

        rospy.loginfo("/sample_sphere_disk service call successful.")

        return response
