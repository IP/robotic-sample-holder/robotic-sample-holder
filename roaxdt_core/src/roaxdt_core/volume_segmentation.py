import rospy
import roaxdt_msgs.srv
import roaxdt_msgs.msg

import numpy as np
import os
import skimage.morphology as morph
from skimage import filters
import json
from pathlib import Path



class VolumeSegmentation:

    def __init__(self):
        self.experiments_path_key = '/roaxdt/experiments/path'
        self.reconstruction_file_name_key = '/roaxdt/experiments/reconstruction_file_name'
        self.reconstruction_file_type_key = '/roaxdt/experiments/reconstruction_file_type'
        self.segmentation_file_name_key = '/roaxdt/experiments/segmentation_file_name'
        self.segmentation_file_type_key = '/roaxdt/experiments/segmentation_file_type'
        self.sample_holder_name_key = '/roaxdt/experiments/sample_holder_name'
        self.home_dir = str(Path.home())
        self.config_files_dir = '.roaxdt'
        self.sample_holders_dir = "sample_holders"


        # retrieve config params
        self.experiments_path = rospy.get_param(self.experiments_path_key)
        self.reconstruction_file_name = rospy.get_param(self.reconstruction_file_name_key)
        self.reconstruction_file_type = rospy.get_param(self.reconstruction_file_type_key)
        self.reconstruction_file = self.reconstruction_file_name + self.reconstruction_file_type  # name and type are joined here
        self.segmentation_file_name = rospy.get_param(self.segmentation_file_name_key)
        self.segmentation_file_type = rospy.get_param(self.segmentation_file_type_key)
        self.segmentation_file = self.segmentation_file_name + self.segmentation_file_type  # name and type are joined here
        self.sample_holder_name = rospy.get_param(self.sample_holder_name_key)

        # get spheres positions
        sample_holder_file = os.path.join(self.home_dir, self.config_files_dir, self.sample_holders_dir, self.sample_holder_name,
                                          self.sample_holder_name + '.json')
        rospy.loginfo("reading sample holder file: %s", sample_holder_file)
        with open(sample_holder_file) as file:
            data = json.load(file)
            self.spheres = data["spheres"]
        
        
        self._segment_volume_service = rospy.Service(
            '/segment_volume', roaxdt_msgs.srv.SegmentVolume, self.segment_volume_cb)
        rospy.loginfo("Started /segment_volume service")

    def segment_volume_cb(self, request):
        rospy.loginfo("Executing request for /segment_volume with parameters:")
        rospy.loginfo("experiment_id: %s", request.experiment_id)
        rospy.loginfo("binning_factor: %s", request.binning_factor)

        experiment_path = os.path.join(self.experiments_path, request.experiment_id)
        reconstruction_path = os.path.join(experiment_path, self.reconstruction_file)
        segmentation_path = os.path.join(experiment_path, self.segmentation_file)

        rospy.loginfo("Reading reconstruction from %s", reconstruction_path)
        file = open(reconstruction_path, 'rb')
        reconstruction = np.fromfile(file, dtype=np.float32)

        volume_size_side = int(2880 / request.binning_factor)
        reconstruction_np = reconstruction.reshape((volume_size_side, volume_size_side, volume_size_side))
        reconstruction_np = (reconstruction_np-reconstruction_np.min())*100/(reconstruction_np.max()-reconstruction_np.min())

        
        
        # Takes everything above the cylinder
        end_of_holder = 266 * volume_size_side // 576 
        # end_of_sample = volume_size_side
        # x_start = 180 * volume_size_side // 576
        # x_end = 406 * volume_size_side // 576
        # y_start = 169 * volume_size_side // 576
        # y_end = 420 * volume_size_side // 576
        
        cropped_reconstruction = reconstruction_np[end_of_holder:,:,:]
        
        
        # spacing
        scaling = 1/request.binning_factor
        sourceToCenter = 1365.0
        centerToDetector = 2155.0 - sourceToCenter
        detectorSpacingTmp = 0.15 / scaling 
        spacing = detectorSpacingTmp / ((sourceToCenter + centerToDetector) / sourceToCenter)
        rospy.loginfo('Domain spacing: %f',spacing)

        volume_center = np.array([volume_size_side / 2.0 , volume_size_side / 2.0 , 0], dtype=int)
        spheres_np = np.array(self.spheres)  / spacing 
        spheres_np = spheres_np + volume_center
        volume_helix_offset = np.array([0.0, 0.0, 15.75/2 / spacing])
        spheres_np = spheres_np + volume_helix_offset

        values = []
        for s in spheres_np:
            s = np.array(s, dtype=int)
            value = reconstruction_np[s[2],s[1],s[0]]
            values.append(value)
    
        values = np.array(values)
        value_spheres = values.mean()
        rospy.loginfo('Value of spheres: %f',value_spheres)
        
        max_sample = cropped_reconstruction.max()
        rospy.loginfo('Max value of sample: %f',max_sample)
        upper_limit = max(value_spheres,max_sample)
        
        
        

        value_holder = reconstruction_np[125*volume_size_side//288, 190*volume_size_side//288, volume_size_side //2]
        rospy.loginfo('Value of the sample holder: %f',value_holder)


        lower_limit = (upper_limit-value_holder)/4 + value_holder
        rospy.loginfo('Lower limit for threshold choice: %f',lower_limit)
        rospy.loginfo('Upper limit for threshold choice: %f',upper_limit)
        
        
        for th in range(int(upper_limit), int(lower_limit), -1):        
            segmented_cropped_mask = cropped_reconstruction >= th
            segmented_cropped = segmented_cropped_mask * cropped_reconstruction
            nonzero  = np.count_nonzero(segmented_cropped)
            
            # 0.005% of the volume 
            if nonzero > 0.00005*volume_size_side*volume_size_side*volume_size_side: 
                break
        

        final_segmented = np.zeros((volume_size_side, volume_size_side, volume_size_side), dtype=np.float32)
        final_segmented[end_of_holder:, :, :] = segmented_cropped
        
        rospy.loginfo('Threshold used: %f',th)
        rospy.loginfo('Max value of segmentation: %f',final_segmented.max())
        rospy.loginfo('Min value of segmentation: %f',final_segmented.min())
        rospy.loginfo('Non-zero elements in segmentation: %f',np.count_nonzero(final_segmented))
        
        
        rospy.loginfo("Writing segmentation to %s", segmentation_path)
        final_segmented.tofile(segmentation_path)

        response = roaxdt_msgs.srv.SegmentVolumeResponse()

        rospy.loginfo("/segment_volume service call successful.")

        return response