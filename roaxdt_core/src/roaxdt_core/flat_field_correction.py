from posixpath import basename
import rospy
import roaxdt_msgs.srv

import cv2
import os
import numpy as np
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler


class FlatFieldCorrection:

    def __init__(self):
        self.experiments_path_key = '/roaxdt/experiments/path'
        self.detector_raw_path_key = '/roaxdt/experiments/detector_raw_path'
        self.detector_corrected_path_key = '/roaxdt/experiments/detector_corrected_path'
        self.detector_path_key = '/roaxdt/experiments/detector_path'
        self.dark_file_key = '/roaxdt/experiments/detector_dark_file'
        self.empty_file_key = '/roaxdt/experiments/detector_empty_file'
        self.corrected_image_file_type_key = '/roaxdt/experiments/corrected_image_file_type'
        self.default_image_file_type_key = '/roaxdt/experiments/default_image_file_type'

        # retrieve config params
        self.experiments_path = rospy.get_param(self.experiments_path_key)
        self.detector_raw_path = rospy.get_param(self.detector_raw_path_key)
        self.detector_corrected_path = rospy.get_param(self.detector_corrected_path_key)
        self.detector_path = rospy.get_param(self.detector_path_key)
        self.detector_dark_file = rospy.get_param(self.dark_file_key)
        self.detector_empty_file = rospy.get_param(self.empty_file_key)
        self.corrected_image_file_type = rospy.get_param(self.corrected_image_file_type_key)
        self.default_image_file_type = rospy.get_param(self.default_image_file_type_key)

        # set up watchdog on both files for hot-reload ability when changing x-ray source parameters
        self.event_handler = PatternMatchingEventHandler(
            patterns=["*" + self.default_image_file_type], ignore_patterns=[], ignore_directories=True)
        self.event_handler.on_any_event = self.on_any_event
        self.observer = Observer()
        self.observer.schedule(self.event_handler, path=os.path.join(
            self.experiments_path, 'dark'), recursive=False)
        self.observer.schedule(self.event_handler, path=os.path.join(
            self.experiments_path, 'empty'), recursive=False)
        self.observer.start()

        # read dark.png and empty.png
        self.detector_dark_file_path = os.path.join(self.experiments_path, 'dark', self.detector_dark_file)
        self.detector_empty_file_path = os.path.join(self.experiments_path, 'empty', self.detector_empty_file)

        if not self.readDetectorInitFiles():
            rospy.logwarn("Couldn't read detector init files!")
            rospy.logwarn("Dark path: %s", self.detector_dark_file_path)
            rospy.logwarn("Empty path: %s", self.detector_empty_file_path)

        self._retrieve_image_service = rospy.Service(
            '/correct_flat_field', roaxdt_msgs.srv.CorrectFlatField, self.executeCallback)
        rospy.loginfo("Started /correct_flat_field service")

    def on_any_event(self, event):
        base_name = os.path.basename(event.src_path)
        accepted_files = [self.detector_dark_file, self.detector_empty_file]
        if (event.event_type == 'created' or event.event_type == 'modified') and (base_name in accepted_files):
            rospy.loginfo("Detected File change for %s", base_name)
            rospy.loginfo("Hot-reloading %s", base_name)

            success_reload = self.readDetectorInitFiles()
            if not success_reload:
                rospy.logerr("Couldn't reload file %s at path", base_name, event.src_path)

    def readDetectorInitFiles(self):
        if os.path.isfile(self.detector_dark_file_path) and os.path.isfile(self.detector_empty_file_path):
            self.detector_dark_image = cv2.imread(self.detector_dark_file_path, cv2.IMREAD_ANYDEPTH)
            self.detector_empty_image = cv2.imread(self.detector_empty_file_path, cv2.IMREAD_ANYDEPTH)

            return True

        else:
            rospy.logerr("Couldn't read detector dark and empty files.")
            return False

    def executeCallback(self, request):
        rospy.loginfo("Executing request for /correct_flat_field with parameters:")
        rospy.loginfo("experiment_id: %s", request.experiment_id)
        rospy.loginfo("image_id: %s", request.image_id)

        image_filename = str(request.image_id) + self.default_image_file_type
        image_raw_filename = str(request.image_id) + self.corrected_image_file_type
        image_raw_path = os.path.join(self.experiments_path, request.experiment_id,
                                      self.detector_raw_path, image_filename)   # sample acq. node saves projection as png
        image_corrected_path = os.path.join(self.experiments_path, request.experiment_id,
                                            self.detector_corrected_path, image_raw_filename)   # flat field corrected float saved as raw array
        image_path = os.path.join(self.experiments_path, request.experiment_id,
                                  self.detector_path, image_filename)   # flat field corrected uint16 saved as png

        response = roaxdt_msgs.srv.CorrectFlatFieldResponse()

        if os.path.isfile(image_raw_path):
            try:
                projection = cv2.imread(image_raw_path, cv2.IMREAD_ANYDEPTH)
                data = np.nan_to_num(-np.log((projection.astype(np.float64) - self.detector_dark_image.astype(np.float64)) /
                                             (self.detector_empty_image.astype(np.float64) - self.detector_dark_image.astype(np.float64))))  # , nan=0.0, posinf=0.0, neginf=0.0)
                with open(image_corrected_path, "wb") as f:
                    data.astype(np.float32).tofile(f)
                    f.close()

                max_val = data.max()
                data[data == max_val] = 0
                data = np.interp(data, (data.min(), data.max()), (0, 65535))
                cv2.imwrite(image_path, data.astype(np.uint16))

                rospy.loginfo("/correct_flat_field service call successful.")
            except Exception as exception:
                rospy.logerr("Could not process image id %i in /correct_flat_field", request.image_id)
                rospy.logerr(exception)

        return response
