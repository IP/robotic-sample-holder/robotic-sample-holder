import rospy
import roaxdt_msgs.srv
import roaxdt_msgs.msg
import geometry_msgs
import actionlib

import os
import healpy as hp
import math
import json


class ScoutScanner:
    _feedback = roaxdt_msgs.msg.StartScoutScanFeedback()
    _result = roaxdt_msgs.msg.StartScoutScanResult()

    def __init__(self):
        self.experiments_path_key = '/roaxdt/experiments/path'
        self.trajectories_path_key = '/roaxdt/trajectories/path'

        # retrieve config params
        self.experiments_path = rospy.get_param(self.experiments_path_key)
        self.trajectories_path = rospy.get_param(self.trajectories_path_key)

        self.get_trajectory_client = actionlib.SimpleActionClient('get_trajectory_from_whitelist',
                                                                  roaxdt_msgs.msg.GetTrajectoryFromWhitelistAction)
        self.get_trajectory_client.wait_for_server()

        self.experiment_client = actionlib.SimpleActionClient('experiment', roaxdt_msgs.msg.ExperimentAction)
        self.experiment_client.wait_for_server()

        rospy.wait_for_service('/analyze_sinograms')
        rospy.wait_for_service('/sample_sphere')

        self._action_name = '/start_scout_scan'
        self._as = actionlib.SimpleActionServer(
            self._action_name, roaxdt_msgs.msg.StartScoutScanAction, execute_cb=self.execute_cb, auto_start=False)
        self._as.start()
        rospy.loginfo("Started /start_scout_scan action server")

    def execute_cb(self, request):
        num_total_steps = 5
        sphere_n_sides = request.sphere_n_sides_scout_scan
        sphere_n_pixels = hp.nside2npix(sphere_n_sides)

        # 1. Run scout scan experiment
        scout_scan_experiment_goal = roaxdt_msgs.msg.ExperimentGoal(
            sample_holder_name=request.sample_holder_name,
            num_iterations=1,
            goal_orientation=request.goal_orientation,
            sample_center=request.sample_center,
            sphere_n_sides=sphere_n_sides,
            run_sample_acquisition=True,
            run_register_images=True,
            run_calibration=True,
            trajectory_type="spherical",
            num_spheres_random_sampling=50,
            helix_sampling_rate=10000
        )

        # Sends the goal to the action server.
        self.experiment_client.send_goal(scout_scan_experiment_goal)

        # Waits for the server to finish performing the action.
        self.experiment_client.wait_for_result()
        scout_scan_experiment_result = self.experiment_client.get_result()
        scout_scan_experiment_id = scout_scan_experiment_result.experiment_id
        # scout_scan_experiment_id = 'eede375c-5bba-402d-847c-e69812730ea5'
        rospy.loginfo('Scout scan succeeded. Experiment id: %s' % scout_scan_experiment_id)

        # publish the feedback
        self._feedback.progress = 1 / num_total_steps
        self._as.publish_feedback(self._feedback)

        metadata_file_name = 'metadata.json'
        metadata_path = os.path.join(self.experiments_path, scout_scan_experiment_id, metadata_file_name)
        experiment_metadata = None
        try:
            with open(metadata_path, "r") as metadata_file:
                experiment_metadata = json.load(metadata_file)
        except:
            print("error reading json file ", metadata_path)

        # 2. Analyze sinograms of scout scan experiment
        blacklisted_images = experiment_metadata["blacklisted_images"]
        scout_scan_trajectory_id = experiment_metadata["trajectory_hash"]
        trajectory_file_name = scout_scan_trajectory_id + '.json'
        trajectory_path = os.path.join(self.trajectories_path, trajectory_file_name)
        scout_scan_trajectory_data = None
        try:
            with open(trajectory_path, "r") as trajectory_file:
                scout_scan_trajectory_data = json.load(trajectory_file)
        except:
            print("error reading json file ", trajectory_path)

        rospy.loginfo("Executing request for /analyze_sinograms with parameters:")
        rospy.loginfo("experiment_id: %s", scout_scan_experiment_id)
        rospy.loginfo("sample_center header: %s", request.sample_center.header.frame_id)
        rospy.loginfo("sample_center: %f", request.sample_center.point.x)
        rospy.loginfo("sample_center: %f", request.sample_center.point.y)
        rospy.loginfo("sample_center: %f", request.sample_center.point.z)

        analyze_sinograms_service = rospy.ServiceProxy('/analyze_sinograms', roaxdt_msgs.srv.AnalyzeSinograms)
        analyze_sinograms_result = None
        try:
            analyze_sinograms_result = analyze_sinograms_service(
                experiment_id=scout_scan_experiment_id,
                sample_center=request.sample_center
            )
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)
            rospy.logerr('%s: Error' % self._action_name)
            self._as.set_aborted(self._result)
            return

        # We don't pass images that couldn't be calibrated or where the robot failed executing the pose as a blacklisted pose
        scout_scan_blacklisted_images = analyze_sinograms_result.blacklisted_image_indices

        # publish the feedback
        self._feedback.progress = 2 / num_total_steps
        self._as.publish_feedback(self._feedback)

        # 3. Sample poses on sphere for whitelisted regions of scout scan experiment
        # spherical_pose_metadata = scout_scan_trajectory_data["spherical_pose_metadata_vec"][0]
        # spherical_pose_indices = spherical_pose_metadata["pose_indices"]

        rospy.loginfo("Executing request for /sample_sphere with parameters:")
        rospy.loginfo("num_sides: %i", sphere_n_sides)
        rospy.loginfo("num_pixels: %i", sphere_n_pixels)
        # rospy.loginfo("spherical_pose_indices: {}".format(' '.join(map(str, spherical_pose_indices))))
        rospy.loginfo("blacklisted_images_indices: {}".format(
            ' '.join(map(str, scout_scan_blacklisted_images))))

        sample_sphere_service = rospy.ServiceProxy('/sample_sphere', roaxdt_msgs.srv.SampleSphere)
        sample_sphere_result = None
        try:
            sample_sphere_result = sample_sphere_service(
                num_sides_new_py=request.sphere_n_sides_new,
                num_sides=sphere_n_sides,
                num_pixels=sphere_n_pixels,
                blacklisted_image_indices=scout_scan_blacklisted_images
            )
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)
            rospy.logerr('%s: Error' % self._action_name)
            self._as.set_aborted(self._result)
            return
        sample_spheres_whitelisted_indices = sample_sphere_result.spherical_pose_indices_new

        num_sides_new = sample_sphere_result.num_sides_new
        num_sides_new_cxx = int(math.log(num_sides_new, 2))
        num_pixels_new = sample_sphere_result.num_pixels_new
        rospy.loginfo("spherical map num sides new (healpy): %i", num_sides_new)
        rospy.loginfo("spherical map num sides new (cxx check): %i", num_sides_new_cxx)
        rospy.loginfo("spherical map num pixels new: %i", num_pixels_new)

        # publish the feedback
        self._feedback.progress = 3 / num_total_steps
        self._as.publish_feedback(self._feedback)

        # 4. Generate trajectory for whitelisted and sampled poses on sphere
        # get goal_orientation from experiment
        goal_orientation = geometry_msgs.msg.QuaternionStamped()
        goal_orientation.header.frame_id = experiment_metadata["goal_orientation"]["frame_id"]
        goal_orientation.quaternion.x = experiment_metadata["goal_orientation"]["x"]
        goal_orientation.quaternion.y = experiment_metadata["goal_orientation"]["y"]
        goal_orientation.quaternion.z = experiment_metadata["goal_orientation"]["z"]
        goal_orientation.quaternion.w = experiment_metadata["goal_orientation"]["w"]

        rospy.loginfo("Executing request for /get_trajectory with parameters:")
        rospy.loginfo("n_sides: %i", num_sides_new)
        rospy.loginfo("whitelist: {}".format(' '.join(map(str, sample_spheres_whitelisted_indices))))
        goal = roaxdt_msgs.msg.GetTrajectoryFromWhitelistGoal(
            trajectory_type="spherical",
            goal_orientation=goal_orientation,
            sample_center=request.sample_center,
            n_sides=num_sides_new,
            whitelist=sample_spheres_whitelisted_indices
        )
        self.get_trajectory_client.send_goal(goal)

        # Waits for the server to finish performing the action.
        self.get_trajectory_client.wait_for_result()
        get_trajectory_result = self.get_trajectory_client.get_result()
        optimized_trajectory_hash = get_trajectory_result.trajectory_hash

        # publish the feedback
        self._feedback.progress = 4 / num_total_steps
        self._as.publish_feedback(self._feedback)

        # 5. Run experiment with optimized trajectory
        optimized_trajectory_experiment_goal = roaxdt_msgs.msg.ExperimentGoal(
            sample_holder_name=request.sample_holder_name,
            goal_orientation=request.goal_orientation,
            sample_center=request.sample_center,
            run_sample_acquisition=True,
            run_register_images=True,
            run_calibration=True,
            trajectory_type="spherical",
            trajectory_hash=optimized_trajectory_hash,
            num_spheres_random_sampling=50,
            helix_sampling_rate=10000
        )

        # Sends the goal to the action server.
        self.experiment_client.send_goal(optimized_trajectory_experiment_goal)

        # Waits for the server to finish performing the action.
        self.experiment_client.wait_for_result()
        scout_scan_experiment_result = self.experiment_client.get_result()
        scout_scan_experiment_id = scout_scan_experiment_result.experiment_id
        rospy.loginfo('Scout scan succeeded. Experiment id: %s' % scout_scan_experiment_id)

        # publish the feedback
        self._feedback.progress = 5 / num_total_steps
        self._as.publish_feedback(self._feedback)

        # self._result.sequence = self._feedback.sequence
        rospy.loginfo('%s: Succeeded' % self._action_name)
        self._as.set_succeeded(self._result)
