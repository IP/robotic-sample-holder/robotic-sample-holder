import rospy
import actionlib
import roaxdt_msgs.msg
import roaxdt_msgs.srv

from scipy import optimize
import numpy as np
import time
from scipy.spatial.transform import Rotation as Rotation
from scipy.spatial import distance_matrix
import random

import sys
import cv2
import os
from math import sin, cos, pi

np.set_printoptions()
float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind': float_formatter}, precision=4)


class optimizer_combined:
    def __init__(self, intrinsic_matrix, helix_t_range, helix_3d_reference_params, correspondences, N_linspace=200, num_spheres=50):
        self.num_spheres = num_spheres
        self.pixel_size = 0.15
        self.helix_3d_reference_params = helix_3d_reference_params
        self.N_linspace = N_linspace
        self.K = intrinsic_matrix

        self.r_ref = self.helix_3d_reference_params['r']
        self.omega_ref = self.helix_3d_reference_params['omega']
        self.phi_ref = self.helix_3d_reference_params['phi']

        self.t_data_3d = np.linspace(helix_t_range[0], helix_t_range[1], self.N_linspace)

        self.detected_spheres = np.zeros([self.num_spheres, 2])
        for index_detected in range(0, self.num_spheres):
            index = index_detected * 5
            self.detected_spheres[index_detected] = np.array(
                [correspondences[index + 3], correspondences[index + 4]])

        self.helix_points = np.zeros([self.N_linspace, 4])
        for i in range(0, self.N_linspace):
            helix_x = self.r_ref * cos(self.omega_ref * self.t_data_3d[i] + self.phi_ref)
            helix_y = self.r_ref * sin(self.omega_ref * self.t_data_3d[i] + self.phi_ref)
            helix_point = np.array([helix_x, helix_y, self.t_data_3d[i]])

            self.helix_points[i] = np.array([helix_point[0], helix_point[1], helix_point[2], 1])

    def get_P(self, a, b, c, x, y, z):
        R = Rotation.from_euler('zyx', [a, b, c]).as_matrix()

        t = np.array([[x], [y], [z]])
        T = np.hstack((R, t))

        P_sy = np.matmul(self.K, T)

        return P_sy

    def calc_norm(self, params):
        return np.linalg.norm(params[0] - params[1])

    def f_helix(self, x_init):
        a, b, c, x, y, z = x_init.tolist()

        P_sy = self.get_P(a, b, c, x, y, z)

        projections_2d = np.matmul(P_sy, self.helix_points.T)
        projections_2d = projections_2d.T
        # normalize homogenous coordinates
        projections_2d[:, 0] /= projections_2d[:, 2]
        projections_2d[:, 1] /= projections_2d[:, 2]

        # linear calc
        norms = distance_matrix(self.detected_spheres, projections_2d[:, :2])

        result = np.empty((0))
        for index_detected in range(0, self.num_spheres):
            min_index = norms[index_detected].argmin()
            residual = self.detected_spheres[index_detected] - projections_2d[min_index, :2]
            result = np.append(result, residual.astype(float), axis=0)

        return result

    def optimize_error(self, initial_solution, correspondeces):
        # check size of correspondences
        correspondences_size = len(correspondeces)
        index_camera_params = self.num_spheres * 5
        expected_correspondences_size = index_camera_params
        if correspondences_size != expected_correspondences_size:
            print("correspondeces tuple size not matching expected size: ",
                  correspondences_size, " vs expected ", expected_correspondences_size)
            return np.eye(4)

        x0 = initial_solution
        print("x0: ", x0)

        print("optimizing camera parameters")

        start_time = time.time()
        print("x_init:, ", x0)

        cost = sys.float_info.max
        result = optimize.least_squares(self.f_helix, x0, method='lm', jac='2-point')
        x0 = result.x
        cost = result.cost
        print("COST: ", cost)

        print("x_result: ", result.x)
        self.P = self.get_P(x0[0], x0[1], x0[2], x0[3], x0[4], x0[5])
        print("P:\n", self.P)

        print("--- %s seconds ---" % (time.time() - start_time))

        return (x0, cost)


class Calibration:

    _result = roaxdt_msgs.msg.CalibrationResult()

    def __init__(self):
        id = rospy.get_param('~id')

        self.output_directory_name = rospy.get_param("/roaxdt/experiments/calibrated_path")
        self.output_guess_directory_name = rospy.get_param("/roaxdt/experiments/calibrated_guess_path")
        self.processed_directory_name = rospy.get_param("/roaxdt/experiments/processed_path")
        self.helix_sampling_rate = rospy.get_param("/roaxdt/calibration/helix_sampling_rate")

        # initialize action clients
        self._action_name = "calibration_" + str(id)
        self._as = actionlib.SimpleActionServer(
            self._action_name, roaxdt_msgs.msg.CalibrationAction, execute_cb=self.execute_cb, auto_start=False)
        self._as.start()
        rospy.loginfo("Started /calibration_%s service", str(id))

    def plot_detected_spheres(self, image_id, correspondeces, counter_correspondences, output_image):
        for i in range(0, counter_correspondences):
            index = i * 5
            sphere_correspondence = np.array([correspondeces[index + 3], correspondeces[index + 4]])

            try:
                output_image = cv2.drawMarker(output_image, tuple(
                    sphere_correspondence.astype(int)), (0, 0, 255), cv2.MARKER_CROSS, markerSize=20, thickness=2)
            except Exception as exception:
                rospy.logerr(exception)
                rospy.logerr("An exception occurred with image", image_id, " sphere ", i)

    def calulateMatrices(self, x0, K):
        a, b, c, x, y, z = x0
        print("a, b, c, x, y, z: ", a, b, c, x, y, z)

        R = Rotation.from_euler('zyx', [a, b, c]).as_matrix()

        t = np.array([[x], [y], [z]])
        T = np.hstack((R, t))
        P = np.matmul(K, T)

        translation = np.array([x, y, z])
        camera_center = np.dot(-R.T, translation)

        return (R, translation.reshape((3,)), camera_center.reshape((3,)), P)

    def plot_helix(self, P, helix_t_range, r_ref, omega_ref, phi_ref, N_linspace, image_id, output_image_path, output_image):
        t_linspace = np.linspace(helix_t_range[0], helix_t_range[1], N_linspace)
        for i in range(0, N_linspace):
            helix_x = r_ref * cos(omega_ref * t_linspace[i] + phi_ref)
            helix_y = r_ref * sin(omega_ref * t_linspace[i] + phi_ref)
            point_tmp = np.array([helix_x, helix_y, t_linspace[i]])
            sphere_position_3d = np.array([point_tmp[0], point_tmp[1], point_tmp[2], 1])

            try:
                projection = P.dot(sphere_position_3d)
                sphere_position_2d = np.array(
                    [projection[0] / projection[2], projection[1] / projection[2]])
                output_image = cv2.drawMarker(output_image, tuple(
                    sphere_position_2d.astype(int)), (255, 0, 0), cv2.MARKER_CROSS, markerSize=20, thickness=2)
            except Exception as exception:
                rospy.logerr(exception)
                rospy.logerr("An exception occurred with image %i sphere %i", image_id, i)

        success_write = cv2.imwrite(output_image_path, output_image)
        rospy.loginfo("success write to %s : %r", output_image_path, success_write)

    def execute_cb(self, request):
        subfolder = str(request.num_spheres_random_sampling) + "-" + str(request.helix_sampling_rate)

        processed_images_path = os.path.join(request.experiment_path, self.processed_directory_name)
        output_images_path = os.path.join(request.experiment_path, self.output_directory_name, subfolder)
        output_guess_images_path = os.path.join(request.experiment_path, self.output_guess_directory_name)

        rospy.loginfo("Helix sampling rate %i", request.helix_sampling_rate)

        try:
            if not os.path.exists(output_images_path):
                os.makedirs(output_images_path)
            if not os.path.exists(output_guess_images_path):
                os.makedirs(output_guess_images_path)
        except Exception as exception:
            print(exception)

        image_filename = str(request.image_id) + ".png"
        image_path = os.path.join(processed_images_path, image_filename)
        output_image_path = os.path.join(output_images_path, image_filename)
        output_guess_image_path = os.path.join(output_guess_images_path, image_filename)

        num_spheres = request.num_spheres
        sphere_correspondences = request.sphere_correspondences

        spheres = []
        for i in range(len(request.spheres)):
            spheres.append([request.spheres[i].x * 1000.0, request.spheres[i].y
                            * 1000.0, request.spheres[i].z * 1000.0])

        spheres_np = np.array(spheres)
        helix_middle = request.helix_height / 2.0
        spheres_np[:, 2] -= (helix_middle * 2.6 * 1000.0)

        r_ref, omega_ref, phi_ref = request.helix_radius, request.helix_omega, -1.55
        rospy.loginfo("Helix params: radius %f, omega %f, phi %f", r_ref, omega_ref, phi_ref)

        N_linspace = request.helix_sampling_rate

        rotation_matrix_result = np.eye(3)
        translation_vector_result = np.array([0, 0, 0])
        camera_center_vector_result = np.array([0, 0, 0])

        K = None
        R_initial = None
        x0 = None
        if (len(request.intrinsic_matrix) != 9):
            rospy.logerr("intrinsic matrix initial not valid! Exiting!")

            self._result.success = False
            rospy.loginfo("calibration action NOT successful.")
            self._as.set_succeeded(self._result)
            return
        else:
            K = np.array(request.intrinsic_matrix).reshape((3, 3))

        quat_tmp = request.sample_holder_pose.orientation
        pos_tmp = request.sample_holder_pose.position
        R_initial = Rotation.from_quat([quat_tmp.x, quat_tmp.y, quat_tmp.z, quat_tmp.w]).as_euler('zyx')
        t_initial = np.array([pos_tmp.x, pos_tmp.y, pos_tmp.z])
        t_initial *= 1000.0

        initial_solution = np.asarray(
            (R_initial[0], R_initial[1], R_initial[2], t_initial[0], t_initial[1], t_initial[2]), dtype=float)

        helix_t_range = [spheres_np[0, 2], spheres_np[-1, 2]]
        matrices_guess = self.calulateMatrices(initial_solution, K)
        P_guess = matrices_guess[3]
        pixel_distance_threshold = 175.0

        index = sum(request.sphere_correspondences_counts[0:request.image_id])
        num_detected_spheres = request.sphere_correspondences_counts[request.image_id]
        correspondeces = ()
        counter_correspondences = 0
        for i in range(0, num_detected_spheres):
            sphere_correspondence_pose = sphere_correspondences[index + i]
            sphere_correspondence = np.array(
                [sphere_correspondence_pose.x, sphere_correspondence_pose.y])

            print(sphere_correspondence)
            if not sphere_correspondence[0] == -1.0 and not sphere_correspondence[1] == -1.0:

                ###

                false_positive = True
                t_linspace = np.linspace(helix_t_range[0], helix_t_range[1], N_linspace)
                for t in range(0, N_linspace):
                    helix_x = r_ref * cos(omega_ref * t_linspace[t] + phi_ref)
                    helix_y = r_ref * sin(omega_ref * t_linspace[t] + phi_ref)
                    point_tmp = np.array([helix_x, helix_y, t_linspace[t]])
                    sphere_position_3d = np.array([point_tmp[0], point_tmp[1], point_tmp[2], 1])

                    try:
                        projection = P_guess.dot(sphere_position_3d)
                        sphere_position_2d = np.array(
                            [projection[0] / projection[2], projection[1] / projection[2]])

                        dist_tmp = np.linalg.norm(sphere_position_2d - sphere_correspondence)
                        if dist_tmp < pixel_distance_threshold:
                            false_positive = False
                            break

                    except Exception as exception:
                        rospy.logerr(exception)
                        rospy.logerr("An exception occurred with image %i helix point %i", request.image_id, t)

                ###

                if not false_positive and counter_correspondences < num_spheres:
                    point_tmp = spheres_np[counter_correspondences]
                    counter_correspondences += 1
                    correspondeces += (point_tmp[0], point_tmp[1], point_tmp[2],
                                       sphere_correspondence[0], sphere_correspondence[1])
        print("counter correspondences: ", counter_correspondences)

        num_correspondences_to_sample = min(counter_correspondences, request.num_spheres_random_sampling)
        rospy.loginfo("Only sampling %i detected spheres from %i",
                      num_correspondences_to_sample, counter_correspondences)
        spheres_indices = np.arange(0, counter_correspondences, 1).tolist()
        spheres_sampled = random.sample(spheres_indices, num_correspondences_to_sample)
        print("Sampled sphere indices: ", spheres_sampled)

        correspondences_sampled = ()
        for i in range(0, num_correspondences_to_sample):
            index_tmp = spheres_sampled[i] * 5
            correspondences_sampled += (correspondeces[index_tmp], correspondeces[index_tmp + 1],
                                        correspondeces[index_tmp + 2], correspondeces[index_tmp + 3], correspondeces[index_tmp + 4])

        optimizer_combined_obj = optimizer_combined(intrinsic_matrix=K, helix_t_range=helix_t_range, helix_3d_reference_params={
            'r': r_ref, 'omega': omega_ref, 'phi': phi_ref}, correspondences=correspondences_sampled, N_linspace=N_linspace, num_spheres=num_correspondences_to_sample)
        # Create an object where all correspondences are passed in for comparison calculations later
        optimizer_combined_all_obj = optimizer_combined(intrinsic_matrix=K, helix_t_range=helix_t_range, helix_3d_reference_params={
            'r': r_ref, 'omega': omega_ref, 'phi': phi_ref}, correspondences=correspondeces, N_linspace=self.helix_sampling_rate, num_spheres=counter_correspondences)

        # plot and save initial guess to filesystem
        output_image = cv2.imread(image_path)
        self.plot_detected_spheres(request.image_id, correspondences_sampled,
                                   num_correspondences_to_sample, output_image)
        output_guess_image = output_image.copy()
        self.plot_helix(P_guess, helix_t_range, r_ref, omega_ref, phi_ref,
                        N_linspace, request.image_id, output_guess_image_path, output_guess_image)

        cost_all_sum = None
        cost_sampled_sum = None
        x0 = initial_solution
        try:
            (x0, cost) = optimizer_combined_obj.optimize_error(
                initial_solution=initial_solution, correspondeces=correspondences_sampled)

            cost_sampled = optimizer_combined_obj.f_helix(x0)
            cost_all = optimizer_combined_all_obj.f_helix(x0)
            cost_sampled_norm = np.linalg.norm(cost_sampled.reshape(
                (num_correspondences_to_sample, 2)), axis=1)
            cost_all_norm = np.linalg.norm(cost_all.reshape((counter_correspondences, 2)), axis=1)
            cost_sampled_sum = cost_sampled_norm.sum()
            cost_all_sum = cost_all_norm.sum()
            print("Geometric error sampled spheres: ", cost_sampled_sum)
            print("Geometric error all spheres: ", cost_all_sum)
            # (x0, cost) = (initial_solution, 100.0)
        except Exception as exception:
            print(exception)
            print("An exception occurred with image", request.image_id, " sphere ", i)
            print("Adding to blacklist")
            cost = 50000.0
            cost_all_sum = 50000.0
            cost_sampled_sum = 50000.0

        self._result.blacklist_image = False
        matrices_calibration = self.calulateMatrices(x0, K)
        rotation_matrix_result = matrices_calibration[0]
        translation_vector_result = matrices_calibration[1]
        camera_center_vector_result = matrices_calibration[2]
        P_calibration = matrices_calibration[3]

        # generate two images: one for initial guess and one for result
        self.plot_helix(P_calibration, helix_t_range, r_ref, omega_ref, phi_ref,
                        N_linspace, request.image_id, output_image_path, output_image)

        cost_threshold = counter_correspondences * request.cost_threshold_per_sphere
        default_run_mode = request.num_spheres == request.num_spheres_random_sampling and request.helix_sampling_rate == self.helix_sampling_rate
        if cost_all_sum > cost_threshold and default_run_mode:
            rospy.logwarn("Error too high for image %i: %f", request.image_id, cost)
            self._result.blacklist_image = True

        self._result.rotation_matrix = rotation_matrix_result.astype(np.float32).flatten().tolist()
        self._result.translation_vector = translation_vector_result.astype(np.float32).flatten().tolist()
        self._result.camera_center_vector = camera_center_vector_result.astype(np.float32).flatten().tolist()
        self._result.calibration_cost = cost_all_sum

        self._result.success = True
        rospy.loginfo("calibration action successful.")
        self._as.set_succeeded(self._result)
