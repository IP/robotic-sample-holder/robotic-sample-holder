#include <roaxdt_core/RobotEnvironmentManager.h>

namespace roaxdt {

namespace nodes {

// using namespace roaxdt::constants; // TODO namespace name collision !!
using namespace roaxdt::configuration;
using namespace roaxdt::robot_geometry;
using namespace roaxdt::moveit_utils;

RobotEnvironmentManager::RobotEnvironmentManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void RobotEnvironmentManager::init() {
  try {
    readConfigParameter<double>(node_handle, roaxdt::constants::robot_control::PANDA_ARM_TO_HAND_OFFSET, end_effector_z_offset);
    readConfigParameter<std::string>(node_handle, roaxdt::constants::robot_control::ATTACHED_OBJECT_LINK, attached_object_link);
    readConfigParameter<std::string>(node_handle, roaxdt::constants::COLLISION_ENV, collision_env);
    readConfigParameter<std::string>(node_handle, roaxdt::constants::experiments::GRIP_SUFFIX, grip_suffix);
    readConfigParameter<std::string>(node_handle, roaxdt::constants::experiments::CENTER_SUFFIX, center_suffix);
    readConfigParameter<std::string>(node_handle, roaxdt::constants::experiments::SAMPLE_SUFFIX, sample_suffix);
    readConfigParameter<std::string>(node_handle, roaxdt::constants::experiments::VTK_SUFFIX, vtk_suffix);
    readConfigParameter<std::string>(node_handle, roaxdt::constants::robot_environment::XRAY_COLLISION_OBJECT_NAME,
                                     xray_collision_object_name);
    readConfigParameter<std::vector<std::string>>(node_handle, roaxdt::constants::robot_environment::XRAY_ALLOWED_COLLISION_LINKS,
                                                  xray_allowed_collision_links);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR_STREAM(exception.what());
    return;
  }

  robot_model_loader = robot_model_loader::RobotModelLoader("robot_description");
  kinematic_model = robot_model_loader.getModel();
  planning_scene_ptr.reset(new planning_scene::PlanningScene(kinematic_model));

  add_sample_holder_service = node_handle.advertiseService(roaxdt::constants::service_endpoints::ADD_SAMPLE_HOLDER_TO_ENVIRONMENT,
                                                           &RobotEnvironmentManager::addSampleHolder, this);
  add_samples_service = node_handle.advertiseService(roaxdt::constants::service_endpoints::ADD_SAMPLES_TO_ENVIRONMENT,
                                                     &RobotEnvironmentManager::addSamples, this);

  planning_scene_diff_publisher = node_handle.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
  while (planning_scene_diff_publisher.getNumSubscribers() < 1) {
    ros::Duration(0.5).sleep();
  }

  // 1. Read in the obstacles
  ROS_INFO_STREAM("collision objects directory contains:");
  fs::directory_iterator collision_objects_iterator = roaxdt::filesystem::readCollisionObjects(collision_env);
  std::set<fs::path> collision_objects_sorted;
  for (auto &entry : boost::make_iterator_range(collision_objects_iterator, {})) {
    ROS_INFO_STREAM(entry);
    collision_objects_sorted.insert(entry.path());
  }

  for (auto &filename : collision_objects_sorted) {
    auto entry_json = roaxdt::filesystem::readJsonFile(filename);
    moveit_msgs::CollisionObject collision_object =
        roaxdt::ros_utils::extractObstacleFromJson(entry_json, filename.stem().string());
    collision_objects.push_back(collision_object);
  }

  // Add the collision objects to the scene
  for (const auto &collision_object : collision_objects) {
    planning_scene_msg.world.collision_objects.push_back(collision_object);
  }

  planning_scene_msg.is_diff = true;
  ROS_WARN_STREAM("(moveit) planning scene collision objects size: " << planning_scene_msg.world.collision_objects.size());
  ROS_WARN_STREAM(
      "(moveit) planning scene attached objects size: " << planning_scene_msg.robot_state.attached_collision_objects.size());

  // Add cone collision object for avoiding robot links detector images
  setUpXrayConeCollisionObject();
  addAllowedCollision("table", "panda_link1", true);
  addAllowedCollision("table", "panda_link0", true);
  addAllowedCollision("panda_hand", "panda_link8", true);

  ros::Duration(8.0).sleep(); // wait for move_group to start
  planning_scene_diff_publisher.publish(planning_scene_msg);

  environment_hash = roaxdt::experiment::getHashOfExperimentEnvironment(collision_objects, attached_objects,
                                                                        planning_scene_msg.allowed_collision_matrix);
  ROS_INFO("Experiment environment hash: %s", environment_hash.c_str());
  this->node_handle.setParam(roaxdt::constants::robot_environment::HASH, environment_hash);

  ROS_INFO_STREAM("robot_environment_manager started successfully");
}

bool RobotEnvironmentManager::addSampleHolder(roaxdt_msgs::AddSampleHolderToEnvironment::Request &request,
                                              roaxdt_msgs::AddSampleHolderToEnvironment::Response &response) {
  clearEnvironment();
  current_samples.clear();
  current_sample_holder = request.sample_holder;
  ROS_INFO("Dimensions sample holder gripper: %f %f %f", current_sample_holder.dimensions_gripper.at(0),
           current_sample_holder.dimensions_gripper.at(1), current_sample_holder.dimensions_gripper.at(2));
  buildEnvironment();

  // reset env hash ROS param
  environment_hash = roaxdt::experiment::getHashOfExperimentEnvironment(collision_objects, attached_objects,
                                                                        planning_scene_msg.allowed_collision_matrix);
  ROS_INFO("Experiment environment hash: %s", environment_hash.c_str());
  this->node_handle.setParam(roaxdt::constants::robot_environment::HASH, environment_hash);

  return true;
}

bool RobotEnvironmentManager::addSamples(roaxdt_msgs::AddSamplesToEnvironment::Request &request,
                                         roaxdt_msgs::AddSamplesToEnvironment::Response &response) {
  clearEnvironment();
  current_samples.clear();
  current_samples = request.samples;
  buildEnvironment();

  // reset env hash ROS param
  environment_hash = roaxdt::experiment::getHashOfExperimentEnvironment(collision_objects, attached_objects,
                                                                        planning_scene_msg.allowed_collision_matrix);
  ROS_INFO("Experiment environment hash: %s", environment_hash.c_str());
  this->node_handle.setParam(roaxdt::constants::robot_environment::HASH, environment_hash);

  return true;
}

void RobotEnvironmentManager::addAllowedCollision(const std::string &entry1, const std::string &entry2, bool allowed) {
  auto acm = planning_scene_ptr->getAllowedCollisionMatrix();

  acm.setEntry(entry1, entry2, allowed);
  acm.setEntry("table", "panda_link1", true);
  acm.setEntry("table", "panda_link0", true);
  acm.setEntry("panda_hand", "panda_link8", true);
  // acm.setEntry("panda_hand", entry1, true); // TODO fix this
  acm.setEntry("panda_link8", entry1, true); // TODO fix this
  acm.setEntry("panda_link7", entry1, true); // TODO fix this

  // TODO this is a duplicate!! But idk how to fix it
  for (const auto &link : xray_allowed_collision_links) {
    acm.setEntry(link, xray_collision_object_name, true);
  }

  acm.print(std::cout);
  acm.getMessage(planning_scene_msg.allowed_collision_matrix);
}

void RobotEnvironmentManager::removeAllowedCollision(const std::string &entry) {
  auto acm = planning_scene_ptr->getAllowedCollisionMatrix();

  acm.removeEntry(entry);
  acm.print(std::cout);
  acm.getMessage(planning_scene_msg.allowed_collision_matrix);
}

void RobotEnvironmentManager::setUpXrayConeCollisionObject() {
  moveit_msgs::CollisionObject cone_object;
  cone_object.header.frame_id = "world";
  cone_object.id = xray_collision_object_name;
  shape_msgs::SolidPrimitive primitive_cone;
  primitive_cone.type = primitive_cone.BOX;
  primitive_cone.dimensions.resize(3);
  primitive_cone.dimensions[0] = 0.11;
  primitive_cone.dimensions[1] = 0.11;
  primitive_cone.dimensions[2] = 2.2;
  geometry_msgs::Pose pose_cone;
  tf::Quaternion quat_cone;
  quat_cone.setRPY(M_PI_2, 0.0, M_PI_2);
  tf::quaternionTFToMsg(quat_cone, pose_cone.orientation);
  pose_cone.position.x = -0.4;
  pose_cone.position.y = -0.35;
  pose_cone.position.z = 0.6;

  cone_object.primitives.push_back(primitive_cone);
  cone_object.primitive_poses.push_back(pose_cone);
  cone_object.operation = cone_object.ADD;
  collision_objects.push_back(cone_object);
  planning_scene_msg.world.collision_objects.push_back(cone_object);

  auto acm = planning_scene_ptr->getAllowedCollisionMatrix();

  for (const auto &link : xray_allowed_collision_links) {
    acm.setEntry(link, xray_collision_object_name, true);
  }
  acm.print(std::cout);
  acm.getMessage(planning_scene_msg.allowed_collision_matrix);
}

void RobotEnvironmentManager::retrieveTransforms(std::string &sample_holder_name) {
  // Retrieve transform of new sample holder center relative to panda_hand, and grip
  std::stringstream stream_grip_frame, stream_center_frame, stream_sample_frame;
  stream_grip_frame << sample_holder_name << grip_suffix;
  sample_holder_grip_frame = stream_grip_frame.str();
  stream_center_frame << sample_holder_name << center_suffix;
  sample_holder_center_frame = stream_center_frame.str();
  stream_sample_frame << sample_holder_name << sample_suffix << vtk_suffix;
  sample_holder_sample_frame = stream_sample_frame.str();
  ROS_INFO_STREAM("sample holder grip link frame: " << sample_holder_grip_frame);
  ROS_INFO_STREAM("sample holder center link frame: " << sample_holder_center_frame);
  ROS_INFO_STREAM("sample holder sample link frame: " << sample_holder_sample_frame);

  tf::StampedTransform grip_to_hand_tf =
      roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, attached_object_link, sample_holder_grip_frame);
  tf::poseTFToMsg(grip_to_hand_tf, pose_grip_to_hand);

  tf::StampedTransform center_to_hand_tf =
      roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, attached_object_link, sample_holder_center_frame);
  tf::poseTFToMsg(center_to_hand_tf, pose_center_to_hand);

  tf::StampedTransform sample_to_hand_tf =
      roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, attached_object_link, sample_holder_sample_frame);
  tf::poseTFToMsg(sample_to_hand_tf, pose_sample_to_hand);
  // tf::poseTFToEigen(sample_to_hand_tf, pose_sample_to_hand_eigen);
}

void RobotEnvironmentManager::clearEnvironment() {
  // create new planning scene for diff
  moveit_msgs::PlanningScene planning_scene_diff;
  planning_scene_diff.is_diff = true;
  planning_scene_diff.robot_state.is_diff = true;

  // remove the old one (if it exists)
  for (const auto &attached_object : attached_objects) {
    // first clear attached objects vector
    planning_scene_diff.robot_state.attached_collision_objects.clear();

    // add object with equal ID and REMOVE operation
    moveit_msgs::AttachedCollisionObject attached_object_old = attached_object;
    moveit_msgs::AttachedCollisionObject attached_object_remove = attached_object;
    attached_object_remove.object.operation = attached_object_remove.object.REMOVE;
    planning_scene_diff.robot_state.attached_collision_objects.push_back(attached_object_remove);

    planning_scene_diff.world.collision_objects.clear();
    planning_scene_diff.world.collision_objects.push_back(attached_object_old.object);

    // Also remove from collision world
    planning_scene_diff.world.collision_objects.push_back(attached_object_remove.object);

    // remove entry from allowed collision matrix of planning scene
    removeAllowedCollision(attached_object_old.object.id);
  }

  planning_scene_diff_publisher.publish(planning_scene_diff);

  collision_objects.clear();
  attached_objects.clear();
}

void RobotEnvironmentManager::buildEnvironment() {
  // create new planning scene for diff
  moveit_msgs::PlanningScene planning_scene_diff;
  planning_scene_diff.is_diff = true;
  planning_scene_diff.robot_state.is_diff = true;

  // Retrieve transform of new sample holder center relative to panda_hand, and grip
  retrieveTransforms(current_sample_holder.name);

  // Add the new attached object(s) to the scene
  moveit_msgs::AttachedCollisionObject attached_object;
  attached_object.link_name = attached_object_link;
  attached_object.object.header.frame_id = attached_object_link;
  attached_object.object.id = current_sample_holder.name;

  shape_msgs::SolidPrimitive primitive_gripper;
  primitive_gripper.type = primitive_gripper.BOX;
  primitive_gripper.dimensions = current_sample_holder.dimensions_gripper;
  attached_object.object.primitives.push_back(primitive_gripper);
  geometry_msgs::Pose pose_gripper = pose_grip_to_hand;
  pose_gripper.position.z += current_sample_holder.dimensions_gripper.at(2) / 2.0;
  attached_object.object.primitive_poses.push_back(pose_gripper);
  shape_msgs::SolidPrimitive primitive;
  primitive.type = primitive.BOX;
  primitive.dimensions = current_sample_holder.dimensions_head;
  attached_object.object.primitives.push_back(primitive);
  geometry_msgs::Pose pose_object = pose_center_to_hand;
  attached_object.object.primitive_poses.push_back(pose_object);
  attached_object.object.operation = attached_object.object.ADD;

  // add sample as collision object
  for (const auto sample_obj : current_samples) {
    ROS_INFO("Adding sample %s to collision environment", sample_obj.name.c_str());
    shape_msgs::SolidPrimitive primitive_sample;
    primitive_sample.type = primitive_sample.BOX;
    std::vector<double> dimensions_tmp{sample_obj.dimensions.at(0) / 1000.0f, sample_obj.dimensions.at(1) / 1000.0f,
                                       sample_obj.dimensions.at(2) / 1000.0f};
    primitive_sample.dimensions = dimensions_tmp;
    attached_object.object.primitives.push_back(primitive_sample);
    geometry_msgs::Pose pose_object_sample = pose_sample_to_hand;
    // TODO need to convert dimensions to hand frame
    // Eigen::Vector3f sample_center_in_hand = pose_sample_to_hand_eigen.rotation() * Eigen::Vector3d(sample_obj.center.data());
    pose_object_sample.position.x += sample_obj.center.at(0) / 1000.0f;
    pose_object_sample.position.y += sample_obj.center.at(1) / 1000.0f;
    attached_object.object.primitive_poses.push_back(pose_object_sample);
    attached_object.object.operation = attached_object.object.ADD;
    attached_object.touch_links = roaxdt::constants::PANDA_IGNORE_LINKS;
  }

  planning_scene_diff.robot_state.attached_collision_objects.push_back(attached_object);
  attached_objects.push_back(attached_object);

  planning_scene_diff_publisher.publish(planning_scene_diff);

  // add entry to allowed collision matrix of planning scene
  addAllowedCollision(attached_object.object.id, xray_collision_object_name, true);
  planning_scene_diff_publisher.publish(planning_scene_msg);
}

// TODO listen for changes in env by user interaction via RViz !!!

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::ROBOT_ENVIRONMENT_MANAGER);

  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::RobotEnvironmentManager node(node_handle, private_node_handle);

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::waitForShutdown();
  return 0;
}