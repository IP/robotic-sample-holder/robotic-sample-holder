#include <roaxdt_core/RobotStateManager.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

RobotStateManager::RobotStateManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void RobotStateManager::init() {
  try {
    //     readConfigParameter<std::string>(node_handle, JOINT_POSITION_COMMAND_TOPIC, joint_position_command_topic);
    //     readConfigParameter<std::string>(node_handle, CONTROLLER_MANAGER_SWITCH_TOPIC, controller_manager_switch_topic);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  error_recovery_client.reset(new ErrorRecoveryClient(action_endpoints::ERROR_RECOVERY, true));
  error_recovery_client->waitForServer();

  ROS_INFO("waiting for franka_control error recovery action goal");
  error_recovery_client->waitForServer(ros::Duration(2.0));

  std::string robot_state_sub_topic("/franka_state_controller/franka_states");
  robot_state_sub = node_handle.subscribe(robot_state_sub_topic, 1, &RobotStateManager::robotStateCallback, this);

  ROS_INFO("Robot state manager is running.");
}

void RobotStateManager::robotStateCallback(const franka_msgs::FrankaStatePtr &robot_state) {
  if (robot_state->robot_mode == franka_msgs::FrankaState::ROBOT_MODE_REFLEX) {
    ROS_WARN("Robot control box triggered a reflex! Trying to recover...");
    executeErrorRecovery();
    ros::Duration(3.0).sleep();
  }
}

void RobotStateManager::executeErrorRecovery() {
  franka_msgs::ErrorRecoveryGoal error_recovery_goal;
  error_recovery_client->sendGoal(error_recovery_goal);
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::ROBOT_STATE_MANAGER);

  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::RobotStateManager node(node_handle, private_node_handle);

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::waitForShutdown();
  return 0;
}