#include <roaxdt_core/RobotControl.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;
using namespace roaxdt::robot_geometry;
using namespace roaxdt::moveit_utils;

RobotControl::RobotControl(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void RobotControl::init() {
  try {
    readConfigParameter<double>(node_handle, robot_control::PANDA_ARM_TO_HAND_OFFSET, end_effector_z_offset);
    readConfigParameter<double>(node_handle, robot_control::PANDA_ARM_VELOCITY_SCALING, panda_arm_velocity_scaling);
    readConfigParameter<double>(node_handle, robot_control::PANDA_ARM_ACCELERATION_SCALING, panda_arm_acceleration_scaling);
    readConfigParameter<double>(node_handle, robot_control::GOAL_ORIENTATION_TOLERANCE, goal_orientation_tolerance);
    readConfigParameter<double>(node_handle, robot_control::GOAL_POSITION_TOLERANCE, goal_position_tolerance);
    readConfigParameter<double>(node_handle, robot_control::GOAL_JOINT_TOLERANCE, goal_joint_tolerance);
    readConfigParameter<std::string>(node_handle, robot_control::POSE_REFERENCE_FRAME, pose_reference_frame);
    readConfigParameter<bool>(node_handle, SIMULATION, simulation);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR_STREAM(exception.what());
    return;
  }

  // sleep for tf_listener to prepare in the background. Otherwise the first tf request returns error
  ros::Duration(3.0).sleep();

  robot_model_loader = robot_model_loader::RobotModelLoader("robot_description");
  kinematic_model = robot_model_loader.getModel();
  ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());

  // move group setup
  move_group_arm = std::make_unique<moveit::planning_interface::MoveGroupInterface>(PLANNING_GROUP_ARM);
  move_group_arm->setMaxVelocityScalingFactor(panda_arm_velocity_scaling);
  move_group_arm->setMaxAccelerationScalingFactor(panda_arm_acceleration_scaling);
  move_group_arm->setPoseReferenceFrame(pose_reference_frame);
  move_group_arm->setGoalTolerance(goal_orientation_tolerance);
  move_group_arm->setPlanningTime(1.5);
  // Check if the goal tolerances were set properly and output to console - IMPORTANT!!!
  if (move_group_arm->getGoalOrientationTolerance() != goal_orientation_tolerance &&
      move_group_arm->getGoalPositionTolerance() != goal_position_tolerance &&
      move_group_arm->getGoalJointTolerance() != goal_joint_tolerance) {
    ROS_WARN_STREAM("goal tolerances not applied correctly by move group: " << move_group_arm->getName());
  }
  ROS_INFO_STREAM("goal orientation tolerance: " << move_group_arm->getGoalOrientationTolerance());
  ROS_INFO_STREAM("goal position tolerance: " << move_group_arm->getGoalPositionTolerance());
  ROS_INFO_STREAM("goal joint tolerance: " << move_group_arm->getGoalJointTolerance());

  // TODO request (attached) collision objects (periodically (?)) from RobotEnvironmentManager node

  stop_service = node_handle.advertiseService(service_endpoints::ROBOT_STOP, &RobotControl::stop, this);
  move_trajectory_service =
      node_handle.advertiseService(service_endpoints::ROBOT_MOVE_TRAJECTORY, &RobotControl::moveTrajectory, this);
  move_cartesian_space_service =
      node_handle.advertiseService(service_endpoints::ROBOT_MOVE_CARTESIAN, &RobotControl::moveCartesianSpace, this);
  move_configuration_space_service =
      node_handle.advertiseService(service_endpoints::ROBOT_MOVE_CONFIGURATION, &RobotControl::moveConfigurationSpace, this);
  validate_move_pose_service =
      node_handle.advertiseService(service_endpoints::ROBOT_VALIDATE_MOVE_POSE, &RobotControl::validateMovePose, this);
  get_planning_scene_client = node_handle.serviceClient<moveit_msgs::GetPlanningScene>("/get_planning_scene");

  ROS_INFO_STREAM("robot_control_node started successfully");
}

bool RobotControl::stop(roaxdt_msgs::RobotStopMsg::Request &request, roaxdt_msgs::RobotStopMsg::Response &response) {
  move_group_arm->stop();
  ROS_INFO("STOP command fired with move_group <panda_arm>");
  STOP_SIGNAL = true;

  return true;
}

bool RobotControl::moveCartesianSpace(roaxdt_msgs::RobotMoveToPointCartesianSpace::Request &request,
                                      roaxdt_msgs::RobotMoveToPointCartesianSpace::Response &response) {
  ros::AsyncSpinner spinner(1);
  spinner.start();
  STOP_SIGNAL = false;
  ROS_WARN_STREAM("robot move cartesian");

  move_group_arm->setStartStateToCurrentState();
  move_group_arm->setPoseReferenceFrame(request.goal.header.frame_id);
  move_group_arm->setPoseTarget(request.goal, PANDA_EE_PARENT_LINK);

  bool errorRecovery = false;

  moveit::planning_interface::MoveGroupInterface::Plan plan;
  auto success_plan = move_group_arm->plan(plan);
  try {
    while (!executePlan(move_group_arm.get(), plan, false)) {
      ROS_WARN_STREAM("robot move exec failed. waiting for recovery. retrying in 5 seconds");
      ros::Duration(6.0).sleep();
      errorRecovery = true;
    }
  } catch (const std::exception &e) { ROS_ERROR_STREAM(e.what()); }

  response.error_recovery = errorRecovery;

  return true;
}

bool RobotControl::moveConfigurationSpace(roaxdt_msgs::RobotMoveToPointConfigurationSpace::Request &request,
                                          roaxdt_msgs::RobotMoveToPointConfigurationSpace::Response &response) {
  ros::AsyncSpinner spinner(1);
  spinner.start();
  STOP_SIGNAL = false;
  ROS_WARN_STREAM("robot move joint");

  ROS_INFO_STREAM("angles:");
  for (auto &angle : request.goal_joint_angles) {
    ROS_INFO_STREAM(angle);
  }

  move_group_arm->setStartStateToCurrentState();
  move_group_arm->setJointValueTarget(request.goal_joint_angles);

  bool errorRecovery = false;

  moveit::planning_interface::MoveGroupInterface::Plan plan;
  auto success_plan = move_group_arm->plan(plan);
  try {
    if (!executePlan(move_group_arm.get(), plan, false)) {
      ROS_WARN_STREAM("robot move exec failed. waiting for recovery. retrying in 5 seconds");
      ros::Duration(6.0).sleep();

      ROS_WARN("Recovering from error and planning to the configuration space goal.");
      ros::Duration(2.0).sleep();
      move_group_arm->setStartStateToCurrentState();
      success_plan = move_group_arm->plan(plan);
      response.error_recovery = true; // TODO it reports successfull execution but the robot doesn't move ?!
      if (!success_plan) {
        ROS_ERROR("Not able to recover from error and plan to the configuration space goal. Returning");
        return false;
      }
    }
  } catch (const std::exception &e) { ROS_ERROR_STREAM(e.what()); }

  response.error_recovery = errorRecovery;

  return true;
}

bool RobotControl::moveTrajectory(roaxdt_msgs::RobotMoveToPointTrajectory::Request &request,
                                  roaxdt_msgs::RobotMoveToPointTrajectory::Response &response) {
  ros::AsyncSpinner spinner(1);
  spinner.start();
  STOP_SIGNAL = false;
  ROS_INFO_STREAM("robot move trajectory");

  bool testRun = false;
  bool errorRecovery = false;

  moveit::planning_interface::MoveGroupInterface::Plan plan;
  plan.trajectory_ = request.trajectory;
  plan.start_state_ = request.start_state;
  try {
    while (!executePlan(move_group_arm.get(), plan, testRun)) {
      ROS_WARN_STREAM("robot move exec failed. waiting for recovery. retrying in 5 seconds");
      ros::Duration(6.0).sleep();

      plan = moveit::planning_interface::MoveGroupInterface::Plan();
      move_group_arm->setStartStateToCurrentState();
      // move_group_arm->setJointValueTarget(PANDA_READY_STATE);  // doesn't work with bigger sample holders
      move_group_arm->setJointValueTarget({0.577, -0.741, -0.311, -2.010, -0.215, 1.299, 1.071});
      auto success_plan = move_group_arm->plan(plan);
      testRun = false;
      errorRecovery = true;
    }
  } catch (const std::exception &e) { ROS_ERROR_STREAM(e.what()); }

  response.error_recovery = errorRecovery;

  return true;
}

bool RobotControl::validateMovePose(roaxdt_msgs::RobotValidateMoveToPoseMsg::Request &request,
                                    roaxdt_msgs::RobotValidateMoveToPoseMsg::Response &response) {
  ros::AsyncSpinner spinner(1);
  spinner.start();
  STOP_SIGNAL = false;
  std::vector<float> robot_link_states;
  robot_link_states.reserve(roaxdt::constants::panda_links_all.size() * 12);

  geometry_msgs::PoseStamped pose_goal = request.goal;
  if (!request.goal.header.frame_id.empty()) {
    move_group_arm->setPoseReferenceFrame(request.goal.header.frame_id);
  } else {
    ROS_WARN("no reference frame passed in the header");
    move_group_arm->setPoseReferenceFrame(pose_reference_frame);
    pose_goal.header.frame_id = pose_reference_frame;
  }

  ROS_INFO_STREAM("moving object to point: " << std::endl << pose_goal);

  auto transform_sample_center_to_ee =
      roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, request.center.header.frame_id, PANDA_EE_PARENT_LINK);
  Eigen::Isometry3d transform_to_ee;
  tf::transformTFToEigen(transform_sample_center_to_ee, transform_to_ee);

  geometry_msgs::Pose pose_object_tmp = generateGraspPoseForSampleHolder(transform_to_ee, pose_goal.pose);

  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = pose_reference_frame;
  pose.pose = pose_object_tmp;

  if (request.start_state.joint_state.position.size() == 0) {
    ROS_INFO("setting current state as start state");
    move_group_arm->setStartStateToCurrentState();
  } else {
    move_group_arm->setStartState(request.start_state);
  }
  move_group_arm->setPoseTarget(pose, PANDA_EE_PARENT_LINK);

  moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  bool success_planning = (move_group_arm->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
  if (!success_planning) {
    ROS_WARN("Could not compute plan successfully.");
    return false;
  } else {
    ROS_INFO_STREAM("------- planning successful -------");

    moveit::core::RobotState goal_state(*move_group_arm->getCurrentState());
    const moveit::core::JointModelGroup *joint_model_group = kinematic_model->getJointModelGroup("panda_arm");

    for (const auto &trajectory_point : my_plan.trajectory_.joint_trajectory.points) {
      goal_state.setJointGroupPositions(joint_model_group, trajectory_point.positions);
      moveit_msgs::RobotState goal_state_msg;
      moveit::core::robotStateToRobotStateMsg(goal_state, goal_state_msg);
      // IMPORTANT transfer attached objects to goal robot state object
      goal_state_msg.attached_collision_objects = attached_objects;
      response.goal_state = goal_state_msg;

      for (const auto robot_link : roaxdt::constants::panda_links_all) {
        Eigen::Isometry3d link_transform = goal_state.getFrameTransform(robot_link);
        Eigen::Matrix<float, 3, 4, Eigen::RowMajor> mat = link_transform.matrix().cast<float>().block<3, 4>(0, 0);
        std::vector<float> vec(mat.data(), mat.data() + mat.rows() * mat.cols());
        for (const auto &elem : vec) {
          robot_link_states.push_back(elem);
        }
      }
    }
  }

  response.robot_link_states = robot_link_states;
  response.success = 1;
  response.goal_result = pose;
  response.start_state = my_plan.start_state_;
  response.trajectory = my_plan.trajectory_;

  return true;
}

void RobotControl::getPlanningSceneTimerCallback(const ros::TimerEvent &e) {
  moveit_msgs::GetPlanningScene get_planning_scene_msg;
  if (get_planning_scene_client.call(get_planning_scene_msg)) {
    attached_objects = get_planning_scene_msg.response.scene.robot_state.attached_collision_objects;
    ROS_DEBUG("attached objects size: %i", attached_objects.size());
  } else {
    ROS_ERROR("Failed calling %s service!", "/get_planning_scene");
  }
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::ROBOT_CONTROL);

  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::RobotControl node(node_handle, private_node_handle);

  // Create Timer in order to pull for changes in planning scene
  ros::Timer timer = node_handle.createTimer(ros::Duration(5.0),
                                             boost::bind(&roaxdt::nodes::RobotControl::getPlanningSceneTimerCallback, &node, _1));
  timer.start();

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::waitForShutdown();
  return 0;
}