#include <roaxdt_core/CalibrationBoxPublisher.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;

CalibrationBoxPublisher::CalibrationBoxPublisher(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void CalibrationBoxPublisher::init() {
  robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
  kinematic_model = robot_model_loader.getModel();
  kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model));

  gazebo_model_state_pub = node_handle.advertise<gazebo_msgs::ModelState>("/gazebo/set_model_state", 1);
  joint_states_sub = node_handle.subscribe("/panda/joint_states", 1, &CalibrationBoxPublisher::jointStatesCallback, this);

  try {
    roaxdt::configuration::readConfigParameter<double>(node_handle, robot_control::PANDA_ARM_TO_HAND_OFFSET,
                                                       end_effector_z_offset);
  } catch (const std::runtime_error &exception) { ROS_ERROR_STREAM(exception.what()); }
}

void CalibrationBoxPublisher::jointStatesCallback(const sensor_msgs::JointState &joint_states_current) {
  const robot_state::JointModelGroup *joint_model_group = kinematic_model->getJointModelGroup(PLANNING_GROUP_ARM);
  const std::vector<std::string> &joint_names = joint_model_group->getJointModelNames();

  std::vector<double> joint_states;
  for (size_t i = 0; i < joint_states_current.position.size() - 2; ++i) {
    joint_states.push_back(joint_states_current.position[i + 2]);
  }
  kinematic_state->setJointGroupPositions(joint_model_group, joint_states);

  const Eigen::Affine3d &end_effector_state = kinematic_state->getGlobalLinkTransform("panda_link8");
  // TODO the value 0.04 needs to be adapted to the attached object size but for now it can stay fixed as the model is static:
  // models/object.xacro
  // add additional 0.01 to ensure that the robot does not vibrate when standing in simulation
  Eigen::Affine3d tmpTransform(Eigen::Translation3d(Eigen::Vector3d(0.0, 0.0, end_effector_z_offset + 0.04 + 0.01)));
  // TODO only for simulation
  tmpTransform.rotate(Eigen::AngleAxisd(-0.785f, Eigen::Vector3d(0.0f, 0.0f, 1.0f)));
  Eigen::Affine3d newState = end_effector_state * tmpTransform;

  geometry_msgs::Pose pose;
  pose.position.x = newState.translation().x();
  pose.position.y = newState.translation().y();
  pose.position.z = newState.translation().z();

  Eigen::Quaterniond quat(newState.rotation());
  pose.orientation.w = quat.w();
  pose.orientation.x = quat.x();
  pose.orientation.y = quat.y();
  pose.orientation.z = quat.z();

  ROS_DEBUG_STREAM("translation" << std::endl << newState.translation());
  ROS_DEBUG_STREAM("rotation" << std::endl << newState.rotation());

  gazebo_msgs::ModelState model_state;
  model_state.model_name = std::string("simple_box");
  model_state.pose = pose;
  model_state.reference_frame = std::string("world");

  gazebo_model_state_pub.publish(model_state);
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::CALIBRATION_BOX_PUBLISHER);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");
  roaxdt::nodes::CalibrationBoxPublisher node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}