#include <roaxdt_core/Gripper.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

Gripper::Gripper(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void Gripper::init() {
  try {
    readConfigParameter<double>(node_handle, gripper::grasp::FORCE, grasp_force);
    readConfigParameter<double>(node_handle, gripper::grasp::SPEED, grasp_speed);
    readConfigParameter<double>(node_handle, gripper::move::MAX_WIDTH, move_max_width);
    readConfigParameter<double>(node_handle, gripper::move::SPEED, move_speed);
    readConfigParameter<float>(node_handle, gripper::grasp::TIMEOUT, grasp_timeout);
    readConfigParameter<float>(node_handle, gripper::move::TIMEOUT, move_timeout);
    readConfigParameter<std::vector<double>>(node_handle, gripper::grasp::EPSILON, grasp_epsilon);
    readConfigParameter<std::string>(node_handle, gripper::TOPIC, gripper_topic);
    readConfigParameter<std::string>(node_handle, gripper::grasp::TOPIC, grasp_topic);
    readConfigParameter<std::string>(node_handle, gripper::move::TOPIC, move_topic);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  // Initialize Gripper -Grasp- action server - providing an abstraction layer to Panda gripper action server
  grasp_server.reset(
      new GraspServer(node_handle, action_endpoints::GRIPPER_GRASP, boost::bind(&Gripper::graspCallback, this, _1), false));
  ROS_INFO("Starting Gripper Grasp Server at %s", action_endpoints::GRIPPER_GRASP.c_str());
  grasp_server->start();

  // Initialize Gripper -Release- action server - providing an abstraction layer to Panda gripper action server
  release_server.reset(
      new ReleaseServer(node_handle, action_endpoints::GRIPPER_RELEASE, boost::bind(&Gripper::releaseCallback, this, _1), false));
  ROS_INFO("Starting Gripper Release Server at %s", action_endpoints::GRIPPER_RELEASE.c_str());
  release_server->start();

  grasp_topic_full = gripper_topic + grasp_topic;
  grasp_client.reset(new GraspClient(grasp_topic_full, true));
  ROS_INFO("Waiting for gripper grasp action server on topic: %s", grasp_topic_full.c_str());
  grasp_client->waitForServer();
  ROS_INFO("gripper grasp action server found at %s", grasp_topic_full.c_str());

  move_topic_full = gripper_topic + move_topic;
  move_client.reset(new MoveClient(move_topic_full, true));
  ROS_INFO("Waiting for gripper move action server on topic: %s", move_topic_full.c_str());
  move_client->waitForServer();
  ROS_INFO("gripper move action server found at %s", grasp_topic_full.c_str());
}

void Gripper::graspCallback(const roaxdt_msgs::GraspGoalConstPtr &goal) {
  franka_gripper::GraspGoal grasp_goal;
  grasp_goal.epsilon.inner = grasp_epsilon.at(0);
  grasp_goal.epsilon.outer = grasp_epsilon.at(1);
  grasp_goal.force = grasp_force;
  grasp_goal.speed = grasp_speed;
  grasp_goal.width = goal->width; // in meters

  grasp_client->sendGoal(grasp_goal);

  bool success_grasp_goal = grasp_client->waitForResult(ros::Duration(grasp_timeout));
  if (success_grasp_goal) {
    actionlib::SimpleClientGoalState state = grasp_client->getState();
    ROS_INFO("gripper grasp action finished: %s", state.toString().c_str());

    roaxdt_msgs::GraspResult result;
    result.success = true;
    grasp_server->setSucceeded(result);
  } else {
    ROS_WARN("gripper grasp action not finished before timeout of %f seconds.", grasp_timeout);

    roaxdt_msgs::GraspResult result;
    result.success = false;
    grasp_server->setSucceeded(result);
  }
}

void Gripper::releaseCallback(const roaxdt_msgs::ReleaseGoalConstPtr &goal) {
  franka_gripper::MoveGoal move_goal;
  move_goal.width = move_max_width; // set to max width of gripper
  move_goal.speed = move_speed;

  move_client->sendGoal(move_goal);

  bool success_move_goal = move_client->waitForResult(ros::Duration(move_timeout));
  if (success_move_goal) {
    actionlib::SimpleClientGoalState state = move_client->getState();
    ROS_INFO("gripper move action finished: %s", state.toString().c_str());

    roaxdt_msgs::ReleaseResult result;
    result.success = true;
    release_server->setSucceeded(result);
  } else {
    ROS_WARN("gripper move action not finished before timeout of %f seconds.", move_timeout);

    roaxdt_msgs::ReleaseResult result;
    result.success = false;
    release_server->setSucceeded(result);
  }
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::GRIPPER);

  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::Gripper node(node_handle, private_node_handle);

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::waitForShutdown();
  return 0;
}