#include <roaxdt_core/ImageRegistration.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;

using namespace roaxdt::configuration;

ImageRegistration::ImageRegistration(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void ImageRegistration::init() {
  try {
    readConfigParameter<std::string>(node_handle, experiments::PATH, experiments_base_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_PATH, detector_path);
    readConfigParameter<std::string>(node_handle, experiments::PROCESSED_PATH, processed_path);
    readConfigParameter<std::string>(node_handle, experiments::SEGMENTED_PATH, segmented_path);
    readConfigParameter<std::string>(node_handle, experiments::DEFAULT_IMAGE_FILE_TYPE, default_image_file_type);

    // read image cropping parameters
    readConfigParameter<int>(node_handle, image_processing::image_cropping::X, image_cropping.x);
    readConfigParameter<int>(node_handle, image_processing::image_cropping::Y, image_cropping.y);
    readConfigParameter<int>(node_handle, image_processing::image_cropping::WIDTH, image_cropping.width);
    readConfigParameter<int>(node_handle, image_processing::image_cropping::HEIGHT, image_cropping.height);

    // read image leveling parameters
    readConfigParameter<double>(node_handle, image_processing::image_leveling::BLACK_POINT, image_leveling.black_point);
    readConfigParameter<double>(node_handle, image_processing::image_leveling::WHITE_POINT, image_leveling.white_point);
    readConfigParameter<double>(node_handle, image_processing::image_leveling::GAMMA, image_leveling.gamma);
    readConfigParameter<int>(node_handle, image_processing::image_leveling::MAX_PIXEL, image_leveling.max_pixel);

    // read image filtering parameters
    readConfigParameter<int>(node_handle, image_processing::image_filtering::MEDIAN_KERNEL_SIZE,
                             image_filtering.median_kernel_size);

    // read circle detection parameters
    readConfigParameter<double>(node_handle, image_processing::circle_detection::DP, circle_detection.dp);
    readConfigParameter<double>(node_handle, image_processing::circle_detection::MIN_DIST, circle_detection.min_dist);
    readConfigParameter<double>(node_handle, image_processing::circle_detection::PARAM_1, circle_detection.param_1);
    readConfigParameter<double>(node_handle, image_processing::circle_detection::PARAM_2, circle_detection.param_2);
    readConfigParameter<int>(node_handle, image_processing::circle_detection::MIN_RADIUS, circle_detection.min_radius);
    readConfigParameter<int>(node_handle, image_processing::circle_detection::MAX_RADIUS, circle_detection.max_radius);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  registerImagesServer.reset(new RegisterImagesServer(node_handle, action_endpoints::REGISTER_IMAGES,
                                                      boost::bind(&ImageRegistration::registerImagesCallback, this, _1), false));
  registerImagesServer->start();
}

void ImageRegistration::registerImagesCallback(const roaxdt_msgs::RegisterImagesGoalConstPtr &goal) {
  ROS_INFO_STREAM("Executing, register images with experiment_id " << goal->experiment_id);
  ROS_INFO_STREAM("images ids size: " << goal->image_ids.size());
  roaxdt_msgs::RegisterImagesResult action_result;
  std::vector<std::vector<geometry_msgs::Pose2D>> sphere_pixels_detected_tmp;
  std::vector<int> num_sphere_pixels_per_image;

  std::string experiment_id{goal->experiment_id};
  auto experiment_path = fs::path(experiments_base_path + experiment_id);

  auto detector_images_path = fs::path(experiment_path) / fs::path(detector_path);
  auto processed_images_path = fs::path(experiment_path) / fs::path(processed_path);
  auto segmented_images_path = fs::path(experiment_path) / fs::path(segmented_path);
  roaxdt::filesystem::makeDirectory(processed_images_path);
  roaxdt::filesystem::makeDirectory(segmented_images_path);

  int num_images = goal->image_ids.size();

  // resize global vec to number of images
  sphere_pixels_detected_tmp.resize(num_images);
  num_sphere_pixels_per_image.resize(num_images);
  // resERVE (!) each vec to number of spheres expected
  for (auto &vec : sphere_pixels_detected_tmp) {
    vec.reserve(goal->num_spheres);
  }

  int numThreads = omp_get_max_threads();
  ROS_INFO_STREAM("OMP num threads: " << numThreads);
  int progress = 0;
#pragma omp parallel for
  for (size_t i = 0; i < num_images; i++) {
    Magick::Image detector_image;
    cv::Mat processed_image;

    size_t current_image_id = goal->image_ids.at(i);

    fs::path pathTmpDetector = detector_images_path;
    pathTmpDetector /= std::to_string(current_image_id) + default_image_file_type;
    std::string detector_image_path = pathTmpDetector.string();
    fs::path pathTmpProcessed = processed_images_path;
    pathTmpProcessed /= std::to_string(current_image_id) + default_image_file_type;
    std::string processed_image_path = pathTmpProcessed.string();
    fs::path pathTmpSegmented = segmented_images_path;
    pathTmpSegmented /= std::to_string(current_image_id) + default_image_file_type;
    std::string segmented_image_path = pathTmpSegmented.string();

    ROS_INFO_STREAM("det path " << detector_image_path);
    ROS_INFO_STREAM("proc path " << processed_image_path);

    // process image
    levelCropImage(detector_image, detector_image_path, processed_image);

    std::thread([processed_image_path, processed_image]() {
      cv::imwrite(processed_image_path.c_str(), processed_image);
    }).detach();

    // detect circles
    cv::medianBlur(processed_image, processed_image, image_filtering.median_kernel_size);
    std::vector<cv::Vec3f> circles = houghCircleDetection(processed_image);

    ROS_INFO_STREAM("projection " << current_image_id << " - num circles: " << circles.size());

    // convert image to RGB
    cv::cvtColor(processed_image, processed_image, cv::COLOR_GRAY2RGB);

    for (size_t j = 0; j < circles.size(); j++) {
      auto circle = circles.at(j);
      cv::drawMarker(processed_image, cv::Point(circle[0], circle[1]), cv::Scalar(0, 0, 255), cv::MARKER_CROSS, 20, 2);
    }

    for (size_t j = 0; j < circles.size(); j++) {
      auto circle = circles.at(j);

      geometry_msgs::Pose2D pixel_detected;
      pixel_detected.x = circle[0];
      pixel_detected.y = circle[1];

      sphere_pixels_detected_tmp.at(i).push_back(pixel_detected);
    }

    cv::imwrite(segmented_image_path.c_str(), processed_image);

#pragma omp atomic update
    progress += 1;

    roaxdt_msgs::RegisterImagesFeedback feedback;
#pragma omp atomic read
    feedback.progress = progress;

    feedback.image_id = current_image_id;

    registerImagesServer->publishFeedback(feedback);
  }

  // count detected spheres
  for (size_t i = 0; i < sphere_pixels_detected_tmp.size(); i++) {
    num_sphere_pixels_per_image.at(i) = sphere_pixels_detected_tmp.at(i).size();
  }
  int totalNumDetectedSpheres = std::accumulate(num_sphere_pixels_per_image.begin(), num_sphere_pixels_per_image.end(), 0);

  // Copy data over
  std::vector<geometry_msgs::Pose2D> sphere_pixels_detected;
  sphere_pixels_detected.reserve(totalNumDetectedSpheres);
  for (size_t i = 0; i < sphere_pixels_detected_tmp.size(); i++) {
    std::copy(sphere_pixels_detected_tmp.at(i).begin(), sphere_pixels_detected_tmp.at(i).end(),
              std::back_inserter(sphere_pixels_detected));
  }

  action_result.success = true;
  action_result.sphere_pixels_detected = sphere_pixels_detected;
  action_result.num_sphere_pixels_per_image = num_sphere_pixels_per_image;
  registerImagesServer->setSucceeded(action_result);
}

void ImageRegistration::levelCropImage(Magick::Image &image, const std::string &image_path, cv::Mat &cv_image) {
  try {
    image.read(image_path);
    image.normalize();

    // check if leveling was done
    double min, max;
    cv::minMaxLoc(cv_image, &min, &max);
    ROS_DEBUG_STREAM("img min: " << min << " max: " << max);

    auto image_size = image.size(); // image size changes after cropping!
    cv_image = cv::Mat(image_size.height(), image_size.width(), CV_8UC1);
    image.write(0, 0, image_size.width(), image_size.height(), "I", Magick::CharPixel, cv_image.data);
  } catch (std::exception &error_) { std::cout << "Caught exception: " << error_.what(); }
}

std::vector<cv::Vec3f> ImageRegistration::houghCircleDetection(cv::Mat &image) {
  std::vector<cv::Vec3f> circles;
  cv::HoughCircles(image, circles, cv::HOUGH_GRADIENT, circle_detection.dp, circle_detection.min_dist, circle_detection.param_1,
                   circle_detection.param_2, circle_detection.min_radius, circle_detection.max_radius);
  return std::move(circles);
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::IMAGE_REGISTRATION);

  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::ImageRegistration node(node_handle, private_node_handle);

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::waitForShutdown();

  return 0;
}