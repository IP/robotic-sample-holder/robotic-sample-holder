#include <roaxdt_core/DetectorManager.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

DetectorManager::DetectorManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void DetectorManager::init() {
  try {
    readConfigParameter<std::string>(node_handle, experiments::PATH, experiments_base_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_DARK_FILE, detector_dark_file);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_EMPTY_FILE, detector_empty_file);
    readConfigParameter<std::vector<int>>(node_handle, reconstruction::DETECTOR_RESOLUTION, detector_resolution);
    readConfigParameter<int>(node_handle, experiments::FLAT_FIELD_CORRECTION_SAMPLE_SIZE, flat_field_correction_sample_size);
    readConfigParameter<std::string>(node_handle, experiments::DEFAULT_IMAGE_FILE_TYPE, default_image_file_type);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  detectorRetrieveImageClient =
      node_handle.serviceClient<roaxdt_msgs::DetectorRetrieveImage>(service_endpoints::DETECTOR_RETRIEVE_IMAGE);

  initializeDetectorServer.reset(new InitializeDetectorServer(node_handle, action_endpoints::INITIALIZE_DETECTOR,
                                                              boost::bind(&DetectorManager::initializeDetectorCallback, this, _1),
                                                              false));
  initializeDetectorServer->start();
}

void DetectorManager::initializeDetectorCallback(const roaxdt_msgs::InitializeDetectorGoalConstPtr &goal) {
  roaxdt_msgs::InitializeDetectorResult action_result;
  ROS_INFO_STREAM("MODE: " << goal->mode);

  auto detector_images_path = fs::path(experiments_base_path + goal->mode);
  ROS_INFO_STREAM("goal path: " << detector_images_path.c_str());
  filesystem::makeDirectory(detector_images_path);

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> img_sum(detector_resolution.at(0), detector_resolution.at(1));
  size_t num_images = flat_field_correction_sample_size;
  for (size_t i = 0; i < num_images; i++) {
    std::ostringstream file_name;
    file_name << i << default_image_file_type;

    fs::path file_path{detector_images_path};
    file_path /= file_name.str();

    roaxdt_msgs::DetectorRetrieveImage srv_detector_image;
    ros::service::waitForService(service_endpoints::DETECTOR_RETRIEVE_IMAGE);
    while (!detectorRetrieveImageClient.call(srv_detector_image)) {
      ROS_WARN("Error retrieving detector image for iteration %u. Retrying in 1 sec...", i);
      ros::Duration(1.0).sleep();
    }
    ROS_INFO("Retrieved image from service %s for iteration %u", service_endpoints::DETECTOR_RETRIEVE_IMAGE.c_str(), i);
    boost::shared_ptr<sensor_msgs::Image const> shared_image;
    shared_image = boost::make_shared<sensor_msgs::Image>(std::move(srv_detector_image.response.image));

    cv::Mat img = cv_bridge::toCvCopy(shared_image, shared_image->encoding)->image;

    cv::imwrite(file_path.c_str(), img);

    Eigen::Matrix<uint16_t, Eigen::Dynamic, Eigen::Dynamic> img_eigen(detector_resolution.at(0), detector_resolution.at(1));
    cv2eigen(img, img_eigen);
    ROS_INFO_STREAM("image max: " << img_eigen.maxCoeff());
    img_sum += img_eigen.cast<double>();
    ROS_INFO_STREAM("image sum max: " << img_sum.maxCoeff());

    roaxdt_msgs::InitializeDetectorFeedback feedback;
    feedback.progress = ((float)(i + 1) / (float)num_images) * 100;
    initializeDetectorServer->publishFeedback(feedback);
  }

  Eigen::Matrix<uint16_t, Eigen::Dynamic, Eigen::Dynamic> img_result = (img_sum / (double)num_images).cast<uint16_t>();
  ROS_INFO_STREAM("image mean max: " << img_result.maxCoeff());
  cv::Mat result(detector_resolution.at(0), detector_resolution.at(1), CV_16UC1);
  eigen2cv(img_result, result);

  // write full image to file system
  fs::path file_path{detector_images_path};
  if (goal->mode.compare("dark") == 0) {
    file_path /= detector_dark_file;
  } else {
    file_path /= detector_empty_file;
  }
  cv::imwrite(file_path.c_str(), result);

  initializeDetectorServer->setSucceeded(action_result);
  return;
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::DETECTOR_MANAGER);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::DetectorManager node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}