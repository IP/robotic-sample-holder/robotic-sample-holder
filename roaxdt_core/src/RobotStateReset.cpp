#include <roaxdt_core/RobotStateReset.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

RobotStateReset::RobotStateReset(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void RobotStateReset::init() {
  try {
    readConfigParameter<std::string>(node_handle, JOINT_POSITION_COMMAND_TOPIC, joint_position_command_topic);
    readConfigParameter<std::string>(node_handle, CONTROLLER_MANAGER_SWITCH_TOPIC, controller_manager_switch_topic);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  switch_controller_client =
      node_handle.serviceClient<controller_manager_msgs::SwitchController>(controller_manager_switch_topic);
  joint_position_publisher = node_handle.advertise<std_msgs::Float64MultiArray>(joint_position_command_topic, 1);

  // sleep for 2 seconds in order to make sure that the system is up and running
  ros::Duration(2.0).sleep();

  // 1. switch controllers
  std::string panda_arm_controller{"panda_arm_controller"}, panda_hand_controller{"panda_hand_controller"},
      joint_position_controller{"joint_position_controller"};
  controller_manager_msgs::SwitchController srv_switch_controller;
  std::vector<std::string> stop_controllers{panda_arm_controller, panda_hand_controller};
  std::vector<std::string> start_controllers{joint_position_controller};
  if (!roaxdt::ros_utils::switchControllers(switch_controller_client, stop_controllers, start_controllers)) {
    return;
  }

  // 2. Publish desired joint positions to command topic
  std_msgs::Float64MultiArray command_msg;
  command_msg.data = PANDA_READY_STATE_WITH_FINGERS;
  joint_position_publisher.publish(command_msg);

  // sleep for 1 seconds in order to make sure that the controller finishes moving the robot
  ros::Duration(1.0).sleep();

  // 3. switch back to default controllers
  if (!roaxdt::ros_utils::switchControllers(switch_controller_client, start_controllers, stop_controllers)) {
    return;
  }

  ROS_INFO("Robot state reset successful.");
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::ROBOT_STATE_RESET);

  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::RobotStateReset node(node_handle, private_node_handle);

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::waitForShutdown();
  return 0;
}