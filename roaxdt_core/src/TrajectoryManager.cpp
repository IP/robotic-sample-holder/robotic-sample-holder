#include <roaxdt_core/TrajectoryManager.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

TrajectoryManager::TrajectoryManager(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void TrajectoryManager::init() {
  try {
    readConfigParameter<std::string>(node_handle, trajectories::PATH, trajectories_path);
    readConfigParameter<std::string>(node_handle, trajectories::FILE_TYPE, trajectory_file_type);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  generateCircularTrajectoryClient =
      node_handle.serviceClient<roaxdt_msgs::GenerateCircularTrajectory>(service_endpoints::GENERATE_CIRCULAR_TRAJECTORY);
  generateSphericalTrajectoryClient =
      node_handle.serviceClient<roaxdt_msgs::GenerateSphericalTrajectory>(service_endpoints::GENERATE_SPHERICAL_TRAJECTORY);
  generateSphericalAXDTTrajectoryClient = node_handle.serviceClient<roaxdt_msgs::GenerateSphericalAXDTTrajectory>(
      service_endpoints::GENERATE_SPHERICAL_AXDT_TRAJECTORY);

  getTrajectoryServer.reset(new GetTrajectoryServer(node_handle, action_endpoints::GET_TRAJECTORY,
                                                    boost::bind(&TrajectoryManager::getTrajectoryCallback, this, _1), false));
  getTrajectoryServer->start();

  getTrajectoryFromWhitelistServer.reset(
      new GetTrajectoryFromWhitelistServer(node_handle, action_endpoints::GET_TRAJECTORY_FROM_WHITELIST,
                                           boost::bind(&TrajectoryManager::getTrajectoryFromWhitelistCallback, this, _1), false));
  getTrajectoryFromWhitelistServer->start();

  // robot environment hash takes some time to get generated
  getEnvironmentHash();
  ROS_INFO("trajectories path: %s", trajectories_path.c_str());
}

void TrajectoryManager::getTrajectoryCallback(const roaxdt_msgs::GetTrajectoryGoalConstPtr &goal) {
  getEnvironmentHash();
  roaxdt_msgs::GetTrajectoryResult action_result;

  ROS_INFO("trajectory hash in request %s", goal->trajectory_hash.c_str());

  if (goal->trajectory_hash.size() > 0) {
    ROS_INFO("User requested trajectory %s directly from filesystem", goal->trajectory_hash.c_str());
    fs::path trajectory_path(trajectories_path);
    trajectory_path /= goal->trajectory_hash + trajectory_file_type;
    ROS_INFO("Traj path: %s", trajectory_path.c_str());

    TrajectoryData trajectory_data(action_result.experiment_poses, action_result.trajectories, action_result.start_states,
                                   action_result.robot_link_states);
    if (fs::exists(trajectory_path) && fs::is_regular_file(trajectory_path)) {
      // trajectory exists
      ROS_INFO("Trajectory with equal hash exists: %s", trajectory_path.c_str());
      ROS_INFO("Checking for compatibility with environment hash: %s", environment_hash.c_str());
      roaxdt::trajectory_utils::readTrajectoryFromDisk(trajectory_data, trajectory_path, environment_hash);

      ROS_INFO("Trajectory %s from filesystem from environment %s is returned", trajectory_path.c_str(),
               environment_hash.c_str());
      action_result.experiment_poses = trajectory_data.experiment_poses;
      action_result.start_states = trajectory_data.robot_states;
      action_result.trajectories = trajectory_data.robot_trajectories;
    }
  } else {

    // Generate trajectory first
    if (goal->trajectory_type.compare(TRAJECTORY_TYPES.at(0)) == 0) {
      // circular trajectory
      auto trajectory_hash = roaxdt::experiment::getHashOfCircularTrajectory(
          goal->min_angle, goal->max_angle, goal->num_iterations, goal->sample_center, goal->goal_orientation,
          goal->sample_holder_name, goal->sample_names, goal->trajectory_type);
      fs::path trajectory_path(trajectories_path);
      trajectory_path /= trajectory_hash + trajectory_file_type;
      ROS_INFO("Traj path: %s", trajectory_path.c_str());

      bool trajectory_compatible = false;
      TrajectoryData trajectory_data(action_result.experiment_poses, action_result.trajectories, action_result.start_states,
                                     action_result.robot_link_states);
      if (fs::exists(trajectory_path) && fs::is_regular_file(trajectory_path)) {
        // trajectory exists
        ROS_INFO("Trajectory with equal hash already exists: %s", trajectory_path.c_str());
        ROS_INFO("Checking for compatibility with environment hash: %s", environment_hash.c_str());
        trajectory_compatible =
            roaxdt::trajectory_utils::readTrajectoryFromDisk(trajectory_data, trajectory_path, environment_hash);
      }

      if (trajectory_compatible) {
        ROS_INFO("Trajectory %s from filesystem from environment %s is returned", trajectory_path.c_str(),
                 environment_hash.c_str());
        action_result.experiment_poses = trajectory_data.experiment_poses;
        action_result.start_states = trajectory_data.robot_states;
        action_result.trajectories = trajectory_data.robot_trajectories;
      } else {
        ROS_WARN("No compatible trajectory found on filesystem.");

        roaxdt_msgs::GenerateCircularTrajectory req;
        req.request.min_angle = goal->min_angle;
        req.request.max_angle = goal->max_angle;
        req.request.num_iterations = goal->num_iterations;
        req.request.sample_center = goal->sample_center;
        req.request.goal_orientation = goal->goal_orientation;

        if (generateCircularTrajectoryClient.call(req)) {
          action_result.experiment_poses = req.response.experiment_poses;
          action_result.start_states = req.response.start_states;
          action_result.trajectories = req.response.trajectories;
          action_result.robot_link_states = req.response.robot_link_states;

          // save to filesystem before returning because no traj with same hash exists
          TrajectoryData trajectory_data(action_result.experiment_poses, action_result.trajectories, action_result.start_states,
                                         action_result.robot_link_states);
          ROS_INFO_STREAM("robot link states size: " << action_result.robot_link_states.size());
          ROS_INFO_STREAM("robot start states size: " << action_result.start_states.size());
          roaxdt::trajectory_utils::writeTrajectoryToDisk(trajectory_data, trajectory_path, environment_hash);
        } else {
          ROS_ERROR("Failed calling %s service!", service_endpoints::GENERATE_CIRCULAR_TRAJECTORY.c_str());
        }
      }

      action_result.trajectory_hash = trajectory_hash;
    } else {
      // spherical trajectory

      // get hash first
      auto trajectory_hash = roaxdt::experiment::getHashOfSphericalTrajectory(
          goal->n_sides, goal->num_iterations, goal->sample_center, goal->goal_orientation, goal->sample_holder_name,
          goal->sample_names, goal->trajectory_type, goal->axdt);
      fs::path trajectory_path(trajectories_path);
      trajectory_path /= trajectory_hash + trajectory_file_type;
      ROS_INFO("Traj path: %s", trajectory_path.c_str());

      bool trajectory_compatible = false;
      TrajectoryData trajectory_data(action_result.experiment_poses, action_result.trajectories, action_result.start_states,
                                     action_result.robot_link_states);
      if (fs::exists(trajectory_path) && fs::is_regular_file(trajectory_path)) {
        // trajectory exists
        ROS_INFO("Trajectory with equal hash already exists: %s", trajectory_path.c_str());
        ROS_INFO("Checking for compatibility with environment hash: %s", environment_hash.c_str());
        trajectory_compatible =
            roaxdt::trajectory_utils::readTrajectoryFromDisk(trajectory_data, trajectory_path, environment_hash);
      }

      // Trajectory exists on filesystem
      if (trajectory_compatible) {
        ROS_INFO("Trajectory %s from filesystem from environment %s is returned", trajectory_path.c_str(),
                 environment_hash.c_str());
        action_result.experiment_poses = trajectory_data.experiment_poses;
        action_result.start_states = trajectory_data.robot_states;
        action_result.trajectories = trajectory_data.robot_trajectories;
      } else {
        ROS_WARN("No compatible trajectory found on filesystem.");

        // distinguish between axdt and non-axdt spherical trajectory
        if (!goal->axdt) {
          ROS_INFO("SPHERICAL");
          // generate conventional spherical trajectory, covering whole sphere
          roaxdt_msgs::GenerateSphericalTrajectory req;
          req.request.n_sides = goal->n_sides;
          req.request.sample_center = goal->sample_center;
          req.request.goal_orientation = goal->goal_orientation;
          req.request.num_pixels_wanted = goal->num_iterations;

          if (generateSphericalTrajectoryClient.call(req)) {
            action_result.experiment_poses = req.response.experiment_poses;
            action_result.start_states = req.response.start_states;
            action_result.trajectories = req.response.trajectories;
            action_result.robot_link_states = req.response.robot_link_states;

            SphericalPoseMetadata sphericalPoseMetadata;
            sphericalPoseMetadata.num_pixels = req.response.num_pixels;
            sphericalPoseMetadata.num_sides = req.response.num_sides;
            sphericalPoseMetadata.pose_indices = req.response.spherical_pose_indices;
            std::vector<SphericalPoseMetadata> spherical_pose_metadata_vec{sphericalPoseMetadata};

            // save to filesystem before returning because no traj with same hash exists
            TrajectoryData trajectory_data(action_result.experiment_poses, action_result.trajectories, action_result.start_states,
                                           action_result.robot_link_states, spherical_pose_metadata_vec);
            ROS_INFO_STREAM("robot link states size: " << action_result.robot_link_states.size());
            roaxdt::trajectory_utils::writeTrajectoryToDisk(trajectory_data, trajectory_path, environment_hash);
          } else {
            ROS_ERROR("Failed calling %s service!", service_endpoints::GENERATE_SPHERICAL_TRAJECTORY.c_str());
          }
        } else {
          ROS_INFO("SPHERICAL AXDT");
          // generate AXDT spherical trajectory
          roaxdt_msgs::GenerateSphericalAXDTTrajectory req;
          req.request.n_sides = goal->n_sides;
          req.request.sample_center = goal->sample_center;
          req.request.goal_orientation = goal->goal_orientation;
          req.request.num_pixels_wanted = goal->num_iterations;

          if (generateSphericalAXDTTrajectoryClient.call(req)) {
            action_result.experiment_poses = req.response.experiment_poses;
            action_result.start_states = req.response.start_states;
            action_result.trajectories = req.response.trajectories;
            action_result.robot_link_states = req.response.robot_link_states;

            SphericalPoseMetadata sphericalPoseMetadata;
            sphericalPoseMetadata.num_pixels = req.response.num_pixels;
            sphericalPoseMetadata.num_sides = req.response.num_sides;
            sphericalPoseMetadata.pose_indices = req.response.spherical_pose_indices;
            std::vector<SphericalPoseMetadata> spherical_pose_metadata_vec{sphericalPoseMetadata};

            // save to filesystem before returning because no traj with same hash exists
            TrajectoryData trajectory_data(action_result.experiment_poses, action_result.trajectories, action_result.start_states,
                                           action_result.robot_link_states, spherical_pose_metadata_vec);
            ROS_INFO_STREAM("robot link states size: " << action_result.robot_link_states.size());
            roaxdt::trajectory_utils::writeTrajectoryToDisk(trajectory_data, trajectory_path, environment_hash);
          } else {
            ROS_ERROR("Failed calling %s service!", service_endpoints::GENERATE_SPHERICAL_AXDT_TRAJECTORY.c_str());
          }
        }
      }

      action_result.trajectory_hash = trajectory_hash;
    }
  }

  action_result.success = true;
  getTrajectoryServer->setSucceeded(action_result);
}

void TrajectoryManager::getTrajectoryFromWhitelistCallback(const roaxdt_msgs::GetTrajectoryFromWhitelistGoalConstPtr &goal) {
  getEnvironmentHash();
  roaxdt_msgs::GetTrajectoryFromWhitelistResult action_result;

  // Generate trajectory first
  if (goal->trajectory_type.compare(TRAJECTORY_TYPES.at(0)) == 0) {
    // circular trajectory
  } else {
    // spherical trajectory

    // get hash first
    int num_iterations = goal->whitelist.size();
    auto trajectory_hash = roaxdt::experiment::getHashOfSphericalTrajectory(goal->n_sides, num_iterations, goal->sample_center,
                                                                            goal->goal_orientation, goal->sample_holder_name,
                                                                            goal->sample_names, goal->trajectory_type);
    fs::path trajectory_path(trajectories_path);
    trajectory_path /= trajectory_hash + trajectory_file_type;
    ROS_INFO("Traj path: %s", trajectory_path.c_str());

    bool trajectory_compatible = false;
    TrajectoryData trajectory_data(action_result.experiment_poses, action_result.trajectories, action_result.start_states,
                                   action_result.robot_link_states);

    roaxdt_msgs::GenerateSphericalTrajectory req;
    req.request.n_sides = goal->n_sides;
    req.request.sample_center = goal->sample_center;
    req.request.goal_orientation = goal->goal_orientation;
    req.request.num_pixels_wanted = 1;
    req.request.spherical_pose_indices = goal->whitelist;

    if (generateSphericalTrajectoryClient.call(req)) {
      action_result.experiment_poses = req.response.experiment_poses;
      action_result.start_states = req.response.start_states;
      action_result.trajectories = req.response.trajectories;
      action_result.robot_link_states = req.response.robot_link_states;

      SphericalPoseMetadata sphericalPoseMetadata;
      sphericalPoseMetadata.num_pixels = req.response.num_pixels;
      sphericalPoseMetadata.num_sides = req.response.num_sides;
      sphericalPoseMetadata.pose_indices = req.response.spherical_pose_indices;
      std::vector<SphericalPoseMetadata> spherical_pose_metadata_vec{sphericalPoseMetadata};

      // save to filesystem before returning because no traj with same hash exists
      TrajectoryData trajectory_data(action_result.experiment_poses, action_result.trajectories, action_result.start_states,
                                     action_result.robot_link_states, spherical_pose_metadata_vec);
      ROS_INFO_STREAM("robot link states size: " << action_result.robot_link_states.size());
      roaxdt::trajectory_utils::writeTrajectoryToDisk(trajectory_data, trajectory_path, environment_hash);
    } else {
      ROS_ERROR("Failed calling %s service!", service_endpoints::GENERATE_SPHERICAL_TRAJECTORY.c_str());
    }

    action_result.trajectory_hash = trajectory_hash;
  }

  action_result.success = true;
  getTrajectoryFromWhitelistServer->setSucceeded(action_result);
}

void TrajectoryManager::getEnvironmentHash() {
  while (!this->node_handle.getParam(robot_environment::HASH, environment_hash)) {
    ROS_WARN_STREAM("Could not read robot environment hash from ROS param server. Waiting for 1 sec and retryiing");
    ros::Duration(1.0).sleep();
  }
  ROS_INFO("env hash: %s", environment_hash.c_str());
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::TRAJECTORY_MANAGER);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::TrajectoryManager node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}