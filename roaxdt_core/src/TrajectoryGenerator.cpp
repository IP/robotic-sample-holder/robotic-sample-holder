
#include <roaxdt_core/TrajectoryGenerator.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

TrajectoryGenerator::TrajectoryGenerator(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void TrajectoryGenerator::init() {
  try {
    readConfigParameter<std::string>(node_handle, CAMERA_LINK_SUFFIX, camera_link_suffix);
    readConfigParameter<std::string>(node_handle, DETECTOR_CAMERA_NAME, detector_camera_id);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  camera_frame = std::string("/" + detector_camera_id + camera_link_suffix);

  ros::Duration(2.0).sleep(); // wait for -tf- service to appear

  generateCircularTrajectoryService = node_handle.advertiseService(service_endpoints::GENERATE_CIRCULAR_TRAJECTORY,
                                                                   &TrajectoryGenerator::generateCircularTrajectory, this);
  generateSphericalTrajectoryService = node_handle.advertiseService(service_endpoints::GENERATE_SPHERICAL_TRAJECTORY,
                                                                    &TrajectoryGenerator::generateSphericalTrajectory, this);
  generateSphericalAXDTTrajectoryService = node_handle.advertiseService(
      service_endpoints::GENERATE_SPHERICAL_AXDT_TRAJECTORY, &TrajectoryGenerator::generateSphericalAXDTTrajectory, this);
  generateWaypointTrajectoryService = node_handle.advertiseService(service_endpoints::GENERATE_WAYPOINT_TRAJECTORY,
                                                                   &TrajectoryGenerator::generateWaypointTrajectory, this);

  validateRobotMovePoseClient =
      node_handle.serviceClient<roaxdt_msgs::RobotValidateMoveToPoseMsg>(service_endpoints::ROBOT_VALIDATE_MOVE_POSE);
}

bool TrajectoryGenerator::generateCircularTrajectory(roaxdt_msgs::GenerateCircularTrajectory::Request &req,
                                                     roaxdt_msgs::GenerateCircularTrajectory::Response &res) {
  std::vector<geometry_msgs::PoseStamped> experiment_poses;
  std::vector<moveit_msgs::RobotTrajectory> robot_trajectories;
  std::vector<moveit_msgs::RobotState> robot_start_states;

  std::string frame_id = req.goal_orientation.header.frame_id;
  auto goal_pose_translation = calculateGoalPoseTranslation(frame_id);

  geometry_msgs::PoseStamped goal_pose;
  goal_pose.pose.position = goal_pose_translation;
  goal_pose.pose.orientation = req.goal_orientation.quaternion;
  std::vector<float> robot_link_states_global;
  robot_link_states_global.reserve(req.num_iterations * panda_links_all.size() * 12);
  moveit_msgs::RobotState robot_state;
  moveit_msgs::RobotState start_robot_state, goal_robot_state;
  moveit_msgs::RobotTrajectory trajectory;

  moveit_msgs::RobotState last_valid_goal_state;
  Eigen::Isometry3d pose_base_eigen;
  tf::poseMsgToEigen(goal_pose.pose, pose_base_eigen);
  double step_size = req.max_angle / req.num_iterations;
  int counter = 0;
  for (size_t i = req.start; i < req.num_iterations; i++) {
    Eigen::Isometry3d pose_base_copy = pose_base_eigen;
    pose_base_copy.rotate(Eigen::AngleAxisd(i * step_size, Eigen::Vector3d::UnitZ()));

    geometry_msgs::Pose goal_pose_tmp;
    tf::poseEigenToMsg(pose_base_copy, goal_pose_tmp);

    std::vector<float> robot_link_states;
    start_robot_state = last_valid_goal_state;
    moveit_msgs::RobotTrajectory trajectory;
    bool successPreparePose =
        preparePose(req.goal_orientation.header.frame_id, goal_pose_tmp.orientation, goal_pose_tmp.position, req.sample_center,
                    goal_pose, goal_robot_state, trajectory, robot_link_states, start_robot_state);
    if (successPreparePose) {
      // if planning was successsful -> overwrite last valid goal state
      last_valid_goal_state = goal_robot_state;

      geometry_msgs::PoseStamped goal_pose_stamped;
      goal_pose_stamped.pose = goal_pose_tmp;
      goal_pose_stamped.header.frame_id = goal_pose.header.frame_id;
      experiment_poses.push_back(goal_pose_stamped);
      robot_trajectories.push_back(trajectory);
      robot_start_states.push_back(start_robot_state);

      for (const auto &elem : robot_link_states) {
        robot_link_states_global.push_back(elem);
      }

      ++counter;
    }
  }

  ROS_INFO_STREAM("robot start states size: " << robot_start_states.size());
  res.experiment_poses = experiment_poses;
  res.trajectories = robot_trajectories;
  res.start_states = robot_start_states;
  res.robot_link_states = robot_link_states_global;

  return true;
}

bool TrajectoryGenerator::generateSphericalTrajectory(roaxdt_msgs::GenerateSphericalTrajectory::Request &req,
                                                      roaxdt_msgs::GenerateSphericalTrajectory::Response &res) {
  std::vector<geometry_msgs::PoseStamped> experiment_poses;
  std::vector<int> spherical_pose_indices;
  std::vector<moveit_msgs::RobotTrajectory> robot_trajectories;
  std::vector<moveit_msgs::RobotState> robot_start_states;
  std::vector<float> robot_link_states_global;
  robot_link_states_global.reserve(req.num_pixels * panda_links_all.size() * 12);

  std::string frame_id = req.goal_orientation.header.frame_id;
  auto goal_pose_translation = calculateGoalPoseTranslation(frame_id);

  std::vector<SphericalPose> spherical_poses;

  int num_spherical_sides_request = req.n_sides;
  if (req.spherical_pose_indices.size() == 0) {
    trajectory_utils::getHealpixMapVectors(num_spherical_sides_request, spherical_poses, req.num_pixels_wanted);
  } else {
    // Generate trajectory based on whitelisted spots on sphere
    trajectory_utils::getHealpixMapVectorsFromIndices(num_spherical_sides_request, spherical_poses, req.spherical_pose_indices);
  }

  int num_spherical_pixels = spherical_poses.size();
  int num_spherical_sides = spherical_poses.at(0).num_sides;
  spherical_pose_indices.reserve(num_spherical_pixels);

  int counter = 0;
  auto spherical_poses_iter = spherical_poses.begin();
  ROS_WARN_STREAM("num pixels on healpix maps: " << num_spherical_pixels);
  moveit_msgs::RobotState last_valid_goal_state;
  while (counter < num_spherical_pixels && spherical_poses_iter != spherical_poses.end()) {
    auto spherical_pose = *spherical_poses_iter;

    ROS_WARN_STREAM("successful waypoints counter: " << counter);
    ROS_WARN_STREAM("spherical pose counter: " << spherical_pose.index);
    if (spherical_pose.index > 0 && req.num_pixels_wanted == 1) {
      float success_rate = (static_cast<float>(counter) / static_cast<float>(spherical_pose.index)) * 100.0;
      ROS_WARN_STREAM("success rate: " << success_rate << "%");
    }
    geometry_msgs::PoseStamped goal_pose;

    Eigen::Matrix3d rotation_matrix;
    rotation_matrix = Eigen::AngleAxisd(spherical_pose.phi, Eigen::Vector3d::UnitZ()) *
                      Eigen::AngleAxisd(spherical_pose.theta, Eigen::Vector3d::UnitY());
    Eigen::Quaterniond quat_eigen(rotation_matrix);
    geometry_msgs::Quaternion quat;
    quat.w = quat_eigen.w();
    quat.x = quat_eigen.x();
    quat.y = quat_eigen.y();
    quat.z = quat_eigen.z();

    std::vector<float> robot_link_states;
    moveit_msgs::RobotState start_robot_state, goal_robot_state;
    start_robot_state = last_valid_goal_state;
    moveit_msgs::RobotTrajectory trajectory;
    bool successPreparePose = preparePose(req.goal_orientation.header.frame_id, quat, goal_pose_translation, req.sample_center,
                                          goal_pose, goal_robot_state, trajectory, robot_link_states, start_robot_state);
    if (successPreparePose) {
      // if planning was successsful -> overwrite last valid goal state
      last_valid_goal_state = goal_robot_state;

      Eigen::Isometry3d pose_base_eigen;
      tf::poseMsgToEigen(goal_pose.pose, pose_base_eigen);
      pose_base_eigen.matrix().block<3, 3>(0, 0) = rotation_matrix;

      geometry_msgs::Pose goal_pose_tmp;
      tf::poseEigenToMsg(pose_base_eigen, goal_pose_tmp);

      geometry_msgs::PoseStamped goal_pose_stamped;
      goal_pose_stamped.pose = goal_pose_tmp;
      goal_pose_stamped.header.frame_id = goal_pose.header.frame_id;
      experiment_poses.push_back(goal_pose_stamped);
      spherical_pose_indices.push_back(spherical_pose.index);
      robot_trajectories.push_back(trajectory);
      robot_start_states.push_back(start_robot_state);

      for (const auto &elem : robot_link_states) {
        robot_link_states_global.push_back(elem);
      }

      ++counter;
    } else {
      ROS_WARN("Skipping pose %i on spherical trajectory. Planning failed", spherical_pose.index);
    }

    ++spherical_poses_iter;
  }

  if (counter < num_spherical_pixels) {
    auto difference = num_spherical_pixels - robot_start_states.size();
    ROS_WARN("Could not sample valid robot states for %i out of %i poses", difference, num_spherical_pixels);
  }

  res.experiment_poses = experiment_poses;
  res.spherical_pose_indices = spherical_pose_indices;
  res.num_pixels = num_spherical_pixels;
  res.num_sides = num_spherical_sides;
  res.trajectories = robot_trajectories;
  res.start_states = robot_start_states;
  res.robot_link_states = robot_link_states_global;

  return true;
}

bool TrajectoryGenerator::generateSphericalAXDTTrajectory(roaxdt_msgs::GenerateSphericalAXDTTrajectory::Request &req,
                                                          roaxdt_msgs::GenerateSphericalAXDTTrajectory::Response &res) {
  std::vector<geometry_msgs::PoseStamped> experiment_poses;
  std::vector<int> spherical_pose_indices;
  std::vector<moveit_msgs::RobotTrajectory> robot_trajectories;
  std::vector<moveit_msgs::RobotState> robot_start_states;
  std::vector<float> robot_link_states_global;
  robot_link_states_global.reserve(req.num_pixels * panda_links_all.size() * 12);

  std::string frame_id = req.goal_orientation.header.frame_id;
  auto goal_pose_translation = calculateGoalPoseTranslation(frame_id);

  std::vector<SphericalPose> spherical_poses, spherical_poses_axdt;

  int num_spherical_sides_request = req.n_sides;
  if (req.spherical_pose_indices.size() == 0) {
    trajectory_utils::getHealpixMapVectors(num_spherical_sides_request, spherical_poses, req.num_pixels_wanted);
  } else {
    // Generate trajectory based on whitelisted spots on sphere
    trajectory_utils::getHealpixMapVectorsFromIndices(num_spherical_sides_request, spherical_poses, req.spherical_pose_indices);
  }

  // clone every sampling pose three times for later creating poses that are sensitive to the gratings
  std::vector<double> angles_deg{0.0, 45.0, 90.0, 135.0};
  for (const auto &pose : spherical_poses) {
    auto pose_new = pose;
    for (const auto &angle_deg : angles_deg) {
      double angle = (angle_deg * (PI / 180));

      // convert given pose from panda_link0 to sample_center frame

      // rotate pose about sample_center z-axis by angle degrees

      // convert pose back to panda_link0 frame

      // or just add rotation angle to pose(?)
      pose_new.axdt_rotation = angle;

      spherical_poses_axdt.push_back(pose_new);
    }
  }

  int num_spherical_pixels = spherical_poses_axdt.size();
  int num_spherical_sides = spherical_poses_axdt.at(0).num_sides;
  spherical_pose_indices.reserve(num_spherical_pixels);

  int counter = 0;
  auto spherical_poses_iter = spherical_poses_axdt.begin();
  ROS_WARN_STREAM("num pixels on healpix maps: " << num_spherical_pixels);
  moveit_msgs::RobotState last_valid_goal_state;
  while (counter < num_spherical_pixels && spherical_poses_iter != spherical_poses_axdt.end()) {
    auto spherical_pose = *spherical_poses_iter;

    ROS_WARN_STREAM("successful waypoints counter: " << counter);
    ROS_WARN_STREAM("spherical pose counter: " << spherical_pose.index);
    if (spherical_pose.index > 0 && req.num_pixels_wanted == 1) {
      float success_rate = (static_cast<float>(counter) / static_cast<float>(spherical_pose.index)) * 100.0;
      ROS_WARN_STREAM("success rate: " << success_rate << "%");
    }
    geometry_msgs::PoseStamped goal_pose;

    Eigen::Matrix3d rotation_matrix;
    rotation_matrix = Eigen::AngleAxisd(spherical_pose.phi, Eigen::Vector3d::UnitZ()) *
                      Eigen::AngleAxisd(spherical_pose.theta, Eigen::Vector3d::UnitY()) *
                      Eigen::AngleAxisd(spherical_pose.axdt_rotation, Eigen::Vector3d::UnitZ());
    Eigen::Quaterniond quat_eigen(rotation_matrix);
    geometry_msgs::Quaternion quat;
    quat.w = quat_eigen.w();
    quat.x = quat_eigen.x();
    quat.y = quat_eigen.y();
    quat.z = quat_eigen.z();

    std::vector<float> robot_link_states;
    moveit_msgs::RobotState start_robot_state, goal_robot_state;
    start_robot_state = last_valid_goal_state;
    moveit_msgs::RobotTrajectory trajectory;
    bool successPreparePose = preparePose(req.goal_orientation.header.frame_id, quat, goal_pose_translation, req.sample_center,
                                          goal_pose, goal_robot_state, trajectory, robot_link_states, start_robot_state);
    if (successPreparePose) {
      // if planning was successsful -> overwrite last valid goal state
      last_valid_goal_state = goal_robot_state;

      Eigen::Isometry3d pose_base_eigen;
      tf::poseMsgToEigen(goal_pose.pose, pose_base_eigen);
      pose_base_eigen.matrix().block<3, 3>(0, 0) = rotation_matrix;

      geometry_msgs::Pose goal_pose_tmp;
      tf::poseEigenToMsg(pose_base_eigen, goal_pose_tmp);

      geometry_msgs::PoseStamped goal_pose_stamped;
      goal_pose_stamped.pose = goal_pose_tmp;
      goal_pose_stamped.header.frame_id = goal_pose.header.frame_id;
      experiment_poses.push_back(goal_pose_stamped);
      spherical_pose_indices.push_back(spherical_pose.index);
      robot_trajectories.push_back(trajectory);
      robot_start_states.push_back(start_robot_state);

      for (const auto &elem : robot_link_states) {
        robot_link_states_global.push_back(elem);
      }

      ++counter;
    } else {
      ROS_WARN("Skipping pose %i on spherical trajectory. Planning failed", spherical_pose.index);
    }

    ++spherical_poses_iter;
  }

  if (counter < num_spherical_pixels) {
    auto difference = num_spherical_pixels - robot_start_states.size();
    ROS_WARN("Could not sample valid robot states for %i out of %i poses", difference, num_spherical_pixels);
  }

  res.experiment_poses = experiment_poses;
  res.spherical_pose_indices = spherical_pose_indices;
  res.num_pixels = num_spherical_pixels;
  res.num_sides = num_spherical_sides;
  res.trajectories = robot_trajectories;
  res.start_states = robot_start_states;
  res.robot_link_states = robot_link_states_global;

  return true;
}

bool TrajectoryGenerator::generateWaypointTrajectory(roaxdt_msgs::GenerateWaypointTrajectory::Request &req,
                                                     roaxdt_msgs::GenerateWaypointTrajectory::Response &res) {
  std::string frame_id = req.goal_orientation.header.frame_id;
  auto goal_pose_translation = calculateGoalPoseTranslation(frame_id);

  SphericalPose spherical_pose;
  trajectory_utils::getHealpixMapVectorSingle(req.num_sides, req.spherical_pose_index, spherical_pose);

  geometry_msgs::PoseStamped goal_pose;

  Eigen::Matrix3d rotation_matrix;
  rotation_matrix = Eigen::AngleAxisd(spherical_pose.phi, Eigen::Vector3d::UnitZ()) *
                    Eigen::AngleAxisd(spherical_pose.theta, Eigen::Vector3d::UnitY());
  Eigen::Quaterniond quat_eigen(rotation_matrix);
  geometry_msgs::Quaternion quat;
  quat.w = quat_eigen.w();
  quat.x = quat_eigen.x();
  quat.y = quat_eigen.y();
  quat.z = quat_eigen.z();

  std::vector<float> robot_link_states;
  moveit_msgs::RobotState start_robot_state, goal_robot_state;
  moveit_msgs::RobotTrajectory trajectory;
  bool successPreparePose = preparePose(req.goal_orientation.header.frame_id, quat, goal_pose_translation, req.sample_center,
                                        goal_pose, goal_robot_state, trajectory, robot_link_states, start_robot_state);
  if (successPreparePose) {
    Eigen::Isometry3d pose_base_eigen;
    tf::poseMsgToEigen(goal_pose.pose, pose_base_eigen);
    pose_base_eigen.matrix().block<3, 3>(0, 0) = rotation_matrix;

    geometry_msgs::Pose goal_pose_tmp;
    tf::poseEigenToMsg(pose_base_eigen, goal_pose_tmp);

    geometry_msgs::PoseStamped goal_pose_stamped;
    goal_pose_stamped.pose = goal_pose_tmp;
    goal_pose_stamped.header.frame_id = goal_pose.header.frame_id;
    res.experiment_pose = goal_pose_stamped;
    res.trajectory = trajectory;
    res.start_state = start_robot_state;

    for (const auto &elem : robot_link_states) {
      res.robot_link_states.push_back(elem);
    }

    res.success = true;
  } else {
    ROS_WARN("Planning failed for pose %i on spherical trajectory", spherical_pose.index);
    res.success = false;
  }

  return true;
}

bool TrajectoryGenerator::preparePose(std::string &frame_id, geometry_msgs::Quaternion &orientation,
                                      geometry_msgs::Point &translation, geometry_msgs::PointStamped &sample_center,
                                      geometry_msgs::PoseStamped &result_pose, moveit_msgs::RobotState &result_state,
                                      moveit_msgs::RobotTrajectory &trajectory, std::vector<float> &robot_link_states,
                                      moveit_msgs::RobotState &start_state) {
  geometry_msgs::PoseStamped goal_pose_req;
  goal_pose_req.header.frame_id = frame_id;
  goal_pose_req.pose.orientation = orientation;
  goal_pose_req.pose.position.x = translation.x;
  goal_pose_req.pose.position.y = translation.y;
  goal_pose_req.pose.position.z = translation.z;
  roaxdt_msgs::RobotValidateMoveToPoseMsg srv_validate;
  srv_validate.request.goal = goal_pose_req;
  srv_validate.request.start_state = start_state;
  srv_validate.request.center = sample_center;
  if (validateRobotMovePoseClient.call(srv_validate)) {
    ROS_INFO_STREAM("Response validate robot move to pose: " << srv_validate.response.success);
    result_pose = srv_validate.response.goal_result;
    ROS_INFO_STREAM("goal pose for robot's last link: " << std::endl << result_pose);
    robot_link_states = srv_validate.response.robot_link_states;
    result_state = srv_validate.response.goal_state;
    start_state = srv_validate.response.start_state;
    trajectory = srv_validate.response.trajectory;
    return true;
  } else {
    ROS_ERROR("Failed calling validate robot move to point service!");
    return false;
  }
}

geometry_msgs::Point TrajectoryGenerator::calculateGoalPoseTranslation(const std::string &frame_id) {
  // retrieve transform from xray link to pose base link - needed to calc where the sample should actually be!
  tf::StampedTransform xray_to_pose_base_tf = roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, frame_id, camera_frame);
  tf::poseTFToEigen(xray_to_pose_base_tf, xray_to_pose_base_eigen);
  ROS_INFO_STREAM("xray to pose base transform" << std::endl << xray_to_pose_base_eigen.matrix());
  // compute intersection of xray z-axis with hyperplane located at robot base
  Eigen::Vector3d xray_ray = xray_to_pose_base_eigen.rotation() * Eigen::Vector3d(0.0, 0.0, 1.0);
  // right-hand rule makes xz-plane of robot the only possible plane for with x-ray the intersection when mounted on table
  Eigen::Hyperplane<double, 3> robot_plane =
      Eigen::Hyperplane<double, 3>::Through(Eigen::Vector3d::Zero(), Eigen::Vector3d::UnitX(), Eigen::Vector3d::UnitZ());
  Eigen::ParametrizedLine<double, 3> xray_line(xray_to_pose_base_eigen.translation(), xray_ray);
  double intersect_param = xray_line.intersection(robot_plane);
  Eigen::Vector3d goal_pose_translation = xray_line.pointAt(intersect_param);
  ROS_INFO_STREAM("goal pose translation: " << goal_pose_translation);

  geometry_msgs::Point pointResult;
  tf::pointEigenToMsg(goal_pose_translation, pointResult);

  return pointResult;
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::TRAJECTORY_GENERATOR);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::TrajectoryGenerator node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}