#include <roaxdt_core/ExperimentPreparation.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

ExperimentPreparation::ExperimentPreparation(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void ExperimentPreparation::init() {
  try {
    readConfigParameter<bool>(node_handle, SIMULATION, simulation);
    readConfigParameter<bool>(node_handle, EXPERIMENT, experiment);
    readConfigParameter<std::string>(node_handle, IMAGE_PLANE_FRAME_SUFFIX, image_plane_frame_suffix);
    readConfigParameter<std::string>(node_handle, CAMERA_LINK_SUFFIX, camera_link_suffix);
    readConfigParameter<std::string>(node_handle, CAMERA_INFO_SUFFIX, camera_info_suffix);
    readConfigParameter<std::string>(node_handle, DETECTOR_CAMERA_NAME, detector_camera_id);
    readConfigParameter<std::string>(node_handle, experiments::PATH, experiments_base_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_PATH, detector_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_RAW_PATH, detector_raw_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_CORRECTED_PATH, detector_corrected_path);
    readConfigParameter<std::string>(node_handle, experiments::RECONSTRUCTED_PATH, reconstructed_path);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  camera_frame = std::string("/" + detector_camera_id + camera_link_suffix);
  camera_imageplane_frame = std::string("/" + detector_camera_id + image_plane_frame_suffix);
  camera_info_topic = std::string("/" + detector_camera_id + camera_info_suffix);

  ros::Duration(2.0).sleep(); // wait for -tf- service to appear

  prepareExperimentServer.reset(
      new PrepareExperimentServer(node_handle, action_endpoints::PREPARE_EXPERIMENT,
                                  boost::bind(&ExperimentPreparation::prepareExperimentCallback, this, _1), false));
  prepareExperimentServer->start();

  validateRobotMovePoseClient =
      node_handle.serviceClient<roaxdt_msgs::RobotValidateMoveToPoseMsg>(service_endpoints::ROBOT_VALIDATE_MOVE_POSE);
}

void ExperimentPreparation::prepareExperimentCallback(const roaxdt_msgs::PrepareExperimentGoalConstPtr &goal) {
  roaxdt_msgs::PrepareExperimentResult action_result;
  action_result.experiment_path = prepareFilesystem(goal->experiment_id);

  // retrieve camera info
  auto shared_camera_info = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(camera_info_topic, node_handle);
  if (shared_camera_info != nullptr) {
    auto K_cv = shared_camera_info->K;
    std::vector<float> intrinsic_matrix_vec{K_cv[0], K_cv[1], K_cv[2], K_cv[3], K_cv[4], K_cv[5], K_cv[6], K_cv[7], K_cv[8]};
    action_result.intrinsic_matrix = intrinsic_matrix_vec;
  } else {
    ROS_WARN_STREAM("Could not read camera intrinsic matrix from topic " << camera_info_topic);
  }

  prepareExperimentServer->setSucceeded(action_result);
  return;
}

std::string ExperimentPreparation::prepareFilesystem(const std::string &experiment_id) {
  auto experiment_path = fs::path(experiments_base_path + experiment_id);
  roaxdt::filesystem::makeDirectory(experiment_path);
  ROS_INFO_STREAM("experiment path: " << experiment_path);
  auto detector_images_path = fs::path(experiment_path) / fs::path(detector_path);
  roaxdt::filesystem::makeDirectory(detector_images_path);
  ROS_INFO_STREAM("detector images path: " << detector_images_path);
  auto detector_raw_images_path = fs::path(experiment_path) / fs::path(detector_raw_path);
  roaxdt::filesystem::makeDirectory(detector_raw_images_path);
  ROS_INFO_STREAM("detector raw images path: " << detector_raw_images_path);
  auto detector_corrected_images_path = fs::path(experiment_path) / fs::path(detector_corrected_path);
  roaxdt::filesystem::makeDirectory(detector_corrected_images_path);
  ROS_INFO_STREAM("detector corrected images path: " << detector_corrected_images_path);
  auto reconstructed_images_path = fs::path(experiment_path) / fs::path(reconstructed_path);
  roaxdt::filesystem::makeDirectory(reconstructed_images_path);
  ROS_INFO_STREAM("reconstructed images path: " << reconstructed_images_path);

  return experiment_path.string();
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::EXPERIMENT_PREPARATION);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::ExperimentPreparation node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}