
#include <roaxdt_core/DetectorClient.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

DetectorClient::DetectorClient(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void DetectorClient::init() {
  try {
    readConfigParameter<std::string>(node_handle, experiments::PATH, experiments_base_path);
    readConfigParameter<std::vector<int>>(node_handle, reconstruction::DETECTOR_RESOLUTION, detector_resolution);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_HOST, detector_host);
    readConfigParameter<int>(node_handle, experiments::DETECTOR_PORT, detector_port);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_REQUEST_STR, detector_request_str);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  image_size = detector_resolution.at(0) * detector_resolution.at(1) * sizeof(detector_image_encoding);

  detectorRetrieveImageService =
      node_handle.advertiseService(service_endpoints::DETECTOR_RETRIEVE_IMAGE, &DetectorClient::retrieveImageFromDetector, this);
}

bool DetectorClient::retrieveImageFromDetector(roaxdt_msgs::DetectorRetrieveImage::Request &req,
                                               roaxdt_msgs::DetectorRetrieveImage::Response &res) {
  boost::asio::io_service io_service;
  tcp::socket socket(io_service);
  socket.connect(tcp::endpoint(boost::asio::ip::address::from_string(detector_host), detector_port));

  boost::system::error_code error;
  boost::asio::write(socket, boost::asio::buffer(detector_request_str), error);
  if (!error) {
    ROS_INFO("Client sent: %s ", detector_request_str.c_str());
    ROS_INFO("Image size %i", image_size);
  } else {
    ROS_ERROR("Send failed: %s", error.message().c_str());
  }

  // std::vector<uint16_t> data{image_size};
  double *data = new double[image_size];
  boost::asio::read(socket, boost::asio::buffer(data, image_size), error);
  if (error && error != boost::asio::error::eof) {
    ROS_ERROR("Receive from python socket server failed: %s", error.message().c_str());

    delete[] data;
    return false;
  } else {
    std::vector<double> data_vec(data, data + image_size);
    sensor_msgs::Image image_ros;
    image_ros.encoding = sensor_msgs::image_encodings::TYPE_16UC1;
    image_ros.width = detector_resolution.at(0);
    image_ros.height = detector_resolution.at(1);
    image_ros.is_bigendian = false;
    image_ros.step = image_ros.width * sizeof(detector_image_encoding);

    uint32_t sinogram_width = detector_resolution.at(0);
    uint32_t sinogram_height = detector_resolution.at(1);
    uint32_t row_step_in_bytes = detector_resolution.at(0) * sizeof(detector_image_encoding);
    sensor_msgs::fillImage(image_ros, sensor_msgs::image_encodings::TYPE_16UC1, sinogram_height, sinogram_width,
                           row_step_in_bytes, data_vec.data());

    res.image = image_ros;

    delete[] data;
    return true;
  }
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::DETECTOR_CLIENT);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::DetectorClient node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}