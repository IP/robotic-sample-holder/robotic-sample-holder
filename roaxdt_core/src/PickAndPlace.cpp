#include <roaxdt_core/PickAndPlace.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

PickAndPlace::PickAndPlace(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void PickAndPlace::init() {
  try {
    readConfigParameter<std::string>(node_handle, CONTROLLER_MANAGER_SWITCH_TOPIC, controller_manager_switch_topic);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  pick_and_place_server.reset(new PickAndPlaceServer(node_handle, action_endpoints::PICK_AND_PLACE,
                                                     boost::bind(&PickAndPlace::pickAndPlaceCallback, this, _1), false));
  ROS_INFO("Starting Gripper Grasp Server at %s", action_endpoints::GRIPPER_GRASP.c_str());
  pick_and_place_server->start();

  switch_controller_client =
      node_handle.serviceClient<controller_manager_msgs::SwitchController>(controller_manager_switch_topic);
  robot_move_client =
      node_handle.serviceClient<roaxdt_msgs::RobotMoveToPointCartesianSpace>(service_endpoints::ROBOT_MOVE_CARTESIAN);
  grasp_client.reset(new GraspClient(action_endpoints::GRIPPER_GRASP, true));
  release_client.reset(new ReleaseClient(action_endpoints::GRIPPER_RELEASE, true));

  ROS_INFO("Pick and Place node started successfully.");
}

void PickAndPlace::pickAndPlaceCallback(const roaxdt_msgs::PickAndPlaceGoalConstPtr &goal) {}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::PICK_AND_PLACE);

  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::PickAndPlace node(node_handle, private_node_handle);

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::waitForShutdown();
  return 0;
}