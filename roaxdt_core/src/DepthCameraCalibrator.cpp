#include <roaxdt_core/DepthCameraCalibrator.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

DepthCameraCalibrator::DepthCameraCalibrator(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
}

void DepthCameraCalibrator::init() {
  try {
    readConfigParameter<std::string>(node_handle, CAMERA_IMAGE_SUFFIX, camera_image_suffix);
    readConfigParameter<std::string>(node_handle, CAMERA_INFO_SUFFIX, camera_info_suffix);
    readConfigParameter<std::vector<std::string>>(node_handle, CAMERAS, cameras);
    readConfigParameter<double>(node_handle, depth_calibration::CHESSBOARD_PATTERN_SQUARE_WIDTH, chessboard_pattern_square_width);
    readConfigParameter<int>(node_handle, depth_calibration::CHESSBOARD_PATTERN_COLUMNS, chessboard_pattern_columns);
    readConfigParameter<int>(node_handle, depth_calibration::CHESSBOARD_PATTERN_ROWS, chessboard_pattern_rows);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  // sleep for tf_listener to prepare in the background. Otherwise the first tf request returns error
  // ros::Duration(3.0).sleep();

  checkerboardPatternSize = cv::Size(chessboard_pattern_columns, chessboard_pattern_rows);
  ROS_INFO_STREAM("Expected checkerboard pattern size: " << checkerboardPatternSize.width << " "
                                                         << checkerboardPatternSize.height);

  for (int i = 0; i < checkerboardPatternSize.height; i++) {
    for (int j = 0; j < checkerboardPatternSize.width; j++) {
      cv::Vec3f pt(j * chessboard_pattern_square_width, i * chessboard_pattern_square_width, 0.0);
      chessboard_points_3d.push_back(pt);
    }
  }

  calibrationServer.reset(new DepthCameraCalibrationServer(node_handle, action_endpoints::DEPTH_CAMERA_CALIBRATION,
                                                           boost::bind(&DepthCameraCalibrator::calibrateCamera, this, _1),
                                                           false));
  calibrationServer->start();
}

void DepthCameraCalibrator::calibrateCamera(const roaxdt_msgs::CalibrateDepthCameraGoalConstPtr &goal) {
  roaxdt_msgs::CalibrateDepthCameraFeedback action_feedback;
  roaxdt_msgs::CalibrateDepthCameraResult action_result;

  // check if depth camera name is valid
  if (std::find(cameras.begin(), cameras.end(), goal->depth_camera_name) != cameras.end()) {
    try {
      auto camera_image_topic = "/" + goal->depth_camera_name + camera_image_suffix;
      auto camera_info_topic = "/" + goal->depth_camera_name + "/color" + camera_info_suffix;

      std::vector<float> intrinsic_matrix_vec;
      auto shared_camera_info = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(camera_info_topic, node_handle);
      if (shared_camera_info != nullptr) {
        auto K_cv = shared_camera_info->K;
        intrinsic_matrix_vec =
            std::vector<float>{K_cv[0], K_cv[1], K_cv[2], K_cv[3], K_cv[4], K_cv[5], K_cv[6], K_cv[7], K_cv[8]};
      } else {
        ROS_WARN_STREAM("Could not read camera info from topic " << camera_info_topic);
      }

      bool pattern_found = false;
      std::vector<cv::Point2f> corners;
      cv_bridge::CvImagePtr cv_ptr;
      while (!pattern_found) {
        ROS_WARN("No chessboard pattern found in the image!");
        ros::Duration(0.5).sleep();
        boost::shared_ptr<sensor_msgs::Image const> shared_image;
        shared_image = ros::topic::waitForMessage<sensor_msgs::Image>(camera_image_topic, node_handle);

        if (shared_image != nullptr) {
          sensor_msgs::Image camera_image = *shared_image;
          ROS_DEBUG_STREAM(goal->depth_camera_name << " image dimensions: width " << camera_image.width << " height "
                                                   << camera_image.height);
          try {
            cv_ptr = cv_bridge::toCvCopy(camera_image, sensor_msgs::image_encodings::BGR8);
            cv::Mat img_resized;
            cv::resize(cv_ptr->image, img_resized, cv::Size(700, 700));

            std::vector<uchar> buf;
            cv::imencode(".jpg", img_resized, buf);
            auto *enc_msg = reinterpret_cast<unsigned char *>(buf.data());
            std::string encoded = base64_encode(enc_msg, buf.size());

            action_feedback.camera_image = encoded;
            calibrationServer->publishFeedback(action_feedback);
          } catch (cv_bridge::Exception &e) { ROS_ERROR("cv_bridge exception: %s", e.what()); }

          pattern_found =
              cv::findChessboardCorners(cv_ptr->image, checkerboardPatternSize, corners,
                                        cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FAST_CHECK | cv::CALIB_CB_NORMALIZE_IMAGE);
        } else {
          ROS_WARN("No image from topic %s", camera_image_topic);
        }
      }

      ROS_WARN("Found pattern. Corners size: %i", corners.size());

      cv::Mat matImg_gray;
      cv::cvtColor(cv_ptr->image, matImg_gray, cv::COLOR_BGR2GRAY);
      cv::cornerSubPix(matImg_gray, corners, cv::Size(11, 11), cv::Size(-1, -1),
                       cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER, 30, 0.001));

      cv::drawChessboardCorners(cv_ptr->image, checkerboardPatternSize, corners, true);

      cv::Mat img_resized;
      cv::resize(cv_ptr->image, img_resized, cv::Size(700, 700));

      std::vector<uchar> buf;
      cv::imencode(".jpg", img_resized, buf);
      auto *enc_msg = reinterpret_cast<unsigned char *>(buf.data());
      std::string encoded = base64_encode(enc_msg, buf.size());

      action_feedback.camera_image = encoded;
      calibrationServer->publishFeedback(action_feedback);

      cv::Mat rvecs, tvecs;
      cv::Mat K = cv::Mat(3, 3, CV_32F, intrinsic_matrix_vec.data());
      cv::Mat D; // = camera_model.distortionCoeffs();
      int flag = 0;
      flag |= cv::CALIB_USE_INTRINSIC_GUESS;
      cv::calibrateCamera(std::vector<std::vector<cv::Vec3f>>{chessboard_points_3d},
                          std::vector<std::vector<cv::Point2f>>{corners}, cv_ptr->image.size(), K, D, rvecs, tvecs, flag);

      ROS_INFO_STREAM("cameraMatrix : " << std::endl << K);
      ROS_INFO_STREAM("distCoeffs : " << std::endl << D);
      ROS_INFO_STREAM("Rotation vector : " << std::endl << rvecs);
      ROS_INFO_STREAM("Translation vector : " << std::endl << tvecs);

      cv::Mat rotation_cv;
      cv::Rodrigues(rvecs, rotation_cv);

      Eigen::Map<Eigen::Matrix3d> rotation(rotation_cv.ptr<double>());
      Eigen::Quaterniond quaternion(rotation);
      Eigen::Map<Eigen::Vector3d> translation(tvecs.ptr<double>());

      // write to camera config json file
      nlohmann::json camera_json;
      fs::path camera_file = roaxdt::filesystem::readDepthCameraConfiguration(goal->depth_camera_name + ".json");

      camera_json["position"] = {{"x", translation.x()}, {"y", translation.y()}, {"z", translation.z()}};
      camera_json["orientation"] = {{"x", quaternion.x()}, {"y", quaternion.y()}, {"z", quaternion.z()}, {"w", quaternion.w()}};

      bool success_write = filesystem::writeJsonFile(camera_file, camera_json);

      calibrationServer->setSucceeded(action_result);
    } catch (const std::runtime_error &exception) {
      ROS_ERROR(exception.what());
      return;
    }
  } else {
    ROS_WARN("No such depth camera %s", goal->depth_camera_name);
  }
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::DEPTH_CAMERA_CALIBRATOR);

  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::DepthCameraCalibrator node(node_handle, private_node_handle);

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::waitForShutdown();
  return 0;
}