#include <roaxdt_core/Experiment.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

Experiment::Experiment(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle), processorCount(std::thread::hardware_concurrency()) {
  this->init();
}

void Experiment::init() {
  try {
    readConfigParameter<std::string>(node_handle, experiments::PATH, experiments_base_path);
    readConfigParameter<std::string>(node_handle, experiments::SEGMENTED_PATH, segmented_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_PATH, detector_path);
    readConfigParameter<std::string>(node_handle, experiments::CALIBRATED_PATH, calibrated_path);
    readConfigParameter<std::string>(node_handle, experiments::RECONSTRUCTED_PATH, reconstructed_path);
    readConfigParameter<float>(node_handle, calibration::COST_THRESHOLD_PER_SPHERE, costThresholdPerSphere);
  } catch (const std::runtime_error &exception) {
    ROS_ERROR(exception.what());
    return;
  }

  loadSampleHolderClient.reset(new LoadSampleHolderClient(action_endpoints::LOAD_SAMPLE_HOLDER, true));
  ROS_INFO("Waiting for load_sample_holder client to show up...");
  loadSampleHolderClient->waitForServer();
  ROS_INFO("load_sample_holder showed up");

  registerImagesClient.reset(new RegisterImagesClient(action_endpoints::REGISTER_IMAGES, true));
  ROS_INFO("Waiting for register_images client to show up...");
  registerImagesClient->waitForServer();
  ROS_INFO("register_images showed up");

  calibrationClients.resize(processorCount);
#pragma omp parallel for
  for (size_t i = 0; i < processorCount; i++) {
    calibrationClients.at(i).reset(new CalibrationClient(action_endpoints::CALIBRATION + "_" + std::to_string(i), true));
    ROS_INFO_STREAM("Waiting for calibration client #" << i << " to show up...");
    calibrationClients.at(i)->waitForServer();
    ROS_INFO_STREAM("calibration client #" << i << " showed up");
  }

  computedTomographyClient.reset(new ComputedTomographyClient(action_endpoints::COMPUTED_TOMOGRAPHY, true));
  ROS_INFO("Waiting for computed_tomography client to show up...");
  computedTomographyClient->waitForServer();
  ROS_INFO("computed_tomography showed up");

  getTrajectoryClient.reset(new GetTrajectoryClient(action_endpoints::GET_TRAJECTORY, true));
  ROS_INFO("Waiting for %s client to show up...", action_endpoints::GET_TRAJECTORY.c_str());
  getTrajectoryClient->waitForServer();
  ROS_INFO("%s showed up", action_endpoints::GET_TRAJECTORY.c_str());

  experimentServer.reset(new ExperimentServer(node_handle, action_endpoints::EXPERIMENT,
                                              boost::bind(&Experiment::experimentCallback, this, _1), false));
  experimentServer->start();
}

void Experiment::experimentCallback(const roaxdt_msgs::ExperimentGoalConstPtr &goal) {
  roaxdt_msgs::ExperimentFeedback action_feedback;
  roaxdt_msgs::ExperimentResult action_result;

  if (!sampleAcquisitionClient) {
    sampleAcquisitionClient.reset(new SampleAcquisitionClient(action_endpoints::SAMPLE_ACQUISITION, true));
    ROS_INFO("Waiting for sample_acquisition client to show up...");
    sampleAcquisitionClient->waitForServer();
    ROS_INFO("sample_acquisition showed up");
  }

  if (goal->run_sample_acquisition) {
    if (!prepareExperimentClient) {
      prepareExperimentClient.reset(new PrepareExperimentClient(action_endpoints::PREPARE_EXPERIMENT, true));
      ROS_INFO("Waiting for prepare_experiment client to show up...");
      prepareExperimentClient->waitForServer();
      ROS_INFO("prepare_experiment showed up");
    }

    boost::uuids::uuid uuid = boost::uuids::random_generator()();
    auto experiment_id = boost::lexical_cast<std::string>(uuid);
    experiment_data = ExperimentData(goal, experiment_id, experiments_base_path);

    numIterations = experiment_data.getGoalData().num_iterations;

    // call traj manager
    roaxdt_msgs::GetTrajectoryGoal getTrajectoryGoal;
    experiment_data.populateGetTrajectory(getTrajectoryGoal);

    getTrajectoryClient->sendGoal(getTrajectoryGoal);
    bool success_get_trajectory = getTrajectoryClient->waitForResult();
    ROS_INFO("Sent request to %s server. Waiting for result...", action_endpoints::GET_TRAJECTORY.c_str());
    if (!success_get_trajectory) {
      ROS_ERROR("%s failed. Returning error!", action_endpoints::GET_TRAJECTORY.c_str());
      action_result.success = false;
      experimentServer->setAborted(action_result);
      return;
    }
    auto getTrajectoryResult = getTrajectoryClient->getResult();
    experiment_data.setExperimentPoses(getTrajectoryResult->experiment_poses);
    experiment_data.setTrajectoryHash(getTrajectoryResult->trajectory_hash);

    roaxdt_msgs::PrepareExperimentGoal prepareExperimentGoal;
    prepareExperimentGoal.experiment_id = experiment_data.getGoalData().experiment_id;

    prepareExperimentClient->sendGoal(prepareExperimentGoal);
    prepareExperimentClient->waitForResult();
    ROS_INFO("Sent request to prepare_experiment server. Waiting for result...");
    auto prepareExperimentResult = prepareExperimentClient->getResult();

    roaxdt::experiment::writeExperimentData(experiment_data);

    // execute experiment preparation and sample acquisition
    action_feedback.experiment_id = experiment_data.getGoalData().experiment_id;
    action_feedback.experiment_path = experiment_data.getExperimentPath().string();
    experimentServer->publishFeedback(action_feedback);

    auto loadSampleHolderResult = loadSampleHolder(experiment_data.getGoalData().sample_holder_name);
    experiment_data.populateLoadSampleHolder(loadSampleHolderResult);

    experiment_data.setCameraMatrix(prepareExperimentResult->intrinsic_matrix);

    if (getTrajectoryResult->trajectories.size() > 0) {
      roaxdt_msgs::SampleAcquisitionGoal sampleAcquisitionGoal;
      sampleAcquisitionGoal.experiment_path = prepareExperimentResult->experiment_path;
      sampleAcquisitionGoal.experiment_id = experiment_data.getGoalData().experiment_id;
      sampleAcquisitionGoal.experiment_poses = experiment_data.getExperimentPoses();
      sampleAcquisitionGoal.spheres = loadSampleHolderResult->spheres;
      sampleAcquisitionGoal.forward_projection = experiment_data.getGoalData().simulate;
      sampleAcquisitionGoal.trajectories = getTrajectoryResult->trajectories;
      sampleAcquisitionGoal.start_states = getTrajectoryResult->start_states;
      sampleAcquisitionGoal.start_from_trajectory = 0;

      sampleAcquisitionClient->sendGoal(sampleAcquisitionGoal,
                                        boost::bind(&Experiment::sampleAcquisitionDoneCallback, this, _1, _2),
                                        boost::bind(&Experiment::sampleAcquisitionActiveCallback, this),
                                        boost::bind(&Experiment::sampleAcquisitionFeedbackCallback, this, _1));
      bool success_sample_acquisition = sampleAcquisitionClient->waitForResult();
      ROS_INFO("Sent request to sample_acquisition server. Waiting for result...");
      if (!success_sample_acquisition) {
        ROS_ERROR("Sample Acquisition failed. Returning error!");
        action_result.success = false;
        experimentServer->setAborted(action_result);
        return;
      }
      auto sampleAcquisitionResult = sampleAcquisitionClient->getResult();

      experiment_data.getSampleHolderPoses() = sampleAcquisitionResult->sample_holder_poses;
      experiment_data.setSuccessExecute(sampleAcquisitionResult->success_execute);
    }

    roaxdt::experiment::writeExperimentData(experiment_data);

  } else {
    // just replay experiment - read robot positions from json
    auto experiment_path = fs::path(experiments_base_path + goal->experiment_id);
    ROS_INFO("Reading file: %s", experiment_path.c_str());
    experiment_data = ExperimentData(goal, goal->experiment_id, experiments_base_path);

    try {
      roaxdt::experiment::readExperimentData(experiment_data, experiment_path);

      auto loadSampleHolderResult = loadSampleHolder(experiment_data.getGoalData().sample_holder_name);
      experiment_data.populateLoadSampleHolder(loadSampleHolderResult);
    } catch (const std::exception &e) {
      ROS_ERROR(e.what());
      action_result.success = false;
      experimentServer->setAborted(action_result);
      return;
    }

    // numIterations = experiment_data.getSampleHolderPoses().size();
  }
  auto experiment_path = fs::path(experiments_base_path + experiment_data.getGoalData().experiment_id);
  action_feedback.progress_sample_acquisition = 100.0;
  action_feedback.experiment_id = experiment_data.getGoalData().experiment_id;
  action_feedback.experiment_path = experiment_path.string();
  experimentServer->publishFeedback(action_feedback);
  // numIterations = experiment_data.getSampleHolderPoses().size();

  /** retrieve trajectory and check if all poses were execute - if not, resume execution until done*/
  roaxdt_msgs::GetTrajectoryGoal getTrajectoryGoal;
  getTrajectoryGoal.trajectory_hash = experiment_data.getTrajectoryHash();

  getTrajectoryClient->sendGoal(getTrajectoryGoal);
  bool success_get_trajectory = getTrajectoryClient->waitForResult();
  ROS_INFO("Sent request to %s server. Waiting for result...", action_endpoints::GET_TRAJECTORY.c_str());
  if (!success_get_trajectory) {
    ROS_ERROR("%s failed. Returning error!", action_endpoints::GET_TRAJECTORY.c_str());
    action_result.success = false;
    experimentServer->setAborted(action_result);
    return;
  }
  auto getTrajectoryResult = getTrajectoryClient->getResult();
  experiment_data.setExperimentPoses(getTrajectoryResult->experiment_poses);

  ROS_INFO("trajectory experiment poses size: %i", getTrajectoryResult->experiment_poses.size());
  ROS_INFO("Sample holder poses size before: %i", experiment_data.getSampleHolderPoses().size());
  size_t num_remaining_poses = getTrajectoryResult->experiment_poses.size() - experiment_data.getSampleHolderPoses().size();
  if (num_remaining_poses > 0) {
    roaxdt_msgs::SampleAcquisitionGoal sampleAcquisitionGoal;
    sampleAcquisitionGoal.experiment_path = experiment_path.string();
    sampleAcquisitionGoal.experiment_id = experiment_data.getGoalData().experiment_id;
    sampleAcquisitionGoal.experiment_poses = experiment_data.getExperimentPoses();
    sampleAcquisitionGoal.spheres = experiment_data.getSampleHolderSpheres();
    sampleAcquisitionGoal.forward_projection = experiment_data.getGoalData().simulate;
    sampleAcquisitionGoal.trajectories = getTrajectoryResult->trajectories;
    sampleAcquisitionGoal.start_states = getTrajectoryResult->start_states;
    sampleAcquisitionGoal.start_from_trajectory = experiment_data.getSampleHolderPoses().size();

    sampleAcquisitionClient->sendGoal(sampleAcquisitionGoal,
                                      boost::bind(&Experiment::sampleAcquisitionDoneCallback, this, _1, _2),
                                      boost::bind(&Experiment::sampleAcquisitionActiveCallback, this),
                                      boost::bind(&Experiment::sampleAcquisitionFeedbackCallback, this, _1));
    bool success_sample_acquisition = sampleAcquisitionClient->waitForResult();
    ROS_INFO("Sent request to sample_acquisition server. Waiting for result...");
    if (!success_sample_acquisition) {
      ROS_ERROR("Sample Acquisition failed. Returning error!");
      action_result.success = false;
      experimentServer->setAborted(action_result);
      return;
    }
    auto sampleAcquisitionResult = sampleAcquisitionClient->getResult();
  }
  ROS_INFO("Sample holder poses size after: %i", experiment_data.getSampleHolderPoses().size());
  /***/
  numIterations = experiment_data.getSampleHolderPoses().size();

  if (goal->run_register_images) {
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    roaxdt_msgs::RegisterImagesGoal registerImagesGoal;
    experiment_data.populateRegisterImages(registerImagesGoal);

    registerImagesClient->sendGoal(registerImagesGoal, boost::bind(&Experiment::registerImagesDoneCallback, this, _1, _2),
                                   boost::bind(&Experiment::registerImagesActiveCallback, this),
                                   boost::bind(&Experiment::registerImagesFeedbackCallback, this, _1));
    registerImagesClient->waitForResult();
    ROS_INFO("Sent request to register_images server. Waiting for result...");
    auto registerImagesResult = registerImagesClient->getResult();
    experiment_data.setSpherePixelsDetected(registerImagesResult->sphere_pixels_detected,
                                            registerImagesResult->num_sphere_pixels_per_image);
    ROS_INFO("Recevied result from register_images server");
    ROS_INFO("sphere pixels detected size: %i", registerImagesResult->sphere_pixels_detected.size());
    ROS_INFO("sphere pixels detected size: %i", experiment_data.getSpherePixelsDetected().size());

    ROS_INFO("Writing received detected sphere pixel coordinates to metadata file");
    roaxdt::experiment::writeExperimentData(experiment_data);
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    ROS_INFO_STREAM("Image registration took " << std::chrono::duration_cast<std::chrono::minutes>(end - begin).count() << ":"
                                               << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count()
                                               << " [min:sec]");
  } else {
    ROS_WARN("Image registration step not executed");
  }
  action_feedback.progress_register_images = 100.0;
  experimentServer->publishFeedback(action_feedback);

  if (goal->run_calibration) {
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    experiment_data.resizeGeometryMatrices();

    std::string calibrationCostsIndex = std::to_string(experiment_data.getGoalData().num_spheres_random_sampling) + "-" +
                                        std::to_string(experiment_data.getGoalData().helix_sampling_rate);
    ROS_INFO("Calibration costs index: %s", calibrationCostsIndex.c_str());
    auto costElement = experiment_data.getCalibrationCosts().find(calibrationCostsIndex);
    if (costElement == experiment_data.getCalibrationCosts().end()) {
      experiment_data.getCalibrationCosts().insert({calibrationCostsIndex, {}});
    }
    experiment_data.getCalibrationCosts().at(calibrationCostsIndex).resize(numIterations);
    std::string calibrationPath = (fs::path(calibrated_path) / fs::path(calibrationCostsIndex)).string();

    ROS_INFO_STREAM("blacklisted images size:" << experiment_data.getBlacklistedImages().size());

    boost::lockfree::queue<int> queue(processorCount);
    for (size_t i = 0; i < processorCount; i++) {
      queue.push(i);
    }

    int progress = 0;
#pragma omp parallel for
    for (size_t i = 0; i < numIterations; i++) {
      if (experiment_data.getSuccessExecute().at(i) == 1) {
        roaxdt_msgs::CalibrationGoal calibrationGoal;
        experiment_data.populateCalibration(calibrationGoal, i, costThresholdPerSphere);

        size_t worker_id;
        queue.pop(worker_id);
        ROS_INFO_STREAM("Image #" << i << " is being processed by worker #" << worker_id);

        calibrationClients.at(worker_id)->sendGoal(calibrationGoal);
        ROS_INFO_STREAM("Sent request to calibration server #" << worker_id << ". Waiting for result...");
        calibrationClients.at(worker_id)->waitForResult();
        auto calibrationResult = calibrationClients.at(worker_id)->getResult();

        queue.push(worker_id);

        auto index_matrix = i * 9;
        auto index_vec = i * 3;
        for (size_t index_tmp = 0; index_tmp < 9; index_tmp++) {
          experiment_data.getRotationMatrices().at(index_matrix + index_tmp) = calibrationResult->rotation_matrix.at(index_tmp);
        }
        for (size_t index_tmp = 0; index_tmp < 3; index_tmp++) {
          experiment_data.getTranslationVectors().at(index_vec + index_tmp) = calibrationResult->translation_vector.at(index_tmp);
          experiment_data.getCameraCenterVectors().at(index_vec + index_tmp) =
              calibrationResult->camera_center_vector.at(index_tmp);
        }

        if (calibrationResult->blacklist_image) {
          experiment_data.getBlacklistedImages().push_back(i);
        }

        experiment_data.getCalibrationCosts().at(calibrationCostsIndex).at(i) = calibrationResult->calibration_cost;

        ROS_INFO_STREAM("Recevied result from calibration server #" << worker_id);

#pragma omp atomic update
        progress += 1;

        auto progress_relative = ((float)progress / (float)numIterations) * 100.0;
        action_feedback.progress_calibration = progress_relative;
        action_feedback.image_id = i;
        action_feedback.directory = calibrationPath;

        experimentServer->publishFeedback(action_feedback);
      } else {
        experiment_data.getBlacklistedImages().push_back(i);
      }
    }

    ROS_INFO("Writing received calibration results to metadata file");
    roaxdt::experiment::writeExperimentData(experiment_data);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    ROS_INFO_STREAM("Calibration took " << std::chrono::duration_cast<std::chrono::hours>(end - begin).count() << ":"
                                        << std::chrono::duration_cast<std::chrono::minutes>(end - begin).count() << " [h:min]");
  } else {
    ROS_WARN("Calibration step not executed");
  }

  if (goal->run_computed_tomography) {
    roaxdt_msgs::ComputedTomographyGoal computedTomographyGoal;
    experiment_data.populateComputedTomography(computedTomographyGoal);

    computedTomographyClient->sendGoal(computedTomographyGoal,
                                       boost::bind(&Experiment::computedTomographyDoneCallback, this, _1, _2),
                                       boost::bind(&Experiment::computedTomographyActiveCallback, this),
                                       boost::bind(&Experiment::computedTomographyFeedbackCallback, this, _1));
    computedTomographyClient->waitForResult();
    ROS_INFO("Sent request to computed_tomography server. Waiting for result...");
    auto computedTomographyErrorResult = computedTomographyClient->getResult();
    ROS_INFO("Recevied result from computed tomography server");
  } else {
    ROS_WARN("Computed tomography step not executed");
  }
  action_feedback.progress_computed_tomography = 100.0;
  experimentServer->publishFeedback(action_feedback);

  action_result.success = true;
  action_result.experiment_id = action_feedback.experiment_id;
  experimentServer->setSucceeded(action_result);

  // cleanup
  experiment_data = ExperimentData();
}

void Experiment::sampleAcquisitionFeedbackCallback(const roaxdt_msgs::SampleAcquisitionFeedbackConstPtr &feedback) {
  ROS_INFO_STREAM("Received sample_acquisition feedback: " << feedback->iterations_finished);
  roaxdt_msgs::ExperimentFeedback experiment_feedback;
  experiment_feedback.progress_sample_acquisition = ((float)feedback->iterations_finished / (float)numIterations) * 100.0;
  experiment_feedback.image_id = feedback->image_id;
  experiment_feedback.directory = detector_path;

  // write intermediate state to filesystem. Makes experiment usable after interruption
  experiment_data.getSampleHolderPoses().push_back(feedback->sample_holder_pose);
  experiment_data.getSuccessExecute().push_back(feedback->success_execute);
  roaxdt::experiment::writeExperimentData(experiment_data);

  experimentServer->publishFeedback(experiment_feedback);
}

void Experiment::sampleAcquisitionDoneCallback(const actionlib::SimpleClientGoalState &state,
                                               const roaxdt_msgs::SampleAcquisitionResultConstPtr &result) {
  ROS_INFO_STREAM("Received sample_acquisition result: " << result->success);
  roaxdt_msgs::ExperimentFeedback experiment_feedback;
  experiment_feedback.progress_sample_acquisition = numIterations;
  experimentServer->publishFeedback(experiment_feedback);
}

void Experiment::sampleAcquisitionActiveCallback() { ROS_INFO_STREAM("sample_acquisition goal active"); }

void Experiment::registerImagesFeedbackCallback(const roaxdt_msgs::RegisterImagesFeedbackConstPtr &feedback) {
  ROS_INFO_STREAM("Received register_images feedback: " << feedback->progress);
  roaxdt_msgs::ExperimentFeedback experiment_feedback;
  experiment_feedback.progress_register_images = ((float)feedback->progress / (float)numIterations) * 100.0;
  experiment_feedback.image_id = feedback->image_id;
  experiment_feedback.directory = segmented_path;
  experimentServer->publishFeedback(experiment_feedback);
}

void Experiment::registerImagesDoneCallback(const actionlib::SimpleClientGoalState &state,
                                            const roaxdt_msgs::RegisterImagesResultConstPtr &result) {
  ROS_INFO_STREAM("Received register_images result: " << result->success);
  roaxdt_msgs::ExperimentFeedback experiment_feedback;
  experiment_feedback.progress_register_images = numIterations;
  experimentServer->publishFeedback(experiment_feedback);
}

void Experiment::registerImagesActiveCallback() { ROS_INFO_STREAM("register_images goal active"); }

void Experiment::computedTomographyFeedbackCallback(const roaxdt_msgs::ComputedTomographyFeedbackConstPtr &feedback) {
  ROS_INFO_STREAM("Received computed_tomography feedback: " << feedback->progress);
  roaxdt_msgs::ExperimentFeedback experiment_feedback;
  experiment_feedback.progress_computed_tomography = ((float)feedback->progress / (float)feedback->num_iterations_recon) * 100.0;
  experiment_feedback.image_id = feedback->image_id;
  experiment_feedback.directory = reconstructed_path;
  experimentServer->publishFeedback(experiment_feedback);
}
void Experiment::computedTomographyDoneCallback(const actionlib::SimpleClientGoalState &state,
                                                const roaxdt_msgs::ComputedTomographyResultConstPtr &result) {
  ROS_INFO_STREAM("Received computed_tomography result: " << result->success);
  roaxdt_msgs::ExperimentFeedback experiment_feedback;
  experiment_feedback.progress_computed_tomography = numIterations;
  experimentServer->publishFeedback(experiment_feedback);
}

void Experiment::computedTomographyActiveCallback() { ROS_INFO_STREAM("computed_tomography goal active"); }

roaxdt_msgs::LoadSampleHolderResultConstPtr Experiment::loadSampleHolder(const std::string &sample_holder) {
  roaxdt_msgs::LoadSampleHolderGoal loadSampleHolderGoal;
  loadSampleHolderGoal.sample_holder_name = sample_holder;

  loadSampleHolderClient->sendGoal(loadSampleHolderGoal);
  loadSampleHolderClient->waitForResult();
  ROS_INFO("Sent request to load_sample_holder server. Waiting for result...");

  return loadSampleHolderClient->getResult();
}

} // namespace nodes
} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, roaxdt::constants::nodes::EXPERIMENT);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");

  roaxdt::nodes::Experiment node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}