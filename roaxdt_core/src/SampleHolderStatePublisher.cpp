#include "roaxdt_core/SampleHolderStatePublisher.h"

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

SampleHolderStatePublisher::SampleHolderStatePublisher(const ros::NodeHandle &node_handle,
                                                       const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle) {
  this->init();
  this->publishState();
}

void SampleHolderStatePublisher::init() {
  // load config values
  try {
    readConfigParameter<std::string>(node_handle, experiments::GRIP_SUFFIX, grip_suffix);
    readConfigParameter<std::string>(node_handle, experiments::SAMPLE_SUFFIX, sample_suffix);
    readConfigParameter<std::string>(node_handle, experiments::CENTER_SUFFIX, center_suffix);
    readConfigParameter<double>(node_handle, robot_control::GRIPPER_HEIGHT, gripper_height);
    readConfigParameter<std::string>(node_handle, experiments::VTK_SUFFIX, vtk_suffix);
    readConfigParameter<std::string>(node_handle, experiments::SAMPLE_HOLDER_REFERENCE_LINK_VTK,
                                     sample_holder_reference_link_vtk);
    readConfigParameter<std::string>(node_handle, roaxdt::constants::robot_control::ATTACHED_OBJECT_LINK, attached_object_link);
  } catch (const std::runtime_error &exception) { ROS_ERROR_STREAM(exception.what()); }

  // attach sample holder to robot's left finger
  if (attached_object_link.compare(std::string("panda_hand")) == 0) {
    sample_holder_parent_frame = PANDA_FINGER_LINKS.at(0);
  } else {
    sample_holder_parent_frame = PANDA_EE_PARENT_LINK;
  }

  ROS_INFO_STREAM("sample holder parent frame: " << sample_holder_parent_frame);
  ROS_INFO_STREAM("girpper height: " << gripper_height);

  set_sample_holder_geometry_server = node_handle.advertiseService(
      service_endpoints::SET_SAMPLE_HOLDER_GEOMETRY, &SampleHolderStatePublisher::setSampleHolderGeometryCallback, this);

  transform_sample_holder_grip.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  transform_sample_holder_grip.setRotation(tf::Quaternion(0.0, 0.0, 0.0, 1.0));
  transform_sample_holder_center.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  transform_sample_holder_center.setRotation(tf::Quaternion(0.0, 0.0, 0.0, 1.0));
  transform_sample_holder_sample.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  transform_sample_holder_sample.setRotation(tf::Quaternion(0.0, 0.0, 0.0, 1.0));
  transform_sample_holder_center_vtk.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  transform_sample_holder_center_vtk.setRotation(tf::Quaternion(0.0, 0.0, 0.0, 1.0));
  transform_sample_holder_sample_vtk.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  transform_sample_holder_sample_vtk.setRotation(tf::Quaternion(0.0, 0.0, 0.0, 1.0));
}

void SampleHolderStatePublisher::publishState() {
  ros::Rate rate(30);
  while (node_handle.ok()) {

    /**
     * CAUTION: we attach the sample holder at it's grip frame to the finger
     * The sample center is similarly directly attached to the holder's center
     * --> sample_holder and sample have single edges in the tf tree!
     *
     * frame -> child frame:
     * panda_leftfinger (sample_holder_parent_frame) -> sample_holder_grip_center
     * sample_holder_grip_center -> sample_holder_center
     * sample_holder_center -> sample_center
     */
    if (!sample_holder_center_frame.empty()) {
      tf_broadcaster.sendTransform(tf::StampedTransform(transform_sample_holder_grip, ros::Time::now(),
                                                        sample_holder_parent_frame, sample_holder_grip_frame));
      tf_broadcaster.sendTransform(tf::StampedTransform(transform_sample_holder_center, ros::Time::now(),
                                                        sample_holder_grip_frame, sample_holder_center_frame));
      tf_broadcaster.sendTransform(tf::StampedTransform(transform_sample_holder_center_vtk, ros::Time::now(),
                                                        sample_holder_center_frame, sample_holder_center_vtk_frame));
      tf_broadcaster.sendTransform(tf::StampedTransform(transform_sample_holder_sample_vtk, ros::Time::now(), sample_center_frame,
                                                        sample_holder_sample_vtk_frame));
      tf_broadcaster.sendTransform(tf::StampedTransform(transform_sample_holder_sample, ros::Time::now(),
                                                        sample_holder_center_frame, sample_center_frame));
    }

    ros::spinOnce();
    rate.sleep();
  }
}

bool SampleHolderStatePublisher::setSampleHolderGeometryCallback(roaxdt_msgs::SetSampleHolderGeometry::Request &req,
                                                                 roaxdt_msgs::SetSampleHolderGeometry::Response &res) {
  // read sample holder properties as it might change over the lifetime of this process
  if (!node_handle.getParam(experiments::SAMPLE_HOLDER_NAME, sample_holder_name)) {
    ROS_ERROR("could not read current sample holder head name from parameter server");
  }
  if (!node_handle.getParam(experiments::SAMPLE_NAME, sample_name)) {
    ROS_ERROR("could not read current sample name from parameter server");
  }

  std::stringstream stream_grip_frame, stream_center_frame, stream_sample_center;
  stream_grip_frame << sample_holder_name << grip_suffix;
  sample_holder_grip_frame = stream_grip_frame.str();
  stream_center_frame << sample_holder_name << center_suffix;
  sample_holder_center_frame = stream_center_frame.str();
  stream_sample_center << sample_holder_name << sample_suffix;
  sample_center_frame = stream_sample_center.str();
  ROS_INFO_STREAM("sample holder grip link frame: " << sample_holder_grip_frame);
  ROS_INFO_STREAM("sample holder center link frame: " << sample_holder_center_frame);
  ROS_INFO_STREAM("sample center link frame: " << sample_center_frame);

  // the assumption is that the sample holder is tightly gripped by the fingers
  // and hence it is centered relative to the hand frame -> x and y are 0.0
  // transform_sample_holder_grip.setOrigin(tf::Vector3(0.0, 0.0, gripper_height));
  // transform_sample_holder_grip.setOrigin(tf::Vector3(0.0, 0.0, req.gripper_sample_holder.z));

  transform_sample_holder_center.setOrigin(
      tf::Vector3(req.center_sample_holder.position.x, req.center_sample_holder.position.y, req.center_sample_holder.position.z));
  tf::Quaternion sample_holder_orientation_tf;
  tf::quaternionMsgToTF(req.center_sample_holder.orientation, sample_holder_orientation_tf);
  transform_sample_holder_center.setRotation(sample_holder_orientation_tf);

  if (attached_object_link.compare(std::string("panda_hand")) == 0) {
    Eigen::Isometry3d transformLeftFingerToWorldEigen, transformRightFingerToWorldEigen;
    tf::StampedTransform transformLeftFingerToWorld =
        roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, PANDA_EE_LINK, PANDA_FINGER_LINKS.at(0));
    tf::transformTFToEigen(transformLeftFingerToWorld, transformLeftFingerToWorldEigen);
    tf::StampedTransform transformRightFingerToWorld =
        roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, PANDA_EE_LINK, PANDA_FINGER_LINKS.at(1));
    tf::transformTFToEigen(transformRightFingerToWorld, transformRightFingerToWorldEigen);
    leftFingerPosition = transformLeftFingerToWorldEigen.translation().head<3>();
    rightFingerPosition = transformRightFingerToWorldEigen.translation().head<3>();

    Eigen::Vector3d fingerDifference = leftFingerPosition - rightFingerPosition;
    fingerDifference.x() *= -1.0;
    fingerDifference.y() *= -1.0;
    ROS_INFO_STREAM("left finger:" << std::endl << leftFingerPosition);
    ROS_INFO_STREAM("right finger:" << std::endl << rightFingerPosition);
    ROS_INFO_STREAM("finger difference:" << std::endl << fingerDifference);
    transform_sample_holder_grip.setOrigin(tf::Vector3(fingerDifference.x() / 2.0, fingerDifference.y() / 2.0, gripper_height));
  }
  tf::Quaternion sample_holder_grip_rotation;
  tf::quaternionMsgToTF(req.gripper_sample_holder.orientation, sample_holder_grip_rotation);
  transform_sample_holder_grip.setRotation(sample_holder_grip_rotation);

  std::stringstream stream_center_vtk_frame;
  stream_center_vtk_frame << sample_holder_center_frame << vtk_suffix;
  sample_holder_center_vtk_frame = stream_center_vtk_frame.str();

  std::stringstream stream_sample_vtk_frame;
  stream_sample_vtk_frame << sample_center_frame << vtk_suffix;
  sample_holder_sample_vtk_frame = stream_sample_vtk_frame.str();

  tf::StampedTransform world_to_vtk_tf =
      roaxdt::ros_utils::getTransformToTargetFrame(tf_listener, WORLD_LINK, sample_holder_reference_link_vtk);
  tf::poseTFToEigen(world_to_vtk_tf, world_to_vtk_eigen);
  ROS_INFO("world to vtk rotation quaternion: x %f y %f z %f w %f", world_to_vtk_tf.getRotation().getX(),
           world_to_vtk_tf.getRotation().getY(), world_to_vtk_tf.getRotation().getZ(), world_to_vtk_tf.getRotation().getW());

  transform_sample_holder_center_vtk.setOrigin(world_to_vtk_tf.getOrigin());
  transform_sample_holder_center_vtk.setRotation(world_to_vtk_tf.getRotation());
  transform_sample_holder_sample_vtk.setOrigin(world_to_vtk_tf.getOrigin());
  transform_sample_holder_sample_vtk.setRotation(world_to_vtk_tf.getRotation());

  transform_sample_holder_sample.setOrigin(tf::Vector3(req.center_sample.x, req.center_sample.y, req.center_sample.z));

  res.grip_frame = sample_holder_grip_frame;
  res.center_frame = sample_holder_center_frame;

  return true;
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  namespace fs = boost::filesystem;

  ros::init(argc, argv, roaxdt::constants::nodes::SAMPLE_HOLDER_STATE_PUBLISHER);
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");
  roaxdt::nodes::SampleHolderStatePublisher node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}