// Adapted from franka_ros/franka_example_controllers
#pragma once

#include <array>
#include <cmath>
#include <memory>
#include <string>
#include <vector>

#include <controller_interface/controller_base.h>
#include <controller_interface/multi_interface_controller.h>
#include <franka_hw/franka_state_interface.h>
#include <hardware_interface/hardware_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <pluginlib/class_list_macros.h>
#include <roaxdt_controllers/CalibrateJointsMsg.h>
#include <ros/node_handle.h>
#include <ros/ros.h>
#include <ros/time.h>

namespace roaxdt {

namespace controllers {

class JointVelocityAlignmentController
    : public controller_interface::MultiInterfaceController<hardware_interface::VelocityJointInterface,
                                                            franka_hw::FrankaStateInterface> {
public:
  bool init(hardware_interface::RobotHW *robot_hardware, ros::NodeHandle &node_handle) override;
  void update(const ros::Time &, const ros::Duration &period) override;
  void starting(const ros::Time &) override;
  void stopping(const ros::Time &) override;

  bool calibrateJointsCallback(roaxdt_controllers::CalibrateJointsMsg::Request &req,
                               roaxdt_controllers::CalibrateJointsMsg::Response &res);
  template <typename T> int sgn(T val);

private:
  hardware_interface::VelocityJointInterface *velocity_joint_interface_;
  std::vector<hardware_interface::JointHandle> velocity_joint_handles_;
  ros::Duration elapsed_time_;
  double v_max;
  double joint_error_max;
  std::vector<double> command_;
  std::vector<bool> command_success_;
  ros::Subscriber subscriber_command_;

  ros::ServiceServer joint_calibration_service;

  std::unique_ptr<franka_hw::FrankaStateHandle> state_handle_;
};

} // namespace controllers

} // namespace roaxdt