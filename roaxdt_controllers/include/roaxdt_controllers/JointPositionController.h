
#include <vector>

#include <controller_interface/controller.h>
#include <hardware_interface/joint_command_interface.h>
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Float64MultiArray.h>

namespace roaxdt {

namespace controllers {

class JointPositionController : public controller_interface::Controller<hardware_interface::PositionJointInterface> {

  bool init(hardware_interface::PositionJointInterface *hw, ros::NodeHandle &n);

  void update(const ros::Time &time, const ros::Duration &period);

  void setCommandCallback(const std_msgs::Float64MultiArrayConstPtr &msg);

  void starting(const ros::Time &time);

  void stopping(const ros::Time &time);

private:
  std::vector<hardware_interface::JointHandle> joint_handles_;
  std::vector<double> gains_vec_;
  std::vector<double> command_;
  ros::Subscriber sub_command_;
};

} // namespace controllers

} // namespace roaxdt