// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
// Adapted from franka_ros/franka_example_controllers
#include <roaxdt_controllers/CartesianVelocityForceFeedbackController.h>

#include <array>
#include <cmath>
#include <memory>
#include <string>

#include <controller_interface/controller_base.h>
#include <hardware_interface/hardware_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

namespace roaxdt {

namespace controllers {

bool CartesianVelocityForceFeedbackController::init(hardware_interface::RobotHW *robot_hardware, ros::NodeHandle &node_handle) {
  std::vector<std::string> joint_names;
  std::string arm_id;
  if (!node_handle.getParam("arm_id", arm_id)) {
    ROS_ERROR("CartesianVelocityController: Could not get parameter arm_id");
    return false;
  }
  if (!node_handle.getParam("v_max", v_max)) {
    ROS_ERROR("CartesianVelocityController: Invalid or no max velocity parameter provided, aborting controller init!");
    return false;
  }
  if (!node_handle.getParam("f_z_desired", f_z_desired)) {
    ROS_ERROR("CartesianVelocityController: Invalid or no force z axis parameter provided, aborting controller init!");
    return false;
  }
  if (!node_handle.getParam("slowdown_factor", slowdown_factor)) {
    ROS_ERROR("CartesianVelocityController: Invalid or no slowdown factor parameter provided, aborting controller init!");
    return false;
  }
  if (!node_handle.getParam("warmup_factor", warmup_factor)) {
    ROS_ERROR("CartesianVelocityController: Invalid or no slowdown factor parameter provided, aborting controller init!");
    return false;
  }

  velocity_cartesian_interface_ = robot_hardware->get<franka_hw::FrankaVelocityCartesianInterface>();
  if (velocity_cartesian_interface_ == nullptr) {
    ROS_ERROR("CartesianVelocityController: Could not get Cartesian velocity interface from hardware");
    return false;
  }
  try {
    velocity_cartesian_handle_ =
        std::make_unique<franka_hw::FrankaCartesianVelocityHandle>(velocity_cartesian_interface_->getHandle(arm_id + "_robot"));
  } catch (const hardware_interface::HardwareInterfaceException &e) {
    ROS_ERROR_STREAM("CartesianVelocityController: Exception getting Cartesian handle: " << e.what());
    return false;
  }

  auto state_interface = robot_hardware->get<franka_hw::FrankaStateInterface>();
  if (state_interface == nullptr) {
    ROS_ERROR("CartesianVelocityController: Could not get state interface from "
              "hardware");
    return false;
  }

  try {
    state_handle_ = std::make_unique<franka_hw::FrankaStateHandle>(state_interface->getHandle(arm_id + "_robot"));
  } catch (hardware_interface::HardwareInterfaceException &ex) {
    ROS_ERROR_STREAM("CartesianVelocityController: Exception getting state handle from interface: " << ex.what());
    return false;
  }

  return true;
}

void CartesianVelocityForceFeedbackController::starting(const ros::Time & /* time */) { elapsed_time_ = ros::Duration(0.0); }

void CartesianVelocityForceFeedbackController::update(const ros::Time & /* time */, const ros::Duration &period) {
  elapsed_time_ += period;

  auto robot_state = state_handle_->getRobotState();
  double f_z = robot_state.K_F_ext_hat_K[2];
  auto f_z_error = f_z_desired - f_z;

  double v_z = 0.0;
  if (f_z_error > 0.0) {
    v_z = v_max * std::tanh(elapsed_time_.toSec() * warmup_factor);
  } else {
    v_z = v_z_last * slowdown_factor;
    ROS_INFO_STREAM(v_z);
  }
  v_z_last = v_z;

  std::array<double, 6> command = {{0.0, 0.0, v_z, 0.0, 0.0, 0.0}};

  velocity_cartesian_handle_->setCommand(command);
}

void CartesianVelocityForceFeedbackController::stopping(const ros::Time & /*time*/) {
  // WARNING: DO NOT SEND ZERO VELOCITIES HERE AS IN CASE OF ABORTING DURING
  // MOTION A JUMP TO ZERO WILL BE COMMANDED PUTTING HIGH LOADS ON THE ROBOT.
  // LET THE DEFAULT BUILT-IN STOPPING BEHAVIOR SLOW DOWN THE ROBOT.
}

} // namespace controllers

} // namespace roaxdt

PLUGINLIB_EXPORT_CLASS(roaxdt::controllers::CartesianVelocityForceFeedbackController, controller_interface::ControllerBase)
