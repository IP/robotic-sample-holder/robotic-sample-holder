#include <roaxdt_controllers/JointPositionController.h>

namespace roaxdt {

namespace controllers {

bool JointPositionController::init(hardware_interface::PositionJointInterface *hw, ros::NodeHandle &n) {
  std::vector<std::string> joint_names;
  if (!n.getParam("joint_names", joint_names)) {
    ROS_ERROR("Could not read joint names from param server");
    return false;
  }

  // retrieve gains
  if (!n.getParam("gains", gains_vec_)) {
    ROS_ERROR("Could not read joint gains from param server");
    return false;
  }

  for (auto &joint_name : joint_names) {
    joint_handles_.push_back(hw->getHandle(joint_name));
  }

  for (auto &joint_handle : joint_handles_) {
    command_.push_back(joint_handle.getPosition());
  }

  sub_command_ =
      n.subscribe<std_msgs::Float64MultiArray>(std::string("command"), 1, &JointPositionController::setCommandCallback, this);

  return true;
}

void JointPositionController::update(const ros::Time &time, const ros::Duration &period) {
  for (size_t i = 0; i < joint_handles_.size(); i++) {
    double error = command_.at(i) - joint_handles_.at(i).getPosition();
    double commanded_effort = error * gains_vec_.at(i);

    joint_handles_.at(i).setCommand(commanded_effort);
  }
}

void JointPositionController::setCommandCallback(const std_msgs::Float64MultiArrayConstPtr &msg) { command_ = msg->data; }

void JointPositionController::starting(const ros::Time &time) {}

void JointPositionController::stopping(const ros::Time &time) {}

} // namespace controllers

} // namespace roaxdt

PLUGINLIB_EXPORT_CLASS(roaxdt::controllers::JointPositionController, controller_interface::ControllerBase)