// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
// Adapted from franka_ros/franka_example_controllers
#include <roaxdt_controllers/JointVelocityAlignmentController.h>

namespace roaxdt {

namespace controllers {

bool JointVelocityAlignmentController::init(hardware_interface::RobotHW *robot_hardware, ros::NodeHandle &node_handle) {
  std::string arm_id;
  if (!node_handle.getParam("arm_id", arm_id)) {
    ROS_ERROR("JointVelocityAlignmentController: Could not get parameter arm_id");
    return false;
  }
  if (!node_handle.getParam("v_max", v_max)) {
    ROS_ERROR("JointVelocityAlignmentController: Invalid or no max velocity parameter provided, aborting controller init!");
    return false;
  }
  if (!node_handle.getParam("joint_error_max", joint_error_max)) {
    ROS_ERROR("JointVelocityAlignmentController: Invalid or no max joint error parameter provided, aborting controller init!");
    return false;
  }

  velocity_joint_interface_ = robot_hardware->get<hardware_interface::VelocityJointInterface>();
  if (velocity_joint_interface_ == nullptr) {
    ROS_ERROR("JointVelocityAlignmentController: Error getting velocity joint interface from hardware!");
    return false;
  }
  std::vector<std::string> joint_names;
  if (!node_handle.getParam("joint_names", joint_names)) {
    ROS_ERROR("JointVelocityAlignmentController: Could not parse joint names");
  }
  if (joint_names.size() != 7) {
    ROS_ERROR_STREAM("JointVelocityAlignmentController: Wrong number of joint names, got " << joint_names.size()
                                                                                           << " instead of 7 names!");
    return false;
  }
  velocity_joint_handles_.resize(7);
  for (size_t i = 0; i < 7; ++i) {
    try {
      velocity_joint_handles_[i] = velocity_joint_interface_->getHandle(joint_names[i]);
    } catch (const hardware_interface::HardwareInterfaceException &ex) {
      ROS_ERROR_STREAM("JointVelocityAlignmentController: Exception getting joint handles: " << ex.what());
      return false;
    }
  }

  auto state_interface = robot_hardware->get<franka_hw::FrankaStateInterface>();
  if (state_interface == nullptr) {
    ROS_ERROR("JointVelocityAlignmentController: Could not get state interface from hardware");
    return false;
  }

  try {
    state_handle_ = std::make_unique<franka_hw::FrankaStateHandle>(state_interface->getHandle(arm_id + "_robot"));
  } catch (hardware_interface::HardwareInterfaceException &ex) {
    ROS_ERROR_STREAM("CartesianVelocityController: Exception getting state handle from interface: " << ex.what());
    return false;
  }

  // Create topic fo subscriber
  joint_calibration_service =
      node_handle.advertiseService("calibrate_joints", &JointVelocityAlignmentController::calibrateJointsCallback, this);

  return true;
}

void JointVelocityAlignmentController::starting(const ros::Time & /* time */) { elapsed_time_ = ros::Duration(0.0); }

void JointVelocityAlignmentController::update(const ros::Time & /* time */, const ros::Duration &period) {
  elapsed_time_ += period;

  if (!command_.empty()) {
    size_t correct_angles_counter = 0;
    for (size_t i = 0; i < velocity_joint_handles_.size(); i++) {
      double error = command_.at(i) - state_handle_->getRobotState().q.at(i);
      double commanded_velocity = 0.0;
      if (abs(error) > joint_error_max) {
        commanded_velocity = sgn(error) * v_max;
      } else {
        ++correct_angles_counter;
      }

      velocity_joint_handles_.at(i).setCommand(commanded_velocity);
    }

    // undo command if tolerances are met for all joints
    if (correct_angles_counter == velocity_joint_handles_.size()) {
      ROS_INFO("Success joint velocity controller!");

      for (size_t i = 0; i < velocity_joint_handles_.size(); i++) {
        double error = command_.at(i) - state_handle_->getRobotState().q.at(i);
        ROS_WARN_STREAM(i << " : " << std::fixed << std::setprecision(8) << error);
      }

      command_.clear();
    }
  } else {
    for (auto joint_handle : velocity_joint_handles_) {
      joint_handle.setCommand(0.0);
    }
  }
}

void JointVelocityAlignmentController::stopping(const ros::Time & /*time*/) {
  // WARNING: DO NOT SEND ZERO VELOCITIES HERE AS IN CASE OF ABORTING DURING
  // MOTION A JUMP TO ZERO WILL BE COMMANDED PUTTING HIGH LOADS ON THE ROBOT.
  // LET THE DEFAULT BUILT-IN STOPPING BEHAVIOR SLOW DOWN THE ROBOT.
}

bool JointVelocityAlignmentController::calibrateJointsCallback(roaxdt_controllers::CalibrateJointsMsg::Request &req,
                                                               roaxdt_controllers::CalibrateJointsMsg::Response &res) {
  ROS_INFO_STREAM("Entering calibrate joints service callback");
  command_ = req.joint_angles;

  while (!command_.empty()) {
    // ros::Duration(0.5).sleep();
    ros::spinOnce();
  }

  res.success = true;

  return true;
}

// https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
template <typename T> int JointVelocityAlignmentController::sgn(T val) { return (T(0) < val) - (val < T(0)); }

} // namespace controllers

} // namespace roaxdt

PLUGINLIB_EXPORT_CLASS(roaxdt::controllers::JointVelocityAlignmentController, controller_interface::ControllerBase)
