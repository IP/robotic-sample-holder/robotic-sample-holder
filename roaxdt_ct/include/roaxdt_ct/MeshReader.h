#pragma once

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <geometry_msgs/Point.h>
#include <roaxdt_msgs/ReadMesh.h>
#include <roaxdt_utils/roaxdt.h>

#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <vtkCleanPolyData.h>
#include <vtkImageData.h>
#include <vtkImageResize.h>
#include <vtkImageStencil.h>
#include <vtkMetaImageWriter.h>
#include <vtkNew.h>
#include <vtkOBJReader.h>
#include <vtkPointData.h>
#include <vtkPolyDataToImageStencil.h>
#include <vtkSmartPointer.h>

namespace roaxdt {

namespace nodes {

class MeshReader {

public:
  MeshReader() = delete;
  ~MeshReader() = default;

  MeshReader(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::MESH_READER};

  // config values loaded during startup

  // ROS communication related - variables
  ros::ServiceServer read_mesh_server;

  void init();
  bool readMeshCallback(roaxdt_msgs::ReadMesh::Request &req, roaxdt_msgs::ReadMesh::Response &res);
};

} // namespace nodes

} // namespace roaxdt