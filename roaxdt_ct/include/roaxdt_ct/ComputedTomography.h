#pragma once

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED

#include <Eigen/Dense>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <boost/algorithm/string.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <eigen_conversions/eigen_msg.h>
#include <elsa.h>
#include <elsa/core/Descriptors/PlanarDetectorDescriptor.h>
#include <elsa/generators/EllipseGenerator.h>
#include <geometry_msgs/Point.h>
#include <iostream>
#include <limits>
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <random>
#include <roaxdt_msgs/BackProjectionAction.h>
#include <roaxdt_msgs/ComputePoseWeight.h>
#include <roaxdt_msgs/ComputedTomographyAction.h>
#include <roaxdt_msgs/ForwardProjection.h>
#include <roaxdt_msgs/SetTargetVolume.h>
#include <roaxdt_utils/roaxdt.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>

namespace roaxdt {

namespace nodes {

namespace fs = ::boost::filesystem;

class ComputedTomography {

  typedef actionlib::SimpleActionServer<roaxdt_msgs::ComputedTomographyAction> ComputedTomographyServer;
  typedef actionlib::SimpleActionServer<roaxdt_msgs::BackProjectionAction> BackProjectionServer;

public:
  ComputedTomography() = delete;
  ~ComputedTomography() = default;

  ComputedTomography(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);

private:
  // public ros node handle
  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle;
  std::string node_name_{roaxdt::constants::nodes::COMPUTED_TOMOGRAPHY};

  // config values loaded during startup
  std::string image_plane_frame_suffix, camera_link_suffix, detector_camera_name, camera_frame, camera_frame_image_plane,
      experiments_base_path, processed_path, detector_corrected_path, reconstructed_path, default_image_file_type,
      corrected_image_file_type, reconstruction_file_type, reconstruction_file_name, back_projection_file_type,
      back_projection_file_name, segmentation_file_type, segmentation_file_name, camera_info_suffix, detector_camera_id,
      camera_info_topic;
  std::vector<elsa::real_t> domain_spacing, detector_spacing, detector_resolution;
  float dim_x, dim_y, dim_z;
  int back_projection_scaling;
  elsa::RealVector_t domainSpacing, rangeSpacing;
  elsa::IndexVector_t sizeRange, sizeDomain;
  Eigen::VectorXf volume;
  std::unique_ptr<elsa::VolumeDescriptor> domainDesc;
  std::unique_ptr<elsa::DetectorDescriptor> sinoDesc;
  std::unique_ptr<elsa::DataContainer<elsa::real_t>> domain;
  std::vector<Eigen::Vector3i> spheres;
  Eigen::Matrix3f intrinsic_matrix_live;
  bool simulation;

  // ROS communication related - variables
  std::unique_ptr<ComputedTomographyServer> computedTomographyServer;
  std::unique_ptr<BackProjectionServer> backProjectionServer;
  ros::ServiceServer forward_projection_server;
  ros::ServiceServer set_target_volume_server;
  ros::ServiceServer compute_pose_weight_server;
  tf::TransformListener tf_listener;

  void init();
  void executeComputedTomography(const roaxdt_msgs::ComputedTomographyGoalConstPtr &goal);
  void executeBackProjection(const roaxdt_msgs::BackProjectionGoalConstPtr &goal);

  void readSinograms(std::string experiment_id, int &image_width, int &image_height, float scaling,
                     Eigen::Matrix<float, Eigen::Dynamic, 1> &sinograms, const std::vector<uint32_t> &blacklisted_images);
  void reconstruct(Eigen::Matrix<float, Eigen::Dynamic, 1> &sinogram_eigen, int num_iter_recon, int num_iterations,
                   int image_width, int image_height, float scaling, std::string experiment_id,
                   const std::vector<float> &rotation_matrices, const std::vector<float> &translation_vectors,
                   const std::vector<float> &camera_centers_vectors, const std::vector<float> &intrinsic_matrix_vec,
                   const std::vector<uint32_t> &blacklisted_images);
  bool setTargetVolume(roaxdt_msgs::SetTargetVolume::Request &req, roaxdt_msgs::SetTargetVolume::Response &res);
  bool forwardProjection(roaxdt_msgs::ForwardProjection::Request &req, roaxdt_msgs::ForwardProjection::Response &res);
  void backProjection(Eigen::Matrix<float, Eigen::Dynamic, 1> &sinogram_eigen, int num_iterations, int image_width,
                      int image_height, float scaling, std::string experiment_id, const std::vector<float> &rotation_matrices,
                      const std::vector<float> &translation_vectors, const std::vector<float> &camera_centers_vectors,
                      const std::vector<float> &intrinsic_matrix_vec, const std::vector<uint32_t> &blacklisted_images);
  bool computePoseWeight(roaxdt_msgs::ComputePoseWeight::Request &req, roaxdt_msgs::ComputePoseWeight::Response &res);
  void plotIteration(elsa::DataContainer<elsa::real_t> &reconstruction, size_t iteration, size_t image_width, size_t image_height,
                     size_t starting_index, fs::path &reconstructed_images_path);
  void convertMatrixToUnsigendInt(Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &image_eigen,
                                  Eigen::Matrix<uint16_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &image_eigen_uint);
};

} // namespace nodes

} // namespace roaxdt