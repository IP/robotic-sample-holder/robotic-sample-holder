#include <roaxdt_ct/ComputedTomography.h>

namespace roaxdt {

namespace nodes {

using namespace roaxdt::constants;
using namespace roaxdt::configuration;

using namespace elsa;
using namespace elsa::geometry;
using namespace std;

ComputedTomography::ComputedTomography(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle)
    : node_handle(node_handle), private_node_handle(private_node_handle), domain_spacing(3), rangeSpacing(3), sizeRange(3),
      sizeDomain(3) {
  this->init();
}

void ComputedTomography::init() {
  try {
    readConfigParameter<std::vector<elsa::real_t>>(node_handle, reconstruction::DETECTOR_SPACING, detector_spacing);
    readConfigParameter<std::vector<elsa::real_t>>(node_handle, reconstruction::DETECTOR_RESOLUTION, detector_resolution);
    readConfigParameter<std::string>(node_handle, experiments::PATH, experiments_base_path);
    readConfigParameter<std::string>(node_handle, experiments::PROCESSED_PATH, processed_path);
    readConfigParameter<std::string>(node_handle, experiments::DETECTOR_CORRECTED_PATH, detector_corrected_path);
    readConfigParameter<std::string>(node_handle, experiments::RECONSTRUCTED_PATH, reconstructed_path);
    readConfigParameter<std::string>(node_handle, experiments::DEFAULT_IMAGE_FILE_TYPE, default_image_file_type);
    readConfigParameter<std::string>(node_handle, experiments::CORRECTED_IMAGE_FILE_TYPE, corrected_image_file_type);
    readConfigParameter<std::string>(node_handle, experiments::RECONSTRUCTION_FILE_TYPE, reconstruction_file_type);
    readConfigParameter<std::string>(node_handle, experiments::RECONSTRUCTION_FILE_NAME, reconstruction_file_name);
    readConfigParameter<std::string>(node_handle, experiments::BACK_PROJECTION_FILE_TYPE, back_projection_file_type);
    readConfigParameter<std::string>(node_handle, experiments::BACK_PROJECTION_FILE_NAME, back_projection_file_name);
    readConfigParameter<std::string>(node_handle, experiments::SEGMENTATION_FILE_TYPE, segmentation_file_type);
    readConfigParameter<std::string>(node_handle, experiments::SEGMENTATION_FILE_NAME, segmentation_file_name);
    readConfigParameter<std::string>(node_handle, CAMERA_INFO_SUFFIX, camera_info_suffix);
    readConfigParameter<std::string>(node_handle, DETECTOR_CAMERA_NAME, detector_camera_id);
    readConfigParameter<int>(node_handle, experiments::BACK_PROJECTION_SCALING, back_projection_scaling);
    readConfigParameter<bool>(node_handle, SIMULATION, simulation);
  } catch (const std::runtime_error &exception) { ROS_ERROR_STREAM(exception.what()); }

  computedTomographyServer.reset(
      new ComputedTomographyServer(node_handle, action_endpoints::COMPUTED_TOMOGRAPHY,
                                   boost::bind(&ComputedTomography::executeComputedTomography, this, _1), false));
  computedTomographyServer->start();

  backProjectionServer.reset(new BackProjectionServer(node_handle, action_endpoints::BACK_PROJECTION,
                                                      boost::bind(&ComputedTomography::executeBackProjection, this, _1), false));
  backProjectionServer->start();

  forward_projection_server =
      node_handle.advertiseService(service_endpoints::FORWARD_PROJECTION, &ComputedTomography::forwardProjection, this);
  set_target_volume_server =
      node_handle.advertiseService(service_endpoints::SET_TARGET_VOLUME, &ComputedTomography::setTargetVolume, this);
  compute_pose_weight_server =
      node_handle.advertiseService(service_endpoints::COMPUTE_POSE_WEIGHT, &ComputedTomography::computePoseWeight, this);

  // retrieve camera info
  camera_info_topic = std::string("/" + detector_camera_id + camera_info_suffix);
  auto shared_camera_info = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(camera_info_topic, node_handle);
  if (shared_camera_info != nullptr) {
    auto K_cv = shared_camera_info->K;
    intrinsic_matrix_live << K_cv[0], K_cv[1], K_cv[2], K_cv[3], K_cv[4], K_cv[5], K_cv[6], K_cv[7], K_cv[8];
  } else {
    ROS_WARN_STREAM("Could not read camera intrinsic matrix from topic " << camera_info_topic);
  }

  ROS_INFO("Computed Tomography node basic setup finished.");
}

void ComputedTomography::executeComputedTomography(const roaxdt_msgs::ComputedTomographyGoalConstPtr &goal) {
  Eigen::Matrix<float, Eigen::Dynamic, 1> sinograms;

  ROS_INFO_STREAM("binning factor: " << goal->binning_factor);
  float scaling = 1.0 / goal->binning_factor;
  int image_height;
  int image_width;

  ROS_INFO_STREAM("blacklist size: " << goal->blacklisted_images.size());
  std::vector<uint32_t> blacklisted_copy = goal->blacklisted_images;
  std::sort(blacklisted_copy.begin(), blacklisted_copy.end());
  blacklisted_copy.erase(std::unique(blacklisted_copy.begin(), blacklisted_copy.end()), blacklisted_copy.end());
  ROS_INFO_STREAM("blacklist filtered size: " << blacklisted_copy.size());

  try {
    readSinograms(goal->experiment_id, image_width, image_height, scaling, sinograms, blacklisted_copy);
    reconstruct(sinograms, goal->num_iterations_recon, goal->num_iterations, image_width, image_height, scaling,
                goal->experiment_id, goal->rotation_matrices, goal->translation_vectors, goal->camera_center_vectors,
                goal->intrinsic_matrix, blacklisted_copy);
    computedTomographyServer->setSucceeded();
  } catch (std::exception &e) {
    ROS_ERROR_STREAM("An exception occurred: " << e.what());
    computedTomographyServer->setAborted();
  }
}

void ComputedTomography::executeBackProjection(const roaxdt_msgs::BackProjectionGoalConstPtr &goal) {
  Eigen::Matrix<float, Eigen::Dynamic, 1> sinograms;

  ROS_INFO_STREAM("binning factor: " << goal->binning_factor);
  float scaling = 1.0 / goal->binning_factor;
  int image_height;
  int image_width;

  ROS_INFO_STREAM("blacklist size: " << goal->blacklisted_images.size());
  std::vector<uint32_t> blacklisted_copy = goal->blacklisted_images;
  std::sort(blacklisted_copy.begin(), blacklisted_copy.end());
  blacklisted_copy.erase(std::unique(blacklisted_copy.begin(), blacklisted_copy.end()), blacklisted_copy.end());
  ROS_INFO_STREAM("blacklist filtered size: " << blacklisted_copy.size());

  try {
    readSinograms(goal->experiment_id, image_width, image_height, scaling, sinograms, blacklisted_copy);
    backProjection(sinograms, goal->num_iterations, image_width, image_height, scaling, goal->experiment_id,
                   goal->rotation_matrices, goal->translation_vectors, goal->camera_center_vectors, goal->intrinsic_matrix,
                   blacklisted_copy);
    backProjectionServer->setSucceeded();
  } catch (std::exception &e) {
    ROS_ERROR_STREAM("An exception occurred: " << e.what());
    backProjectionServer->setAborted();
  }
}

void ComputedTomography::readSinograms(string experiment_id, int &image_width, int &image_height, float scaling,
                                       Eigen::Matrix<float, Eigen::Dynamic, 1> &sinograms,
                                       const std::vector<uint32_t> &blacklisted_images) {
  auto experiment_path = fs::path(experiments_base_path + experiment_id);
  auto corrected_images_path = fs::path(experiment_path) / fs::path(detector_corrected_path);
  auto processed_images_path = fs::path(experiment_path) / fs::path(processed_path);
  auto detector_images_path = fs::path(experiment_path) / fs::path(std::string("detector"));
  std::vector<fs::path> processed_images_path_vec;
  fs::directory_iterator processed_images_iterator(processed_images_path);
  for (auto &processed_image_iter : boost::make_iterator_range(processed_images_iterator, {})) {
    if (processed_image_iter.path().extension().string().compare(default_image_file_type) == 0) {
      processed_images_path_vec.push_back(processed_image_iter.path());
    }
  }

  // Read single image and determine image dimensions
  fs::path pathTmpProcessed = processed_images_path;
  pathTmpProcessed /= std::to_string(0) + default_image_file_type;
  std::string processed_image_path = pathTmpProcessed.string();
  cv::Mat image_cv_single = cv::imread(processed_image_path.c_str(), cv::IMREAD_ANYDEPTH);
  auto image_width_orig = image_cv_single.cols;
  auto image_height_orig = image_cv_single.rows;

  image_width = image_width_orig * scaling;
  image_height = image_height_orig * scaling;

  int image_size = image_width * image_height;
  ROS_INFO_STREAM("image size: " << image_size);
  auto size_after_blacklist = processed_images_path_vec.size() - blacklisted_images.size();
  ROS_INFO_STREAM("size after blacklist: " << size_after_blacklist);
  sinograms.resize(size_after_blacklist * image_size);
  ROS_INFO_STREAM("sinograms size: " << sinograms.size());

  std::vector<size_t> image_indices;
  image_indices.reserve(processed_images_path_vec.size() - blacklisted_images.size());
  for (size_t i = 0; i < processed_images_path_vec.size(); i++) {
    if (std::find(blacklisted_images.begin(), blacklisted_images.end(), i) == blacklisted_images.end()) {
      image_indices.push_back(i);
    }
  }

#pragma omp parallel for
  for (size_t i = 0; i < image_indices.size(); i++) {
    auto image_id = image_indices.at(i);
    fs::path pathTmpCorrected = simulation ? detector_images_path : corrected_images_path;
    std::string file_type = simulation ? default_image_file_type : corrected_image_file_type;
    pathTmpCorrected /= std::to_string(image_id) + file_type;
    std::string corrected_image_path = pathTmpCorrected.string();

    ROS_INFO_STREAM("Reading " << corrected_image_path);
    cv::Mat image_cv_resized;
    if (simulation) {
      cv::Mat image_cv_orig = cv::imread(corrected_image_path.c_str(), cv::IMREAD_GRAYSCALE);
      cv::resize(image_cv_orig, image_cv_resized, cv::Size(image_width, image_height));
    } else {
      Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> image_eigen(1, image_width_orig * image_height_orig);
      std::ifstream f(corrected_image_path, std::ifstream::binary);
      float val;
      size_t index = 0;
      while (f.read(reinterpret_cast<char *>(&val), sizeof(float))) {
        image_eigen(0, index) = val;
        ++index;
      }
      image_eigen.resize(image_width_orig, image_height_orig);

      cv::Mat image_cv(image_width_orig, image_height_orig, CV_32FC1);
      cv::eigen2cv(image_eigen, image_cv);
      cv::resize(image_cv, image_cv_resized, cv::Size(image_width, image_height));
    }

    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> image_eigen_resized(image_width, image_height);
    cv2eigen(image_cv_resized, image_eigen_resized);

    long int vector_index;
    vector_index = i;
    vector_index *= image_size;

    auto block_ref = sinograms.block(vector_index, 0, image_size, 1);
    block_ref = Eigen::VectorXf(
        Eigen::Map<Eigen::VectorXf>(image_eigen_resized.data(), image_eigen_resized.cols() * image_eigen_resized.rows()));
  }
}

void ComputedTomography::reconstruct(Eigen::Matrix<float, Eigen::Dynamic, 1> &sinogram_eigen, int num_iter_recon,
                                     int num_iterations, int image_width, int image_height, float scaling,
                                     std::string experiment_id, const std::vector<float> &rotation_matrices,
                                     const std::vector<float> &translation_vectors,
                                     const std::vector<float> &camera_centers_vectors,
                                     const std::vector<float> &intrinsic_matrix_vec,
                                     const std::vector<uint32_t> &blacklisted_images) {
  auto experiment_path = fs::path(experiments_base_path + experiment_id);
  auto sizeAfterBlacklist = num_iterations - blacklisted_images.size();
  sinogram_eigen = sinogram_eigen.unaryExpr([](float v) { return std::isfinite(v) ? v : 0.0f; }).matrix();

  float sourceToCenter = 1365.0;
  float centerToDetector = 2155.0 - sourceToCenter;
  auto detectorSpacingTmp = 0.15 / scaling;
  auto domainSpacingTmp = detectorSpacingTmp / ((sourceToCenter + centerToDetector) / sourceToCenter);
  ROS_INFO_STREAM("domainSpacing: " << domainSpacingTmp);

  RealVector_t domainSpacing(3);
  IndexVector_t sizeDomain(3);
  domainSpacing << domainSpacingTmp, domainSpacingTmp, domainSpacingTmp;
  sizeDomain << image_width, image_width, image_width;

  RealVector_t sinogramSpacing(3);
  // increase detector pixel size in order to make up for resizing of detector images
  sinogramSpacing << detectorSpacingTmp, detectorSpacingTmp, 1;
  IndexVector_t sizeSinogram(3);

  sizeSinogram << image_width, image_height, sizeAfterBlacklist;

  auto domainDesc = VolumeDescriptor(sizeDomain, domainSpacing);

  Eigen::Matrix3f intrinsic_matrix;
  intrinsic_matrix << intrinsic_matrix_vec.at(0) * scaling, intrinsic_matrix_vec.at(1), intrinsic_matrix_vec.at(2) * scaling,
      intrinsic_matrix_vec.at(3), intrinsic_matrix_vec.at(4) * scaling, intrinsic_matrix_vec.at(5) * scaling,
      intrinsic_matrix_vec.at(6), intrinsic_matrix_vec.at(7), intrinsic_matrix_vec.at(8);

  std::vector<Geometry> geometries;
  for (size_t i = 0; i < num_iterations; i++) {
    if (std::find(blacklisted_images.begin(), blacklisted_images.end(), i) == blacklisted_images.end()) {
      auto index = i * 9;
      auto index_vec = i * 3;
      Eigen::Matrix3f rotation_matrix, rotation_holder;
      Eigen::Vector3f translation_vector, camera_center_vector;
      rotation_matrix << rotation_matrices.at(index), rotation_matrices.at(index + 1), rotation_matrices.at(index + 2),
          rotation_matrices.at(index + 3), rotation_matrices.at(index + 4), rotation_matrices.at(index + 5),
          rotation_matrices.at(index + 6), rotation_matrices.at(index + 7), rotation_matrices.at(index + 8);
      translation_vector << translation_vectors.at(index_vec), translation_vectors.at(index_vec + 1),
          translation_vectors.at(index_vec + 2);
      camera_center_vector << camera_centers_vectors.at(index_vec), camera_centers_vectors.at(index_vec + 1),
          camera_centers_vectors.at(index_vec + 2);

      Eigen::Vector3f translation_vector_new = rotation_matrix * (-camera_center_vector - domainDesc.getLocationOfOrigin());

      geometries.push_back(Geometry(VolumeData3D{domainDesc.getSpacingPerDimension(), domainDesc.getLocationOfOrigin()},
                                    SinogramData3D{Size3D{sizeSinogram}, Spacing3D{sinogramSpacing}}, rotation_matrix,
                                    translation_vector_new, intrinsic_matrix));
    }
  }
  ROS_INFO_STREAM("new image dimensions: " << image_width << "," << image_height);
  ROS_INFO_STREAM("geometries size: " << geometries.size());

  auto sinogramDesc = PlanarDetectorDescriptor(sizeSinogram, sinogramSpacing, geometries);

  // create sinogram as datacontainer
  DataContainer<real_t> sinogram(sinogramDesc, sinogram_eigen);

  JosephsMethod projector(dynamic_cast<const VolumeDescriptor &>(domainDesc), sinogramDesc);

  // l2 norm regularization term
  L2NormPow2<real_t> regFunc(domainDesc);
  real_t weight = 10;
  RegularizationTerm<real_t> regTerm(weight, regFunc);

  // setup reconstruction problem
  WLSProblem problem(projector, sinogram);
  TikhonovProblem<real_t> tikh(problem, std::vector<RegularizationTerm<real_t>>{regTerm});

  // solve the reconstruction problem:
  CG solver(tikh);

  auto reconstructed_images_path = fs::path(experiment_path) / fs::path(reconstructed_path);
  int num_iter_per_run = 1;
  int num_runs = num_iter_recon / num_iter_per_run;
  auto starting_index = sizeDomain[0] * sizeDomain[1] * (sizeDomain[2] / 2);

  DataContainer<real_t> reconstruction(dynamic_cast<const VolumeDescriptor &>(domainDesc));
  for (size_t i = 0; i < num_runs; i++) {
    reconstruction = solver.solve(num_iter_per_run);

    // save to disk
    ROS_INFO("Plotting slice %u of reconstruction", i);
    plotIteration(reconstruction, i, image_width, image_height, starting_index, reconstructed_images_path);

    // publish feedback
    roaxdt_msgs::ComputedTomographyFeedback feedback;
    feedback.progress = i * num_iter_per_run;
    feedback.num_iterations_recon = num_iter_recon;
    feedback.image_id = i;

    computedTomographyServer->publishFeedback(feedback);
  }

  // write the reconstruction out
  experiment_path /= reconstruction_file_name + reconstruction_file_type;
  std::ofstream os_recon(experiment_path.c_str(), std::ios::binary);
  if (!os_recon.good())
    throw std::runtime_error("Cannot write to " + experiment_path.string());

  for (index_t i = 0; i < reconstruction.getSize(); ++i)
    os_recon.write(reinterpret_cast<const char *>(&reconstruction[i]), sizeof(elsa::real_t));
  os_recon.close();
  ROS_INFO_STREAM("Finished writing reconstruction");

  computedTomographyServer->setSucceeded();
}

void ComputedTomography::backProjection(Eigen::Matrix<float, Eigen::Dynamic, 1> &sinogram_eigen, int num_iterations,
                                        int image_width, int image_height, float scaling, std::string experiment_id,
                                        const std::vector<float> &rotation_matrices,
                                        const std::vector<float> &translation_vectors,
                                        const std::vector<float> &camera_centers_vectors,
                                        const std::vector<float> &intrinsic_matrix_vec,
                                        const std::vector<uint32_t> &blacklisted_images) {
  roaxdt_msgs::BackProjectionResult action_result;

  auto experiment_path = fs::path(experiments_base_path + experiment_id);
  auto sizeAfterBlacklist = num_iterations - blacklisted_images.size();
  sinogram_eigen = sinogram_eigen.unaryExpr([](float v) { return std::isfinite(v) ? v : 0.0f; }).matrix();

  float sourceToCenter = 1365.0f;
  float centerToDetector = 2155.0 - sourceToCenter;
  auto detectorSpacingTmp = 0.15 / scaling;
  auto domainSpacingTmp = detectorSpacingTmp / ((sourceToCenter + centerToDetector) / sourceToCenter);
  ROS_INFO_STREAM("domainSpacing: " << domainSpacingTmp);

  RealVector_t domainSpacing(3);
  IndexVector_t sizeDomain(3);
  domainSpacing << domainSpacingTmp, domainSpacingTmp, domainSpacingTmp;
  sizeDomain << image_width, image_width, image_width;

  RealVector_t sinogramSpacing(3);
  // increase detector pixel size in order to make up for resizing of detector images
  sinogramSpacing << detectorSpacingTmp, detectorSpacingTmp, 1;
  IndexVector_t sizeSinogram(3);

  sizeSinogram << image_width, image_height, sizeAfterBlacklist;

  auto domainDesc = VolumeDescriptor(sizeDomain, domainSpacing);

  Eigen::Matrix3f intrinsic_matrix;
  intrinsic_matrix << intrinsic_matrix_vec.at(0) * scaling, intrinsic_matrix_vec.at(1), intrinsic_matrix_vec.at(2) * scaling,
      intrinsic_matrix_vec.at(3), intrinsic_matrix_vec.at(4) * scaling, intrinsic_matrix_vec.at(5) * scaling,
      intrinsic_matrix_vec.at(6), intrinsic_matrix_vec.at(7), intrinsic_matrix_vec.at(8);

  std::vector<Geometry> geometries;
  for (size_t i = 0; i < num_iterations; i++) {
    if (std::find(blacklisted_images.begin(), blacklisted_images.end(), i) == blacklisted_images.end()) {
      auto index = i * 9;
      auto index_vec = i * 3;
      Eigen::Matrix3f rotation_matrix, rotation_holder;
      Eigen::Vector3f translation_vector, camera_center_vector;
      rotation_matrix << rotation_matrices.at(index), rotation_matrices.at(index + 1), rotation_matrices.at(index + 2),
          rotation_matrices.at(index + 3), rotation_matrices.at(index + 4), rotation_matrices.at(index + 5),
          rotation_matrices.at(index + 6), rotation_matrices.at(index + 7), rotation_matrices.at(index + 8);
      translation_vector << translation_vectors.at(index_vec), translation_vectors.at(index_vec + 1),
          translation_vectors.at(index_vec + 2);
      camera_center_vector << camera_centers_vectors.at(index_vec), camera_centers_vectors.at(index_vec + 1),
          camera_centers_vectors.at(index_vec + 2);

      Eigen::Vector3f translation_vector_new = rotation_matrix * (-camera_center_vector - domainDesc.getLocationOfOrigin());

      geometries.push_back(Geometry(VolumeData3D{domainDesc.getSpacingPerDimension(), domainDesc.getLocationOfOrigin()},
                                    SinogramData3D{Size3D{sizeSinogram}, Spacing3D{sinogramSpacing}}, rotation_matrix,
                                    translation_vector_new, intrinsic_matrix));
    }
  }
  ROS_INFO_STREAM("new image dimensions: " << image_width << "," << image_height);
  ROS_INFO_STREAM("geometries size: " << geometries.size());

  auto sinogramDesc = PlanarDetectorDescriptor(sizeSinogram, sinogramSpacing, geometries);

  // create sinogram as datacontainer
  DataContainer<real_t> sinogram(sinogramDesc, sinogram_eigen);

  JosephsMethod projector(dynamic_cast<const VolumeDescriptor &>(domainDesc), sinogramDesc);

  DataContainer<real_t> backProjection(dynamic_cast<const VolumeDescriptor &>(domainDesc));
  backProjection = projector.applyAdjoint(sinogram);

  // write the back-projection out
  experiment_path /= back_projection_file_name + back_projection_file_type;
  std::ofstream os_back_projection(experiment_path.c_str(), std::ios::binary);
  if (!os_back_projection.good())
    throw std::runtime_error("Cannot write to " + experiment_path.string());

  for (index_t i = 0; i < backProjection.getSize(); ++i)
    os_back_projection.write(reinterpret_cast<const char *>(&backProjection[i]), sizeof(elsa::real_t));
  os_back_projection.close();
  ROS_INFO_STREAM("Finished writing reconstruction");

  action_result.image_scaling = scaling;
  backProjectionServer->setSucceeded(action_result);
}

bool ComputedTomography::computePoseWeight(roaxdt_msgs::ComputePoseWeight::Request &req,
                                           roaxdt_msgs::ComputePoseWeight::Response &res) {
  float scaling = 1.0 / req.binning_factor;
  float scaled_size = 2880.0f * scaling;
  auto sizeAfterBlacklist = req.image_ids.size();
  std::vector<float> volume_dimensions = {scaled_size, scaled_size, scaled_size};
  int volume_size = static_cast<int>(volume_dimensions.at(0)) * static_cast<int>(volume_dimensions.at(1)) *
                    static_cast<int>(volume_dimensions.at(2));
  ROS_INFO_STREAM("volume size: " << volume_dimensions.at(0) << " - " << volume_dimensions.at(1) << " - "
                                  << volume_dimensions.at(2));

  auto experiment_path = fs::path(experiments_base_path + req.experiment_id);
  fs::path segmented_volume_path = experiment_path;
  segmented_volume_path /= segmentation_file_name + segmentation_file_type;
  ROS_INFO_STREAM("Reading segmented volume from file: " << segmented_volume_path.c_str());
  std::vector<float> volume_vec;
  ifstream is(segmented_volume_path.c_str(), ios::binary);
  volume_vec.resize(volume_size);
  is.read((char *)&volume_vec[0], volume_size * sizeof(float));

  ROS_INFO_STREAM("volume vec from file size: " << volume_vec.size());
  Eigen::VectorXf eigen_volume(Eigen::Map<Eigen::VectorXf>(volume_vec.data(), volume_size));

  float sourceToCenter = 1365.0f;
  float centerToDetector = 2155.0 - sourceToCenter;
  auto detectorSpacingTmp = 0.15 / scaling;
  auto domainSpacingTmp = detectorSpacingTmp / ((sourceToCenter + centerToDetector) / sourceToCenter);
  ROS_INFO_STREAM("domainSpacing: " << domainSpacingTmp);

  RealVector_t domainSpacing(3);
  domainSpacing << domainSpacingTmp, domainSpacingTmp, domainSpacingTmp;
  IndexVector_t sizeDomain(3);
  sizeDomain << volume_dimensions.at(0), volume_dimensions.at(1), volume_dimensions.at(2);
  VolumeDescriptor domainSegmentedDesc(sizeDomain, domainSpacing);
  DataContainer domainSegmented(domainSegmentedDesc, eigen_volume);

  // set detector descriptor up
  RealVector_t sinogramSpacing(3);
  sinogramSpacing << detectorSpacingTmp, detectorSpacingTmp, detectorSpacingTmp;
  IndexVector_t sizeSinogram(3);
  sizeSinogram << scaled_size, scaled_size, sizeAfterBlacklist;

  Eigen::Matrix3f intrinsic_matrix;
  intrinsic_matrix << req.intrinsic_matrix.at(0) * scaling, req.intrinsic_matrix.at(1), req.intrinsic_matrix.at(2) * scaling,
      req.intrinsic_matrix.at(3), req.intrinsic_matrix.at(4) * scaling, req.intrinsic_matrix.at(5) * scaling,
      req.intrinsic_matrix.at(6), req.intrinsic_matrix.at(7), req.intrinsic_matrix.at(8);

  std::vector<Geometry> geometries;
  for (auto i : req.image_ids) {
    if (std::find(req.blacklisted_images.begin(), req.blacklisted_images.end(), i) == req.blacklisted_images.end()) {
      auto index = i * 9;
      auto index_vec = i * 3;
      Eigen::Matrix3f rotation_matrix, rotation_holder;
      Eigen::Vector3f translation_vector, camera_center_vector;
      rotation_matrix << req.rotation_matrices.at(index), req.rotation_matrices.at(index + 1),
          req.rotation_matrices.at(index + 2), req.rotation_matrices.at(index + 3), req.rotation_matrices.at(index + 4),
          req.rotation_matrices.at(index + 5), req.rotation_matrices.at(index + 6), req.rotation_matrices.at(index + 7),
          req.rotation_matrices.at(index + 8);
      translation_vector << req.translation_vectors.at(index_vec), req.translation_vectors.at(index_vec + 1),
          req.translation_vectors.at(index_vec + 2);
      camera_center_vector << req.camera_center_vectors.at(index_vec), req.camera_center_vectors.at(index_vec + 1),
          req.camera_center_vectors.at(index_vec + 2);

      Eigen::Vector3f translation_vector_new =
          rotation_matrix * (-camera_center_vector - domainSegmentedDesc.getLocationOfOrigin());

      geometries.push_back(
          Geometry(VolumeData3D{domainSegmentedDesc.getSpacingPerDimension(), domainSegmentedDesc.getLocationOfOrigin()},
                   SinogramData3D{Size3D{sizeSinogram}, Spacing3D{sinogramSpacing}}, rotation_matrix, translation_vector_new,
                   intrinsic_matrix));
    }
  }

  try {
    PlanarDetectorDescriptor sinogramDesc(sizeSinogram, sinogramSpacing, geometries);

    // BinaryMethod projector(domainSegmentedDesc, sinogramDesc);
    JosephsMethod projector(domainSegmentedDesc, sinogramDesc);

    // simulate the sinogram
    auto sinogram = projector.apply(domainSegmented);

    for (auto i = 0; i < geometries.size(); i++) {
      const auto &slice = sinogram.slice(i);
      res.scores.push_back(slice.l0PseudoNorm()); // retrieve number of non-zero elements
    }
  } catch (const std::exception &e) { ROS_ERROR(e.what()); }

  return true;
}

bool ComputedTomography::forwardProjection(roaxdt_msgs::ForwardProjection::Request &req,
                                           roaxdt_msgs::ForwardProjection::Response &res) {

  // std::vector<float> volume_vec(domain->begin(), domain->end());
  // Eigen::VectorXf eigen_volume(Eigen::Map<Eigen::VectorXf>(volume_vec.data(), volume_vec.size()));

  // set detector descriptor up
  RealVector_t sinogramSpacing(3);
  sinogramSpacing << detector_spacing.at(0), detector_spacing.at(1), detector_spacing.at(2);
  IndexVector_t sizeSinogram(3);
  sizeSinogram << detector_resolution.at(0), detector_resolution.at(1), detector_resolution.at(2);

  // sample is read in millimeters. convert distances from m to mm:
  Eigen::Matrix3f intrinsic_matrix;
  intrinsic_matrix << 14133.33203125, 0.0, 1440.0, 0.0, 14133.33203125, 1440.0, 0.0, 0.0, 1.0;

  Eigen::Quaternionf pose_quaternion(req.goal_pose.orientation.w, req.goal_pose.orientation.x, req.goal_pose.orientation.y,
                                     req.goal_pose.orientation.z);
  Eigen::Matrix3f rotation_matrix = pose_quaternion.matrix();
  Eigen::Vector3f translation_vector(req.goal_pose.position.x, req.goal_pose.position.y, req.goal_pose.position.z),
      camera_center_vector;
  translation_vector *= 1000.0;
  camera_center_vector << -1.0 * rotation_matrix.transpose() * translation_vector;

  Eigen::Vector3f translation_vector_new = rotation_matrix * (-camera_center_vector - domainDesc->getLocationOfOrigin());

  std::vector<Geometry> geometries_tmp;
  geometries_tmp.push_back(Geometry(VolumeData3D{domainDesc->getSpacingPerDimension(), domainDesc->getLocationOfOrigin()},
                                    SinogramData3D{Size3D{sizeSinogram}, Spacing3D{sinogramSpacing}}, rotation_matrix,
                                    translation_vector_new, intrinsic_matrix));

  try {
    sinoDesc.reset(new PlanarDetectorDescriptor(sizeSinogram, sinogramSpacing, geometries_tmp));

    JosephsMethod projector(*domainDesc, *sinoDesc);

    // simulate the sinogram
    auto sinogram = projector.apply(*domain);

    std::vector<float> sinogram_vec(sinogram.cbegin(), sinogram.cend());
    sensor_msgs::Image sinogram_img;
    uint32_t sinogram_width = sinoDesc->getNumberOfCoefficientsPerDimension()[0];
    uint32_t sinogram_height = sinoDesc->getNumberOfCoefficientsPerDimension()[1];
    uint32_t row_step_in_bytes = sinogram_width * sizeof(elsa::real_t);
    sensor_msgs::fillImage(sinogram_img, sensor_msgs::image_encodings::TYPE_32FC1, sinogram_height, sinogram_width,
                           row_step_in_bytes, sinogram_vec.data());

    res.sinogram = sinogram_img;

  } catch (const std::exception &e) { ROS_ERROR(e.what()); }

  return true;
}

bool ComputedTomography::setTargetVolume(roaxdt_msgs::SetTargetVolume::Request &req,
                                         roaxdt_msgs::SetTargetVolume::Response &res) {
  std::vector<float> volume_dimensions = {req.dimensions.at(0) / req.spacing.at(0), req.dimensions.at(1) / req.spacing.at(1),
                                          req.dimensions.at(2) / req.spacing.at(2)};
  int volume_size = static_cast<int>(volume_dimensions.at(0)) * static_cast<int>(volume_dimensions.at(1)) *
                    static_cast<int>(volume_dimensions.at(2));
  ROS_INFO_STREAM("volume size: " << volume_dimensions.at(0) << " - " << volume_dimensions.at(1) << " - "
                                  << volume_dimensions.at(2));

  ROS_INFO_STREAM("Reading rasterized volume from file: " << req.volume_path.c_str());
  std::vector<float> volume_vec;
  ifstream is(req.volume_path.c_str(), ios::binary);
  // int size2;
  // is.read((char *)&size2, 4);
  volume_vec.resize(volume_size);
  is.read((char *)&volume_vec[0], volume_size * sizeof(float));

  ROS_INFO_STREAM("volume vec from file size: " << volume_vec.size());
  Eigen::VectorXf eigen_volume(Eigen::Map<Eigen::VectorXf>(volume_vec.data(), volume_size));

  RealVector_t domainSpacing(3);
  domainSpacing << req.spacing.at(0), req.spacing.at(1), req.spacing.at(2);
  this->domainSpacing = domainSpacing;
  IndexVector_t sizeDomain(3);
  sizeDomain << volume_dimensions.at(0), volume_dimensions.at(1), volume_dimensions.at(2);
  this->sizeDomain = sizeDomain;
  domainDesc.reset(new VolumeDescriptor(sizeDomain, domainSpacing));
  domain.reset(new DataContainer(*domainDesc, eigen_volume));

  ROS_INFO_STREAM("volume min: " << eigen_volume.minCoeff());
  ROS_INFO_STREAM("volume max: " << eigen_volume.maxCoeff());
  ROS_INFO_STREAM("volume size: " << eigen_volume.size());
  ROS_INFO_STREAM("volume size elsa: " << domain->getSize());
  ROS_INFO_STREAM("volume location of origin: " << domainDesc->getLocationOfOrigin());

  return true;
}

void ComputedTomography::convertMatrixToUnsigendInt(
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &image_eigen,
    Eigen::Matrix<uint16_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &image_eigen_uint) {
  // stretch values to uint16 range
  float low = std::numeric_limits<uint16_t>::min();
  float high = std::numeric_limits<uint16_t>::max();
  float range = high - low;
  float current_min = image_eigen.minCoeff();
  float current_max = image_eigen.maxCoeff();
  float current_range = current_max - current_min;
  ROS_DEBUG("current min %f max %f", current_min, current_max);

  image_eigen =
      (image_eigen - Eigen::MatrixXf::Constant(image_eigen.rows(), image_eigen.cols(), current_min)) * (range / current_range);
  image_eigen = (image_eigen + Eigen::MatrixXf::Constant(image_eigen.rows(), image_eigen.cols(), low));

  ROS_DEBUG("result float min %f max %f", image_eigen.minCoeff(), image_eigen.maxCoeff());
  image_eigen_uint = image_eigen.cast<uint16_t>();
  ROS_DEBUG("result uint min %u max %u", image_eigen_uint.minCoeff(), image_eigen_uint.maxCoeff());
}

void ComputedTomography::plotIteration(DataContainer<real_t> &reconstruction, size_t iteration, size_t image_width,
                                       size_t image_height, size_t starting_index, fs::path &reconstructed_images_path) {
  fs::path pathTmpReconstructed = reconstructed_images_path;
  pathTmpReconstructed /= std::to_string(iteration) + default_image_file_type;
  std::string reconstructed_image_path = pathTmpReconstructed.string();
  ROS_INFO_STREAM("Writing reconstruction slice to " << reconstructed_image_path);

  auto image_size = image_width * image_height;
  Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> image_eigen(1, image_size);
  Eigen::Matrix<uint16_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> image_eigen_uint(1, image_size);
  auto recon_index = starting_index;
  for (index_t i = 0; i < image_size; ++i) {
    image_eigen(0, i) = reconstruction[recon_index];
    ++recon_index;
  }
  image_eigen.resize(image_height, image_width);
  convertMatrixToUnsigendInt(image_eigen, image_eigen_uint);

  cv::Mat image_cv(image_width, image_height, CV_16UC1);
  cv::eigen2cv(image_eigen_uint, image_cv);
  cv::imwrite(reconstructed_image_path.c_str(), image_cv);
}

} // namespace nodes

} // namespace roaxdt

int main(int argc, char **argv) {
  ros::init(argc, argv, std::string("computed_tomography"));
  ros::NodeHandle node_handle("");
  ros::NodeHandle private_node_handle("~");
  roaxdt::nodes::ComputedTomography node(node_handle, private_node_handle);

  ros::spin();
  return 0;
}
