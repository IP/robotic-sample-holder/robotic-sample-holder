cmake_minimum_required(VERSION 3.0.2)
project(roaxdt_ct)

find_package(catkin REQUIRED
             COMPONENTS roscpp
                        rospy
                        std_msgs
                        actionlib_msgs
                        actionlib
                        roaxdt_msgs
                        sensor_msgs
                        geometry_msgs
                        eigen_conversions
                        roaxdt_utils)

# needed for roaxdt_utils dependency - fix this!
find_package(gazebo)
find_package(OpenMP REQUIRED)

find_package(OpenCV)
find_package(elsa REQUIRED)

if(ELSA_GPU)
  enable_language(CUDA)
endif()

find_package(VTK
             COMPONENTS CommonCore
                        CommonDataModel
                        IOGeometry
                        IOImage
                        ImagingStencil
                        vtkFiltersCore
                        vtkFiltersSources
             REQUIRED)

catkin_python_setup()

catkin_package(INCLUDE_DIRS include
               # LIBRARIES roaxdt_ct
               CATKIN_DEPENDS
               # roscpp
               # std_msgs
               # roaxdt_msgs
               # geometry_msgs
               # eigen_conversions
               roaxdt_utils
               # DEPENDS system_lib
               )

include_directories(include ${catkin_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS})

# if(ELSA_GPU)
  add_executable(computed_tomography src/roaxdt_ct/ComputedTomography.cpp)
  add_dependencies(computed_tomography roaxdt_msgs_generate_messages_cpp)
  target_link_libraries(computed_tomography
                        ${catkin_LIBRARIES}
                        ${OpenCV_LIBRARIES}
                        elsa::all
                        OpenMP::OpenMP_CXX)
  target_compile_features(computed_tomography PUBLIC cxx_std_17)
# endif()

add_executable(mesh_reader src/roaxdt_ct/MeshReader.cpp)
add_dependencies(mesh_reader roaxdt_msgs_generate_messages_cpp)
target_link_libraries(mesh_reader
                      ${catkin_LIBRARIES}
                      ${VTK_LIBRARIES}
                      OpenMP::OpenMP_CXX)

