import numpy as np
import scipy.linalg
from scipy.stats import skewnorm
from scipy.ndimage import rotate
import scipy.constants as const


# ###############################################################################
# xray conversions
def energy2wl(energy):
    """Convert photon energy to wavelength

    florian.schaff@monash.edu

    Parameters
    ----------
    energy : float or array
        Energy in [keV]

    Returns
    -------
    wl : float or array
        Photon wavelength in [m]

    """
    return const.h/const.eV * const.c/(energy*1000)


def beta2mu(beta, energy):
    """Calculate mu from beta for the given energy
    IMPORTANT: beta has an energy dependence, i.e. a beta should only be used
    with the correct energy!

    florian.schaff@monash.edu

    Parameters
    ----------
    beta : float or array
        imaginary part of the refractive index
    energy : float or array
        energy in [keV]

    Returns
    -------
    mu : float or array
        attenuation coefficient im [m^-1]

    """
    return beta * 4 * np.pi / energy2wl(energy)



###############################################################################
# stuff to plot

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
 
 
def _colorbar(mappable):
    """Appends a matplotlib vertical colorbar to a plot.
    http://joseph-long.com/writing/colorbars/

    """
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    return fig.colorbar(mappable, cax=cax)


def _dict_vals_to_items(mydict, i):
    """Takes a dict and returns another replacing all values that are lists
    by the ith item in those lists.

    """
    rd = {}
    for key, val in mydict.items():
        if isinstance(val, list):
            rd[key] = val[i]
        else:
            rd[key] = val
    return rd


def imshow_grid(images, titles=None, sharex=True, sharey=True, shgrid=None,
                colorbar=True, figwidth=10, shareclim=None, **kwargs):
    """convenience function to show multiple images in a grid

    florian.schaff@monash.edu
    gary.ruben@monash.edu

    Parameters
    ----------
    images : list
        images to be shown
    titles : list, optional
        titles for the images
    sharex, sharey : bool
        share axes across all images, e.g. zoom works on all images
        simultaneously. Default: True
    shgrid : tuple of int, optional
        optional user specified shape of the grid if defined.
        format as (nrows, ncols). Default: None
    colorbar : bool
        display colorbars or not, optional. Default: True
    figwidth : float
        width of figure in inches. Height is scaled accordingly. Default : 10
    shareclim : None or nested list
        specify which images should charge clims. Used nested list: e.g.
        [[0, 2, 4], [1, 3, 5]] means images 0,2,4 and 1,3,5 share clims.
        Default: None. Use 'all', 'All', 'full', 'Full' or True to share all
        clims. Overwrites any user specified clims.
    **kwargs passed onto ax.imshow
        Each kwarg can be either a single value, or a list of length len(images).
        A single value is passed on to all images whereas a list of values is unpacked
        and items are passed to the image with the corresponding index. e.g. For a grid
        of 2 images, you could pass clim=(0.0, 0.7) or clim=[(0.0, 0.7), (0.0, 0.4)]

    Returns
    ----------
    The matplotlib figure and axes objects.

    """
    nim = np.array(images).shape[0]
    if shgrid is None:
        nrows = int(np.sqrt(nim))
        ncols = int(np.ceil(nim/nrows))
    else:
        nrows, ncols = shgrid
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols,
                              figsize=(figwidth, figwidth * nrows/ncols * .8),
                              sharex=sharex, sharey=sharey)

    if shareclim:
        mins, maxs = np.min(images, (1, 2)), np.max(images, (1, 2))
        kwargs['clim'] = [(a, b) for a, b in zip(mins, maxs)]
        if shareclim in ['all', 'All', 'full', 'Full', True]:
            shareclim = [[*range(nim)]]
        lflat = [x for y in shareclim for x in y]
        if len(set(lflat)) != len(lflat):
            print('Duplicate entries for shared clim, skipping!')
            pass
        else:
            for shared in shareclim:
                clim = (min([kwargs['clim'][i][0] for i in shared]),
                        max([kwargs['clim'][i][1] for i in shared]))
                kwargs['clim'] = [clim if i in shared else kwargs['clim'][i] for i in range(len(kwargs['clim']))]

    for i, ax in enumerate(np.atleast_1d(axes).ravel()):
        if i >= nim:
            ax.axis('off')
            continue

        kwarg = _dict_vals_to_items(kwargs, i)
        im = ax.imshow(images[i].astype(np.float32), **kwarg)
        if titles is not None:
            ax.title.set_text(titles[i])
        if colorbar:
            _colorbar(mappable=im)

    plt.tight_layout()
    return fig, axes



###############################################################################
# stuff to make phantom objects

def addpart(grid, part, origin):
    """Add a 2-d array ('part') into a larger 2-d array ('grid')

    florian.schaff@monash.edu

    Parameters
    ----------
    grid : 2-d array
        2-d array to which part is added
    part : 2-d array
        2-d array to be added to grid
    origin : tuple
        origin of where to add part (in grid coordinates)

    Returns
    -------
    out : 2-d array
        grid with added part
    """
    psh = part.shape
    gsh = grid.shape

    if origin[0] < 0:
        if origin[0] + psh[0] < 0:
            return
        elif origin[0] + psh[0] <= gsh[0]:
            or0, y0, y1 = 0, -origin[0], psh[0]
        else:
            or0, y0, y1 = 0, -origin[0], gsh[0]-origin[0]
    else:
        if origin[0] >= gsh[0]:
            return
        elif origin[0] + psh[0] <= gsh[0]:
            or0, y0, y1 = origin[0], 0, psh[0]
        else:
            or0, y0, y1 = origin[0], 0, gsh[0]-origin[0]

    if origin[1] < 0:
        if origin[1] + psh[1] < 0:
            return
        elif origin[1] + psh[1] <= gsh[1]:
            or1, x0, x1 = 0, -origin[1], psh[1]
        else:
            or1, x0, x1 = 0, -origin[1], gsh[1]-origin[1]
    else:
        if origin[1] >= gsh[1]:
            return
        elif origin[1] + psh[1] <= gsh[1]:
            or1, x0, x1 = origin[1], 0, psh[1]
        else:
            or1, x0, x1 = origin[1], 0, gsh[1]-origin[1]

    grid_plus_part = grid[or0:or0+y1-y0, or1:or1+x1-x0] + part[y0:y1, x0:x1]
    grid[or0:or0+y1-y0, or1:or1+x1-x0] = grid_plus_part
    return grid


def squircleproj(r, a=1, b=1, c=1, n=4):
    """Create the 2-d thickness map of a projected squircle described by

    (x/a)^n + (y/b)^n + (z/c)^n = r^n

    The special case of n=2 and a=b=c=1 results in a sphere
    The special case of n=2 results in an ellipsoid

    florian.schaff@monash.edu

    Parameters
    ----------
    r : int
        radius parameter of the squircle
    a, b, c : float
        scaling parameters for the squircle axes. default = 1
    n : float
        squircle exponent, i.e. sharpness of the edges. default = 4

    Returns
    -------
    out : 2-d array
        2br x 2ar thickness map in pixels of the projected squircle

    """
    y, x = np.abs(np.mgrid[(-b*r*2+1)/2:(b*r*2+1)/2, (-a*r*2+1)/2:(a*r*2+1)/2])
    grid = c**n*(r**n - y**n/b**n - x**n/a**n)*((r**n - y**n/b**n - x**n/a**n) > 0)
    return 2*(grid.astype('float64'))**(1./n)  # for large n grid turns into type 'O'?


def multisquircles(n=10, size=(500, 500), r=(15, 150), a=(0.5, 1), b=(0.5, 1),
                    c=(0.9, 1.1), deg=(0, 360)):
    """Creates a simple thickness map of n randomly positioned squircles with
    random radii, squircle parameters and rotations within the given ranges.
    Using scipy.stats.skewnorm for the distribution of the squircle exponent,
    with its peak at 2 (i.e. perfect circle).
    florian.schaff@monash.edu

    Parameters
    ----------
    n : int, optional
        number of squircles, default = 10
    size : tuple, optional
        size of the returned 2-d array, default: (500,500)
    r : tuple, optional
        low and high radius for the squircles, default: (15,50)
        NOTE: Uses scipy.stats.skewnorm for the distribution of r, peaked at
        approximately [0.2 * (r_max-r_min) + r_min]
    a, b, c : tuple, optional
        low and high values for the squircle parameters a,b and c
    deg : tuple, optional
        low and high values for squircle rotation in degree, default: (0,360)

    Returns
    -------
    out : 2-d array
        2-d array containing a map of projected squircles
    """
    if r[0] > r[1]:
        raise('r[0] needs to be < r[1]')
    if r[1]*2*np.sqrt(2) > min(size)-20:
        print('Upper radius limit too large, using largest possible!')
        r = (r[0], np.int((min(size)-20)/(2*np.sqrt(2))))
    grid = np.zeros(size)
    xs = np.arange(1.5, 3, 0.01)
    pdf = skewnorm(4, loc=1.6).pdf(xs)**8
    pdf = pdf/pdf.sum()
    for i in range(n):
        # current_r = np.random.randint(*r)
        current_r = np.random.choice(np.linspace(*r, len(pdf)), p=pdf)
        a_ = np.random.uniform(*a)
        b_ = np.random.uniform(*b)
        c_ = np.random.uniform(*c)
        n_ = np.random.choice(xs, p=pdf)
        part = squircleproj(current_r, a_, b_, c_, n_)
        part = rotate(part, np.random.uniform(*deg))
        part[part < 0] = 0  # rotation can introduce negative values on edges
        # y = np.random.randint(1.1*int(current_r),
        #                       size[0]-1.1*int(current_r)-part.shape[0])
        # x = np.random.randint(1.1*int(current_r),
        #                       size[1]-1.1*int(current_r)-part.shape[1])
        y = np.random.randint(10, size[0]-10-part.shape[0])
        x = np.random.randint(10, size[1]-10-part.shape[1])
        grid = addpart(grid, part, (y, x))
    return grid



###############################################################################
# stuff specific for GBI

def fit_sine(y, x=None):
    """
    Perform standard sine fit to stepping data

    florian.schaff@tum.de

    Parameters
    ----------
    y : array
        input data as [image in series, N-d]
    x : int, optional
        Phase steping positions. y.shape[0] steps over [0, 2pi[  if None
        Default: None

    Returns
    -------
    (3, N-d) array
        output array containing phase stepping coefficients [amp, phase, vis]

    """
    # generate stepping positions if not given
    w = np.linspace(0, 2*np.pi, y.shape[0], endpoint=False) if x is None else x
    # prepare solver matrix
    A = np.c_[np.ones(y.shape[0]), np.sin(w), np.cos(w)]
    # solve Ax = y
    new_sh = (y.shape[0], np.prod(y.shape[1:]))
    a, b, c = scipy.linalg.lstsq(A, y.reshape(new_sh))[0].reshape((3,) + y.shape[1:])
    # get coeffs for A + Bcos(x+C)
    return np.array([a, np.arctan2(-b, c), np.sqrt(b**2 + c**2)/a])


def coeff_to_images(coeff_ff, coeff_data):
    """
    Convert flatfield and data coefficients to amp, dpc, df images

    florian.schaff@tum.de

    Parameters
    ----------
    coeff_ff : [3, N-d] array
        array containing ff phase stepping coefficients [amp, phase, vis]
    coeff_data : [3, N-d] array
        array containing data phase stepping coefficients [amp, phase, vis]

    Returns
    -------
    (3, N-d) array
        output array containing processed images [AMP, DPC, DF]

    """
    AMP = coeff_data[0]/coeff_ff[0]
    DPC = np.angle(np.exp(1j*(coeff_data[1]-coeff_ff[1])))
    DF = coeff_data[2]/coeff_ff[2]
    return AMP, DPC, DF


def create_ff_coeff_map(sh, vis=.3):
    """
    Try to mimic a somewhat realistic phase map from a grating interferometer.

    florian.schaff@tum.de

    Parameters
    ----------
    sh :
        tuple, shape in y, x coordinates

    Returns
    -------
    phase_map :
        phase map scaled from [-pi, pi]

    """
    offset_y = np.random.randint(sh[0]/4, 3*sh[0]/4)
    offset_x = np.random.randint(sh[1]/4, 3*sh[1]/4)

    Y, X = np.mgrid[-offset_y:sh[0]-offset_y, -offset_x:sh[1]-offset_x]
    a, b = np.random.uniform(1, 3), np.random.uniform(1, 3)
    R = np.sqrt((X/a)**2 + (Y/b)**2)

    nperiods = np.random.uniform(1, 6)
    c, d = np.random.uniform(0, 3), np.random.uniform(0, 3)
    phase = np.mod(nperiods*R**3/(R.max()**3) +
                    np.sin(c * X/X.max()) +
                    np.sin(d * Y/Y.max()), 1) * np.pi*2
    visibility = vis + .1*R/R.max()
    intensity = 1 - .1*R/R.max()

    return np.array([intensity, phase, visibility])


def create_stepping_series(coeff, nsteps=7, x_pos=None):
    """
    Create phase stepping images from coefficients

    florian.schaff@tum.de

    Parameters
    ----------
    coeff :
        coefficients [I, phase, vis]
    nsteps : int, optional
        Number of phase steps. The default is 7.
    x_pos : list, optional
        Exact phase stepping positions if provided. Default: None

    Returns
    -------
    3-d array
        array containing phase stepping images [image in series, y, x]

    """
    x = np.linspace(0, np.pi*2, nsteps, False) if x_pos is None else x_pos
    series = coeff[0] * (1 + coeff[2]*np.cos(np.add.outer(x, coeff[1])))
    return series

