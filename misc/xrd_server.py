import socketserver
import PyLab
import numpy as np


class XRDTCPHandler(socketserver.StreamRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    s = PyLab.SetupControl()

    def handle(self):
        try:
            while True:
                request = self.rfile.readline()
                if not request:
                    break

                print("calling detector")
                raw_image_buffer = self.s.exposure('xrd', wait=True)
                print("raw image buffer: ", raw_image_buffer.shape)
                raw_image_buffer = np.rot90(raw_image_buffer[0], 1)

                print(raw_image_buffer.dtype)
                print("sending back raw data")
                self.wfile.write(raw_image_buffer.tostring())
        except Exception as e:
            print("ER [%s]: %r" % (self.client_address[0], e))
        print("DC [%s]: disconnected" % (self.client_address[0]))

        return


if __name__ == "__main__":

    HOST = "localhost"
    PORT = 17999

    print('Host: %s' % HOST)
    print('Port: %s' % PORT)

    socketserver.TCPServer.allow_reuse_address = True
    # Create the server, binding to localhost on port 17999
    server = socketserver.TCPServer((HOST, PORT), XRDTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
