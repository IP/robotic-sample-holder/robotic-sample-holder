#!/usr/bin/python3.8
import os
import re
import json
import h5py
import numpy as np

from flat_field_corrections import replace_infnan
from functions_from_xri import (fit_sine, coeff_to_images)
import momose_corrections
from matplotlib import pyplot as plt


class RegionOfInterest:
    def __init__(self, x_range, y_range):
        self.x1, self.x2 = x_range
        self.y1, self.y2 = y_range


class ExperimentData:
    def __init__(self, root_path="", series_id="01513",
                 raw_path=None, output_path="detector-corrected",
                 create_folders=True):
        metadata_f = open(os.path.join(root_path, "metadata.json"))
        documentation_f = open(os.path.join(root_path, "documentation.json"))
        self.meta_data = json.load(metadata_f)
        self.doc_data = json.load(documentation_f)
        self.number_of_images = self._check_coherency_of_pose_numbers()

        amp_path = os.path.join(root_path, output_path, 'attenuation')
        self.amp_image_path = os.path.join(amp_path, '%i.raw')
        dark_field_path = os.path.join(root_path, output_path, 'dark_field')
        self.dark_field_image_path = os.path.join(dark_field_path, '%i.raw')
        phase_path = os.path.join(root_path, output_path, 'phase')
        self.phase_image_path = os.path.join(phase_path, '%i.raw')
        if create_folders:
            # create sub-folders for corrected images
            for target_dir in [amp_path, dark_field_path, phase_path]:
                try:
                    os.mkdir(target_dir)
                except FileExistsError:
                    pass

        if raw_path is None:
            raw_path = "series_" + series_id
        self.raw_path = os.path.join(root_path, raw_path)
        self.raw_image_path = os.path.join(root_path, raw_path, "xrd_" + series_id + "_%05d_%05d.h5")

        self.runs = self._analyze_runs()
        print("Experiment metadata loaded successfully")

    def _analyze_runs(self):
        runs = []
        flat_fields = self.doc_data['flat_fields']
        detector_dark = self.doc_data['detector_dark']
        detector_empty = self.doc_data['detector_empty']

        last_ff = -1
        last_empty = -1
        last_dark = -1
        for (raw_id, num_images, exp_id) in self.doc_data['exp_starts']:
            # find flat_fields
            run_ff = []
            for ff in reversed(flat_fields):
                if ff <= last_ff:
                    break
                if ff < raw_id:
                    run_ff.append(ff)
            run_ff = list(reversed(run_ff))
            # find dark
            run_dark = []
            for dark in reversed(detector_dark):
                if dark <= last_dark:
                    break
                if dark < raw_id:
                    run_dark.append(dark)
            run_dark = list(reversed(run_dark))
            # find empty
            run_empty = []
            for empty in reversed(detector_empty):
                if empty <= last_empty:
                    break
                if empty < raw_id:
                    run_empty.append(empty)
            run_empty = list(reversed(run_empty))
            if len(run_ff) == 0 and len(run_dark) == 0 and len(run_empty) == 0:
                # just append to last run
                if len(runs) == 0:
                    runs.append({"flat_field": run_ff,
                                 "dark": run_dark,
                                 "empty": run_empty,
                                 "measurement": list(range(raw_id, num_images + 1)),
                                 "exp_id": exp_id
                                 })
                else:
                    runs[-1]["measurement"].extend(list(range(raw_id, num_images + 1)))
            else:
                if runs:
                    if not run_ff:
                        run_ff = runs[-1]["flat_fields"]
                    if not run_empty:
                        run_empty = runs[-1]["empty"]
                    if not run_dark:
                        run_dark = runs[-1]["dark"]
                runs.append({"flat_field": run_ff,
                             "dark": run_dark,
                             "empty": run_empty,
                             "measurement": list(range(raw_id, num_images + 1)),
                             "exp_id": exp_id
                             })
            if run_ff:
                last_ff = run_ff[-1]
            if run_empty:
                last_empty = run_empty[-1]
            if run_dark:
                last_dark = run_dark[-1]
        return runs

    def _check_coherency_of_pose_numbers(self):
        """
        Checks if metadata.json and documentation.json are in sync
        """
        num_rotation_mat = len(self.meta_data['rotation_matrices'])
        num_translation_vec = len(self.meta_data['translation_vectors'])
        num_camera_center_vec = len(self.meta_data['camera_center_vectors'])
        num_images = sum([run[1] - run[0] + 1 for run in self.doc_data['exp_starts']])

        if num_rotation_mat != num_translation_vec:
            raise AssertionError(f"Metadata inconsistent: |Rotation Matrices| = {num_rotation_mat} "
                                 f"!= {num_translation_vec} = |Translation Vectors|")
        if num_rotation_mat != num_camera_center_vec:
            raise AssertionError(f"Metadata inconsistent: |Rotation Matrices| = {num_rotation_mat} "
                                 f"!= {num_camera_center_vec} = |Camera Vectors|")
        if num_rotation_mat != num_images:
            raise AssertionError(f"Metadata inconsistent: |Rotation Matrices| = {num_rotation_mat} "
                                 f"!= {num_translation_vec} = |Images|")
        return num_images

    def get_raw_image_from_raw_id(self, raw_id, num_shots=None):
        """
        Function to get a numpy array with all the images from one shot as a 3D Numpy array

        raw_id: id of the experiment in the raw dataset
        num_shots: if given specifies of how many shots the image consists

        Returns: np.array of shape (number of images, image.x, image.y)
        """

        images = []
        if num_shots is None:
            file_name_regex = rf"xrd_01513_{raw_id:05}_*"

            files = [f for f in os.listdir(self.raw_path) if re.match(file_name_regex, f)]
            files.sort()
            for file in files:
                with h5py.File(os.path.join(self.raw_path, file), "r") as f:
                    image = f['raw_data'][0]
                    images.append(image)
        else:
            for i in range(num_shots):
                with h5py.File(self.raw_image_path % (raw_id, i), "r") as f:
                    image = f['raw_data'][0]
                    images.append(image)

        return np.array(images, dtype=np.float32)

    def _load_images_into_memory(self, num_to_load):
        # load first image to get detector size and number of shots per image
        (num_shots, width, height) = self.get_raw_image_from_raw_id(self.runs[0]["measurement"][0]).shape
        data = np.empty([num_to_load, num_shots, width, height], dtype=np.float32)
        ff = np.empty([len(self.runs), num_shots, width, height], dtype=np.float32)
        data_index = 0
        print(f"Loading {num_to_load}")
        for (run_index, run) in enumerate(self.runs):
            ff[run_index] = self.get_raw_image_from_raw_id(run["flat_field"][0], num_shots)
            for image_id in run["measurement"]:
                if data_index == num_to_load:
                    return data, ff[0:run_index + 1]
                print(data_index)
                data[data_index] = self.get_raw_image_from_raw_id(image_id, num_shots)
                data_index += 1
        return data, ff

    @staticmethod
    def remove_inf_and_nan(data):
        """
        data a numpy array of shape [number_of_images, num_shots, width, height]

        Returns the array without any infs or nans
        -------
        """
        # work inplace to reduce memory allocation
        [number_of_images, num_shots, width, height] = data.shape
        for i in range(number_of_images):
            for j in range(num_shots):
                print(f"Image {i}, Shot{j}")
                data[i][j] = replace_infnan(data[i][j])

    def select_roi_from_list(self, data, rois):
        """

        Parameters
        ----------
        data: a numpy 2D array; one detector image
        rois: possible regions of interest

        Returns
        -------

        """
        data_roi_means = np.empty([len(rois)], dtype=np.float32)
        for (i, roi) in enumerate(rois):
            data_roi_means[i] = np.mean(data[roi.x1:roi.x2, roi.y1:roi.y2])

        # take the highest mean => the least obstruction
        index = np.argmax(data_roi_means)
        return index, rois[index]

    @staticmethod
    def precompute_ff_means(ff, rois):
        num_shots = ff.shape[0]
        ff_roi_means = np.empty([len(rois), num_shots], dtype=np.float32)
        for (i, roi) in enumerate(rois):
            for shot in range(num_shots):
                ff_roi_means[i, shot] = np.mean(ff[shot, roi.x1:roi.x2, roi.y1:roi.y2])

        return ff_roi_means

    @staticmethod
    def precompute_corrected_ffs(ff, ff_crop, rois):
        num_ffs, num_shots, width, height = ff_crop.shape
        ff_series = np.empty([len(rois), num_ffs, num_shots, width, height], dtype=np.float32)
        for im in range(num_ffs):
            means = ExperimentData.precompute_ff_means(ff[im], rois)
            for shot in range(num_shots):
                for roi in range(len(rois)):
                    ff_series[roi, im, shot] = (means[roi, shot] / means[roi, 0]) * ff_crop[im, shot]

        return ff_series

    @staticmethod
    def crop_detector(array, roi):
        return array[:, :, roi.x1:roi.x2, roi.y1:roi.y2]

    def retrieve_coeff(self, data_series):
        expected_shape = list(data_series.shape)
        expected_shape[1] = 3
        coeff_ret = np.empty(expected_shape, dtype=np.float32)
        print("Retrieving stepping positions")
        for i in range(data_series.shape[0]):
            print(f"Image {i + 1} of {data_series.shape[0]}")
            x_adj = momose_corrections.stepping_pos(data_series[i])
            coeff_ret[i] = fit_sine(data_series[i], x_adj)
        return coeff_ret

    def process_dataset(self, num_to_load=-1, remove_nans=False):
        if num_to_load == -1:
            num_to_load = self.number_of_images
        data, ff = self._load_images_into_memory(num_to_load)
        total_ff_size = ff.shape[0]

        if remove_nans:
            self.remove_inf_and_nan(data)
            self.remove_inf_and_nan(ff)

        crop_roi = RegionOfInterest((1000, 2100), (900, 1900))
        data_crop = self.crop_detector(data, crop_roi)
        ff_crop = self.crop_detector(ff, crop_roi)

        print("Starting Flat Field Correction")
        rois = [RegionOfInterest((500, 505), (500, 505)),
                RegionOfInterest((500, 505), (2500, 2505)),
                RegionOfInterest((2500, 2505), (500, 505)),
                RegionOfInterest((2500, 2505), (2500, 2505))]
        ff_series = self.precompute_corrected_ffs(ff, ff_crop, rois)

        data_series = np.empty(data_crop.shape, dtype=np.float32)
        data_corresponding_roi = np.empty([num_to_load], int)

        for im in range(num_to_load):
            flat_field_index = 0
            for run_index in range(len(self.runs)):
                accumulated_runs = sum(len(self.runs[counter]["measurement"]) for counter in range(run_index + 1))
                if im < accumulated_runs:
                    flat_field_index = run_index
                    break

            ff_means = ExperimentData.precompute_ff_means(ff[flat_field_index], rois)
            roi_index, roi = self.select_roi_from_list(data[im, 0], rois)
            data_corresponding_roi[im] = roi_index

            for shot in range(7):
                data_roi_mean = np.mean(self.crop_detector(data, roi)[im, shot])
                data_series[im, shot] = (data_roi_mean / ff_means[roi_index, 0]) * data_crop[im, shot]

        print("Extracting Stepping positions")
        coeff_ff_ret = []
        for i in range(len(rois)):
            coeff_ff_ret.append(self.retrieve_coeff(ff_series[i]))
        coeff_data_ret = self.retrieve_coeff(data_series)

        print("Extracting Amplitude, Phase and Darkfield")
        data_expected_shape = list(data_series.shape)
        data_expected_shape.pop(1)  # we only need 2d images per image type
        amp_data = np.empty(data_expected_shape, dtype=np.float32)
        dpc_data = np.empty(data_expected_shape, dtype=np.float32)
        df_data = np.empty(data_expected_shape, dtype=np.float32)
        for i in range(num_to_load):
            flat_field_index = 0
            for run_index in range(len(self.runs)):
                accumulated_runs = sum(len(self.runs[counter]["measurement"]) for counter in range(run_index + 1))
                if i < accumulated_runs:
                    flat_field_index = run_index
                    break

            # print('%i -> %i' % (i, flat_field_index))
            AMP, DPC, DF = coeff_to_images(coeff_ff_ret[data_corresponding_roi[i]][flat_field_index], coeff_data_ret[i])
            amp_data[i] = AMP
            dpc_data[i] = DPC
            df_data[i] = DF

            print(f"Saving image {i + 1} of {num_to_load}")
            with open(self.amp_image_path % (i), "wb") as f:
                AMP.astype(np.float32).tofile(f)
                f.close()
            with open(self.phase_image_path % (i), "wb") as f:
                DPC.astype(np.float32).tofile(f)
                f.close()
            with open(self.dark_field_image_path % (i), "wb") as f:
                DF.astype(np.float32).tofile(f)
                f.close()
            plt.imshow(DF)
            plt.show()
        print("Done")


expData = ExperimentData(root_path="/space/ndorm/c68441bf-596b-4309-a13f-e2755f0fcfda")
expData.process_dataset()
