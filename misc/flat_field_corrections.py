import numpy as np
from scipy.ndimage import median_filter

###############################################################################
# FF correction Florian


def replace_infnan(ndarray, size=3, recursive=True, minval=None, maxval=None,
                   quiet=True):
    """Replace nans, positive infs and negative infs in an array with the
    median value of the surrounding pixels. Function is called recursively with
    increasing filter kernel size to handle patches of infs/nans. Warning: This
    can be slow!

    florian.schaff@monash.edu

    Parameters
    ----------
    array : n-d array of some float type
        input array, mostly 2-d image. ToDO: Function should work on n-d arrays
        , but this is not tested!!
    size : int, optional
        Initial kernel size of the median filter. Default = 2
    recursive : boolean
        flag to toggle recursive behaviour on or off. Default = True
    minval, maxval : float
        Defines a valid range, with values greater/smaller or equal range
        treated as inf. If set to None (default) the min/max possible values
        for the given data type is taken (min/max possible values excluded)
    quiet : boolean
        Print a message to track recursiveness if False. Default=False
    Returns
    -------
    Corrected n-d array

    Examples
    --------
    >>> arr = np.random.random((100,100)).astype(np.float32)
    >>> arr[10, 10], arr[20, 20], arr[30, 30] = np.nan, np.inf, -np.inf
    >>> arr[40, 40] = np.finfo(np.float32).min
    >>> arr[50, 50] = np.finfo(np.float32).max
    >>> arr[10:15, 50:55] = np.nan
    >>> arr_corrected = replace_infnan(arr, quiet=False)

    """
    if quiet is False:
        print('Filtering with size {}'.format(size))

    minv = np.finfo(ndarray.dtype).min if minval is None else minval
    maxv = np.finfo(ndarray.dtype).max if maxval is None else maxval

    with np.errstate(invalid='ignore'):  # suppress output for <= with nan/inf
        fixed = ndarray.copy().astype(np.float64)
        fixed[fixed <= minv] = np.inf
        fixed[fixed >= maxv] = np.inf
        fixed[np.isnan(ndarray)] = np.inf  # filter acts strangley with nan

    if np.isinf(fixed).any():
        idx = np.isinf(fixed)
        y0, x0 = np.min(np.where(idx), 1)
        y1, x1 = np.max(np.where(idx), 1)
        crop = (slice(max(y0 - size * 2, 0), min(y1 + size * 2, fixed.shape[0]), None),
                slice(max(x0 - size * 2, 0), min(x1 + size * 2, fixed.shape[1]), None))
        if quiet is False:
            print('Cropping with {}'.format(crop))
        fixed[crop][idx[crop]] = median_filter(fixed[crop], size)[idx[crop]]
        # check for clusters that didn't get fixed with increasing filter kernel
        if np.isinf(fixed).any() == True and recursive:  # must be == and not 'is'
            fixed = replace_infnan(fixed, size + 1, True, minv, maxv, quiet)
    else:
        pass
    return fixed.astype(ndarray.dtype)
