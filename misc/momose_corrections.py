# -*- coding: utf-8 -*-
"""
Created on Wed May 26 16:40:22 2021

Implementation of phase stepping position retrieval method from:

KOH HASHIMOTO,* HIDEKAZU TAKANO, AND ATSUHI MOMOSE: Improved reconstruction
method for phase stepping data with stepping errors and dose fluctuations
2020 Optics Express

@author: Florian
"""

import numpy as np
from scipy.optimize import minimize

"""
Input is probably the stacked images from all the different grating positions from one take
"""
def stepping_pos(datm, init=None):
    
    def calc_H(nsteps, xi):
        x = np.linspace(0, 2*np.pi, nsteps, False)
        H = np.ones((nsteps, 3))
        H[:, 1] = np.cos(x+xi)
        H[:, 2] = -np.sin(x+xi)
        return H
    
    def gramschmidt(X):
        Q, R = np.linalg.qr(X)
        return Q
    
    C = np.einsum('ixy, jxy -> ij', datm, datm)
    
    def cost_func(xi):
        o = gramschmidt(calc_H(len(xi)+1, np.r_[0, xi]))
        cost = -np.diag(o.T @ C @ o)[1:].sum() # negative because we want to minimize
        return cost
    
    _init = np.zeros((datm.shape[0]-1)) if init is None else init
    xi = np.r_[0, minimize(cost_func, _init)['x']]
    x_adj = np.linspace(0, 2*np.pi, datm.shape[0], False) + xi 
    
    return x_adj
    
  
def flux_correction(datm, x, init=None):
    
    def calc_H(nsteps, lam, x):
        H = np.ones((nsteps, 3))
        H[:, 1] = np.cos(x)
        H[:, 2] = -np.sin(x)
        return np.diag(np.exp(lam)) @ H
    
    def gramschmidt(X):
        Q, R = np.linalg.qr(X)
        return Q
    
    C = np.einsum('ixy, jxy -> ij', datm, datm)
    
    def cost_func(lam):
        o = gramschmidt(calc_H(len(lam)+1, np.r_[0, lam], x))
        cost = -np.diag(o.T @ C @ o).sum() # negative because we want to minimize
        return cost
    
    _init = np.zeros((datm.shape[0]-1)) if init is None else init
    lam = np.r_[0, minimize(cost_func, _init)['x']]
    
    return datm/np.exp(lam)[:, None, None]


def combined_correction(datm, init=None):

    def calc_H(nsteps, xi, lam):
        x = np.linspace(0, 2*np.pi, nsteps, False)
        H = np.ones((nsteps, 3))
        H[:, 1] = np.cos(x+xi)
        H[:, 2] = -np.sin(x+xi)
        return np.diag(np.exp(lam)) @ H
    
    def gramschmidt(X):
        Q, R = np.linalg.qr(X)
        return Q
    
    C = np.einsum('ixy, jxy -> ij', datm, datm)
    
    def cost_func(xi_lam):
        xi, lam = xi_lam[:len(xi_lam)//2], xi_lam[len(xi_lam)//2:]
        o = gramschmidt(calc_H(len(xi_lam)+2, np.r_[0, xi], np.r_[0, lam]))
        cost = -np.diag(o.T @ C @ o).sum() # negative cos we want to minimize
        return cost

    _init = np.zeros((2*datm.shape[0]-2)) if init is None else init
    xi_lam = minimize(cost_func, _init)['x']
    xi, lam = np.r_[0, xi_lam[:datm.shape[0]-1]], np.r_[0, xi_lam[datm.shape[0]-1:]] 
    
    x_adj = np.linspace(0, 2*np.pi, datm.shape[0], False) + xi 
    
    return datm / np.exp(lam)[:, None, None], x_adj

