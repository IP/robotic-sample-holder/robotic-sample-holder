#!/bin/bash

echo -e "\e[34mModifying the system for using libfranka with realtime capability without a realtime kernel\e[39m"
echo -e "\e[34mThis script needs sudo privileges\e[39m"

echo -e "\e[34mAdding user to realtime group\e[39m"
sudo addgroup realtime
sudo usermod -a -G realtime $(whoami)

echo -e "\e[34mModifying the file /etc/security/limits.conf\e[39m"
sudo echo -e "\n# custom entries for libfranka realtime configuration" >> /etc/security/limits.conf
sudo echo -e "@realtime soft rtprio 99" >> /etc/security/limits.conf
sudo echo -e "@realtime soft priority 99" >> /etc/security/limits.conf
sudo echo -e "@realtime soft memlock 102400" >> /etc/security/limits.conf
sudo echo -e "@realtime hard rtprio 99" >> /etc/security/limits.conf
sudo echo -e "@realtime hard priority 99" >> /etc/security/limits.conf
sudo echo -e "@realtime hard memlock 102400" >> /etc/security/limits.conf

echo -e "\e[34mReboot your system in order for these changes to take effect!\e[39m"