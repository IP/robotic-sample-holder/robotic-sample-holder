#!/bin/bash

# install docker
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# build the docker conatiner image
echo -e "\e[34m"
echo -e "Building the roaxdt-nvidia docker image"
echo -e "This might take at least 10 minutes (Install size 3.4 GB)"
echo -e "\e[39m"

sudo docker build --device=/dev/dri -t roaxdt-amd .

# install gitlab-runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner

echo -e "\e[34m"
echo -e "Here are the parameters you should enter during the gitlab-runner register process:"
echo -e "URL: https://gitlab.lrz.de/"
echo -e "TOKEN: look it up on https://gitlab.lrz.de/IP/robotics_assisted_tomography/settings/ci_cd#js-runners-settings"
echo -e "DESCRIPTION: a useful description e.g. ipws02.campar.in.tum.de OR roaxdt-nvidia"
echo -e "TAGS: the tag specifies which which jobs are run on this runner. In our case -nvidia- or -amd- are valid distinctions between runners"
echo -e "EXECUTOR: docker"
echo -e "IMAGE: roaxdt-amd:latest"
echo -e "!IMPORTANT!: Do not forget to edit a newly created runner and enable the option -Run untagged jobs-"
echo -e "\e[39m"
sleep 3

sudo gitlab-runner register

echo -e "\e[34m"
echo -e "The configuration is not finished. We need to modify /etc/gitlab-runner/config.toml"
echo -e "Add the missing configuration lines from the included config.toml (docker/nvidia/config.toml)"
echo -e "Start the gitlab-runner with -sudo gitlab-runner run- afterwards"
echo -e "\e[39m"
