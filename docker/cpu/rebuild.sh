#!/bin/bash

git clone --single-branch --branch develop git@gitlab.lrz.de:IP/RoAXDT/robotics_assisted_tomography.git /tmp/robotics_assisted_tomography/

cd /tmp/robotics_assisted_tomography/docker/cpu/

sudo docker build -t roaxdt-cpu . --no-cache

rm -rf /tmp/robotics_assisted_tomography