#!/bin/bash

git clone --single-branch --branch develop git@gitlab.lrz.de:IP/RoAXDT/robotics_assisted_tomography.git /tmp/robotics_assisted_tomography/

cd /tmp/robotics_assisted_tomography/docker/nvidia/

sudo docker build -t roaxdt-nvidia . --no-cache

rm -rf /tmp/robotics_assisted_tomography